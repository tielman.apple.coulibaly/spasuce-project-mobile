import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/edit_file.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/dao/entity/user_entity.dart';
import 'package:spasuce/dao/repository/database_helper.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';

class FormOffres extends StatefulWidget {
  User model = User();
  @override
  _FormOffresState createState() => _FormOffresState();

  FormOffres({this.model});
}

class _FormOffresState extends State<FormOffres> {
  TextEditingController controllerCommentaire;
  final _formKey = GlobalKey<FormState>();

  Map<String, dynamic> listFileWithContent = Map();

  bool _isDisable = false;
  bool _isSoumettre = false;

  TextStyle textStyleTitle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: fontSizeCustom);

  bool _isLoading;
  bool _autovalidate = false;

  //TextEditingController _controllerDate = TextEditingController();

  DateTime _dateLivraison = DateTime.now();
  DateTime _dateDeMiseADisponibilite = DateTime.now();

  TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());

  String _getFormatDate() {
    return DateFormat(dd_MM_yyyy_slash).format(_dateLivraison);
  }

  List<User> itemsCriteres;
  User itemOF = User();
  User itemAO = User();
  bool _isNewOffre = true;
  //bool _isOffreRetenu = true;

  List<Widget> widgetsCriteres = List();
  List<Widget> widgetsCriteresOffres = List();
  List<Widget> widgetsCriteresAll = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    buildCritere(
        baseUrl: widget.model.baseUrl,
        appelDoffresId: widget.model.appelDoffresId,
        userFournisseurId: widget.model.userFournisseurId);
  }

  @override
  Widget build(BuildContext context) {
    Utilities.begin(Utilities.currentDate());
    Utilities.end(Utilities.parseDate(string: itemAO.dateLimiteDepot));

    // TODO: implement build
    return (_isLoading)
        ? Center(child: CircularProgressIndicator())
        : Form(
            key: _formKey,
            autovalidate: _autovalidate,
            child: ListView(
              children: <Widget>[
                Column(
                  children: [
                    Text(partieAppelDoffres),
                    //widgetsCriteres.add(Text(partieAppelDoffres,style: textStyleTitle,));

                    MyTextFormField(
                      decoration: InputDecoration(labelText: libelle),
                      initialValue:
                          Utilities.initialStringValueWidget(itemAO.libelle),
                      enabled: false,
                      //controller: controllerLibelle,
                    ),

                    MyTextFormField(
                      decoration: InputDecoration(labelText: LPays),
                      initialValue: Utilities.initialStringValueWidget(
                          itemAO.paysLibelle),
                      enabled: false,
                      //controller: controllerLibelle,
                    ),

                    MyTextFormField(
                      decoration: InputDecoration(labelText: LVille),
                      initialValue: Utilities.initialStringValueWidget(
                          itemAO.villeLibelle),
                      enabled: false,
                      //controller: controllerLibelle,
                    ),
                    MyTextFormField(
                      decoration: InputDecoration(labelText: duree),
                      initialValue: Utilities.initialiserDureeAOEnGet(
                          nombreTypeDuree: itemAO.nombreTypeDuree,
                          typeDureeLibelle: itemAO.typeDureeLibelle),

                      /*
          (itemAO.nombreTypeDuree != null
                  ? itemAO.nombreTypeDuree.toString() + espace
                  : "") +
              (Utilities.isNotBlank(itemAO.typeDureeLibelle)
                  ? itemAO.typeDureeLibelle
                  : ""),
          (Utilities.isNotBlank(itemAO.typeDureeLibelle))
              ? itemAO.nombreTypeDuree.toString() +
                  espace +
                  itemAO.typeDureeLibelle
              : "",
              */
                      enabled: false,
                      //controller: controllerLibelle,
                    ),
                    MyTextFormField(
                      decoration: InputDecoration(labelText: dateLimite),
                      initialValue: Utilities.initialStringValueWidget(
                          itemAO.dateLimiteDepot),
                      enabled: false,
                      //controller: controllerLibelle,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(top: 16.0, bottom: 16),
                        child: MyTextFormField(
                          decoration: InputDecoration(
                            labelText: description,
                            border:
                                OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                          ),
                          initialValue: Utilities.initialStringValueWidget(
                              itemAO.description),
                          //enabled: false,
                          readOnly: true, //TODO: a revoir
                          maxLines: maxLines,
                          //controller: controllerLibelle,
                        )),

                    // ajout des  domaines de l'AO
                    (itemAO != null &&
                            Utilities.isNotEmpty(
                                itemAO.datasSecteurDactiviteAppelDoffres))
                        ? Utilities.afficherMultiSelectFormFieldInRead(
                            list: itemAO.datasSecteurDactiviteAppelDoffres,
                            textField: secteurDactiviteLibelle,
                            titleText: LDomaine)
                        : Container(),

                    // ajout des fichiers
                    (itemAO != null &&
                            Utilities.isNotEmpty(itemAO.datasFichier))
                        ? Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(listeFichiers),
                              ],
                            ),
                          )
                        : Container(),
                    /*(itemAO != null &&
                            Utilities.isNotEmpty(itemAO.datasFichier))
                        ? Utilities.afficherListeFichierTechargeable(
                            context: context, datasFichier: itemAO.datasFichier)
                        : Container(),*/

                    Utilities.afficherListeFichierTechargeable(
                        context: context, datasFichier: itemAO.datasFichier),

                    //******************* Debut formulaire de l'offre ***************************
                    Divider(),

                    (_isNewOffre)
                        ? (itemAO.dateLimiteDepot != null &&
                                (Utilities.parseDate(
                                        string: itemAO.dateLimiteDepot)
                                    .isBefore(Utilities.currentDate())))
                            ? Center(
                                child: Text(
                                  messageDateLimitePasse,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 30,
                                      color: Colors.red),
                                ),
                              )
                            :
                            // ajout de boutton de creation de Offres
                            Center(
                                child: Utilities.getButtonSubmit(
                                    shape: Utilities.getDefaultShape(),
                                    child: Text(
                                        _isSoumettre ? annuler : soumettre),
                                    onPressed: soumettreOuAnnulerOffre),
                              )
                        :
                        // retirer si possible
                        Center(
                            child: Utilities.getButtonSubmit(
                                shape: Utilities.getDefaultShape(),
                                child: Text(retirer),
                                onPressed: retirerOffre,
                                isDisable: _isDisable),
                          ),

                    // verification si l'offre est retenu ou pas
                    (_isOffreRetenu(item: itemOF))
                        ? getContenuSiOffreRetenu()
                        : (!_isNewOffre
                            ? getContenuSiOffreEnEdition()
                            : (_isSoumettre
                                ? getContenuSiOffreEnEdition()
                                : Container())),

                    // ancien ************

                    /*Container(
                      child: Column(
                        children: <Widget>[
                          (_isNewOffre)
                              ?
                              // ajout de boutton de creation de Offres
                              Center(
                                  child: Utilities.getButtonSubmit(
                                      shape: Utilities.getDefaultShape(),
                                      child: Text(
                                          _isSoumettre ? annuler : soumettre),
                                      onPressed: soumettreOuAnnulerOffre),
                                )
                              :
                              // retirer si possible
                              Center(
                                  child: Utilities.getButtonSubmit(
                                      shape: Utilities.getDefaultShape(),
                                      child: Text(retirer),
                                      onPressed: retirerOffre,
                                      isDisable: _isDisable),
                                ),

                          // condition pour afficher ou pas

                          // ajouter le libelle de l'offre en question
                          Text((_isNewOffre) ? partieNewOffre : partieOffre),

                          // faire apparaitre les criteres

                          Container(
                            child: Column(
                                children: constructionCriteresOffresAO(
                                    itemsCriteres)),
                          ),
                          //constructionCriteresOffresAO(itemsCriteres),

                          // ajout des autres widgets
                          Padding(
                            padding: const EdgeInsets.only(top: 16.0),
                            child: MyTextFormField(
                              decoration: InputDecoration(
                                labelText: commentaire,
                                border:
                                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                              ),
                              validator: (value) {
                                Utilities.validator(value,
                                    typeContentToValidate: v_text);
                              },
                              onSaved: (input) {
                                //widget.model.libelle = input;
                                itemOF.commentaire = input;
                              },
                              initialValue:
                                  (Utilities.isNotBlank(itemOF.commentaire)
                                      ? itemOF.commentaire
                                      : ""),
                              maxLines: maxLines,
                              keyboardType: TextInputType.multiline,
                              //controller: controllerLibelle,
                            ),
                          ),

                          // ajouter des fichiers
                          EditFile(
                            listFile: listFileWithContent,
                          ),

                          // ajouter un divider pour separer entre la liste des fichiers et la suite du formulaire
                          Divider(),

                          // les boutons de validation du formulaire de l'OF
                          Row(children: <Widget>[
                            Utilities.getButtonSubmit(
                                shape: Utilities.getDefaultShape(),
                                child: Text(enregistrer),
                                onPressed: () {
                                  submit(etatCode: BROUILLON, action: CREATE);
                                },
                                isDisable: _isDisable),
                            Spacer(),
                            Utilities.getButtonSubmit(
                                shape: Utilities.getDefaultShape(),
                                child: Text((itemOF != null && itemOF.id != null
                                    ? modifier
                                    : creer)),
                                onPressed: () => (itemOF != null &&
                                        itemOF.id != null
                                    ? submit(etatCode: ENTRANT, action: UPDATE)
                                    : submit(
                                        etatCode: ENTRANT, action: CREATE)),
                                isDisable: _isDisable),
                          ])
                        ],
                      ),
                    ),
                    


                    (_isNewOffre)
                        ?
                        // ajout de boutton de creation de Offres
                        Center(
                            child: Utilities.getButtonSubmit(
                                shape: Utilities.getDefaultShape(),
                                child: Text(_isSoumettre ? annuler : soumettre),
                                onPressed: soumettreOuAnnulerOffre),
                          )
                        :
                        // retirer si possible
                        Center(
                            child: Utilities.getButtonSubmit(
                                shape: Utilities.getDefaultShape(),
                                child: Text(retirer),
                                onPressed: retirerOffre,
                                isDisable: _isDisable),
                          ),

                    // ajouter le libelle de l'offre en question
                    Text((_isNewOffre) ? partieNewOffre : partieOffre),

                    // faire apparaitre les criteres

                    Container(
                      child: Column(
                          children:
                              constructionCriteresOffresAO(itemsCriteres)),
                    ),
                    //constructionCriteresOffresAO(itemsCriteres),

                    // ajout des autres widgets
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: MyTextFormField(
                        decoration: InputDecoration(
                          labelText: commentaire,
                          border:
                              OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                        ),
                        validator: (value) {
                          Utilities.validator(value,
                              typeContentToValidate: v_text);
                        },
                        onSaved: (input) {
                          //widget.model.libelle = input;
                          itemOF.commentaire = input;
                        },
                        initialValue: (Utilities.isNotBlank(itemOF.commentaire)
                            ? itemOF.commentaire
                            : ""),
                        maxLines: maxLines,
                        keyboardType: TextInputType.multiline,
                        //controller: controllerLibelle,
                      ),
                    ),

                    // ajouter des fichiers
                    EditFile(
                      listFile: listFileWithContent,
                    ),

                    // ajouter un divider pour separer entre la liste des fichiers et la suite du formulaire
                    Divider(),

                    // les boutons de validation du formulaire de l'OF
                    Row(
                      children: <Widget>[
                        Utilities.getButtonSubmit(
                            shape: Utilities.getDefaultShape(),
                            child: Text(enregistrer),
                            onPressed: () {
                              submit(etatCode: BROUILLON, action: CREATE);
                            },
                            isDisable: _isDisable),
                        Spacer(),
                        Utilities.getButtonSubmit(
                            shape: Utilities.getDefaultShape(),
                            child: Text((itemOF != null && itemOF.id != null
                                ? modifier
                                : creer)),
                            onPressed: () => (itemOF != null &&
                                    itemOF.id != null
                                ? submit(etatCode: ENTRANT, action: UPDATE)
                                : submit(etatCode: ENTRANT, action: CREATE)),
                            isDisable: _isDisable),

                        /*
            RaisedButton(
              shape: Utilities.getDefaultShape(),
              child: Text(enregistrer),
              onPressed: () {
                submit(etatCode: BROUILLON, action: CREATE);
              },
            ),
            Spacer(),
            RaisedButton(
              shape: Utilities.getDefaultShape(),
              child: Text(
                  (itemOF != null && itemOF.id != null ? modifier : creer)),
              onPressed: () {
                (itemOF != null && itemOF.id != null
                    ? submit(etatCode: ENTRANT, action: UPDATE)
                    : submit(etatCode: ENTRANT, action: CREATE));
              },
            )
            */
                      ],
                    ),
                    */
                  ],
                )
              ],
            )

            //: Center(child: Text("Aucun critère pour cet appel d'offres")),
            );
  }

  // construction des criteres à renseigner
  buildCritere(
      {String baseUrl, int appelDoffresId, int userFournisseurId}) async {
    RequestCustom request = RequestCustom();

    // AO
    User dataAO = User();
    dataAO.id = appelDoffresId;
    dataAO.isSelect = false;

    // offres
    User dataOF = User();
    dataOF.appelDoffresId = appelDoffresId;
    //dataOF.userFournisseurId =  userFournisseurId == null ? userConnecter : userFournisseurId;

    UserEntity userConnected = await DatabaseHelper.instance.getUserConnected;
    dataOF.userFournisseurId =
        (userFournisseurId == null && userConnected != null)
            ? userConnected.id
            : userFournisseurId;
    dataOF.isRetirer = false;

    request.data = dataAO;

    setState(() => _isLoading = true);

    ResponseCustom responseCustom = await baseApi.testApi(
        apiBaseUrl: baseUrl + appelDoffres + getByCriteria, request: request);

    widgetsCriteresAll.clear();
    widgetsCriteres
        .clear(); // widgetsCriteres = List() ; possible ici car il n'est pas passé en parametre donc pas de souci de changement d'adresse de la variable
    if (responseCustom != null &&
        !responseCustom.hasError &&
        Utilities.isNotEmpty(responseCustom.items)) {
      Utilities.log(responseCustom.items);

      setState(() {
        itemAO = User.fromJson(responseCustom.items[0]);
      });

      //setState(() {
      if (Utilities.isNotEmpty(itemAO.datasValeurCritereAppelDoffres)) {
        itemsCriteres = User.fromJsons(itemAO.datasValeurCritereAppelDoffres);
      }
      //});

      //if (Utilities.isNotEmpty(datasValeurCritereAppelDoffres)) {
      // si l'OA a des criteres
      bool isNewOffre = true;
      request.data = dataOF;

      responseCustom = await baseApi.testApi(
          apiBaseUrl: baseUrl + offre + getByCriteria, request: request);

      if (responseCustom != null &&
          !responseCustom.hasError &&
          Utilities.isNotEmpty(responseCustom.items)) {
        // construction de la liste des criteres avec libelle/valeur

        //User itemOF = User.fromJson(responseCustom.items[0]);

        _isNewOffre = false; // car il avait deja une offre pour cet AO
        //setState(() {
        itemOF = User.fromJson(responseCustom.items[0]);
        //});

        //List<User> datasValeurCritereOffre = itemOF.datasValeurCritereOffre;
        List<User> datasValeurCritereOffre = List();
        if (Utilities.isNotEmpty(itemsCriteres) &&
            Utilities.isNotEmpty(itemOF.datasValeurCritereOffre)) {
          datasValeurCritereOffre =
              User.fromJsons(itemOF.datasValeurCritereOffre);
          // si l'OF contient des criteres  deja renseigner
          for (User valeurCritereAppelDoffres in itemsCriteres) {
            for (User valeurCritereOffre in datasValeurCritereOffre) {
              if (valeurCritereAppelDoffres.critereId ==
                  valeurCritereOffre.critereId) {
                valeurCritereAppelDoffres.valeur = valeurCritereOffre.valeur;
                valeurCritereAppelDoffres.offreId = valeurCritereOffre.offreId;
                break;
              }
            }
          }
        } else {
          // l'AO est sans critère
        }

        //MAJ du widget des fichiers
        if (Utilities.isNotEmpty(itemOF.datasFichier)) {
          Utilities.log("datasFichier datasFichier datasFichier");
          Map<String, dynamic> tampon = Map();
          //Map<String, String> tampon = Map();
          for (var item in itemOF.datasFichier) {
            //tampon[item[name]] = item[fichierBase64];
            //tampon[item[name]] = item[urlFichier];
            tampon["${item[name]}.${item[libelleExtension]}"] =
                item[urlFichier];
            Utilities.log("datasFichier name fichierBase64 ${item[name]}");
            Utilities.log(
                "datasFichier datasFichier fichierBase64 ${item[urlFichier]}");
          }
          //setState(() {
          listFileWithContent.addAll(tampon);
          //});

          // appel asynchrone pour appeler les base64
          // recuperation des base64 des files
          RequestCustom requestFile = RequestCustom();
          requestFile.datas = itemOF.datasFichier;
          baseApi
              .testApi(
                  apiBaseUrl: baseUrl + fichier + getBase64Files,
                  request: requestFile)
              .then((res) {
            if (res != null &&
                !res.hasError &&
                Utilities.isNotEmpty(res.items)) {
              Map<String, dynamic> tamponLocal = Map();
              for (var item in res.items) {
                //tamponLocal[item[name]] = item[fichierBase64];
                tamponLocal["${item[name]}.${item[libelleExtension]}"] =
                    item[fichierBase64];
              }
              //setState(() {
              listFileWithContent.clear();
              listFileWithContent.addAll(tamponLocal);
              //});
            }
          }).catchError((err) {
            print(err);
          });
        }
      }

      // ajout des informations dans le widget
      //setState(() {

      setState(() {
        //widgetsCriteresAll = widgetsCriteres;
        _isLoading = false;
      });
      //});

    } else {
      setState(() => _isLoading = false);
    }
  }

  // construction de la listes des criteres de l'AO/Offres
  //Widget constructionCriteresOffresAO(List<User> criteres) {
  List<Widget> constructionCriteresOffresAO(List<User> criteres,
      {bool enable = true}) {
    List<Widget> widgets = List();
    if (Utilities.isNotEmpty(criteres)) {
      //widgetsCriteres.add(Text(partieOffre, style: textStyleTitle,));
      for (User element in criteres) {
        //for (User element in datasValeurCritereAppelDoffres) {
        //User data = User.fromJson(element);

        Utilities.log(element.typeCritereAppelDoffresCode);

        switch (element.typeCritereAppelDoffresCode) {
          case typeDate:
            TextEditingController _controllerDate = TextEditingController();

            _controllerDate.text =
                (Utilities.isNotBlank(element.valeur) ? element.valeur : "");
            widgets.add(MyTextFormField(
              decoration: InputDecoration(
                  labelText: element.critereAppelDoffresLibelle +
                      (enable ? marqueObligatoire : "")),
              //initialValue: (Utilities.isNotBlank(element.valeur)? element.valeur: ""),
              validator: (value) {
                Utilities.log("value typeDate typeDate $value");
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              autovalidate: true,
              enabled: enable,

              //enabled: false,
              controller: _controllerDate,
              readOnly: true,
              onSaved: (input) {
                element.valeur =
                    input; // pour recuperer la valeur saisie lors du submit
              },
              onTap: () {
                Utilities.showDatePickerCustom(
                  context: context,
                  controller: _controllerDate,
                );
                //_showDatePicker(_controllerDate);
                //if (_controllerDate != null) {
                // Utilities.log(_controllerDate.text = _getFormatDate()) ;
                //}
              },
              keyboardType: TextInputType.datetime,
              //controller: controllerLibelle,
            ));
            break;

          case typeString:
            widgets.add(MyTextFormField(
              decoration: InputDecoration(
                  labelText: element.critereAppelDoffresLibelle +
                      (enable ? marqueObligatoire : "")),
              initialValue:
                  (Utilities.isNotBlank(element.valeur) ? element.valeur : ""),
              validator: (value) {
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              onSaved: (input) {
                element.valeur =
                    input; // pour recuperer la valeur saisie lors du submit
              },
              enabled: enable,

              //keyboardType: TextInputType.datetime,
              //controller: controllerLibelle,
            ));
            break;

          case typeNumber:
            TextEditingController _controllerNumber = TextEditingController();

            _controllerNumber.text =
                (element.valeur != null ? element.valeur : "");
            widgets.add(MyTextFormField(
              decoration: InputDecoration(
                  labelText: element.critereAppelDoffresLibelle +
                      (enable ? marqueObligatoire : "")),
              //initialValue: (Utilities.isNotBlank(element.valeur)? element.valeur : ""),
              validator: (value) {
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              controller: _controllerNumber,
              onChanged: (value) {
                /* 
                  if (Utilities.isNotBlank(value) &&
                      value.toString().length >= 4) {
                    _controllerNumber.text = ;
                  }
                  */
              },
              onSaved: (input) {
                element.valeur =
                    input; // pour recuperer la valeur saisie lors du submit
              },
              keyboardType: TextInputType.number,
              enabled: enable,

              //controller: controllerLibelle,
            ));
            break;

          case typeBoolean:
            widgets.add(Radio(
                //value: element.valeur,
                value: false,
                groupValue: null,
                onChanged: (input) {
                  element.valeur =
                      input; // pour recuperer la valeur saisie lors du submit
                }));
            break;

          default:
            break;
        }
      }
    }
    return widgets;
    //return test;
  }

  //
  soumettreOuAnnulerOffre() {
    Utilities.begin("soumettreOuAnnulerOffre");
    // rendre visible le formulaire s'il ne l'est pas ou le contraire

    //Utilities.progressingAfterSubmit(context: context);
    setState(() {
      if (_isSoumettre) {
        //widgetsCriteresAll += widgetsCriteresOffres;
        _isSoumettre = !_isSoumettre;
      } else {
        //int lengthWidgetsCriteresAll = widgetsCriteresAll.length;
        //int lengthWidgetsCriteresOffres = widgetsCriteresOffres.length;
        //widgetsCriteresAll = widgetsCriteresAll.sublist(0, lengthWidgetsCriteresAll - lengthWidgetsCriteresOffres);
        _isSoumettre = !_isSoumettre;
        clearFormOffre(itemsCriteres); // nettoyer formulaire
      }
    });

    Utilities.end("soumettreOuAnnulerOffre");
  }

  // retirer une offre
  retirerOffre() async {
    bool succes = false;
    // disableButton
    disableButton();

    // boite de dialogue

    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(attention),
          content: new Text(questionRetirerOffre),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(reponseNon),
              onPressed: () {
                Navigator.of(context).pop();
                //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
              },
            ),
            new FlatButton(
              child: new Text(reponseOui),
              onPressed: () async {
                Navigator.of(context).pop();
                RequestCustom request = RequestCustom();

                request.user = itemOF.userFournisseurId;
                request.datas = [itemOF];

                ResponseCustom responseCustom = await baseApi.testApi(
                    apiBaseUrl:
                        AppConfig.of(context).apiBaseUrl + offre + forceDelete,
                    request: request);
                Utilities.messageApi(response: responseCustom);
                if (responseCustom != null && !responseCustom.hasError) {
                  setState(() {
                    _isNewOffre = true; // rendre l'offre nouvelle
                    // vider les valeurs de itemsCriteres
                    clearFormOffre(itemsCriteres);
                  });
                } else {}

                // restart le widget si supprimer
              },
            ),
          ],
        );
      },
    );

    // enableButton
    enableButton();
  }

  // soumission du formulaire
  submit({String etatCode, String action}) {
    //NumberFormat()
    if (_formKey.currentState.validate()) {
      // disableButton
      Utilities.progressingAfterSubmit(context: context);
      disableButton();
      // executer les controles sur les inputs
      _formKey.currentState.save(); // recuperer les donnees saisies
      if (itemOF != null) {
        // formatage de la date
        for (var item in itemsCriteres) {
          switch (item.typeCritereAppelDoffresCode) {
            case typeDate:
              break;
            case typeNumber:
              break;
            case typeBoolean:
              break;
            case typeString:
              break;
            default:
          }
        }
        // recuperation des datasFichier

        // recuperation données saisies
        itemOF.datasValeurCritereOffre = itemsCriteres;
        itemOF.etatCode = etatCode;

        if (itemOF.userFournisseurId == null) {
          itemOF.userFournisseurId = widget.model.userFournisseurId;
        }
        if (itemOF.appelDoffresId == null) {
          itemOF.appelDoffresId = widget.model.appelDoffresId;
        }

        Utilities.log("itemOF.userFournisseurId ${itemOF.userFournisseurId}");
        Utilities.log("itemOF.appelDoffresId ${itemOF.appelDoffresId}");
        for (var item in itemOF.datasValeurCritereOffre) {
          Utilities.log(item.valeur);
          //Utilities.log(item.toJson());
        }

        // ajout des fichiers
        itemOF.datasFichier = Utilities.formationDatasFiles(
            listFileWithContent: listFileWithContent, isFichierOffre: true);

        // construction de l'URI
        String endpoint = "";

        //sho
        if (action == CREATE) {
          endpoint = create;
        } else {
          endpoint = update;
        }
        RequestCustom request = RequestCustom();

        List<User> datas = List();
        datas.add(itemOF);
        request.user = itemOF.userFournisseurId;
        request.datas = datas;
        baseApi
            .testApi(
                apiBaseUrl: widget.model.baseUrl + offre + endpoint,
                request: request)
            .then((res) {
          Navigator.pop(context); // pour retirer le progressing
          if (res != null && !res.hasError) {
            Utilities.getToast(succes);
            Navigator.pop(context);
          } else {
            Utilities.messageApi(response: res);
          }

          // enableButton
          enableButton();
        }).catchError((err) {
          // erreur avec le service
          // erreur de connexion
          // enableButton
          enableButton();
        });
      } else {}
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }

  void disableButton() {
    setState(() {
      _isDisable = true;
    });
  }

  bool _isOffreRetenu({User item}) {
    return item != null &&
        item.isSelectedByClient != null &&
        item.isSelectedByClient;
  }

  bool _isVisibleByClient() {
    return itemOF != null &&
        itemOF.isVisibleByClient != null &&
        itemOF.isVisibleByClient;
  }

  void enableButton() {
    setState(() {
      _isDisable = false;
    });
  }

  // contenu si offre est retenu
  Widget getContenuSiOffreRetenu() {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            messageOffreRetenue,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 30, color: Colors.green),
          ),
          Text(partieOffre),
          // liste des criteres en disable
          Container(
            child: Column(
                children:
                    constructionCriteresOffresAO(itemsCriteres, enable: false)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: MyTextFormField(
              decoration: InputDecoration(
                labelText: commentaire,
                border:
                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
              ),

              initialValue:
                  Utilities.initialStringValueWidget(itemOF.commentaire),
              maxLines: maxLines,
              keyboardType: TextInputType.multiline,
              //enabled: false,
              readOnly: true, //TODO: a revoir

              //controller: controllerLibelle,
            ),
          ),

          /*(itemOF != null && Utilities.isNotEmpty(itemOF.datasFichier))
              ? Utilities.afficherListeFichierTechargeable(
                  context: context, datasFichier: itemOF.datasFichier)
              : Container(),*/

          Utilities.afficherListeFichierTechargeable(
              context: context, datasFichier: itemOF.datasFichier),

          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: MyTextFormField(
              decoration: InputDecoration(
                labelText: labelMessageClient,
                border:
                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
              ),
              initialValue: Utilities.initialStringValueWidget(
                  itemOF.messageClient,
                  defaultValue: defaultMessageClient),
              maxLines: maxLines,
              //enabled: false,
              readOnly: true, //TODO: a revoir

              //controller: controllerLibelle,
            ),
          ),
          MyTextFormField(
            decoration: InputDecoration(labelText: labelNoteClient),
            initialValue: Utilities.initialNumberValueWidget(itemAO.noteOffre,
                defaultValue: defaultMessageNoteClient),
            enabled: false,
            //controller: controllerLibelle,
          ),
        ],
      ),
    );
  }

  // suppression des valeurs dans le suppression
  clearFormOffre(List<User> itemsCriteres) {
    for (User item in itemsCriteres) {
      item.valeur = null;
      item.offreId = null;
    }
    listFileWithContent.clear(); // vider la liste des fichiers
    itemOF = User(); // vider les champs du formulaire
  }

  // contenu si offre est en edition
  Widget getContenuSiOffreEnEdition() {
    return Container(
      child: Column(
        children: <Widget>[
          // ajouter le libelle de l'offre en question
          Text((_isNewOffre) ? partieNewOffre : partieOffre),

          // faire apparaitre les criteres
          Container(
            child: Column(
                children: constructionCriteresOffresAO(itemsCriteres,
                    enable: !_isVisibleByClient())),
          ),
          //constructionCriteresOffresAO(itemsCriteres),

          // ajout des autres widgets
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: MyTextFormField(
              decoration: InputDecoration(
                labelText: commentaire,
                border:
                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
              ),
              validator: (value) {
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              onSaved: (input) {
                //widget.model.libelle = input;
                itemOF.commentaire = input;
              },
              initialValue: (Utilities.isNotBlank(itemOF.commentaire)
                  ? itemOF.commentaire
                  : ""),
              maxLines: maxLines,
              keyboardType: TextInputType.multiline,
              readOnly: _isVisibleByClient(),
              //controller: controllerLibelle,
            ),
          ),

          // ajouter des fichiers
          (_isVisibleByClient())
              ? Utilities.afficherListeFichierTechargeable(
                  context: context, datasFichier: itemOF.datasFichier)
              : EditFile(
                  listFile: listFileWithContent,
                ),

          // ajouter un divider pour separer entre la liste des fichiers et la suite du formulaire
          Divider(),

          // les boutons de validation du formulaire de l'OF
          (_isVisibleByClient())
              ? Text(
                  offreNonModifiable,
                  style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                )
              : Row(children: <Widget>[
                  Utilities.getButtonSubmit(
                      shape: Utilities.getDefaultShape(),
                      child: Text(enregistrer),
                      onPressed: () {
                        submit(etatCode: BROUILLON, action: CREATE);
                      },
                      isDisable: _isDisable),
                  Spacer(),
                  Utilities.getButtonSubmit(
                      shape: Utilities.getDefaultShape(),
                      child: Text((itemOF != null && itemOF.id != null
                          ? modifier
                          : creer)),
                      onPressed: () => (itemOF != null && itemOF.id != null
                          ? submit(etatCode: ENTRANT, action: UPDATE)
                          : submit(etatCode: ENTRANT, action: CREATE)),
                      isDisable: _isDisable),
                ])
        ],
      ),
    );
  }
}
