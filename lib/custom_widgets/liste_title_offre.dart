import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/user.dart';

class ListTileOffre extends StatelessWidget {
  Function onTap;
  User data;
  String logoEntreprise;

  bool withAllDetails;

  ListTileOffre(
      {this.data,
      this.logoEntreprise,
      this.onTap,
      this.withAllDetails = false});

  @override
  Widget build(BuildContext context) {
    Utilities.begin("ListTileOffre/build");
    //Utilities.begin("ListTileOffre/build data : ${this.data}");
    print(data.toJson());
    return Container(
      //color: (data.id % 2 == 0 ) ? ConstApp.color_app :  ConstApp.color_app_rouge,

      child: ListTile(
        // dense: false, // le contenu n'est pas trop dense
        // isThreeLine: true, // mettre un espace entre les ListTile
        // enabled: false, // desactive le clic
        // selected: true, // si selectionner alors les elements prennent la couleur de base

        onTap: onTap != null
            ? onTap
            : () {
                Utilities.getToast(
                    "Vous devez recevoir les détails de l'offre. $messageDev!!!");
              },
        leading: CircleAvatar(
          child: Image.asset(
              logoEntreprise != null ? logoEntreprise : logoSpasuce),
        ),
        title: Text(data.entrepriseNom +
            (data.code != null ? pipeAvecEspace + data.code : "")),
        subtitle: (withAllDetails)
            ? allDetails(data: data)
            : Text(data.userNom +
                espace +
                Utilities.initialStringValueWidget(data.userPrenoms)),
        trailing: Icon(Icons.work),
      ),
    );
  }

  allDetails({User data}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Date soumission : " +
            Utilities.initialStringValueWidget(data.dateSoumission,
                defaultValue: "indisponible")),
        Text(
          "Commentaire : " +
              espace +
              Utilities.initialStringValueWidget(data.commentaire,
                  defaultValue: "indisponible"),
          maxLines: minLines,
          overflow: TextOverflow.ellipsis,
        ),
        /*Text(data.userNom +
            espace +
            Utilities.initialStringValueWidget(data.userPrenoms)),
        Text(data.userNom +
            espace +
            Utilities.initialStringValueWidget(data.paysLibelle)),*/
      ],
    );
  }
}
