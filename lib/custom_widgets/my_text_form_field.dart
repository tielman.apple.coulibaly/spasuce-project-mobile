import 'package:flutter/material.dart';

class MyTextFormField extends StatelessWidget {
  /*
  final Function validator;
  final Function onSaved;
  final Function onChanged;
  final Function onTap;

  final bool isPassword;
  final bool isEmail;
  final bool enabled;
  final bool readOnly;
  final bool autofocus;
  final bool obscureText;

  final String hintText;
  final String initialValue;

  final InputDecoration decoration;
  final TextEditingController controller;
  final FocusNode focusNode;
  final TextInputType keyboardType;
  final TextStyle style;
  final TextAlign textAlign;

  final int maxLines;
  final int maxLength;
  */




  Function validator;
  Function onSaved;
  Function onChanged;
  Function onTap;

  bool isPassword = false;
  bool isEmail;
  bool enabled;
  bool readOnly;
  bool autofocus;
  bool obscureText;
  bool autovalidate;
  //bool expands;
  

  String hintText;
  String initialValue;

  InputDecoration decoration = InputDecoration(
    //hintText: hintText,
    contentPadding: EdgeInsets.all(15.0),
    border: InputBorder.none,
    filled: true,
    fillColor: Colors.grey[200],
  );
  TextEditingController controller;
  FocusNode focusNode;
  TextInputType keyboardType;
  TextStyle style;
  TextAlign textAlign;

  int maxLines ;
  int maxLength;


  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autovalidate: autovalidate,
      
      //expands: expands,
      // functions
      onTap: onTap,
      onChanged: onChanged,
      onSaved: onSaved,
      validator: validator,

      // boolean values
      autofocus: autofocus,
      readOnly: readOnly,
      style: style,
      initialValue: initialValue,
      enabled: enabled,
      obscureText: obscureText,

      // int values
      maxLines: maxLines,
      maxLength: maxLength,

      //objets
      focusNode: focusNode,
      textAlign: textAlign,
      controller: controller,
      decoration: decoration,
      keyboardType: keyboardType,
    );
  }

  MyTextFormField(
      {this.validator,
      this.onSaved,
      this.onChanged,
      this.onTap,
      this.isPassword,
      this.isEmail,
      this.enabled  = true,
      this.readOnly  = false,
      this.autofocus  = false,
      this.obscureText  = false,
      this.hintText,
      this.initialValue,
      this.decoration,
      this.controller,
      this.focusNode,
      this.keyboardType,
      this.autovalidate = false,
      this.style,
      this.textAlign = TextAlign.start,
      this.maxLines = 1,
      //this.maxLines,
      this.maxLength,
      //this.expands = false,
      });

/*  
   */
}
