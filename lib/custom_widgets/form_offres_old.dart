/*
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/edit_file.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';

class FormOffres extends StatefulWidget {
  User model = User();
  @override
  _FormOffresState createState() => _FormOffresState();

  FormOffres({this.model});
}

class _FormOffresState extends State<FormOffres> {
  TextEditingController controllerCommentaire;
  final _formKey = GlobalKey<FormState>();

  Map<String, dynamic> listFileWithContent = Map();

  bool _isDisable = false;
  bool _isSoumettre = false;

  TextStyle textStyleTitle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: fontSizeCustom);

  bool _isLoading;
  bool _autovalidate = false;

  //TextEditingController _controllerDate = TextEditingController();

  DateTime _dateLivraison = DateTime.now();
  DateTime _dateDeMiseADisponibilite = DateTime.now();

  TimeOfDay _fromTime = TimeOfDay.fromDateTime(DateTime.now());

  String _getFormatDate() {
    return DateFormat(dd_MM_yyyy_slash).format(_dateLivraison);
  }

  // methode de date
  _showDatePicker(TextEditingController controller) {
    //Locale locale = Locale('fr', '') ;
    DateTime currentDate = (Utilities.isNotBlank(controller.text)
        ? Utilities.parseDate(string: controller.text)
        : DateTime.now());
    showDatePicker(
            context: context,
            initialDate: currentDate,
            firstDate: Utilities.firstDateCalendar(),
            lastDate: Utilities.lastDateCalendar())
        .then((picked) {
      //if (picked != null && picked != currentDate) {
      if (picked != null) {
        setState(() {
          controller.text = Utilities.formatDate(date: picked);
        });
      }
    });
  }

  /*Future<void> _showDatePicker() async {
    final picked = await showDatePicker(context: context, initialDate: _dateLivraison, firstDate: DateTime(2020, 1), lastDate: DateTime(2200, 1));
    if (picked != null && picked != _dateLivraison) {
      setState(() {
        _dateLivraison = picked ;
      });
    }
  }*/

  // methode de time
  Future<void> _showTimePicker() async {
    final picked =
        await showTimePicker(context: context, initialTime: _fromTime);
    if (picked != null && picked != _fromTime) {
      setState(() {
        _fromTime = _fromTime;
      });
    }
  }

  List<User> itemsCriteres;
  User itemOF = User();
  List<Widget> widgetsCriteres = List();
  List<Widget> widgetsCriteresOffres = List();
  List<Widget> widgetsCriteresAll = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    buildCritere(
        baseUrl: widget.model.baseUrl,
        appelDoffresId: widget.model.appelDoffresId,
        userFournisseurId: widget.model.userFournisseurId);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return (_isLoading)
        ? Center(child: CircularProgressIndicator())
        : Form(
            key: _formKey,
            autovalidate: _autovalidate,
            child: Utilities.isNotEmpty(widgetsCriteresAll)
                ? ListView(
                    children: <Widget>[
                      Column(
                        children: widgetsCriteresAll,
                      )
                    ],
                  )
                : Center(child: Text("Aucun critère pour cet appel d'offres")),
          );
  }

  // construction des criteres à renseigner
  buildCritere(
      {String baseUrl, int appelDoffresId, int userFournisseurId}) async {
    RequestCustom request = RequestCustom();

    // AO
    User dataAO = User();
    dataAO.id = appelDoffresId;
    dataAO.isSelect = false;

    // offres
    User dataOF = User();
    dataOF.appelDoffresId = appelDoffresId;
    dataOF.userFournisseurId =
        userFournisseurId == null ? userConnecter : userFournisseurId;
    dataOF.isRetirer = false;

    request.data = dataAO;

    setState(() => _isLoading = true);

    ResponseCustom responseCustom = await baseApi.testApi(
        apiBaseUrl: baseUrl + appelDoffres + getByCriteria, request: request);

    widgetsCriteresAll.clear();
    widgetsCriteres
        .clear(); // widgetsCriteres = List() ; possible ici car il n'est pas passé en parametre donc pas de souci de changement d'adresse de la variable
    if (responseCustom != null &&
        !responseCustom.hasError &&
        Utilities.isNotEmpty(responseCustom.items)) {
      Utilities.log(responseCustom.items);
      List<dynamic> listTest = responseCustom.items;
      User itemAO = User.fromJson(listTest[0]);

      //List<User> datasValeurCritereAppelDoffres = User.fromJsons(itemAO.datasValeurCritereAppelDoffres);

      //setState(() {
      if (Utilities.isNotEmpty(itemAO.datasValeurCritereAppelDoffres)) {
        itemsCriteres = User.fromJsons(itemAO.datasValeurCritereAppelDoffres);
      }
      //});

      List<User> listCritere = List();

      if (Utilities.isNotEmpty(itemsCriteres)) {
        //if (Utilities.isNotEmpty(datasValeurCritereAppelDoffres)) {
        // si l'OA a des criteres
        bool isNewOffre = true;
        request.data = dataOF;

        responseCustom = await baseApi.testApi(
            apiBaseUrl: baseUrl + offre + getByCriteria, request: request);

        if (responseCustom != null &&
            !responseCustom.hasError &&
            Utilities.isNotEmpty(responseCustom.items)) {
          // construction de la liste des criteres avec libelle/valeur

          //User itemOF = User.fromJson(responseCustom.items[0]);

          isNewOffre = false; // car il avait deja une offre pour cet AO
          //setState(() {
          itemOF = User.fromJson(responseCustom.items[0]);
          //});

          //List<User> datasValeurCritereOffre = itemOF.datasValeurCritereOffre;
          List<User> datasValeurCritereOffre = List();
          if (Utilities.isNotEmpty(itemOF.datasValeurCritereOffre)) {
            datasValeurCritereOffre =
                User.fromJsons(itemOF.datasValeurCritereOffre);
            // si l'OF contient des criteres  deja renseigner
            for (User valeurCritereAppelDoffres in itemsCriteres) {
              for (User valeurCritereOffre in datasValeurCritereOffre) {
                if (valeurCritereAppelDoffres.critereId ==
                    valeurCritereOffre.critereId) {
                  valeurCritereAppelDoffres.valeur = valeurCritereOffre.valeur;
                  valeurCritereAppelDoffres.offreId =
                      valeurCritereOffre.offreId;
                  break;
                }
              }
            }
          }

          //MAJ du widget des fichiers
          if (Utilities.isNotEmpty(itemOF.datasFichier)) {
            Utilities.log("datasFichier datasFichier datasFichier");
            Map<String, dynamic> tampon = Map();
            //Map<String, String> tampon = Map();
            for (var item in itemOF.datasFichier) {
              //tampon[item[name]] = item[fichierBase64];
              tampon[item[name]] = item[urlFichier];
              Utilities.log("datasFichier name fichierBase64 ${item[name]}");
              Utilities.log(
                  "datasFichier datasFichier fichierBase64 ${item[urlFichier]}");
            }
            Utilities.log("datasFichier datasFichier datasFichier");

            //setState(() {
            listFileWithContent.addAll(tampon);
            //});
          }
        }

        // ajout des informations dans le widget
        //setState(() {
        // ajouter le libelle de l'offre en question

        //Text(itemAO.libelle);

        widgetsCriteres.add(Text(partieAppelDoffres));
        //widgetsCriteres.add(Text(partieAppelDoffres,style: textStyleTitle,));

        widgetsCriteres.add(MyTextFormField(
          decoration: InputDecoration(labelText: libelle),
          initialValue: Utilities.initialStringValueWidget(itemAO.libelle),
          enabled: false,
          //controller: controllerLibelle,
        ));

        widgetsCriteres.add(MyTextFormField(
          decoration: InputDecoration(labelText: LPays),
          initialValue: Utilities.initialStringValueWidget(itemAO.paysLibelle),
          enabled: false,
          //controller: controllerLibelle,
        ));

        widgetsCriteres.add(MyTextFormField(
          decoration: InputDecoration(labelText: LVille),
          initialValue: Utilities.initialStringValueWidget(itemAO.villeLibelle),
          enabled: false,
          //controller: controllerLibelle,
        ));
        widgetsCriteres.add(MyTextFormField(
          decoration: InputDecoration(labelText: duree),
          initialValue: Utilities.initialiserDureeAOEnGet(
              nombreTypeDuree: itemAO.nombreTypeDuree,
              typeDureeLibelle: itemAO.typeDureeLibelle),

          /*
          (itemAO.nombreTypeDuree != null
                  ? itemAO.nombreTypeDuree.toString() + espace
                  : "") +
              (Utilities.isNotBlank(itemAO.typeDureeLibelle)
                  ? itemAO.typeDureeLibelle
                  : ""),
          (Utilities.isNotBlank(itemAO.typeDureeLibelle))
              ? itemAO.nombreTypeDuree.toString() +
                  espace +
                  itemAO.typeDureeLibelle
              : "",
              */
          enabled: false,
          //controller: controllerLibelle,
        ));
        widgetsCriteres.add(MyTextFormField(
          decoration: InputDecoration(labelText: dateLimite),
          initialValue:
              Utilities.initialStringValueWidget(itemAO.dateLimiteDepot),
          enabled: false,
          //controller: controllerLibelle,
        ));
        widgetsCriteres.add(Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16),
            child: MyTextFormField(
              decoration: InputDecoration(
                labelText: description,
                border:
                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
              ),
              initialValue:
                  Utilities.initialStringValueWidget(itemAO.description),
              enabled: false,
              maxLines: maxLines,
              //controller: controllerLibelle,
            )));

        // ajout des  domaines de l'AO
        if (itemAO != null &&
            Utilities.isNotEmpty(itemAO.datasSecteurDactiviteAppelDoffres)) {
          List<dynamic> initial = List();
          initial.addAll(itemAO.datasSecteurDactiviteAppelDoffres);
          widgetsCriteres.add(AbsorbPointer(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: MultiSelectFormField(
                titleText: LDomaine,
                hintText: hintMultiSelectForm,
                initialValue:
                    initial.map((e) => e[secteurDactiviteLibelle]).toList(),
                dataSource: initial,
                textField: secteurDactiviteLibelle,
                valueField: secteurDactiviteLibelle,
              ),
            ),
          ));
        }
        // ajout des fichiers
        if (itemAO != null && Utilities.isNotEmpty(itemAO.datasFichier)) {
          List<dynamic> datasFichier = List();
          datasFichier.addAll(itemAO.datasFichier);
          widgetsCriteres.add(Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Liste(s) de(s) fichier(s)"),
              ],
            ),
          ));
          widgetsCriteres.add(Container(
            padding: const EdgeInsets.only(bottom: 30.0),
            height: MediaQuery.of(context).size.height * 0.25,
            child: Scrollbar(
                child: ListView.separated(
              itemCount: datasFichier.length,
              itemBuilder: (BuildContext context, int index) {
                final String nameFile = datasFichier[index][name];
                final String urlFile = datasFichier[index][urlFichier];

                return new ListTile(
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text(telechargerFichier),
                          content: new Text(questionTelechargerFichier),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text(reponseNon),
                              onPressed: () {
                                Navigator.of(context).pop();
                                //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                              },
                            ),
                            new FlatButton(
                              child: new Text(reponseOui),
                              onPressed: () {
                                // telechager le file

                                //Utilities.base64InFile(base64: datasFichier[index][fichierBase64],nameFile: nameFile);

                                Utilities.saveFileFromUri(
                                    uriFile: urlFile, nameNewFile: nameFile);

                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },

                  title: Text(
                    nameFile,
                  ),
                  //subtitle: new Text(path),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  new Divider(),
            )),
          ));
        }

        //******************* Debut formulaire de l'offre ***************************
        widgetsCriteres.add(Divider());

        if (isNewOffre) {
          // ajout de boutton de creation de Offres
          widgetsCriteres.add(Row(
            children: <Widget>[
              Center(
                child: Utilities.getButtonSubmit(
                    shape: Utilities.getDefaultShape(),
                    child: Text(_isSoumettre ? annuler : soumettre),
                    onPressed: soumettreOuAnnulerOffre),
              ),
            ],
          ));
        } else {
          // retirer si possible
          widgetsCriteres.add(Row(
            children: <Widget>[
              Center(
                child: Utilities.getButtonSubmit(
                    shape: Utilities.getDefaultShape(),
                    child: Text(retirer),
                    onPressed: retirerOffre,
                    isDisable: _isDisable),
              ),
            ],
          ));
        }

        widgetsCriteres.add(Text((isNewOffre) ? partieNewOffre : partieOffre));

        //widgetsCriteres.add(Text(partieOffre, style: textStyleTitle,));
        for (User element in itemsCriteres) {
          //for (User element in datasValeurCritereAppelDoffres) {
          //User data = User.fromJson(element);

          Utilities.log(element.typeCritereAppelDoffresCode);

          switch (element.typeCritereAppelDoffresCode) {
            case typeDate:
              TextEditingController _controllerDate = TextEditingController();

              _controllerDate.text =
                  (Utilities.isNotBlank(element.valeur) ? element.valeur : "");
              widgetsCriteres.add(MyTextFormField(
                decoration: InputDecoration(
                    labelText:
                        element.critereAppelDoffresLibelle + marqueObligatoire),
                //initialValue: (Utilities.isNotBlank(element.valeur)? element.valeur: ""),
                validator: (value) {
                  Utilities.log("value typeDate typeDate $value");
                  Utilities.validator(value, typeContentToValidate: v_text);
                },
                autovalidate: true,

                //enabled: false,
                controller: _controllerDate,
                readOnly: true,
                onSaved: (input) {
                  element.valeur =
                      input; // pour recuperer la valeur saisie lors du submit
                },
                onTap: () {
                  _showDatePicker(_controllerDate);
                  //if (_controllerDate != null) {
                  // Utilities.log(_controllerDate.text = _getFormatDate()) ;
                  //}
                },
                keyboardType: TextInputType.datetime,
                //controller: controllerLibelle,
              ));
              break;

            case typeString:
              widgetsCriteres.add(MyTextFormField(
                decoration: InputDecoration(
                    labelText:
                        element.critereAppelDoffresLibelle + marqueObligatoire),
                initialValue: (Utilities.isNotBlank(element.valeur)
                    ? element.valeur
                    : ""),
                validator: (value) {
                  Utilities.validator(value, typeContentToValidate: v_text);
                },
                onSaved: (input) {
                  element.valeur =
                      input; // pour recuperer la valeur saisie lors du submit
                },
                //keyboardType: TextInputType.datetime,
                //controller: controllerLibelle,
              ));
              break;

            case typeNumber:
              TextEditingController _controllerNumber = TextEditingController();

              _controllerNumber.text =
                  (element.valeur != null ? element.valeur : "");
              widgetsCriteres.add(MyTextFormField(
                decoration: InputDecoration(
                    labelText:
                        element.critereAppelDoffresLibelle + marqueObligatoire),
                //initialValue: (Utilities.isNotBlank(element.valeur)? element.valeur : ""),
                validator: (value) {
                  Utilities.validator(value, typeContentToValidate: v_text);
                },
                controller: _controllerNumber,
                onChanged: (value) {
                  /* 
                  if (Utilities.isNotBlank(value) &&
                      value.toString().length >= 4) {
                    _controllerNumber.text = ;
                  }
                  */
                },
                onSaved: (input) {
                  element.valeur =
                      input; // pour recuperer la valeur saisie lors du submit
                },
                keyboardType: TextInputType.number,
                //controller: controllerLibelle,
              ));
              break;

            case typeBoolean:
              widgetsCriteres.add(Radio(
                  //value: element.valeur,
                  value: false,
                  groupValue: null,
                  onChanged: (input) {
                    element.valeur =
                        input; // pour recuperer la valeur saisie lors du submit
                  }));
              break;

            default:
              break;
          }
        }

        // ajout des autres widgets
        widgetsCriteres.add(Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: MyTextFormField(
            decoration: InputDecoration(
              labelText: commentaire,
              border:
                  OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
            ),
            validator: (value) {
              Utilities.validator(value, typeContentToValidate: v_text);
            },
            onSaved: (input) {
              //widget.model.libelle = input;
              itemOF.commentaire = input;
            },
            initialValue: (Utilities.isNotBlank(itemOF.commentaire)
                ? itemOF.commentaire
                : ""),
            maxLines: maxLines,
            keyboardType: TextInputType.multiline,
            //controller: controllerLibelle,
          ),
        ));

        // ajouter des fichiers
        widgetsCriteres.add(EditFile(
          listFile: listFileWithContent,
        ));

        // ajouter un divider
        widgetsCriteres.add(Divider());

        // les boutons de validation du formulaire de l'OF
        widgetsCriteres.add(Row(
          children: <Widget>[
            Utilities.getButtonSubmit(
                shape: Utilities.getDefaultShape(),
                child: Text(enregistrer),
                onPressed: () {
                  submit(etatCode: BROUILLON, action: CREATE);
                },
                isDisable: _isDisable),
            Spacer(),
            Utilities.getButtonSubmit(
                shape: Utilities.getDefaultShape(),
                child: Text(
                    (itemOF != null && itemOF.id != null ? modifier : creer)),
                onPressed: () => (itemOF != null && itemOF.id != null
                    ? submit(etatCode: ENTRANT, action: UPDATE)
                    : submit(etatCode: ENTRANT, action: CREATE)),
                isDisable: _isDisable),

            /*
            RaisedButton(
              shape: Utilities.getDefaultShape(),
              child: Text(enregistrer),
              onPressed: () {
                submit(etatCode: BROUILLON, action: CREATE);
              },
            ),
            Spacer(),
            RaisedButton(
              shape: Utilities.getDefaultShape(),
              child: Text(
                  (itemOF != null && itemOF.id != null ? modifier : creer)),
              onPressed: () {
                (itemOF != null && itemOF.id != null
                    ? submit(etatCode: ENTRANT, action: UPDATE)
                    : submit(etatCode: ENTRANT, action: CREATE));
              },
            )
            */
          ],
        ));

        setState(() {
          widgetsCriteresAll = widgetsCriteres;
          _isLoading = false;
        });
        //});
      } else {
        // l'AO est sans critère
      }
    } else {
      setState(() => _isLoading = false);
    }
  }

  //
  soumettreOuAnnulerOffre() {
    Utilities.begin("soumettreOuAnnulerOffre");
    // rendre visible le formulaire s'il ne l'est pas ou le contraire

    setState(() {
      if (_isSoumettre) {
        widgetsCriteresAll += widgetsCriteresOffres;
        _isSoumettre = !_isSoumettre;
      } else {
        int lengthWidgetsCriteresAll = widgetsCriteresAll.length;
        int lengthWidgetsCriteresOffres = widgetsCriteresOffres.length;
        widgetsCriteresAll = widgetsCriteresAll.sublist(
            0, lengthWidgetsCriteresAll - lengthWidgetsCriteresOffres);
        _isSoumettre = !_isSoumettre;
      }
    });

    Utilities.end("soumettreOuAnnulerOffre");
  }

  // retirer une offre
  retirerOffre() async {
    // disableButton
    disableButton();
    RequestCustom request = RequestCustom();

    request.datas = [itemOF];

    ResponseCustom responseCustom = await baseApi.testApi(
        apiBaseUrl: AppConfig.of(context).apiBaseUrl + offre + forceDelete,
        request: request);

    // restart le widget

    // enableButton
    enableButton();
  }

  // soumission du formulaire
  submit({String etatCode, String action}) {
    //NumberFormat()
    if (_formKey.currentState.validate()) {
      // disableButton
      disableButton();
      // executer les controles sur les inputs
      _formKey.currentState.save(); // recuperer les donnees saisies
      if (itemOF != null) {
        // formatage de la date
        for (var item in itemsCriteres) {
          switch (item.typeCritereAppelDoffresCode) {
            case typeDate:
              break;
            case typeNumber:
              break;
            case typeBoolean:
              break;
            case typeString:
              break;
            default:
          }
        }
        // recuperation des datasFichier

        // recuperation données saisies
        itemOF.datasValeurCritereOffre = itemsCriteres;
        itemOF.etatCode = etatCode;

        if (itemOF.userFournisseurId == null) {
          itemOF.userFournisseurId = widget.model.userFournisseurId;
        }
        if (itemOF.appelDoffresId == null) {
          itemOF.appelDoffresId = widget.model.appelDoffresId;
        }

        Utilities.log("itemOF.userFournisseurId ${itemOF.userFournisseurId}");
        Utilities.log("itemOF.appelDoffresId ${itemOF.appelDoffresId}");
        for (var item in itemOF.datasValeurCritereOffre) {
          Utilities.log(item.valeur);
          //Utilities.log(item.toJson());
        }

        // ajout des fichiers
        itemOF.datasFichier = Utilities.formationDatasFiles(
            listFileWithContent: listFileWithContent, isFichierOffre: true);

        // construction de l'URI
        String endpoint = "";

        //sho
        if (action == CREATE) {
          endpoint = create;
        } else {
          endpoint = update;
        }
        RequestCustom request = RequestCustom();

        List<User> datas = List();
        datas.add(itemOF);
        request.user = itemOF.userFournisseurId;
        request.datas = datas;
        baseApi
            .testApi(
                apiBaseUrl: widget.model.baseUrl + offre + endpoint,
                request: request)
            .then((res) {
          if (res != null && !res.hasError) {
            Utilities.getToast(succes);
            Navigator.pop(context);
          } else {
            Utilities.messageApi(response: res);
          }

          // enableButton
          enableButton();
        }).catchError((err) {
          // erreur avec le service
          // erreur de connexion
          // enableButton
          enableButton();
        });
      } else {}
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }

  void disableButton() {
    setState(() {
      _isDisable = true;
    });
  }

  void enableButton() {
    setState(() {
      _isDisable = false;
    });
  }
}
*/*/
