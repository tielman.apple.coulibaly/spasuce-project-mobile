import 'dart:convert';
import 'dart:io' as Io;

import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/user.dart';

class EditFile extends StatefulWidget {
  String tets;
  User model;
  Map<String, dynamic> listFile;

  @override
  State<StatefulWidget> createState() {
    return _EditFile();
  }

  EditFile({this.tets, this.model, this.listFile});
}

class _EditFile extends State<EditFile> {
  String _fileName;
  String _path;
  //Map<String, String> _paths;
  Map<String, dynamic> _paths;
  String _extension;
  bool _loadingPath = false;
  //bool _multiPick = false;
  bool _multiPick = true;
  bool _hasValidMime = false;
  FileType _pickingType = FileType.custom;
  TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    //_controller.addListener(() => _extension = _controller.text);

    //widget.model.datasDomaine = List();
  }

  void _openFileExplorer() async {
    //if (_pickingType != FileType.custom || _hasValidMime) {
    setState(() => _loadingPath = true);
    try {
      if (_multiPick) {
        _path = null;
        _paths = await FilePicker.getMultiFilePath(
            //type: _pickingType, allowedExtensions: [_extension]);
            type: _pickingType,
            allowedExtensions: ['pdf', 'svg', 'jpg']);
      } else {
        _paths = null;
        _path = await FilePicker.getFilePath(
            //type: _pickingType, fileExtension: _extension);
            //type: _pickingType, allowedExtensions: [_extension]);
            type: _pickingType,
            allowedExtensions: ['pdf', 'svg', 'jpg']);
      }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      _fileName = _path != null
          ? _path.split('/').last
          : _paths != null ? _paths.keys.toString() : '...';
    });
    //}
  }

  @override
  Widget build(BuildContext context) {
    Utilities.begin("EditFile/build");
    List<dynamic> listFileWithContent = List();

    bool varBolAnd = (_paths != null && _paths.length > 0) &&
        (widget.listFile != null && widget.listFile.length > 0);
    bool varBolOr = (_paths != null && _paths.length > 0) ||
        (widget.listFile != null && widget.listFile.length > 0);

    // construire le data final
    if (widget.listFile == null) {
      widget.listFile = Map<String, dynamic>();
    }
    if (varBolAnd) {
      Map<String, dynamic> tampon = Map();
      tampon.addAll(widget.listFile);
      widget.listFile.clear();
      widget.listFile.addAll(_paths);
      widget.listFile.addAll(tampon);
    } else {
      if (_paths != null && _paths.length > 0) {
        widget.listFile.addAll(_paths);
      }
    }

    return Container(
      child: Center(
          child: Column(
        children: <Widget>[
          new Padding(
            padding: const EdgeInsets.only(top: 30.0, bottom: 20.0),
            child: new RaisedButton(
              shape: Utilities.getDefaultShape(),
              onPressed: () => _openFileExplorer(),
              child: new Text(ajouterFichier),
            ),
          ),
          new Builder(
            builder: (BuildContext context) => _loadingPath
                ? Padding(
                    padding: const EdgeInsets.only(bottom: 10.0),
                    child: const CircularProgressIndicator())
                //: _path != null || (_paths != null && _paths.length > 0)
                : _path != null ||
                        (widget.listFile != null && widget.listFile.length > 0)
                    ? new Container(
                        padding: const EdgeInsets.only(bottom: 30.0),
                        height: MediaQuery.of(context).size.height * 0.25,
                        child: new Scrollbar(
                            child: new ListView.separated(
                          //itemCount: _paths != null && _paths.isNotEmpty ? _paths.length: 0,
                          //itemCount: varBolAnd ? widget.listFile.length + _paths.length : (widget.listFile != null && widget.listFile.length > 0) ? widget.listFile.length : (_paths != null && _paths.isNotEmpty) ? _paths.length: 0,
                          itemCount: (widget.listFile != null &&
                                  widget.listFile.length > 0)
                              ? widget.listFile.length
                              : 0,
                          itemBuilder: (BuildContext context, int index) {
                            //final bool isMultiPath = _paths != null && _paths.isNotEmpty;
                            final bool isMultiPath = widget.listFile != null &&
                                widget.listFile.length > 0;
                            //final String name = 'File $index: ' +
                            final String name = (isMultiPath
                                ? widget.listFile.keys.toList()[index]
                                : _fileName ?? '...');
                            final path = isMultiPath
                                ? widget.listFile.values
                                    .toList()[index]
                                    .toString()
                                : _path;
                            //Map<String, String> currentFile = {name:Utilities.fileInBase64(path)} ;
                            //widget.listFile.
                            //widget.listFile.addAll({name:Utilities.fileInBase64(path)}) ;
                            /*
                                if (_paths != null && _paths.isNotEmpty && index < _paths.length) {
                                  widget.listFile.values.toList()[index] = Utilities.fileInBase64(path) ;
                                }
                                */
                            //widget.listFile.add(name:Utilities.fileInBase64(path)) ;

                            //fileInBase64(path) ;
                            return new ListTile(
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    // return object of type Dialog
                                    return AlertDialog(
                                      title: new Text(supprimerFichier),
                                      content:
                                          new Text(questionSupprimerFichier),
                                      actions: <Widget>[
                                        // usually buttons at the bottom of the dialog
                                        new FlatButton(
                                          child: new Text(reponseNon),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                                          },
                                        ),
                                        new FlatButton(
                                          child: new Text(reponseOui),
                                          onPressed: () {
                                            // le supprimer dans la liste
                                            setState(() {
                                              if (_paths != null &&
                                                  _paths.isNotEmpty &&
                                                  index < _paths.length) {
                                                _paths.remove(_paths.keys
                                                    .toList()[index]);
                                              }
                                              widget.listFile.remove(widget
                                                  .listFile.keys
                                                  .toList()[index]);
                                            });
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },

                              title: Text(
                                name,
                              ),
                              //subtitle: new Text(path),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) =>
                              new Divider(),
                        )),
                      )
                    : new Container(),
          ),
        ],
      )),
    );
  }

  // convert file in base64
  static base64InFile(String base64, String pathFile) {
    /*
    try{

    }catch(Exception e){

    }

     */
    final decodedBytes = base64Decode(base64);

    var file = Io.File(pathFile);
    file.writeAsBytesSync(decodedBytes);
  }

// convert base64 in file
  static String fileInBase64(String pathFile) {
    print("pathFile: $pathFile");
    final bytes = Io.File(pathFile).readAsBytesSync();

    String img64 = base64Encode(bytes);
    //print(img64.substring(0, 100));

    print("img64: $img64");

    return img64;
  }
}
