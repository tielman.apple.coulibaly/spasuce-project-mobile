/*import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/const_app.dart' as globals;
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/views/connexion.dart';
import 'package:spasuce/views/accueil_offres.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/views/edit_ao.dart';
import 'package:spasuce/views/show_ao.dart';

class CardAccueil extends StatelessWidget {
  String dateLimiteDepot;
  String dateCreation;
  String codeAO;
  int nbreOF;

  String libelle;

  String description;

  User data ;

  CardAccueil({this.dateLimiteDepot = "dateLimite",
    this.dateCreation = "dateCreation",
    this.codeAO = "codeAO",
    this.data,
    this.nbreOF = 0,
    this.libelle = "libelle",
    this.description = "descrition"});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      //elevation: 0.0 ,
      //color:LinearGradient(colors: null),
      child: InkWell(
        onTap: () { // detail de l'offre

          print("*****************InkWell ${data.toJson()}");

          if (globals.isLoggedIn) {
            Navigator.push(
                //context, MaterialPageRoute(builder: (context) => AccueilOffres()));
                context, MaterialPageRoute(builder: (context) => ShowAO()));
          } else {
            print("*****************AlertDialog");
            showDialog(
              context: context,
              builder: (BuildContext context) {
                // return object of type Dialog
                return AlertDialog(
                  title: new Text(accesInterdit),
                  content: new Text("Voulez vous inscrire ou connecter ?"),
                  actions: <Widget>[
                    // usually buttons at the bottom of the dialog
                    new FlatButton(
                      child: new Text("Non"),
                      onPressed: () {
                        Navigator.of(context).pop();
                        //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                      },
                    ),new FlatButton(
                      child: new Text("Oui"),
                      onPressed: () {
                        Navigator.of(context).pop();
                        //Utilities.onSelectDrawerItem(context: context, view: Offres());
                        Utilities.onSelectDrawerItem(context: context, view: MyApp(currentIndex: 4,));
                      },
                    ),
                  ],
                );
              },
            );
          }
        },
        splashColor: ConstApp.color_app,
         child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                color: Colors.blue,
                height: 100,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  //textDirection: TextDirection.rtl,

                  //verticalDirection: VerticalDirection.up,
                  //mainAxisAlignment: MainAxisAlignment.start,
                  //crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    CircleAvatar(
                      //radius: 50, backgroundImage: NetworkImage("url"),
                      radius: 25,
                      backgroundImage: AssetImage('images/logo.jpeg'),
                      backgroundColor: Colors.white,
                    ),
                    //CircleAvatar(child: Image.asset('images/logo.jpeg'),),
                    SizedBox(
                      // mettre espace entre 2 elements width/height
                      width: 12.0,
                    ),
                    Container(
                      color: Colors.green,
                      //width: double.infinity,
                      child: Column(
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                        
                        Text(data.libelle, style: TextStyle(fontWeight: FontWeight.bold),),
                        Container(
                          color: Colors.white,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                            /*
                            Expanded(
                                    child: SizedBox(
                                      //height: 20.0,
                                      child: Text(data.code), )),
                                      */
Column(
                            //mainAxisSize: MainAxisSize.max,
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                            /*
                            Expanded(
                                    child: SizedBox(
                                      //height: 20.0,
                                      child: Text(data.code), )),
                                      */
                            Text(data.code)   ,
                            //Container(width: 50,),
                            //Expanded(child: SizedBox()),

                            //Spacer(),
                            Text("${(data.nbreOF == null) ? 0 : data.nbreOF}"),


                          ],)  ,                        //Container(width: 50,),
                            //Expanded(child: SizedBox()),

                            //Spacer(),
Column(
                            //mainAxisSize: MainAxisSize.max,
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                            /*
                            Expanded(
                                    child: SizedBox(
                                      //height: 20.0,
                                      child: Text(data.code), )),
                                      */
                            Text(data.code)   ,
                            //Container(width: 50,),
                            //Expanded(child: SizedBox()),

                            //Spacer(),
                            Text("${(data.nbreOF == null) ? 0 : data.nbreOF}"),


                          ],)

                          ],),
                        ),
                        Container(
                          color: Colors.red,
                          child: Row(
                            //mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                            /*
                            Expanded(
                                    child: SizedBox(
                                      //height: 20.0,
                                      child: Text(data.code), )),
                                      */
                            Text(data.code)   ,
                            //Container(width: 50,),
                            //Expanded(child: SizedBox()),

                            //Spacer(),
                            Text("${(data.nbreOF == null) ? 0 : data.nbreOF}"),


                          ],),
                        ),
                        Container(
                          color: Colors.red,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: <Widget>[
                            Text(data.dateCreation),
                            //Expanded(child: SizedBox()),
                            //Spacer(),

                            Text(data.dateLimiteDepot),


                          ],),
                        ),

                      ],),
                    ),
                    
                    //Icon(Icons.more_vert)
                  ],
                ),
              ),
              SizedBox(
                height: 12.0,
              ),
              Text(data.description, maxLines: 2,),
              /*
              Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(libelle),
                      Text(codeAO),
                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(codeAO),
                      Text(dateLimite),
                    ],
                  ),
                  //Icon(Icons.more_vert)
                ],
              ),
              */
            ],
          ),
        



        /*
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(data.libelle),
              Row(
                children: <Widget>[
                  CircleAvatar(
                    //radius: 50, backgroundImage: NetworkImage("url"),
                    radius: 25,
                    backgroundImage: AssetImage('images/logo.jpeg'),
                    backgroundColor: Colors.white,
                  ),
                  //CircleAvatar(child: Image.asset('images/logo.jpeg'),),
                  SizedBox(
                    // mettre espace entre 2 elements width/height
                    width: 12.0,
                  ),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(data.code),
                      Text(data.dateCreation),

                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("${(data.nbreOF == null) ? 0 : data.nbreOF}"),
                      Text(data.dateLimiteDepot),
                    ],
                  ),
                  //Icon(Icons.more_vert)
                ],
              ),
              SizedBox(
                height: 12.0,
              ),
              Text(data.description, maxLines: 2,),
              /*
              Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(libelle),
                      Text(codeAO),
                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(codeAO),
                      Text(dateLimite),
                    ],
                  ),
                  //Icon(Icons.more_vert)
                ],
              ),
              */
            ],
          ),*/






        ),
      ),
    );
  }
}

/*


 Row(
                children: <Widget>[
                  CircleAvatar(
                    //radius: 50, backgroundImage: NetworkImage("url"),
                    radius: 25,
                    backgroundImage: AssetImage('images/logo.jpeg'),
                    backgroundColor: Colors.white,
                  ),
                  //CircleAvatar(child: Image.asset('images/logo.jpeg'),),
                  SizedBox(
                    // mettre espace entre 2 elements width/height
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(libelle),
                      Text(codeAO),

                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(codeAO),
                      Text(dateLimite),
                    ],
                  ),
                  //Icon(Icons.more_vert)
                ],
              ),
 */
*/