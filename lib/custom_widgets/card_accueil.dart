import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/const_app.dart' as globals;
import 'package:spasuce/dao/entity/user_entity.dart';
import 'package:spasuce/dao/repository/database_helper.dart';
import 'package:spasuce/dao/repository/database_helper_old_.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/views/connexion.dart';
import 'package:spasuce/views/accueil_offres.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/views/edit_ao.dart';
import 'package:spasuce/views/edit_offres.dart';
import 'package:spasuce/views/voir_liste_offres_ao.dart';

class CardAccueil extends StatelessWidget {
  String dateLimiteDepot;
  String dateCreation;
  String codeAO;
  int nbreOF;
  int userId;

  String libelle;

  String description;

  User data;

  CardAccueil(
      {this.dateLimiteDepot = "dateLimite",
      this.userId,
      this.dateCreation = "dateCreation",
      this.codeAO = "codeAO",
      this.data,
      this.nbreOF = 0,
      this.libelle = "libelle",
      this.description = "descrition"});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
        //elevation: 0.0 ,
        //color:LinearGradient(colors: null),
        shape: Utilities.getShape(
            topLeft: 10, topRight: 10, bottomLeft: 10, bottomRight: 10),
        child: InkWell(
          onTap: () async {
            // detail de l'offre

            //print("*****************InkWell ${data.toJson()}");
            print("*****************InkWell ${this.data.typeEntrepriseCode}");
            print(
                "*****************InkWell ${this.data.typeEntrepriseLibelle}");

            UserEntity userConnected =
                await DatabaseHelper.instance.getUserConnected;

            if (userConnected != null) {
              //if (globals.isLoggedIn) {
              //List<UserEntity> users = await DatabaseHelper.instance.getByCriteria();

              print(
                  "*****************this.data.typeEntrepriseCode   ${this.data.typeEntrepriseCode}");

              if (this.data != null &&
                  this.data.typeEntrepriseCode == CLIENTE) {
                // messagge d'interdiction de visualisation

                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return AlertDialog(
                      title: new Text(accesInterdit),
                      content: new Text(contenuImpossibleDeSoumettre),
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        new FlatButton(
                          child: new Text(okButtonLabel),
                          onPressed: () {
                            Navigator.of(context).pop();
                            //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                          },
                        )
                      ],
                    );
                  },
                );
              } else {
                Utilities.navigate(
                    context,
                    EditOffres(
                      title: ConstApp.show_oa,
                      data: data,
                      userFournisseurId: userId,
                    ));
              }
            } else {
              print("*****************AlertDialog");
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  // return object of type Dialog
                  return AlertDialog(
                    title: new Text(accesInterdit),
                    content: new Text(questionInscriptionConnexion),
                    actions: <Widget>[
                      // usually buttons at the bottom of the dialog
                      new FlatButton(
                        child: new Text(reponseNon),
                        onPressed: () {
                          Navigator.of(context).pop();
                          //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                        },
                      ),
                      new FlatButton(
                        child: new Text(reponseOui),
                        onPressed: () {
                          Navigator.of(context).pop();
                          //Utilities.onSelectDrawerItem(context: context, view: Offres());
                          Utilities.onSelectDrawerItem(
                              context: context,
                              view: MyApp(
                                currentIndex: 4,
                              ));
                        },
                      ),
                    ],
                  );
                },
              );
            }
          },
          splashColor: ConstApp.color_app,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Wrap(children: <Widget>[
                  Text(
                    data.libelle,
                    style: Utilities.styleTextTitreH3(),
                    maxLines: minLines,
                    overflow: TextOverflow.ellipsis,
                  ),
                ]),
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      //radius: 50, backgroundImage: NetworkImage("url"),
                      radius: 25,
                      backgroundImage: AssetImage('images/logo.jpeg'),
                      backgroundColor: Colors.white,
                    ),
                    //CircleAvatar(child: Image.asset('images/logo.jpeg'),),
                    SizedBox(
                      // mettre espace entre 2 elements width/height
                      width: 12.0,
                    ),

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(data.code),
                        Text(data.dateCreation),
                      ],
                    ),
                    Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start, 
                      children: <Widget>[
                        //Text("${(data.nbreOF == null) ? 0 : data.nbreOF}"),
                        Text(data.nombreOffre != null ? data.nombreOffre : ""),
                        Text(data.dateLimiteDepot),
                      ],
                    ),
                    //Icon(Icons.more_vert)
                  ],
                ),
                SizedBox(
                  height: 12.0,
                ),
                Text(
                  data.description,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                /*
              Row(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(libelle),
                      Text(codeAO),
                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(codeAO),
                      Text(dateLimite),
                    ],
                  ),
                  //Icon(Icons.more_vert)
                ],
              ),
              */
              ],
            ),
          ),
        ));
  }
}

/*


 Row(
                children: <Widget>[
                  CircleAvatar(
                    //radius: 50, backgroundImage: NetworkImage("url"),
                    radius: 25,
                    backgroundImage: AssetImage('images/logo.jpeg'),
                    backgroundColor: Colors.white,
                  ),
                  //CircleAvatar(child: Image.asset('images/logo.jpeg'),),
                  SizedBox(
                    // mettre espace entre 2 elements width/height
                    width: 12.0,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(libelle),
                      Text(codeAO),

                    ],
                  ),
                  Spacer(),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(codeAO),
                      Text(dateLimite),
                    ],
                  ),
                  //Icon(Icons.more_vert)
                ],
              ),
 */
