//import 'dart:html';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_multiselect/flutter_multiselect.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/edit_file.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/ressources/config_app.dart';

class FormAO extends StatefulWidget {
  String baseUrl;

// mode du form update/create/read

// externData form

// model form
  User model = User();
  String mode;

  FormAO({this.baseUrl, this.model, this.mode = CREATE});

  @override
  _FormAOState createState() => _FormAOState();
}

class _FormAOState extends State<FormAO> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController controllerLibelle;
  TextEditingController _controllerDate = TextEditingController();

  List<DropdownMenuItem<dynamic>> _dmiPays = List();
  List<DropdownMenuItem<dynamic>> _dmiVille = List();
  List<DropdownMenuItem<dynamic>> _dmiUniteDuree = List();
  List<DropdownMenuItem<dynamic>> _dmiTypeAO = List();
  List<DropdownMenuItem<dynamic>> _dmiFournisseurs = List();
  List<DropdownMenuItem<dynamic>> _dmiCriteres = List();
  List<DropdownMenuItem<dynamic>> _dmiDomaines = List();

  bool isDataDispo = false;
  bool _isVisibleFournisseur = false;
  bool _isDisable = false;

  //User _itemPaysSelected = User();
  User currentPays;
  User currentVille;
  //User _itemPaysSelected;
  //User _itemVilleSelected = User();
  User _itemUniteDureeSelected;
  User _itemTypeAOSelected;
  User _itemDomaineSelected;

  int _itemUniteDureeSelectedId;

  //List<User> _itemDomaineSelected = List();
  List<User> _itemFournisseursSelected = List();
  List<User> _itemCriteresSelected = List();

  List<dynamic> _itemsDomaines = List();
  List<dynamic> _initialItemsDomaines = List();
  List<dynamic> _itemsCriteres = List();
  List<dynamic> _initialItemsCriteres = List();
  List<dynamic> _itemsFournisseurs = List();
  List<dynamic> _initialItemsFournisseurs = List();

  String selectedValue;

  bool _autovalidate = false;

  User itemAO = User();

  //Map<String, String> listFileWithContent = Map();
  Map<String, dynamic> listFileWithContent = Map();

  /*Map<String, dynamic> listFileWithContent = {
    "tetst.jpg":
        //"iVBORw0KGgoAAAANSUhEUgAAAYYAAAAGCAYAAADHRzmJAAAMZWlDQ1BJQ0MgUHJvZmlsZQAASImVlwdYU8kWgOeWVBJaIAJSQm+iSA0gJYQWQUCqICohCSSUGBOCih1dVsG1oCKKZUVXRRRdXQFZCyJ2F8XuWjYWVFbWxVVsqLwJCayrr3xvvm/u/Dlz5sw5JzP3zgCgp+LLZPmoPgAF0kJ5QmQoa0JaOov0CGDADNCAA/DmCxQyTnx8DIBlsP1neX0dIOr2ipva1tf9/7UYCkUKAQBIBuQsoUJQALkFALxEIJMXAkAMg3Lb6YUyNYshG8mhg5BnqzlHwyvUnKXhbQM6SQlcyE0AkGl8vjwHAN02KGcVCXKgHd1HkN2lQokUAD0jyEECMV8IOQnyiIKCqWqeD9kJ6ssg74TMzvrMZs4/7GcN2efzc4ZYE9dAIYdJFLJ8/sz/MzX/uxTkKwfncICVJpZHJajjhzm8mTc1Ws00yN3SrNg4da4hv5UINXkHAKWKlVHJGn3UXKDgwvwBJmR3IT8sGrI55AhpfmyMVp6VLYngQYarBZ0hKeQlaccuFinCE7U2N8inJsQNcracy9GOrefLB+ZV67cp85I5Wvs3xSLeoP1XxeKkVMhUADBqkSQlFrIuZCNFXmK0RgezKRZzYwd15MoEtf92kNkiaWSoxj6WkS2PSNDqywoUg/FipWIJL1bLVYXipChNfrBdAv6A/yaQG0RSTvKgHZFiQsxgLEJRWLgmdqxdJE3WxovdkxWGJmjH9sjy47X6OFmUH6mW20A2UxQlasfiYwrh4tTYx2NkhfFJGj/xzFz+2HiNP3gRiAFcEAZYQAlrFpgKcoGkvbuxG/7S9EQAPpCDHCACblrJ4IjUgR4pfCaCYvAHJBFQDI0LHegVgSIo/zgk1TzdQPZAb9HAiDzwGHIBiAb58LdyYJR0aLYU8AhKJF/NLoC+5sOq7vtaxoGSGK1EOWiXpTeoSQwnhhGjiBFEZ9wMD8ID8Bj4DIHVA2fjfoPe/q1PeEzoIDwgXCOoCLemSErkX/gyDqig/QhtxFmfR4w7QJveeCgeCK1DyzgTNwNuuBech4MHw5m9oZSr9VsdO+vfxDkUwWc51+pR3CkoZRglhOL05UhdF13vISvqjH6eH42vWUNZ5Q71fDk/97M8C2Eb/aUmthg7gJ3GjmNnscNYI2Bhx7Am7AJ2RM1Da+jRwBoanC1hwJ88aEfy1Xx87ZzqTCrc69y73D9o+0ChaEaheoNxp8pmyiU54kIWB34FRCyeVDByBMvD3cMdAPU3RfOaeskc+FYgzHN/y0oeAxA4ub+///DfsuhsAPa3wm3+mZ5TGXwXqwA4s12glBdpZLj6QYBvAz24o0yBJbAFTjAiD+ADAkAICAdjQRxIAmlgMsyzGK5nOZgOZoMFoBSUgxVgDVgPNoOtYCfYA/aDRnAYHAenwHlwCVwDt+H66QTPQA94DfoQBCEhdISBmCJWiD3iinggbCQICUdikAQkDclEchApokRmIwuRcqQCWY9sQWqRH5FDyHHkLNKB3ELuI13IX8h7FENpqBFqgTqgo1A2ykGj0SR0EpqDTkOL0UXoMrQKrUF3ow3ocfQ8eg1Voc/QXgxgOhgTs8bcMDbGxeKwdCwbk2NzsTKsEqvB6rFm+E9fwVRYN/YOJ+IMnIW7wTUchSfjAnwaPhdfiq/Hd+INeBt+Bb+P9+CfCHSCOcGV4E/gESYQcgjTCaWESsJ2wkHCSbibOgmviUQik+hI9IW7MY2YS5xFXErcSNxLbCF2EB8Se0kkkinJlRRIiiPxSYWkUtI60m7SMdJlUifpLVmHbEX2IEeQ08lScgm5kryLfJR8mfyE3EfRp9hT/ClxFCFlJmU5ZRulmXKR0knpoxpQHamB1CRqLnUBtYpaTz1JvUN9qaOjY6PjpzNeR6IzX6dKZ5/OGZ37Ou9ohjQXGpeWQVPSltF20Fpot2gv6XS6Az2Enk4vpC+j19JP0O/R3+oydEfq8nSFuvN0q3UbdC/rPtej6NnrcfQm6xXrVeod0Luo161P0XfQ5+rz9efqV+sf0r+h32vAMBhtEGdQYLDUYJfBWYOnhiRDB8NwQ6HhIsOthicMHzIwhi2DyxAwFjK2MU4yOo2IRo5GPKNco3KjPUbtRj3GhsZexinGM4yrjY8Yq5gY04HJY+YzlzP3M68z3w+zGMYZJhq2ZFj9sMvD3pgMNwkxEZmUmew1uWby3pRlGm6aZ7rStNH0rhlu5mI23my62Sazk2bdw42GBwwXDC8bvn/4r+aouYt5gvks863mF8x7LSwtIi1kFussTlh0WzItQyxzLVdbHrXssmJYBVlJrFZbHbP6nWXM4rDyWVWsNlaPtbl1lLXSeot1u3WfjaNNsk2JzV6bu7ZUW7Zttu1q21bbHjsru3F2s+3q7H61p9iz7cX2a+1P279xcHRIdfjWodHhqaOJI8+x2LHO8Y4T3SnYaZpTjdNVZ6Iz2znPeaPzJRfUxdtF7FLtctEVdfVxlbhudO0YQRjhN0I6ombEDTeaG8etyK3O7f5I5siYkSUjG0c+H2U3Kn3UylGnR31y93bPd9/mfnu04eixo0tGN4/+y8PFQ+BR7XHVk+4Z4TnPs8nzhZerl8hrk9dNb4b3OO9vvVu9P/r4+sh96n26fO18M303+N5gG7Hj2UvZZ/wIfqF+8/wO+73z9/Ev9N/v/2eAW0BewK6Ap2Mcx4jGbBvzMNAmkB+4JVAVxArKDPo+SBVsHcwPrgl+EGIbIgzZHvKE48zJ5ezmPA91D5WHHgx9w/XnzuG2hGFhkWFlYe3hhuHJ4evD70XYRORE1EX0RHpHzopsiSJERUetjLrBs+AJeLW8nrG+Y+eMbYumRSdGr49+EOMSI49pHoeOGztu1bg7sfax0tjGOBDHi1sVdzfeMX5a/M/jiePjx1ePf5wwOmF2wulERuKUxF2Jr5NCk5Yn3U52SlYmt6bopWSk1Ka8SQ1LrUhVTRg1Yc6E82lmaZK0pnRSekr69vTeieET10zszPDOKM24Pslx0oxJZyebTc6ffGSK3hT+lAOZhMzUzF2ZH/hx/Bp+bxYva0NWj4ArWCt4JgwRrhZ2iQJFFaIn2YHZFdlPcwJzVuV0iYPFleJuCVeyXvIiNyp3c+6bvLi8HXn9+an5ewvIBZkFh6SG0jxp21TLqTOmdshcZaUy1TT/aWum9cij5dsViGKSoqnQCB7eLyidlN8o7xcFFVUXvZ2eMv3ADIMZ0hkXZrrMXDLzSXFE8Q+z8FmCWa2zrWcvmH1/DmfOlrnI3Ky5rfNs5y2a1zk/cv7OBdQFeQt+KXEvqSh5tTB1YfMii0XzFz38JvKbulLdUnnpjW8Dvt28GF8sWdy+xHPJuiWfyoRl58rdyyvLPywVLD333ejvqr7rX5a9rH25z/JNK4grpCuurwxeubPCoKK44uGqcasaVrNWl61+tWbKmrOVXpWb11LXKteqqmKqmtbZrVux7sN68fpr1aHVezeYb1iy4c1G4cbLm0I21W+22Fy++f33ku9vbonc0lDjUFO5lbi1aOvjbSnbTv/A/qF2u9n28u0fd0h3qHYm7Gyr9a2t3WW+a3kdWqes69qdsfvSnrA9TfVu9Vv2MveW7wP7lPt+/zHzx+v7o/e3HmAfqP/J/qcNBxkHyxqQhpkNPY3iRlVTWlPHobGHWpsDmg/+PPLnHYetD1cfMT6y/Cj16KKj/ceKj/W2yFq6j+ccf9g6pfX2iQknrraNb2s/GX3yzKmIUydOc04fOxN45vBZ/7OHzrHPNZ73Od9wwfvCwV+8fznY7tPecNH3YtMlv0vNHWM6jl4Ovnz8StiVU1d5V89fi73WcT35+s0bGTdUN4U3n97Kv/Xi16Jf+27Pv0O4U3ZX/27lPfN7Nb85/7ZX5aM6cj/s/oUHiQ9uPxQ8fPZI8ehD56LH9MeVT6ye1D71eHq4K6Lr0u8Tf+98JnvW1136h8EfG547Pf/pz5A/L/RM6Ol8IX/R/9fSl6Yvd7zyetXaG99773XB6743ZW9N3+58x353+n3q+yd90z+QPlR9dP7Y/Cn6053+gv5+GV/OHzgKYLCi2fDc8NcOAOhpADAuwfPDRM2db6AgmnvqAIH/xJp74UDxAaAeNurjOrcFgH2wOsCqOx8A9VE9KQSgnp5DVVsU2Z4eGls0eOMhvO3vf2kBAKkZgI/y/v6+jf39H+EdFbsFQMs0zV1TXYjwbvC9r5qubFvXBL4omnvoZzF+2QK1B17gy/ZflFOLYoZ8d/4AAACKZVhJZk1NACoAAAAIAAQBGgAFAAAAAQAAAD4BGwAFAAAAAQAAAEYBKAADAAAAAQACAACHaQAEAAAAAQAAAE4AAAAAAAAAkAAAAAEAAACQAAAAAQADkoYABwAAABIAAAB4oAIABAAAAAEAAAGGoAMABAAAAAEAAAAGAAAAAEFTQ0lJAAAAU2NyZWVuc2hvdHtCLLUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAHUaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPGV4aWY6UGl4ZWxYRGltZW5zaW9uPjM5MDwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlVzZXJDb21tZW50PlNjcmVlbnNob3Q8L2V4aWY6VXNlckNvbW1lbnQ+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj42PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CunDUVgAAAAcaURPVAAAAAIAAAAAAAAAAwAAACgAAAADAAAAAwAAA83YQkdKAAADmUlEQVRoBcSXzUorQRCFa+IP+AcK7nQvgj8o+igu9OH0DXwMXSoiigtBN24E3blQb3L7dOabVIqeJPe6sKGp6qpzTtVkMlNJ1UvLfrBElkJVa8hmxUa1djoJU/WsCTcMEWF7T8iET2Ld7p9sq6pKGlko5WqbYMJUCSt92f6qNRNncMavQ8G8vLzY+fl53s/Pz/b6+morKyt2dHSU99raWj4vLCw0zO/vb/v6+rLb21s7PT21s7Mzm5mZscXFRdvd3bWTk5O8p6enG050uAXY/nWWewUjDeHaVsRxlh2l36Y3Ko62MPiT1ACLpUaJKwy72+3ax8eHvb+/29vbm01NTVmn07H5+XlbXl7O22vIb1ul2m3Ytjh9KU9dfZceHx/t4eHBrq6u8l5dXbWNjQ3b3Ny07e1t29rasrm5uUbW9zJpz+DogfqN6A8c+pEm+pLzNXycnLfy4/K65IhxluXafCz6vr7vS7hSjuf17u4uP6t6ZvX9WVpasp2dHTs+Ps7Pq55hlu9tXE8eK77Hk/MxangLLvI9Rj64cXrwwHOWhRtzVQrwJvX4iX2xJaBHj8cvK2bV/iDIL2u9tDUYmocU9LDtn2hpMBi6SbSTuBoMeTjUFYXUJQwPBqeZXXcecWW/ORi4DbpR3Cy1mq+t+cwGXwYuw2OJoQUXjM4xBud/LbXgc/bXQQyMbOwp5sgTl4Z2jHst+aVaaJS4EV/CwG+z9EZeGk9PT3Zzc2PX19d2eXlpFxcXtr6+bgcHB3nv7++btoYZK/aieKkf6ilHvhRDd5z1db0ePMXQ9zH8Eh8dMNgSVjkf17mNrxyr1JPn+bzi2n4w8EMuDgYNh9nZWcpkS39efwiQDmB8fBTe4/C9xjgu2HG4kjYxuGg18RTgLUzsn6xn8/pNn1DWSI9yrZVsr5v8tNPNSW/49CIXur97vYaZYnrJD5YUcovS1M1VVhp59a0wefioXho+5PrZYWydLJrfHAy+Id0sbousztxAcIqD8blR3BKeGLrjLLXgyRKLXHoRBjxYbORw9nm45GR93sepFfOKK+bjUdfnvKb8qBuxaGE1GPQvUoNBQ4HBcHh42AyGvb29PBjgqI50qcW5VEsYxX0OHWKcpVNawlEr6qEh3jidqO255KJGxPh8zKEh63Hy27CKgwWnmAbD5+en3d/fN//wS4NB/xjatH0/+NTgHC29EPfaMcdZGG3O4noeWpNarwMHvZj7CwAA///snLr2AAAET0lEQVTNl0lOLDsQRV30rYAxsARAIGAnSMDegB2wCxgDQjAEJkyY0omuvo8zT1a8fAnvSV/6+pYiw75xo0nbaVf1+rmlf9M6vQE1ZN3/Sv3+Z1afqTc0VCQ/MgfpZVumZOmVOvq1xlQhlcV+IRV+6WW4ovXrjObF2qtj/epbRfj9eX9/n46Pj4vc3d2lh4eHtLCwkHZ2doosLi6W8fT0dOP88fGR3t/f0+XlZTo8PExHR0dpdHQ0zczMpLW1tbS/v19kZGSk8Ykdp7/XvGtlBUe+vr7SEHOW7ZGjHS0uB4ymTXsVefBs87SId/mJwVGsT/+oqR+Bqy86SuS3+z/V0sW1JuPDiRi1ipVOfliX4y5NDN7DuN/5yGP/sCcuLi7SyclJkeXl5bS9vZ02NzfT+vp6kYmJiZIOP2MbAwOYNVsX9sgXb2t54NYL5hiM8efnZ7PPXEv5cKOPYzA56jYPLk1cH/mVtXrK6bK1eXLR1hsxYhhH3DHfKnJ1dVW+Vb7X4eHhNDs7m1ZXV9Pu7m7a29tLY2NjTYyYv6vfzvEdJ/KsR0wfxojvgI4YPH31+VvdzhVjRVvJnYF4kv5tjgEP7yZEHao5h4ux2PNnlXWW/KJIr1wMEIdq98qp18QapMgO1QBfW+ZBLVB+9IspY+V6yPHlNZdDA3zb+a8vhvbUxwXHpr0sVP3uYmpfRl91287HzwfhRfb29lZc+bD4MDigEMb6Gssc4NYiR0yOPtFuP3LkqdscuV36Tz7tmoghpq8YGoz5YW6cHzQXgXYue4QLnvlCYqxCzA/yILe3t+ViOD8/L5fC6elpWlpaSltbW0W8GCYnJ3UtmpjGAGDczoM9Nuq0bg8+MNYSGR8fL2tL7foa11zyxWP86AOuD/3I7+LB+a75XvpFHvUgvA97FW1e3oODO65H9KXfju3YefJi4MdcvBi4FBBix6Z/xOxTV7R3vQ9ccbjyxYzV5onLi75y27a4l507uPiyH9zL7mOw2EqOHPTXXRYZf+qXc7/6EPJ2rtlZ5wKqkzn3S3gwzNWxXfU5vAf/GoqzISpq4TdhiUlDE9OyS64qe78XA5Cy9sGtOP/8+D9dDFQal4bFcqyGAx51GdQPeAob5OXlpcjT01NCaGwSDo75+fk0NzdXDj5jYjevORlHu/Hh0rTzUWOTq65Yg7qNKx7H+qDNI/YTX1tbt321E5uD5/X1tZmj5+fnclnA4cPhXyL/Ark8PZSwdcUk3s3NTbkYzs7OEpeCFwP/FpCNjY0iU1NTzbrGeM5DV3x4NOeE2llb6ndtOQA9AFhXhNpjPPvmIqYYfZt2bebFDibe5jNu+8pB62e8OPYAZx0eHx8T2suCOeMXPmviWuiLNmc7B2MPzevr63RwcFD+5XsxrKyslH/3Xgzko7H+xi9A/TBP2wauL1Tt6hijCzMuvNhnLF9tLjR1IvFbZ97YE9rZE8xbnDt/5OBLg/sPrwIQPUl4IkAAAAAASUVORK5CYII="
        "/9j/4QJwRXhpZgAATU0AKgAAAAgACAEQAAIAAAAUAAAAbgEAAAQAAAABAAAC0AEBAAQAAAABAAAFAAEyAAIAAAAUAAAAggESAAMAAAABAAEAAIdpAAQAAAABAAAAnYglAAQAAAABAAABswEPAAIAAAAHAAAAlgAAAABBT1NQIG9uIElBIEVtdWxhdG9yADIwMjA6MDY6MDIgMjM6NTU6MDkAR29vZ2xlAAAQgp0ABQAAAAEAAAFjgpoABQAAAAEAAAFrkpIAAgAAAAQyMzQAkpEAAgAAAAQyMzQAkpAAAgAAAAQyMzQAkgoABQAAAAEAAAFzkgkAAwAAAAEAAAAAiCcAAwAAAAEAZAAAkAQAAgAAABQAAAF7kAMAAgAAABQAAAGPoAMABAAAAAEAAAUApAMAAwAAAAEAAAAAoAIABAAAAAEAAALQkgIABQAAAAEAAAGjkgEACgAAAAEAAAGrkAAABwAAAAQwMjIwAAAAAAAAARgAAABkAYSY4DuaygAAABOIAAAD6DIwMjA6MDY6MDIgMjM6NTU6MDkAMjAyMDowNjowMiAyMzo1NTowOQAAAAEpAAAAZAAAFK8AAAPoAAcAAQACAAAAAk4AAAAAAgAKAAAAAwAAAg0AAwACAAAAAlcAAAAABAAKAAAAAwAAAiUABgAFAAAAAQAAAj0ABwAFAAAAAwAAAkUAHQACAAAACwAAAl0AAAAAAAAAJQAAAAEAAAAZAAAAAQAAB38AAABkAAAAegAAAAEAAAAFAAAAAQAAAPAAAABkAAAABQAAAAEAAAAXAAAAAQAAADcAAAABAAAACQAAAAEyMDIwOjA2OjAyAP/gABBKRklGAAEBAAABAAEAAP/bAEMAAgEBAQEBAgEBAQICAgICBAMCAgICBQQEAwQGBQYGBgUGBgYHCQgGBwkHBgYICwgJCgoKCgoGCAsMCwoMCQoKCv/bAEMBAgICAgICBQMDBQoHBgcKCgoKCgoKCgo"
  };*/

  String tets = "";

/*
onChangeDropdownItemPays(dynamic itemSelected) {
    Utilities.begin("FormAO/onChangeDropdownItemPays") ;
    setState(() {
      print("itemSelected itemSelected $itemSelected") ;
      _itemPaysSelected = itemSelected;


      //_dropdownMenuItemsVille =  List() ;
      _dmiVille.clear() ;
      _itemVilleSelected = null;
      //_dmiVille.clear(); // vider la liste des villes
      //_itemVilleSelected = null ;

      // load data ville in _dropdownMenuItemsVille
    });
    if (_itemPaysSelected != null &&
          Utilities.isNotEmpty(_itemPaysSelected.datasVille)) {
        for (var item in _itemPaysSelected.datasVille) {
          //print(item);
          //print(item['libelle']) ;
          User user = User.fromJson(item);
          _dmiVille.add(DropdownMenuItem(
            child: Text(user.libelle),
            value: user,
          ));
        }
      }else{
          //print("itemSelected valeur NULL") ;
        //_itemVilleSelected = null ;
        //_dmiVille = List() ;
      }
  }

  onChangeDropdownItemPaysOld(dynamic itemSelected) {
    Utilities.begin("FormAO/onChangeDropdownItemPays") ;
    setState(() {
      print("itemSelected itemSelected $itemSelected") ;
      _itemPaysSelected = itemSelected;

      //_dropdownMenuItemsVille =  List() ;
      _itemVilleSelected = null;
      _dmiVille.clear() ;
      //_dmiVille.clear(); // vider la liste des villes
      //_itemVilleSelected = null ;

      // load data ville in _dropdownMenuItemsVille
      if (_itemPaysSelected != null &&
          Utilities.isNotEmpty(_itemPaysSelected.datasVille)) {
        for (var item in _itemPaysSelected.datasVille) {
          //print(item);
          //print(item['libelle']) ;
          User user = User.fromJson(item);
          _dmiVille.add(DropdownMenuItem(
            child: Text(user.libelle),
            value: user,
          ));
        }
      }else{
          //print("itemSelected valeur NULL") ;
        //_itemVilleSelected = null ;
        //_dmiVille = List() ;
      }
    });
  }

  onChangeDropdownItemVille(dynamic itemSelected) {
    Utilities.begin("FormAO/onChangeDropdownItemVille") ;

    setState(() {
      _itemVilleSelected = itemSelected;
      //itemAO.villeId = _itemVilleSelected.id;
      if (_itemVilleSelected != null) {
        widget.model.villeId = itemSelected.id;
      }
    });
  }

*/
  onChangeDropdownItemDomaine(dynamic itemSelected) {
    setState(() {
      _itemDomaineSelected = itemSelected;
    });
  }

  onChangeDropdownItemTypeAO(dynamic itemSelected) {
    setState(() {
      //_itemTypeAOSelected = itemSelected;
      (itemSelected != null && itemSelected == RESTREINT)
          ? _isVisibleFournisseur = true
          : _isVisibleFournisseur = false;
      //_isVisibleFournisseur = (itemSelected.code == RESTREINT) ? true  : false ;
    });
  }

  onChangeDropdownItemUniteDuree(dynamic itemSelected) {
    setState(() {
      //_itemUniteDureeSelected = itemSelected;
      _itemUniteDureeSelectedId = itemSelected;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Utilities.begin("FormAO/initState");
    Utilities.begin("widget.baseUrl ${widget.baseUrl}");

    initialize(baseUrl: widget.baseUrl);

    //initializeTest(url: widget.baseUrl) ;
    //initialize(baseUrl: widget.baseUrl) ;
  }

//*
  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    //initialize(baseUrl: widget.baseUrl) ;

    //_dmiPays.add(DropdownMenuItem(child: Text(user.libelle),  value: user,));
  }

  //*/
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Utilities.begin("FormAO/build");

    // initialiser le model
    if (widget.model == null) {
      widget.model = User();
    }

    return Form(
        autovalidate: _autovalidate,
        key: _formKey,
        child: ListView(children: <Widget>[
          Column(
            children: <Widget>[
              MyTextFormField(
                //expands: true,
                decoration: InputDecoration(
                  labelText: libelle,
                  //border: OutlineInputBorder(), pour la bordure dans le input. Remarque on doit ajouter un padding
                ),
                validator: (value) {
                  Utilities.validator(value, typeContentToValidate: v_text);
                },
                onSaved: (input) {
                  //itemAO.libelle = input;
                  widget.model.libelle = input;
                },
                initialValue:
                    Utilities.initialStringValueWidget(widget.model.libelle),
                enabled: isEnableWidget(),
              ),
              MyTextFormField(
                decoration: InputDecoration(
                  labelText: dateLimiteDepot,
                  //border: OutlineInputBorder(),
                ),
                validator: (value) {
                  Utilities.validator(value, typeContentToValidate: v_text);
                },

                readOnly: true,
                onSaved: (input) {
                  //itemAO.dateLimiteDepot = input;
                  widget.model.dateLimiteDepot = input;
                },
                onTap: () {
                  /*Utilities.showDatePickerCustom(
                    context: context,
                    controller: _controllerDate,
                  ).then((picked) => //setState(() {
                          //Utilities.log(picked);
                          _controllerDate.text =
                              Utilities.formatDate(date: picked)
                      //})
                      );
                      */
                  Utilities.showDatePickerCustom(
                    context: context,
                    controller: _controllerDate,
                  );

                  //if (_controllerDate != null) {
                  // Utilities.log(_controllerDate.text = _getFormatDate()) ;
                  //}
                },
                controller: _controllerDate,
                enabled: isEnableWidget(),
                //keyboardType: TextInputType.datetime,
              ),

              /*
            SearchableDropdown.single(
              label: Text(dateLimiteDepot),
              items: _dmiDomaines,
              value: selectedValue,
              hint: "Select one",
              searchHint: "Select one",
              onChanged: (value) {
                setState(() {
                  selectedValue = value;
                });
              },
              isExpanded: true,
            ),
            */

              MyTextFormField(
                decoration: InputDecoration(labelText: duree),
                validator: (value) {
                  Utilities.validator(value, typeContentToValidate: v_text);
                },
                onSaved: (input) {
                  //itemAO.nombreTypeDuree = int.parse(input);
                  widget.model.nombreTypeDuree = int.parse(input);
                },
                keyboardType: TextInputType.number,
                initialValue: Utilities.initialIntValueWidget(
                    widget.model.nombreTypeDuree),
                enabled: isEnableWidget(),
              ),
              DropdownButtonFormField(
                isDense: true,

                //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                items: _dmiUniteDuree,
                onChanged: onChangeDropdownItemUniteDuree,
                value: _itemUniteDureeSelectedId,
                //value: _itemUniteDureeSelected,
                //validator: (value) => Utilities.validatorObject(value, typeContentToValidate: v_text),
                decoration: InputDecoration(labelText: uniteDuree),
                //InputDecoration(labelText: uniteDuree, helperText: 'test'),
                onSaved: (input) {
                  //Utilities.log(input.libelle);
                  //itemAO.typeDureeId = input.id;
                  //widget.model.typeDureeId = input.id;
                  widget.model.typeDureeId = input;
                },
              ),

              /*
            DropdownButtonFormField(
              //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
              items: _dmiDomaines,
              onChanged: onChangeDropdownItemDomaine,
              value: _itemDomaineSelected,
              validator: (value) => Utilities.validatorObject(value,
                  typeContentToValidate: v_text),
              decoration:
                  InputDecoration(labelText: domaine),
                  //InputDecoration(labelText: domaine, helperText: domaine),
              onSaved: (input) {
                Utilities.log("_dmiDomaines");
                Utilities.log(input.code);
                widget.model.typeAppelDoffresCode = input.code;
              },
            ),
            */

              //MultiSelectFormField(dataSource: _dmiPays,),
              Divider(),

              DropdownSearch<User>(
                mode: Mode.BOTTOM_SHEET,
                showSelectedItem: true,
                compareFn: (User i, User s) => i.isEqual(s),
                label: LPays,
                onFind: (String filter) => getData(filter: filter),
                onChanged: (User data) {
                  print(data);
                  setState(() {
                    currentPays = data;
                    if (data == null) {
                      currentVille = null;
                    }
                  });
                },
                validator: (value) => Utilities.validatorObject(value),
                selectedItem: currentPays,
                showClearButton: true,
                showSearchBox: true,
                searchBoxDecoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                  labelText: searchHintPays,
                ),
                popupTitle: Container(
                  height: 50,
                  decoration: Utilities.getBoxDecoration(),
                  child: Center(
                    child: Text(
                      LPays,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                popupShape: Utilities.getRoundedRectangleBorder(),
                emptyBuilder: (context) => Utilities.dataNotFound(),
                //dropdownBuilder: _customDropDownExample,
                //popupItemBuilder: _customPopupItemBuilderExample2,
              ),
              Divider(),

              DropdownSearch<User>(
                mode: Mode.BOTTOM_SHEET,
                showSelectedItem: true,
                compareFn: (User i, User s) => i.isEqual(s),
                label: LVille,
                onFind: (String filter) => getData(
                    filter: (currentPays != null)
                        ? currentPays.id.toString()
                        : filter,
                    isVille: true),
                onChanged: (User data) {
                  //print(data);
                  currentVille = data;
                },
                onSaved: (value) {
                  if (value != null) {
                    widget.model.villeId = value.id;
                  }
                },
                validator: (value) => Utilities.validatorObject(value),
                selectedItem: currentVille,
                showClearButton: true,
                showSearchBox: true,
                searchBoxDecoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                  labelText: searchHintVille,
                ),
                popupTitle: Container(
                  height: 50,
                  decoration: Utilities.getBoxDecoration(),
                  child: Center(
                    child: Text(
                      LVille,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                popupShape: Utilities.getRoundedRectangleBorder(),
                emptyBuilder: (context) => Utilities.dataNotFound(),

                //dropdownBuilder: _customDropDownExample,
                //popupItemBuilder: _customPopupItemBuilderExample2,
              ),
              Divider(),

              (widget.mode == null || widget.mode != UPDATE)
                  ? MultiSelectFormField(
                      //autovalidate: true,
                      titleText: LDomaine,
                      //autovalidate: false,
                      okButtonLabel: okButtonLabel,
                      cancelButtonLabel: cancelButtonLabel,
                      hintText: hintMultiSelectForm,

                      //initialValue: _initialItemsDomaines,
                      dataSource: _itemsDomaines,
                      textField: LIBELLE,
                      valueField: LIBELLE,

                      close: () {
                        Utilities.log("close");
                        getFournisseursByDomaines(_itemsDomaines);
                      },
                      open: () {
                        Utilities.log("open");
                        Utilities.log("domainedomainedomainedomainedomaine");
                      },
                      change: () => Utilities.log("change") //;
                      // Utilities.log("domainedomainedomainedomainedomaine") ;
                      ,

                      validator: (value) {
                        //if (value == null && value.length == 0) {
                        if (value == null || value.length == 0) {
                          return messageChoixMultiSelectForm;
                        }
                        return null;
                      },
                      onSaved: (value) {
                        //setState(() { pas besoin de ca ici
                        //itemAO.datasSecteurDactiviteAppelDoffres = value;

                        List<User> itemsLocal = List();
                        for (var item in value) {
                          for (var itemDomaine in _itemsDomaines) {
                            if (itemDomaine[LIBELLE] == item) {
                              User user = User();
                              user.secteurDactiviteId = itemDomaine[id];
                              user.secteurDactiviteLibelle =
                                  itemDomaine[LIBELLE];
                              itemsLocal.add(user);
                              break;
                            }
                          }
                        }
                        widget.model.datasSecteurDactiviteAppelDoffres =
                            itemsLocal;
                        //});
                      },
                    )
                  : (Utilities.isNotEmpty(_initialItemsDomaines) &&
                          Utilities.isNotEmpty(_itemsDomaines))
                      ? MultiSelectFormField(
                          //autovalidate: true,
                          titleText: LDomaine + LVille,
                          //autovalidate: false,
                          okButtonLabel: okButtonLabel,
                          cancelButtonLabel: cancelButtonLabel,
                          hintText: hintMultiSelectForm,

                          initialValue: _initialItemsDomaines,
                          dataSource: _itemsDomaines,
                          textField: LIBELLE,
                          valueField: LIBELLE,

                          close: () {
                            Utilities.log("close");
                            getFournisseursByDomaines(_itemsDomaines);
                          },
                          open: () {
                            Utilities.log("open");
                            Utilities.log(
                                "domainedomainedomainedomainedomaine");
                          },
                          change: () => Utilities.log("change") //;
                          // Utilities.log("domainedomainedomainedomainedomaine") ;
                          ,

                          validator: (value) {
                            //if (value == null && value.length == 0) {
                            if (value == null || value.length == 0) {
                              return messageChoixMultiSelectForm;
                            }
                            return null;
                          },
                          onSaved: (value) {
                            //setState(() { pas besoin de ca ici
                            //itemAO.datasSecteurDactiviteAppelDoffres = value;

                            List<User> itemsLocal = List();
                            for (var item in value) {
                              for (var itemDomaine in _itemsDomaines) {
                                if (itemDomaine[LIBELLE] == item) {
                                  User user = User();
                                  user.secteurDactiviteId = itemDomaine[id];
                                  user.secteurDactiviteLibelle =
                                      itemDomaine[LIBELLE];
                                  itemsLocal.add(user);
                                  break;
                                }
                              }
                            }
                            widget.model.datasSecteurDactiviteAppelDoffres =
                                itemsLocal;
                            //});
                          },
                        )
                      : Container(),

              DropdownButtonFormField(
                isDense: true,

                //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                items: _dmiTypeAO,
                onChanged: onChangeDropdownItemTypeAO,
                value: (widget.model != null)
                    ? widget.model.typeAppelDoffresLibelle
                    : null,
                //value: _itemTypeAOSelected,
                decoration: InputDecoration(labelText: typeAO),
                //InputDecoration(labelText: typeAO, helperText: 'test'),
                onSaved: (input) {
                  //itemAO.typeAppelDoffresLibelle = input.libelle;
                  widget.model.typeAppelDoffresLibelle = input;
                  //widget.model.typeAppelDoffresLibelle = input.libelle;
                },
                validator: (value) => Utilities.validatorObject(value,
                    typeContentToValidate: v_text),
              ),
              (_isVisibleFournisseur)
                  ? MultiSelectFormField(
                      titleText: choisirFournisseur,
                      //autovalidate: false,
                      okButtonLabel: okButtonLabel,
                      cancelButtonLabel: cancelButtonLabel,
                      hintText: hintMultiSelectForm,

                      dataSource: _itemsFournisseurs,
                      textField: LIBELLE,
                      valueField: LIBELLE,

                      validator: (value) {
                        //if (value == null && value.length == 0) {
                        if (value == null || value.length == 0) {
                          return messageChoixMultiSelectForm;
                        }
                        return null;
                      },
                      onSaved: (value) {
                        setState(() {
                          //itemAO.datasEntreprise = value;
                          widget.model.datasEntreprise = value;
                        });
                      },
                    )
                  : Container(),

              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: MyTextFormField(
                  maxLines: maxLines,
                  decoration: InputDecoration(
                    labelText: description,
                    border:
                        OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                  ),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    //itemAO.description = input;
                    widget.model.description = input;
                  },
                  keyboardType: TextInputType.multiline,
                  initialValue: Utilities.initialStringValueWidget(
                      widget.model.description),
                  enabled: isEnableWidget(),
                ),
              ),

              //MultiSelect
              (widget.mode == null || widget.mode != UPDATE)
                  ? MultiSelectFormField(
                      titleText: ajouterCritere,
                      //autovalidate: false,
                      okButtonLabel: okButtonLabel,
                      cancelButtonLabel: cancelButtonLabel,
                      hintText: hintMultiSelectForm,

                      //initialValue: _initialItemsCriteres,
                      dataSource: _itemsCriteres,
                      textField: LIBELLE,
                      valueField: LIBELLE,

                      validator: (value) {
                        //if (value == null && value.length == 0) {
                        if (value == null || value.length == 0) {
                          return messageChoixMultiSelectForm;
                        }
                        return null;
                      },
                      change: () => Utilities.log("change/ajouterCritere") //;
                      // Utilities.log("domainedomainedomainedomainedomaine") ;
                      ,

                      onSaved: (value) {
                        //setState(() { Pas vraiment besoin
                        //itemAO.datasSecteurDactiviteAppelDoffres = value;

                        List<User> itemsLocal = List();
                        for (var item in value) {
                          for (var itemCritere in _itemsCriteres) {
                            if (itemCritere[LIBELLE] == item) {
                              User user = User();
                              user.critereId = itemCritere[id];
                              user.critereAppelDoffresLibelle =
                                  itemCritere[LIBELLE];
                              itemsLocal.add(user);
                              break;
                            }
                          }
                        }
                        widget.model.datasValeurCritereAppelDoffres =
                            itemsLocal;
                        //});
                      },
                    )
                  : (Utilities.isNotEmpty(_initialItemsCriteres) &&
                          Utilities.isNotEmpty(_itemsCriteres))
                      ? MultiSelectFormField(
                          titleText: ajouterCritere,
                          //autovalidate: false,
                          okButtonLabel: okButtonLabel,
                          cancelButtonLabel: cancelButtonLabel,
                          hintText: hintMultiSelectForm,

                          initialValue: _initialItemsCriteres,
                          dataSource: _itemsCriteres,
                          textField: LIBELLE,
                          valueField: LIBELLE,

                          validator: (value) {
                            //if (value == null && value.length == 0) {
                            if (value == null || value.length == 0) {
                              return messageChoixMultiSelectForm;
                            }
                            return null;
                          },
                          change: (value) =>
                              Utilities.log("change/ajouterCritere  $value") //;
                          // Utilities.log("domainedomainedomainedomainedomaine") ;
                          ,

                          onSaved: (value) {
                            //setState(() { Pas vraiment besoin
                            //itemAO.datasSecteurDactiviteAppelDoffres = value;

                            List<User> itemsLocal = List();
                            for (var item in value) {
                              for (var itemCritere in _itemsCriteres) {
                                if (itemCritere[LIBELLE] == item) {
                                  User user = User();
                                  user.critereId = itemCritere[id];
                                  user.critereAppelDoffresLibelle =
                                      itemCritere[LIBELLE];
                                  itemsLocal.add(user);
                                  break;
                                }
                              }
                            }
                            widget.model.datasValeurCritereAppelDoffres =
                                itemsLocal;
                            //});
                          },
                        )
                      : Container(),
              // ajouter des fichiers
              EditFile(
                listFile: listFileWithContent,
                model: itemAO,
                tets: tets,
              ),

              Divider(),
              // les boutons de validation du formulaire de l'OF
              Row(
                children: <Widget>[
                  Utilities.getButtonSubmit(
                      child: Text(enregistrer),
                      onPressed: () {
                        submit(etatCode: BROUILLON, action: create);
                      },
                      isDisable: _isDisable),
                  Spacer(),
                  Utilities.getButtonSubmit(
                      child:
                          Text((widget.model != null && widget.model.id != null
                              //child: Text((itemAO != null && itemAO.id != null
                              ? modifier
                              : creer)),
                      onPressed: () =>
                          (widget.model != null && widget.model.id != null)
                              //(itemAO != null && itemAO.id != null
                              ? submit(etatCode: ENTRANT, action: update)
                              : submit(etatCode: ENTRANT, action: create),
                      isDisable: _isDisable),
                  /*
                  RaisedButton(
                    child: Text(enregistrer),
                    onPressed: () {
                      submit(etatCode: BROUILLON, action: create);
                    },
                  ),
                  Spacer(),
                  RaisedButton(
                    child: Text((widget.model != null && widget.model.id != null
                        //child: Text((itemAO != null && itemAO.id != null
                        ? modifier
                        : creer)),
                    onPressed: () =>
                        (widget.model != null && widget.model.id != null)
                            //(itemAO != null && itemAO.id != null
                            ? submit(etatCode: ENTRANT, action: update)
                            : submit(etatCode: ENTRANT, action: create),
                  )
                  */
                ],
              ),

              /*
            DropdownButtonFormField(
              //isExpanded: false ,
              itemHeight: kMinInteractiveDimension,
              icon: Icon(Icons.ac_unit),
              iconDisabledColor: ConstApp.color_app,
              iconEnabledColor: ConstApp.color_app_rouge,
              //style: TextStyle(backgroundColor: ConstApp.color_app_gris),
              //iconSize: 12,
              items: _dmiPays,
              onChanged: onChangeDropdownItemPays,
              value: _itemPaysSelected,
              validator: (value) => Utilities.validatorObject(value,
                  typeContentToValidate: v_text),
              decoration: InputDecoration(
                labelText: pays,
                //helperText: 'test'
              ),
            ),

            DropdownButtonFormField(
              //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
              items: _dmiVille,
              onChanged: onChangeDropdownItemVille,
              value: _itemVilleSelected,
              validator: (value) => Utilities.validatorObject(value,
                  typeContentToValidate: v_text),
              decoration: InputDecoration(labelText: LVille),
              //InputDecoration(labelText: LVille, helperText: 'test'),
              onSaved: (input) {
                Utilities.log("_dmiTypeAO");
                Utilities.log(input.code);
                widget.model.villeId = input.id;
              },
            ),

          DropdownButtonFormField(
            //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
            isExpanded: false ,
            items: _dmiDomaines,
            //onChanged: onChangeDropdownItem,
            value: _itemDomaineSelected,
            validator:(value) => Utilities.validatorObject(value, typeContentToValidate: v_text),
            decoration: InputDecoration(
              labelText: domaine,
              //helperText: 'test'
            ),
          ),
          
          
          DropdownButtonFormField(
            //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
            isExpanded: false ,
            items: _dmiCriteres,
            //onChanged: onChangeDropdownItem,
            value: _itemCriteresSelected,
            validator:(value) => Utilities.validatorObject(value, typeContentToValidate: v_text),
            decoration: InputDecoration(
              labelText: ajouterCritere,
              //helperText: 'test'
            ),
          ),
          
          DropdownButtonFormField(
            //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
            isExpanded: false ,
            items: _dmiVille,
            onChanged: onChangeDropdownItemVille,
            value: _itemPaysSelected,
            validator:(value) => Utilities.validatorObject(value, typeContentToValidate: v_text),
            decoration: InputDecoration(
              labelText: ville,
              //helperText: 'test'
            ),
          ),
          DropdownButtonFormField(
            //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
            isExpanded: false ,
            items: _dmiTypeAO,
            onChanged: onChangeDropdownItemTypeAO,
            value: _itemTypeAOSelected,
            validator:(value) => Utilities.validatorObject(value, typeContentToValidate: v_text),
            decoration: InputDecoration(
              labelText: typeAO,
              //helperText: 'test'
            ),
          ),
          DropdownButtonFormField(
            //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
            hint: Text(choisirFournisseur),
            isExpanded: false ,
            items: _dmiTypeAO,
            onChanged: onChangeDropdownItemTypeAO,
            value: _itemTypeAOSelected,
            validator:(value) => Utilities.validatorObject(value, typeContentToValidate: v_text),
            decoration: InputDecoration(
              //labelText: choisirFournisseur,
              helperText: choisirFournisseur
            ),
          ),
          //
          */
            ],
          )
        ]));
  }

  // recuperation des fournisseurs selon les domaines pour une AO RESTREINT
  getFournisseursByDomaines(List<User> datasDomaines) {
    List<dynamic> datas = List();
    List<dynamic> datasLocal = List();
    User data = User();

    if (Utilities.isNotEmpty(datasDomaines)) {
      datasDomaines.forEach((domaine) {
        User dto = User();
        dto.typeEntrepriseCode = CLIENTE;
        dto.typeEntrepriseCodeParam = operatorDifferent;
        dto.secteurDactiviteLibelle = domaine.libelle;
        datasLocal.add(dto);
      });
      data = datasLocal[0];
      datas = Utilities.getElementInList(list: datasLocal, firstIndex: 1);

      //let  datas = {roleCode :"", typeEntrepriseCode:environment.MIXTE};
      //let isAnd = false;

      RequestCustom request = RequestCustom();
      request.isAnd = false;
      request.user = widget.model.userClientId;
      if (Utilities.isNotEmpty(datas)) {
        request.datas = datas;
      }

      baseApi
          .testApi(
              apiBaseUrl: widget.baseUrl +
                  secteurDactiviteEntreprise +
                  getByCriteriaCustom,
              request: request)
          .then((res) {
        if (res != null && !res.hasError) {
          //Utilities.messageApi(response: res);
          // recuperation des fournisseurs

          if (Utilities.isNotEmpty(res.items)) {
            _itemsFournisseurs = res.items;
          } else {
            _itemsFournisseurs.clear();
          }
        } else {
          Utilities.messageApi(response: res); // erreur avec le service
        }
      }).catchError((err) => {
                // erreur de connexion
              });
    } else {
      // vider la liste et la selection
      _itemsFournisseurs.clear();
    }
  }

  isEnableWidget() {
    return true;
  }

  submit({String etatCode, String action}) {
    Utilities.begin("submit");
    if (_formKey.currentState.validate()) {
      Utilities.progressingAfterSubmit(context: context);
      // disableButton
      setState(() {
        _isDisable = true;
      });

      _formKey.currentState.save();

      // preciser le userClientId. Il correspond au user connecté
      //widget.model.userClientId = 26;
      //widget.model.villeId = _itemVilleSelected.id;

      widget.model.etatCode = etatCode;

      // ajout des fichiers
      widget.model.datasFichier = Utilities.formationDatasFiles(
          listFileWithContent: listFileWithContent, isFichierOffre: false);

      Utilities.log("nbre de fichier ${widget.model.datasFichier.length}");

      RequestCustom request = RequestCustom();
      List<User> datas = List();
      datas.add(widget.model);
      request.user = widget.model.userClientId;
      request.datas = datas;
      baseApi
          .testApi(
              apiBaseUrl: widget.baseUrl + appelDoffres + action,
              request: request)
          .then((res) {
        Navigator.pop(context); // pour retirer le progressing
        if (res != null && !res.hasError) {
          Utilities.getToast(succes);
          Navigator.pop(context); // TODO : a revoir
        } else {
          Utilities.messageApi(response: res); // erreur avec le service
        }
        // enableButton
        setState(() {
          _isDisable = false;
        });
      }).catchError((err) {
        // erreur de connexion
        // enableButton
        setState(() {
          _isDisable = false;
        });
      });

      Utilities.end("submit");
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }

  Future<List<User>> getData({String filter, bool isVille = false}) async {
    print("getData filter $filter");
    print("getData isVille $isVille");
    RequestCustom request = RequestCustom();
    User data = User();
    List<User> models;
    String endpoint = endpointGetPays;

    if (Utilities.isNotBlank(filter)) {
      data.paysId = int.parse(filter);
      endpoint = ville + getByCriteria;
    } else {
      if (isVille) {
        return models;
      }
    }

    request.data = data;
    var res = await baseApi.testApi(
        apiBaseUrl: widget.baseUrl + endpoint, request: request);

    if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
      models = User.fromJsons(res.items);
    }
    return models;
  }

  // initialisation des datas
  initialize({String baseUrl}) {
    Utilities.begin("initialize");

    // gestion des cas de mise à jour

    if (Utilities.isNotBlank(widget.mode) && widget.mode == UPDATE) {
      currentPays = User();
      currentVille = User();
      currentPays.id = widget.model.paysId;
      currentPays.libelle = widget.model.paysLibelle;
      currentVille.libelle = widget.model.villeLibelle;
      currentVille.id = widget.model.villeId;

      _controllerDate.text = widget.model.dateLimiteDepot;
      _itemUniteDureeSelectedId = widget.model.typeDureeId;

      // recuperation de l'AO
      RequestCustom requestAO = RequestCustom();
      User dataAO = User();
      dataAO.id = widget.model.id;

      requestAO.data = dataAO;
      baseApi
          .testApi(
              apiBaseUrl: baseUrl + appelDoffres + getByCriteria,
              request: requestAO)
          .then((res) {
        print(res);
        //setState(() { Pas vraiment besoin
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          dataAO = User.fromJson(res.items[0]);

          //MAJ du widget des fichiers
          if (Utilities.isNotEmpty(dataAO.datasFichier)) {
            Map<String, dynamic> tampon = Map();
            //Map<String, String> tampon = Map();
            for (var item in dataAO.datasFichier) {
              //tampon[item[name]] = item[fichierBase64];
              //tampon[item[name]] = item[urlFichier];
              tampon["${item[name]}.${item[libelleExtension]}"] =
                  item[urlFichier];
            }
            setState(() {
              listFileWithContent.addAll(tampon);
            });

            // recuperation des base64 des files
            RequestCustom requestFile = RequestCustom();
            requestFile.datas = dataAO.datasFichier;
            baseApi
                .testApi(
                    apiBaseUrl: baseUrl + fichier + getBase64Files,
                    request: requestFile)
                .then((res) {
              if (res != null &&
                  !res.hasError &&
                  Utilities.isNotEmpty(res.items)) {
                Map<String, dynamic> tamponLocal = Map();
                for (var item in res.items) {
                  //tamponLocal[item[name]] = item[fichierBase64];
                  tampon["${item[name]}.${item[libelleExtension]}"] =
                      item[fichierBase64];
                }
                //setState(() {
                listFileWithContent.clear();
                listFileWithContent.addAll(tamponLocal);
                //});
              }
            }).catchError((err) {
              print(err);
            });
          }

          //MAJ du widget des secteurs d'activite
          if (Utilities.isNotEmpty(dataAO.datasSecteurDactiviteAppelDoffres)) {
            List<dynamic> initialItemsDomaines = List();
            for (var item in dataAO.datasSecteurDactiviteAppelDoffres) {
              initialItemsDomaines.add(item[secteurDactiviteLibelle]);
            }
            setState(() {
              _initialItemsDomaines = initialItemsDomaines;
            });
          }
          //MAJ du widget des criteres
          if (Utilities.isNotEmpty(dataAO.datasValeurCritereAppelDoffres)) {
            List<dynamic> initialItemsCriteres = List();
            for (var item in dataAO.datasValeurCritereAppelDoffres) {
              initialItemsCriteres.add(item[critereAppelDoffresLibelle]);
            }
            setState(() {
              _initialItemsCriteres = initialItemsCriteres;
            });
            for (var item in _initialItemsCriteres) {
              Utilities.log("_initialItemsCriteres $item");
            }
          }
        }
        //});
      }).catchError((err) {
        print(err);
      });
    }

    RequestCustom request = RequestCustom();
    //String uri = apiBaseUrl + endpointObjetNousContacter;

    //String uri = base_url_prod + endpointObjetNousContacter;
    //String baseUrl = AppConfig.of(context).apiBaseUrl ;
    request.data = User();
    baseApi
        .testApi(
            apiBaseUrl: baseUrl + endpointGetTypeAppelDoffres, request: request)
        .then((res) {
      print(res);
      //setState(() {
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        List<DropdownMenuItem<dynamic>> dmiTypeAO = List();

        for (var element in res.items) {
          User user = User.fromJson(element);

          dmiTypeAO.add(DropdownMenuItem(
            child: Text(user.libelle),
            //value: user,
            //value: user.id,
            value: user.libelle,
          ));
        }
        setState(() {
          //_dmiTypeAO.addAll(dmiTypeAO) ;
          _dmiTypeAO = dmiTypeAO;
        });
      }
      //});
    }).catchError((err) {
      print(err);
    });

    baseApi
        .testApi(
            apiBaseUrl: baseUrl + secteurDactivite + getByCriteria,
            request: request)
        .then((res) {
      print(res);
      setState(() {
        //Pas vraiment besoin
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          _itemsDomaines = res.items;
          /*
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dmiDomaines.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
              //value: user.libelle,
            ));
          }
          */
        }
      });
    }).catchError((err) {
      print(err);
    });

    baseApi
        .testApi(
            apiBaseUrl: baseUrl + typeDuree + getByCriteria, request: request)
        .then((res) {
      print(res);
      //setState(() { Pas vraiment besoin
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        List<DropdownMenuItem<dynamic>> dmiUniteDuree = List();

        for (var element in res.items) {
          User user = User.fromJson(element);
          dmiUniteDuree.add(DropdownMenuItem(
            child: Text(user.libelle),
            //value: user,
            value: user.id,
          ));
        }
        setState(() {
          //_dmiTypeAO.addAll(dmiTypeAO) ;
          _dmiUniteDuree = dmiUniteDuree;
        });
      }

      //});
    }).catchError((err) {
      print(err);
    });

    baseApi
        .testApi(
            apiBaseUrl: baseUrl + endpointGetCritereAppelDoffres,
            request: request)
        .then((res) {
      print(res);
      //setState(() { Pas vraiment besoin
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        _itemsCriteres = res.items;
        for (var item in _itemsCriteres) {
          Utilities.log("_itemsCriteres ${item[LIBELLE]}");
        }
        /*
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dmiCriteres.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }*/
      }
      //});
    }).catchError((err) {
      print(err);
    });

    /*
    baseApi
        .testApi(apiBaseUrl: baseUrl + endpointGetPays, request: request)
        .then((res) {
      print(res);
      setState(() {
        //List<DropdownMenuItem<dynamic>> dmiPaysLocal = List();

        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dmiPays.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
              //value: user,
            ));
          }

          //_dmiPays = dmiPaysLocal ;
        }
      });
    }).catchError((err) {
      print(err);
    });
    */
  }
}
