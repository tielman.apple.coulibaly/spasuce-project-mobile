import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/edit_ao.dart';
import 'package:spasuce/views/voir_ao.dart';

class CardAO extends StatefulWidget {
  User model;
  //static const User defaultValue = User (libelle: "test",nombreOffre: 10, code: "20201343AO00012", dateLimiteDepotAppelDoffres :"02/12/2020",dateCreationAppelDoffres :"12/12/2020") ;

  CardAO({this.model});

  @override
  _CardAOState createState() => _CardAOState();
}

class _CardAOState extends State<CardAO> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    bool isContenuNotificationDispo =
        (widget.model.isContenuNotificationDispo != null &&
            widget.model.isContenuNotificationDispo);
    TextStyle textStyle = TextStyle();

    if (isContenuNotificationDispo) {
      textStyle = TextStyle(fontWeight: FontWeight.bold);
    }

    return Container(
      child: Card(
        color: isContenuNotificationDispo
            ? ConstApp.color_app_gris_degrade_1
            : ConstApp.color_app_gris_degrade,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Wrap(
                children: <Widget>[
                  Text(widget.model.libelle,
                      //maxLines: 1,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: fontSizeCustom)),
                  //Text("test", style: TextStyle(fontWeight: FontWeight.bold, fontSize: fontSizeCustom),),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    Utilities.initialStringValueWidget(widget.model.code,
                        defaultValue: aucunCode),
                    style: textStyle,
                  ),
                  //Text("20201343AO00012"),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    "${widget.model.nombreOffre}",
                    style: textStyle,
                  ),
                  //Text("10"),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    "${widget.model.dateCreation} - ${widget.model.dateLimiteDepot}",
                    style: textStyle,
                  ),
                  //Text("02/12/2020 - 12/12/2020"),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    (widget.model.isSelectedByClient != null &&
                            widget.model.isSelectedByClient)
                        ? etatConclu
                        : etatEnCours,
                    style: textStyle,
                  ),
                  //Text(etatEnCours),
                ],
              ),
              Row(
                children: <Widget>[
                  // les actions liées aux AO
                  Spacer(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      child: Icon(Icons.visibility),
                      onTap: () => onTapMethod(
                          context: context,
                          isContenuNotificationDispo: false,
                          widget: VoirAO(
                              appelDoffres: widget.model,
                              baseUrl: AppConfig.of(context).apiBaseUrl)),
                      /* 
                      {
                        Utilities.navigate(
                            context,
                            VoirAO(
                                appelDoffres: widget.model, baseUrl: baseUrl));
                      },*/
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      child: Icon(Icons.visibility_off),
                      onTap: () => onTapMethod(
                        context: context,
                        isContenuNotificationDispo: true,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      child: Icon(Icons.edit),
                      onTap: () {
                        Utilities.navigate(
                            context,
                            EditAO(
                              data: widget.model,
                              mode: UPDATE,
                              userClientId: widget.model.userClientId,
                            ));
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      child: Icon(Icons.delete),
                      onTap: () {
                        Utilities.getToast(messageDev);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  onTapMethod(
      {BuildContext context, bool isContenuNotificationDispo, Widget widget}) {
    RequestCustom request = RequestCustom();
    this.widget.model.isContenuNotificationDispo = isContenuNotificationDispo;

    request.datas = [this.widget.model];
    request.user = this.widget.model.userClientId;
    baseApi
        .testApi(
            apiBaseUrl: AppConfig.of(context).apiBaseUrl +
                appelDoffres +
                updateContenuNotificationDispo,
            request: request)
        .then((res) {
      print(res);
      if (res != null && !res.hasError) {
        //_itemsDomaines = res.items;
        if (widget != null) {
          Utilities.navigate(context, widget); //TODO : Voir AppelDoffres
        }
        setState(
            () {}); // pour recharger la page afin de prendre en compte la modif
      } else {
        Utilities.getToast(echec);
      }
    }).catchError((err) {
      print(err);
    });
  }
}
