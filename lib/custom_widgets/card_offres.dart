import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/edit_ao.dart';
import 'package:spasuce/views/edit_offres.dart';

class CardOffres extends StatefulWidget {
  User model;
  //static const User defaultValue = User (libelle: "test",nombreOffre: 10, code: "20201343AO00012", dateLimiteDepotAppelDoffres :"02/12/2020",dateCreationAppelDoffres :"12/12/2020") ;

  CardOffres({this.model});

  @override
  _CardOffresState createState() => _CardOffresState();
}

class _CardOffresState extends State<CardOffres> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    TextStyle textStyle = TextStyle();
    bool isAppelRestreint = (widget.model.isAppelRestreint != null &&
        widget.model.isAppelRestreint);
    bool isNotVisualiserContenuByFournisseur =
        (widget.model.isVisualiserContenuByFournisseur != null &&
            !widget.model.isVisualiserContenuByFournisseur);
    if (isAppelRestreint && isNotVisualiserContenuByFournisseur) {
      textStyle = TextStyle(
          color: ConstApp.color_app_rouge, fontWeight: FontWeight.bold);
    } else {
      if (isAppelRestreint) {
        textStyle = TextStyle(color: ConstApp.color_app_rouge);
      } else {
        if (isNotVisualiserContenuByFournisseur) {
          textStyle = TextStyle(fontWeight: FontWeight.bold);
        }
      }
    }

    return Container(
      child: Card(
        color: isNotVisualiserContenuByFournisseur
            ? ConstApp.color_app_gris_degrade_1
            : ConstApp.color_app_gris_degrade,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Wrap(
                children: <Widget>[
                  Text(widget.model.appelDoffresLibelle,
                      //maxLines: 1,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: fontSizeCustom,
                          color: (isAppelRestreint)
                              ? ConstApp.color_app_rouge
                              : null)),
                  //Text("test", style: TextStyle(fontWeight: FontWeight.bold, fontSize: fontSizeCustom),),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    widget.model.villeAppelDoffres,
                    style: textStyle,
                  ),
                  //Text("20201343AO00012"),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    "${widget.model.nombreOffre}",
                    style: textStyle,
                  ),
                  //Text("10"),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    "${widget.model.dateCreationAppelDoffres} - ${widget.model.dateLimiteDepotAppelDoffres}",
                    style: textStyle,
                  ),
                  //Text("02/12/2020 - 12/12/2020"),
                ],
              ),
              Row(
                children: <Widget>[
                  Text(
                    (widget.model.isSelectedByClient != null &&
                            widget.model.isSelectedByClient)
                        ? etatConclu
                        : etatEnCours,
                    style: textStyle,
                  ),
                  //Text(etatEnCours),
                ],
              ),
              Row(
                children: <Widget>[
                  // les actions liées aux AO
                  Spacer(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      child: Icon(
                        Icons.visibility,
                      ),
                      onTap: () => onTapMethod(
                          context: context,
                          isVisualiserContenuByFournisseur: true,
                          widget: EditOffres(
                            title: ConstApp.show_oa,
                            data: widget.model,
                            userFournisseurId: widget.model.userFournisseurId,
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      child: Icon(Icons.visibility_off),
                      onTap: () => onTapMethod(
                          context: context,
                          isVisualiserContenuByFournisseur: false),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  onTapMethod(
      {BuildContext context,
      bool isVisualiserContenuByFournisseur,
      Widget widget}) {
    RequestCustom request = RequestCustom();
    //User data = User();
    this.widget.model.isVisualiserContenuByFournisseur =
        isVisualiserContenuByFournisseur;
    request.datas = [this.widget.model];
    request.user = this.widget.model.userFournisseurId;
    baseApi
        .testApi(
            apiBaseUrl: AppConfig.of(context).apiBaseUrl +
                appelDoffresRestreint +
                update,
            request: request)
        .then((res) {
      print(res);
      if (res != null && !res.hasError) {
        //_itemsDomaines = res.items;

        if (widget != null) {
          Utilities.navigate(context, widget); //TODO : Voir AppelDoffres
        }
        setState(
            () {}); // pour recharger la page afin de prendre en compte la modif
      } else {
        Utilities.getToast(echec);
      }
    }).catchError((err) {
      print(err);
    });
  }
}
