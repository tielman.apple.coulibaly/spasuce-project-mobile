/*import 'dart:convert';
import 'dart:io' as Io;

import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/user.dart';


class EditFileCopy extends StatefulWidget {


  String tets ;
  User model ;
  Map<String, String> listFile ;
  

  @override
  State<StatefulWidget> createState() {
    return _EditFile();
  }

  EditFile({this.tets, this.model, this.listFile}) ;
}

class _EditFile extends State<EditFile> {
  String _fileName;
  String _path;
  Map<String, String> _paths;
  String _extension;
  bool _loadingPath = false;
  //bool _multiPick = false;
  bool _multiPick = true;
  bool _hasValidMime = false;
  FileType _pickingType = FileType.custom;
  TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    //_controller.addListener(() => _extension = _controller.text);
      
    //widget.model.datasDomaine = List();
  }

  void _openFileExplorer() async {
    //if (_pickingType != FileType.custom || _hasValidMime) {
    setState(() => _loadingPath = true);
    try {
      if (_multiPick) {
        _path = null;
        _paths = await FilePicker.getMultiFilePath(
            //type: _pickingType, allowedExtensions: [_extension]);
            type: _pickingType,
            allowedExtensions: ['pdf', 'svg', 'jpg']);
      } else {
        _paths = null;
        _path = await FilePicker.getFilePath(
            //type: _pickingType, fileExtension: _extension);
            //type: _pickingType, allowedExtensions: [_extension]);
            type: _pickingType,
            allowedExtensions: ['pdf', 'svg', 'jpg']);
      }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }
    if (!mounted) return;
    setState(() {
      _loadingPath = false;
      _fileName = _path != null
          ? _path.split('/').last
          : _paths != null ? _paths.keys.toString() : '...';
    });
    //}
  }

  @override
  Widget build(BuildContext context) {

    List<dynamic> listFileWithContent = List();

    bool varBolAnd = (_paths != null && _paths.length > 0) && (widget.listFile != null && widget.listFile.length > 0) ;
    bool varBolOr = (_paths != null && _paths.length > 0) || (widget.listFile != null && widget.listFile.length > 0) ;

    // construire le data final
    if (varBolAnd) {
      Map<String, String> tampon = Map() ;
      tampon.addAll(widget.listFile) ;
      widget.listFile.clear() ;
      widget.listFile.addAll(_paths) ;
      widget.listFile.addAll(tampon) ;
    }else{
      if (_paths != null && _paths.length > 0) {
        widget.listFile.addAll(_paths) ;
      }
    }

    return Container(
          child: Center(
              child: Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.only(top: 50.0, bottom: 20.0),
                child: new RaisedButton(
                  onPressed: () => _openFileExplorer(),
                  child: new Text(ajouterFichier),
                ),
              ),
              new Builder(
                builder: (BuildContext context) => _loadingPath
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: const CircularProgressIndicator())
                    : _path != null || (_paths != null && _paths.length > 0) || (widget.listFile != null && widget.listFile.length > 0)
                        ? 
                        new Container(
                            padding: const EdgeInsets.only(bottom: 30.0),
                            height: MediaQuery.of(context).size.height * 0.25,
                            child: new Scrollbar(
                                child: new ListView.separated(
                              //itemCount: _paths != null && _paths.isNotEmpty ? _paths.length: 0,
                              itemCount: varBolAnd ? widget.listFile.length + _paths.length : (widget.listFile != null && widget.listFile.length > 0) ? widget.listFile.length : (_paths != null && _paths.isNotEmpty) ? _paths.length: 0,
                              itemBuilder: (BuildContext context, int index) {
                                


                                //final bool isMultiPath = _paths != null && _paths.isNotEmpty;
                                final bool isMultiPath = varBolOr ;
                                final String name = 'File $index: ' +
                                    (isMultiPath
                                        ? _paths.keys.toList()[index]
                                        : _fileName ?? '...');
                                final path = isMultiPath
                                    ? _paths.values.toList()[index].toString()
                                    : _path;


                                //Map<String, String> currentFile = {name:Utilities.fileInBase64(path)} ;
                                widget.listFile.addAll({name:Utilities.fileInBase64(path)}) ;
                                //widget.listFile.add(name:Utilities.fileInBase64(path)) ;

                                //fileInBase64(path) ;

                                return new ListTile(
                                  onTap: (){

                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        // return object of type Dialog
                                        return AlertDialog(
                                          title: new Text(supprimerFichier),
                                          content: new Text(questionSupprimerFichier),
                                          actions: <Widget>[
                                            // usually buttons at the bottom of the dialog
                                            new FlatButton(
                                              child: new Text(reponseNon),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                                //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                                              },
                                            ),new FlatButton(
                                              child: new Text(reponseOui),
                                              onPressed: () {
                                                // le supprimer dans la liste 
                                                setState(() {
                                                  print(_paths.keys.toList()[index]);
                                                  _paths.remove(_paths.keys.toList()[index]) ;
                                                });
                                                Navigator.of(context).pop();

                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                  title: new Text(
                                    name,
                                  ),
                                  subtitle: new Text(path),

                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      new Divider(),
                            )),
                          )
                        : new Container(),
              ),
            ],
          )),
        );
  }

  // convert file in base64
  static base64InFile(String base64, String pathFile){

    /*
    try{

    }catch(Exception e){

    }

     */
  final decodedBytes = base64Decode(base64);

  var file = Io.File(pathFile);
  file.writeAsBytesSync(decodedBytes);
}

// convert base64 in file
  static String fileInBase64(String pathFile){

    print("pathFile: $pathFile") ;
    final bytes = Io.File(pathFile).readAsBytesSync();

    String img64 = base64Encode(bytes);
    //print(img64.substring(0, 100));

    print("img64: $img64") ;

    return img64 ;
  }
}
*/