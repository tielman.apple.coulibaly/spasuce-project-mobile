import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';

class CarouselCustom extends StatefulWidget{
  List<dynamic> images ;

  @override
  _CarouselCustomState createState() => _CarouselCustomState();


  static const defaultImage = [ AssetImage("images/mecanisme.jpg") ] ;
  CarouselCustom({this.images = defaultImage});


}

class _CarouselCustomState extends State<CarouselCustom> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 200.0,
      child: Carousel(
        boxFit: BoxFit.cover,
        images: widget.images,
        autoplay: true,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(microseconds: 1000),
        dotSize: 4.0, // la taille des icones de defilement pas defaut
        dotColor: ConstApp.color_app_rouge, // couleur des icones
        indicatorBgPadding: 2.0, // la hauteur de la bande

      ),

    );
  }
}