import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/ressources/config_app.dart';

class NetworkCalls {

  Future<String> post({RequestCustom bodyRequest, String uri}) async {
    Map<String, String> headers = {"Content-type": "application/json"};
    Utilities.log("jsonEncode bodyRequest ${json.encode(bodyRequest)}");
    //client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);



    var rep = await client.post(uri, body: json.encode(bodyRequest), headers: headers);
    //var rep = await client.post(uri, body: bodyRequest.toJson(), headers: headers) ;
    //var rep = await client.post(uri, body: bodyRequest) ;
    checkAndThrowError(rep);
    return rep.body;
  }

  Future<String> postByHttpClient({RequestCustom bodyRequest, String uri}) async {
    Map<String, String> headers = {"Content-type": "application/json"};
    Utilities.log("jsonEncode bodyRequest ${json.encode(bodyRequest)}");
    //client.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);



    httpClient.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);


    var rep = await client.post(uri, body: json.encode(bodyRequest), headers: headers);
    //var rep = await client.post(uri, body: bodyRequest.toJson(), headers: headers) ;
    //var rep = await client.post(uri, body: bodyRequest) ;
    checkAndThrowError(rep);
    return rep.body;
  }



  dynamic postCustom({RequestCustom bodyRequest, String uri}){
    Map<String, String> headers = {"Content-type": "application/json"};

    client.post(uri, body: json.encode(bodyRequest), headers: headers).then((rep) {
      checkAndThrowError(rep);
      return rep.body;
    }).catchError((err) {
      print(err) ;
    });
  }

  static void checkAndThrowError(Response response) {
    if (response.statusCode != HttpStatus.ok) throw Exception(response.body);
  }
}
