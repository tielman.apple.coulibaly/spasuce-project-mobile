import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';

class MainAppBar extends StatefulWidget implements PreferredSizeWidget {
  // class MainAppBar extends StatefulWidget implements PreferredSizeWidget{$
  final Color backgroundColor;
  final Text title;
  final double elevation;

  final AppBar appBar;
  final List<Widget> widgets;

  /// you can add more fields that meet your needs

  const MainAppBar(
      {Key key,
      this.title,
      this.appBar,
      this.widgets,
      this.elevation,
      this.backgroundColor})
      : super(key: key);

  @override
  _MainAppBarState createState() => _MainAppBarState();

  @override
  // TODO: implement preferredSize
  //Size get preferredSize => null ;
  //Size get preferredSize => Size.fromHeight(kToolbarHeight + (bottom?.preferredSize?.height ?? 0.0)) ;
  //Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
  Size get preferredSize => new Size.fromHeight(AppBar().preferredSize.height);
}

class _MainAppBarState extends State<MainAppBar> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //return AppBar(title: Text("Mon Appli"), backgroundColor: Colors.deepOrange,) ;
    return AppBar(
        elevation: widget.elevation != null
            ? widget.elevation
            : 0.0, // pour supprimer l'ombre du appBar
        title: widget.title,
        backgroundColor: (widget.backgroundColor == null
            ? ConstApp.color_app
            : widget.backgroundColor),
        actions: widget.widgets);
  }
}

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Color backgroundColor = Colors.red;
  final Text title;
  final AppBar appBar;
  final List<Widget> widgets;

  /// you can add more fields that meet your needs

  const BaseAppBar({Key key, this.title, this.appBar, this.widgets})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: title,
      backgroundColor: backgroundColor,
      actions: widgets,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}
