import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/card_offres.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/views/edit_ao.dart';
import 'package:spasuce/views/edit_offres.dart';

class AccueilOffres extends StatefulWidget {
  String baseUrl;
  int userFournisseurId;
  MainDrawer mainDrawer;

  @override
  _AccueilOffresState createState() => _AccueilOffresState();

  AccueilOffres({this.baseUrl, this.userFournisseurId, this.mainDrawer});
}

class _AccueilOffresState extends State<AccueilOffres> {
  //List<User> datas;
  List<User> datas;
  List<User> datas2;
  List<dynamic> datasJsonTable;

  List<Widget> _widget = List();

  bool erreurDeConnexion;
  bool erreurService;
  bool datasEmpty = false;
  bool _isWaiting = true;

  @override
  void initState() {
    // TODO: implement initState
    Utilities.begin("AccueilOffres/initState");
    super.initState();
    initialiser();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Utilities.begin("AccueilOffres/build");

    //initialiser();
    // liste des informations
    return Scaffold(
      drawer: widget.mainDrawer,
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(ConstApp.app_name),
          //title: Text(ConstApp.offres),
          appBar: AppBar(),
          widgets: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 8.0, 16.0, 8.0),
              child: InkWell(
                child: Icon(Icons.filter_list),
                onTap: () {},
              ),
            )
          ]),
      body: (_isWaiting)
          ? Center(child: CircularProgressIndicator())
          : (datas != null && datas.length > 0)
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView.builder(
                    itemCount: (Utilities.isNotEmpty(datas) ? datas.length : 0),
                    itemBuilder: (context, index) {
                      return CardOffres(model: datas[index]);
                    },
                  ),
                )
              : (datasEmpty)
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          messageAucuneOffrePourLeUser,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              color: ConstApp.color_app_rouge),
                        ),
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          problemeDeConnexion,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              color: ConstApp.color_app_rouge),
                        ),
                      ),
                    ),
    );
  }

  initialiser() async {
    User data = User();
    List<User> datasLocal = List();
    User dataSecond = User();

    data.isVisibleByClient = true;
    data.isRetirer = false;
    data.isLocked = false;
    data.etatCode = ENTRANT;

    data.isAttribuer = false;
    data.userFournisseurId = widget.userFournisseurId;
    //data.isVisibleByClient = true ;

    dataSecond.isVisibleByClient = true;
    dataSecond.isRetirer = false;
    dataSecond.isAttribuer = true;
    dataSecond.isSelectedByClient = true;
    dataSecond.isLocked = false;
    dataSecond.etatCode = ENTRANT;
    dataSecond.userFournisseurId = widget.userFournisseurId;

    datasLocal.add(dataSecond);

    RequestCustom request = RequestCustom();
    request.isAnd = false;
    request.data = data;
    request.datas = datasLocal;

    await baseApi
        .testApi(
            apiBaseUrl: widget.baseUrl + appelDoffresRestreint + getByCriteria,
            request: request)
        .then((res) {
      if (res != null && !res.hasError) {
        if (Utilities.isNotEmpty(res.items)) {
          datas = User.fromJsons(res.items);
        } else {
          datasEmpty = true;
          datas = [];
        }
      } else {
        erreurService = true;
        Utilities.messageApi(response: res); // erreur avec le service
      }
    }).catchError((err) {
      erreurDeConnexion = true;
      // erreur de connexion
    });

    setState(() {
      _isWaiting = false;
    });
  }
}
