import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/form_offres.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';


class EditOffres extends StatefulWidget{

  User data ;
  int userFournisseurId ;
  String title ;
  EditOffres({this.data, this.userFournisseurId, this.title :ConstApp.edit_offres});

  @override
  _EditOffresState createState() => _EditOffresState();
}

class _EditOffresState extends State<EditOffres> {
  @override
  Widget build(BuildContext context) {

//CupertinoApp()

    User data = User() ;
    data.baseUrl = AppConfig.of(context).apiBaseUrl;
    data.appelDoffresId = (widget.data != null && widget.data.appelDoffresId != null)  ? widget.data.appelDoffresId  : (widget.data != null ? widget.data.id : null) ;    
    data.userFournisseurId = widget.userFournisseurId ;
    // TODO: implement build
    return Scaffold(
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(widget.title),
          appBar: AppBar()),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FormOffres(model: data),
      ) ,
    );
  }
}