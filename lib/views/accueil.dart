import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/card_accueil.dart';
import 'package:spasuce/custom_widgets/carousel.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/models/menu.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/spasuce_loader.dart';

class Accueil extends StatefulWidget {
  String apiBaseUrl = "";

  User data = User(
      isSelect: false,
      isAttribuer: false,
      etatCode: 'ENTRANT',
      typeEntrepriseCode: 'GENERAL');

  int size;
  User userConnecter;

  MainDrawer mainDrawer;
  //Drawer mainDrawer ;

  //const RequestCustom ;
  RequestCustom request;

  List<dynamic> listAO;

  Accueil(
      {this.size = 5,
      this.request,
      this.listAO,
      this.mainDrawer,
      this.userConnecter});

  @override
  _AccueilState createState() => _AccueilState();
}

class _AccueilState extends State<Accueil> {
  //bool _isSelectCritere = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Utilities.begin("initState accueil");
  }

  @override
  void didChangeDependencies() {
    // TODO: a comprendre
    super.didChangeDependencies();
    //initialize(widget.request);
  }

  @override
  Widget build(BuildContext context) {
    //AppConfig.of(context).apiBaseUrl + endpoint;

    Utilities.begin("build accueil");
    //Utilities.begin(widget.mainDrawer);

    // TODO: implement build
    return Scaffold(
      drawer: widget.mainDrawer,

      //drawer: Drawer() ,
      body: Container(
        child: projectWidget(),
        color: ConstApp.color_app_gris_degrade_1,

        //decoration: BoxDecoration(gradient:LinearGradient(colors: [ConstApp.color_app_gris, Colors.white, ConstApp.color_app_gris, Colors.white, ConstApp.color_app_gris])
        //gradient:LinearGradient(colors: [ Colors.white, ConstApp.color_app_gris,])
        //),/**/
      ),
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(ConstApp.app_name),
          //title: Text(AppLocalizations.of(context).translate('title')),
          //backgroundColor: Color(0xff139BEA),
          appBar: AppBar(),

          //actions : <Widget>[
          widgets: (widget.userConnecter != null
              ? <Widget>[
                  // action button

                  IconButton(
                    icon: Icon(Icons.notifications),
                    onPressed: () {
                      //_select(choices[0]);
                    },
                  ),
                  // user connect
                  /*Center(
                        child: (widget.userConnecter != null
                            ? Text(widget.userConnecter.nom)
                            : Text(""))),*/
                  // overflow menu
                  IconButton(
                    icon: Icon(Icons.filter_list),
                    onPressed: () {
                      _filtreEditModal(context);
                    },
                  ),

                  /*
          PopupMenuButton<Choice>(

            onSelected: _select,
           //icon: Icon(Icons.notifications_active),
            icon: Icon(Icons.filter_list),
            itemBuilder: (BuildContext context) {
              return choices.map((Choice choice) {
              //return choices.skip(2).map((Choice choice) {
                return PopupMenuItem<Choice>(


                  value: choice,
                  child : Text(choice.title), // sans icon
                  //child:ListTile(title: Text(choice.title), leading: Icon(choice.icon),), avec icon
                );
              }).toList();
            },
          ),
         */
                ]
              : // sinon
              <Widget>[
                  // overflow menu
                  IconButton(
                    icon: Icon(Icons.filter_list),
                    onPressed: () {
                      _filtreEditModal(context);
                    },
                  ),
                ])),
    );
  }

  Widget projectWidget() {
    Utilities.begin(" accueil/projectWidget ");

    Widget carousel = Container(
      height: 200.0,
      child: Carousel(
        boxFit: BoxFit.cover,
        images: imagesCarousel,
        autoplay: true,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(microseconds: 1000),
        dotSize: 4.0, // la taille des icones de defilement pas defaut
        dotColor: ConstApp.color_app_rouge, // couleur des icones
        indicatorBgPadding: 2.0, // la hauteur de la bande
      ),
    );

    return FutureBuilder(
      builder: (context, projectSnap) {
        Utilities.begin(" accueil/projectWidget/FutureBuilder ");

        /*if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          //print('project snapshot data is: ${projectSnap.data}');
          return Center(
            child: CircularProgressIndicator(),
          );
        }*/

        // choisir lequel envoyer

        //if (projectSnap != null && projectSnap.data != null) {
        return (projectSnap.data == null)
            ? /*Center(
                child: CircularProgressIndicator(),
              )*/
            SpasuceLoader()
            : ListView.builder(
                //itemCount: projectSnap.data.items.length,
                itemCount: ((projectSnap.data != null &&
                        Utilities.isNotEmpty(projectSnap.data.items))
                    ? projectSnap.data.items.length
                    : 1),
                itemBuilder: (context, index) {
                  Utilities.begin(
                      " accueil/projectWidget/FutureBuilder/itemBuilder ");

                  //ProjectModel project = projectSnap.data[index];

                  //User user = null;
                  User user;
                  if (projectSnap != null &&
                      projectSnap.data != null &&
                      Utilities.isNotEmpty(projectSnap.data.items)) {
                    user = User.fromJson(projectSnap.data.items[index]);

                    // ajout du type de user connecter

                    if (widget.userConnecter != null) {
                      user.typeEntrepriseCode =
                          widget.userConnecter.typeEntrepriseCode;
                    }
                  }

                  //User user = User.fromJson(projectSnap.data.items[index]);

                  return (((user != null))
                      ? ((User.fromJson(projectSnap.data.items[0]).id ==
                              user.id)
                          ? Column(
                              children: <Widget>[
                                // ajout du carousel

                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: CarouselCustom(
                                    images: imagesCarousel,
                                  ),
                                ),
                                CardAccueil(
                                  data: user,
                                  userId: (widget.userConnecter != null)
                                      ? widget.userConnecter.id
                                      : null,
                                )
                              ],
                            )
                          : CardAccueil(
                              data: user,
                              userId: (widget.userConnecter != null)
                                  ? widget.userConnecter.id
                                  : null,
                            ))
                      : ((projectSnap != null &&
                              projectSnap.data != null &&
                              !Utilities.isNotEmpty(projectSnap.data.items))
                          ? Column(
                              children: <Widget>[
                                // ajout du carousel

                                CarouselCustom(
                                  images: imagesCarousel,
                                ),
                                Center(
                                  child: Text(
                                    "Aucun appel d'offres disponible!!!",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: fontSizeCustom),
                                  ),
                                )
                              ],
                            )
                          : //Center( child: CircularProgressIndicator(),)

                          Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[CircularProgressIndicator()],
                              ),
                            )));
                  //return ((index == 0) ?  Column(children: <Widget>[CardAccueil(data: user,)]): CardAccueil(data: user,));
                  //return (projectSnap.data != null ? Text(projectSnap.data) : Text("En cours"));
                },
              );
        //}
        /*else {
          return ListView.builder(
            //itemCount: projectSnap.data.items.length,
            itemCount: ((projectSnap.data != null &&
                    Utilities.isNotEmpty(projectSnap.data.items))
                ? projectSnap.data.items.length
                : 1),
            itemBuilder: (context, index) {
              //ProjectModel project = projectSnap.data[index];

              User user = null;
              if (projectSnap.data != null &&
                  Utilities.isNotEmpty(projectSnap.data.items)) {
                user = User.fromJson(projectSnap.data.items[index]);
              }

              //User user = User.fromJson(projectSnap.data.items[index]);

              return ((user != null)
                  ? CardAccueil(
                      data: user,
                    )
                  : Center(
                      child: Text(
                        "Aucun appel d'offres disponible!!!",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ));
              //return ((index == 0) ?  Column(children: <Widget>[CardAccueil(data: user,)]): CardAccueil(data: user,));
              //return (projectSnap.data != null ? Text(projectSnap.data) : Text("En cours"));
            },
          );
        }

         */
      },
      //future: initializeCustom(uri :base_url_prod + endpointSecteurDactiviteAppelDoffresGetByCriteriaCustom),
      future: initializeCustom(
          uri: AppConfig.of(context).apiBaseUrl +
              endpointAppelDoffresGetByCriteria),
      // endpointSecteurDactiviteAppelDoffresGetByCriteriaCustom),
    );
  }

  void initialize(RequestCustom request) async {
    if (request == null) {
      request = RequestCustom(isAnd: false, data: widget.data);
    }
    setState(() {
      widget.apiBaseUrl = AppConfig.of(context).apiBaseUrl;
      //widget.apiBaseUrl = base_url_prod ;
    });
    var endpoint = "secteurDactiviteAppelDoffres/getByCriteriaCustom";
    var uri = widget.apiBaseUrl + endpoint;

    print("Future<Request> sent ${request}");

    ResponseCustom response =
        await baseApi.testApi(apiBaseUrl: uri, request: request);

    print("Future<ResponseCustom> ${response.hasError}");
    if (response != null &&
        !response.hasError &&
        Utilities.isNotEmpty(response.items)) {
      setState(() {
        widget.listAO = response.items;
      });
    }
  }

  void _filtreEditModal(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext builderContext) {
          return Container(
              child: (ConstApp.test == null)
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      itemCount:
                          (ConstApp.test == null ? 0 : ConstApp.test.length),
                      // nombre d'element a afficher dans la liste
                      itemBuilder: (context, index) {
                        //bool _isSelectCritere = false;

                        Test test = ConstApp.test[index];

                        return ListTile(
                          title: Text(ConstApp.app_name),
                          leading: test.isSelect
                              ? Icon(Icons.check_box_outline_blank)
                              : Icon(Icons.check_box),
                          //leading: Icon(ConstApp.test[index].icon),
                          trailing: Switch(
                            value: test.isSelect,
                            onChanged: (val) {
                              Utilities.log("val.val :  $val");
                              val = !val;
                              setState(() {});
                            },
                          ),
                          /*
                          onTap: () {
                            //context.widget.createElement();

                            setState(() {
                              test.isSelect = !test.isSelect;
                              Utilities.log(
                                  "test.isSelect :  ${test.isSelect}");
                            });
                          },
                          */
                          //leading: Icon(Icons.indeterminate_check_box),
                          //leading: Icon(Icons.check_box),
                        );
                      },
                    ));
        });
  }

  Future<ResponseCustom> initializeCustom(
      {RequestCustom request = null, String uri}) async {
    if (request == null) {
      request = RequestCustom(isAnd: false, data: widget.data);
    }
    //print("uriuriuriuriuri ${uri}");
    //print("Future<Request> sent ${request}");

    ResponseCustom response =
        await baseApi.testApi(apiBaseUrl: uri, request: request);

    return response;
  }
}

class Test {
  String title;
  String code;
  List<String> childrens;
  IconData icon;
  bool isSelect;
  Color color;
  dynamic view;

  Test(
      {this.title,
      this.icon,
      this.color,
      this.childrens,
      this.view,
      this.code,
      this.isSelect = false});
}
