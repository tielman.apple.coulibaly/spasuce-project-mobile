import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';

class ChangerPassword extends StatefulWidget {
  User data;

  @override
  _ChangerPasswordState createState() => _ChangerPasswordState();
  ChangerPassword({this.data});
}

class _ChangerPasswordState extends State<ChangerPassword> {
  bool _autovalidate = false;
  bool _autovalidatePassword = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerPassword;
  TextEditingController _controllerNewPassword = TextEditingController();
  TextEditingController _controllerConfirmNewPassword = TextEditingController();
  bool isInvisiblePassword = true;

  User model = User();

  @override
  Widget build(BuildContext context) {
    //apiBaseUrl = base_url_prod ;

    // TODO: implement build
    return Scaffold(
      body: ListView(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            autovalidate: _autovalidate,
            key: _formKey,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  child: Image.asset('images/logo.jpeg'),
                ),
                //ImageIcon('images/logo.jpeg'),
                //Utilities.loginFormField(_user, labelText: login),
                //Utilities.passwordFormField(_user, labelText: password),
                /*
                      TextFormField(
                        onSaved: (input) { _user.password = input ;}, // recuperation des données saisies
                        //obscureText: true, // pour rendre un texte invisible
                        validator:(value) => Utilities.validator(value),
                        decoration: InputDecoration(
                            labelText: password,
                            helperText: "test password"
                        ),
                        keyboardType: TextInputType.visiblePassword, // le type du input
                      ),

                       */

                TextFormField(
                  controller: _controllerPassword,
                  obscureText: isInvisiblePassword,
                  onSaved: (input) {
                    model.password = input.trim();
                  },
                  // recuperation des données saisies
                  validator: (value) => Utilities.validator(value,
                      typeContentToValidate: v_password),
                  decoration: InputDecoration(
                      labelText: oldPassword,
                      border: OutlineInputBorder(),
                      hintText: oldPassword),
                  keyboardType:
                      TextInputType.visiblePassword, // le type du input
                ),
                Divider(),
                TextFormField(
                  autovalidate: _autovalidatePassword,
                  controller: _controllerNewPassword,
                  obscureText: isInvisiblePassword,
                  onSaved: (input) {
                    model.newPassword = input.trim();
                  },
                  onChanged: (value) {
                    print(value);
                    setAutovalidatePassword();
                  },
                  autocorrect: true,
                  // recuperation des données saisies
                  //validator: (value) => Utilities.validator(value,typeContentToValidate: v_password),
                  validator: (value) {
                    if (Utilities.isNotBlank(value) &&
                        Utilities.isNotBlank(
                            _controllerConfirmNewPassword.text) &&
                        value !=
                            _controllerConfirmNewPassword.text.toString()) {
                      return erreurSaisieConfirmPassword;
                    }
                    if ((value == null || value.isEmpty) && _autovalidate) {
                      return erreurSaisieTexte;
                    }
                  },
                  decoration: InputDecoration(
                      labelText: newPassword,
                      border: OutlineInputBorder(),
                      hintText: newPassword),
                  keyboardType:
                      TextInputType.visiblePassword, // le type du input
                ),
                Divider(),
                TextFormField(
                  autovalidate: _autovalidatePassword,

                  controller: _controllerConfirmNewPassword,
                  obscureText: isInvisiblePassword,
                  onChanged: (value) {
                    setState(() {
                      _autovalidatePassword = true;
                    });
                  },
                  // recuperation des données saisies
                  //validator: (value) => Utilities.validator(value,typeContentToValidate: v_password),
                  validator: (value) {
                    if (Utilities.isNotBlank(value) &&
                        Utilities.isNotBlank(_controllerNewPassword.text) &&
                        value != _controllerNewPassword.text.toString()) {
                      return erreurSaisieConfirmPassword;
                    }
                    if ((value == null || value.isEmpty) && _autovalidate) {
                      return erreurSaisieTexte;
                    }
                  },
                  decoration: InputDecoration(
                      labelText: confirmerNewPassword,
                      border: OutlineInputBorder(),
                      hintText: confirmerNewPassword),
                  keyboardType:
                      TextInputType.visiblePassword, // le type du input
                ),
                Divider(),

                Row(
                  children: <Widget>[
                    Spacer(),
                    InkWell(
                      child: Text(
                        voirPassword,
                        style: TextStyle(color: ConstApp.color_app),
                      ),
                      onTap: () {
                        setState(() {
                          isInvisiblePassword = !isInvisiblePassword;
                        });
                      },
                    ),
                  ],
                ),
                SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                    child: Text(modifier),
                    onPressed: () =>
                        //Navigator.pop(context),
                        _onSubmit(baseUrl: AppConfig.of(context).apiBaseUrl),
                    shape: Utilities.getShape(topRight: 50, topLeft: 50),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }

  setAutovalidatePassword() {
    print("setAutovalidatePassword");
    setState(() {
      _autovalidatePassword = true;
    });
  }

  //void _onSubmit() {
  void _onSubmit({String baseUrl}) async {
    if (_formKey.currentState.validate()) {
      var request = RequestCustom();

      // permet d'executer toutes les validator des TextFormField
      _formKey.currentState
          .save(); // permet d'executer toutes les onSaved des TextFormField

      print(model.toJson());

      if (widget.data != null) {
        model.login = widget.data.login;
        request.user = widget.data.id;
      }
      //baseUrl = (baseUrl == null ) ? "http://192.168.1.101:8080/" : baseUrl;

      // appel de service
      request.data = model;
      var uri = baseUrl + user + changerPassword;
      //Navigator.pop(context); // pour fermer le menu apres clic
      //Utilities.getToast(succes);

      baseApi.testApi(apiBaseUrl: uri, request: request).then((res) {
        if (res != null && !res.hasError) {
          //Navigator.of(context).pop(); // pour fermer le menu apres clic

          Utilities.getToast(succes);
          Navigator.pop(context); // pour fermer le menu apres clic

          // retour
        } else {
          Utilities.messageApi(response: res);
          //Navigator.pop(context); // pour fermer le menu apres clic

          // Utilities.getToast('connexion echoué!!! Login et/ou pasword incorrect') ;
        }
      }).catchError((err) => print(err));
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }
}
