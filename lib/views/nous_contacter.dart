import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/const_app.dart' as globals;
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/accueil.dart';

class NousContacter extends StatefulWidget {
  String titre;

  @override
  _NousContacterState createState() => _NousContacterState();

  NousContacter({this.titre});
}

class _NousContacterState extends State<NousContacter> {
  final _formKey = GlobalKey<FormState>();
  var apiBaseUrl = "";

  bool _isDisable = false;

  //static const apiBaseUrl = AppConfig.of(context).test;

  var endpointNousContacter = "nousContacter/create";

  var endpointObjetNousContacter = "objetNousContacter/getByCriteria";

  List<DropdownMenuItem<dynamic>> _dropdownMenuItems = List();
  //dynamic _itemSelected, _user = User();
  User _itemSelected, _user = User();

  String _email;
  bool _autovalidate = false;
  bool _dynamicShape = true;

  String _login;
  String _message;
  String _objet;
  TextEditingController _controllerNomPrenoms = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerMessage = TextEditingController();

  onChangeDropdownItem(dynamic itemSelected) {
    setState(() {
      _itemSelected = itemSelected;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Utilities.begin("initState");

    //initialize() ;
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    Utilities.begin("didChangeDependencies");

    initialize();
  }

  @override
  Widget build(BuildContext context) {
    apiBaseUrl = AppConfig.of(context).apiBaseUrl;
    //apiBaseUrl = base_url_prod ;

    //initialize();

    /*
    RequestCustom request = RequestCustom();
    String uri = apiBaseUrl + endpointObjetNousContacter;
    request.data = User();

    baseApi.testApi(apiBaseUrl: uri, request: request).then((res) {
      print(res);
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        for (var element in res.items) {
          User user = User.fromJson(element);
          _dropdownMenuItems.add(DropdownMenuItem(
            child: Text(user.libelle),
            value: user,
          ));
        }
      }
    }).catchError((err) {
      print(err);
    });
    */
    Utilities.begin("build");

    // TODO: implement build
    return Scaffold(
        //------------ new begin ----------
        /* endDrawer: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.filter_list),
              title: Text('Filter List'),
              subtitle: Text('Hide and show items'),
              trailing: Switch(
                value: true,
                onChanged: (val) {},
              ),
            ),
            ListTile(
              leading: Icon(Icons.image_aspect_ratio),
              title: Text('Size Settings'),
              subtitle: Text('Change size of images'),
            ),
            ListTile(
              title: Slider(
                value: 0.5,
                onChanged: (val) {},
              ),
            ),
            ListTile(
              leading: Icon(Icons.sort_by_alpha),
              title: Text('Sort List'),
              subtitle: Text('Change layout behavior'),
              trailing: Switch(
                value: false,
                onChanged: (val) {},
              ),
            ),
          ],
        ),
        */
        //------------ new end ----------

        appBar: AppBar(
            title: Text(widget.titre), backgroundColor: ConstApp.color_app),
        //(Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
        //body: (Utilities.isNotEmpty(_dropdownMenuItems) ? ListView(
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Form(
                autovalidate: _autovalidate, // verifie la validation
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    /*
                        TextFormField(
                          onSaved: (input) { _user.login = input ;}, // recuperation des données saisies
                          validator:(value) => Utilities.validator(value),
                          decoration: InputDecoration(
                              labelText: login,
                              helperText: "test login"
                          ),
                        ),
 CircleAvatar(
                    child: Image.asset('images/logo.jpeg'),
                  ),
                        Text("Bienvenue chez SPASUCE", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),

                        */

                    //ImageIcon('images/logo.jpeg'),
                    Utilities.textFormFieldNom(_user,
                        labelText: "$nom & $prenoms",
                        controller: _controllerNomPrenoms,
                        border: OutlineInputBorder()),
                    Divider(),

                    Utilities.emailFormField(_user,
                        labelText: email,
                        controller: _controllerEmail,
                        border: OutlineInputBorder()),
                    Divider(),

                    Utilities.phoneFormField(_user,
                        labelText: phone,
                        controller: _controllerPhone,
                        border: OutlineInputBorder()),
                    //Utilities.textFormFieldObjet(_user, labelText: objet),

                    /*
                  Container(
                    width: double.infinity,

                    child: DropdownButton(
                      //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                      items: _dropdownMenuItems,
                      onChanged: onChangeDropdownItem,
                      value: _itemSelected,
                    ),
                  ),
                  DropdownButtonFormField
                   */
                    Divider(),

                    DropdownButtonFormField(
                      //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                      isExpanded: false,
                      items: _dropdownMenuItems,
                      onChanged: onChangeDropdownItem,
                      value: _itemSelected,
                      validator: (value) => Utilities.validatorObject(value,
                          typeContentToValidate: v_text),
                      decoration: InputDecoration(
                          labelText: objet, border: OutlineInputBorder()),
                    ),

                    Divider(),
                    TextFormField(
                      controller: _controllerMessage,
                      maxLines: maxLines,
                      onSaved: (input) {
                        _user.contenu = input.trim();
                      },
                      onChanged: (value) {
                        setState(() {
                          _dynamicShape = !_dynamicShape;
                        });
                      },
                      // recuperation des données saisies
                      validator: (value) => Utilities.validator(value,
                          typeContentToValidate: v_text),
                      decoration: InputDecoration(
                          labelText: message, border: OutlineInputBorder()),
                      keyboardType: TextInputType.multiline, // le type du input
                    ),
                    Divider(),

                    //Utilities.multilineFormField(_user, labelText: message, controller: _controllerMessage),
                    /*
                        TextFormField(
                          onSaved: (input) { _user.password = input ;}, // recuperation des données saisies
                          //obscureText: true, // pour rendre un texte invisible
                          validator:(value) => Utilities.validator(value),
                          decoration: InputDecoration(
                              labelText: password,
                              helperText: "test password"
                          ),
                          keyboardType: TextInputType.visiblePassword, // le type du input
                        ),

                         */

                    Utilities.getButtonSubmit(
                        shape: (_dynamicShape)
                            ? Utilities.getShape(topLeft: 50, bottomRight: 50)
                            : Utilities.getShape(bottomLeft: 50, topRight: 50),
                        child: Text(envoyer),
                        onPressed: _onSubmit,
                        isDisable: _isDisable),

                    /*RaisedButton(
                      shape: (_dynamicShape)
                          ? Utilities.getShape(topLeft: 50, bottomRight: 50)
                          : Utilities.getShape(bottomLeft: 50, topRight: 50),
                      child: Text(envoyer),
                      onPressed: _onSubmit,
                    ),
                    */
                  ],
                ),
              ),
            ),
          ],
        )
        //Center(child: CircularProgressIndicator(),),
        // :Container())
        );
  }

  Widget projectWidget() {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          //print('project snapshot data is: ${projectSnap.data}');
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
          //itemCount: projectSnap.data.items.length,
          itemCount: ((projectSnap.data != null &&
                  Utilities.isNotEmpty(projectSnap.data.items))
              ? projectSnap.data.items.length
              : 0),
          itemBuilder: (context, index) {
            //ProjectModel project = projectSnap.data[index];
            User user = User.fromJson(projectSnap.data.items[index]);
            return ExpansionTile(
              key: PageStorageKey<User>(user),
              //subtitle: Text("Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
              title: Text(
                user.titre,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  //child: Markdown (data: html2md.convert(user.contenu),)
                  //child:Html(data: user.contenu,useRichText: true,),
                  //child: Text(user.contenu),
                )
              ],
            );
          },
        );
      },
      //future: initialise(),
    );
  }

  // initialisation des datas
  initialize({String url}) {
    Utilities.begin("initialize");

    RequestCustom request = RequestCustom();
    //String uri = apiBaseUrl + endpointObjetNousContacter;

    //String uri = base_url_prod + endpointObjetNousContacter;
    String uri = AppConfig.of(context).apiBaseUrl + endpointObjetNousContacter;
    request.data = User();
    baseApi.testApi(apiBaseUrl: uri, request: request).then((res) {
      print(res);
      setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItems.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });
  }

  //void _onSubmit() {
  void _onSubmit() async {
    if (_formKey.currentState.validate()) {
      // permet d'executer toutes les validator des TextFormField

      // disableButton
      setState(() {
        _isDisable = true;
      });

      _formKey.currentState
          .save(); // permet d'executer toutes les onSaved des TextFormField

      print("_itemSelected du form");
      print(_itemSelected.toJson());
      print("Content du form");
      print(_user.toJson());

      _user.objetNousContacterId = _itemSelected.id;

      List<User> items = List();
      items.add(_user);
      // appel de service
      var request = RequestCustom();
      request.datas = items;
      var uri = apiBaseUrl + endpointNousContacter;
      ResponseCustom response =
          await baseApi.testApi(apiBaseUrl: uri, request: request);
      //ResponseCustom response =  baseApi.testApiCustom(apiBaseUrl: uri, request: request);
      print("Future<ResponseCustom> ${response.hasError}");
      if (response != null) {
        if (!response.hasError) {
          Utilities.messageApi(response: response);
          // vider le formmualaire
          _controllerEmail.clear();
          Utilities.clearForm([
            _controllerEmail,
            _controllerPhone,
            _controllerNomPrenoms,
            _controllerMessage
          ]);

          /*
          setState(() {
            //_user = User() ;
            //controllerEmail.clear();
            //_itemSelected = User() ;
          });
           */

        } else {
          Utilities.messageApi(response: response);
          // enableButton
        }

        //globals.isLoggedIn = true; // montre que le user est connecte

        //print(user.toJson());
        /*
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) =>
                MyApp(userConnecter: User.fromJson(response.items[0])),
          ),
        );

         */
      } else {
        Utilities.messageApi();
      }
      // enableButton
      setState(() {
        _isDisable = false;
      });
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }
}
