import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';

class DetailsOffresAO extends StatefulWidget {
  int offreId;
  int appelDoffresId;
  DetailsOffresAO({this.offreId, this.appelDoffresId});

  @override
  _DetailsOffresAOState createState() => _DetailsOffresAOState();
}

class _DetailsOffresAOState extends State<DetailsOffresAO> {
  List<User> offres;
  List<User> appelDoffre;

  User offreAO;
  User itemAO;

  bool _isWaiting = true;
  bool _isVisibleAO = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    initialize();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: MainAppBar(
        appBar: AppBar(),
        title: Text(detailsOffresAO),
      ),
      body: (_isWaiting)
          ? Center(child: CircularProgressIndicator())
          : (offreAO != null)
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView(
                    children: <Widget>[
                      Column(
                        children: [
                          //Text(partieAppelDoffres),

                          Center(
                            child: Utilities.getButtonSubmit(
                                shape: Utilities.getDefaultShape(),
                                child: Text(_isVisibleAO
                                    ? cacherDetailsAO
                                    : afficherDetailsAO),
                                onPressed: () => voirDetailsAO()),
                          ),
                          //widgetsCriteres.add(Text(partieAppelDoffres,style: textStyleTitle,));
                          (_isVisibleAO)
                              ? Column(
                                  children: <Widget>[
                                    MyTextFormField(
                                      decoration:
                                          InputDecoration(labelText: libelle),
                                      initialValue:
                                          Utilities.initialStringValueWidget(
                                              itemAO.libelle),
                                      enabled: false,
                                      //controller: controllerLibelle,
                                    ),

                                    MyTextFormField(
                                      decoration:
                                          InputDecoration(labelText: LPays),
                                      initialValue:
                                          Utilities.initialStringValueWidget(
                                              itemAO.paysLibelle),
                                      enabled: false,
                                      //controller: controllerLibelle,
                                    ),

                                    MyTextFormField(
                                      decoration:
                                          InputDecoration(labelText: LVille),
                                      initialValue:
                                          Utilities.initialStringValueWidget(
                                              itemAO.villeLibelle),
                                      enabled: false,
                                      //controller: controllerLibelle,
                                    ),
                                    MyTextFormField(
                                      decoration:
                                          InputDecoration(labelText: duree),
                                      initialValue:
                                          Utilities.initialiserDureeAOEnGet(
                                              nombreTypeDuree:
                                                  itemAO.nombreTypeDuree,
                                              typeDureeLibelle:
                                                  itemAO.typeDureeLibelle),

                                      /*
          (itemAO.nombreTypeDuree != null
                    ? itemAO.nombreTypeDuree.toString() + espace
                    : "") +
                (Utilities.isNotBlank(itemAO.typeDureeLibelle)
                    ? itemAO.typeDureeLibelle
                    : ""),
          (Utilities.isNotBlank(itemAO.typeDureeLibelle))
                ? itemAO.nombreTypeDuree.toString() +
                    espace +
                    itemAO.typeDureeLibelle
                : "",
                */
                                      enabled: false,
                                      //controller: controllerLibelle,
                                    ),
                                    MyTextFormField(
                                      decoration: InputDecoration(
                                          labelText: dateLimite),
                                      initialValue:
                                          Utilities.initialStringValueWidget(
                                              itemAO.dateLimiteDepot),
                                      enabled: false,
                                      //controller: controllerLibelle,
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            top: 16.0, bottom: 16),
                                        child: MyTextFormField(
                                          decoration: InputDecoration(
                                            labelText: description,
                                            border:
                                                OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                                          ),
                                          initialValue: Utilities
                                              .initialStringValueWidget(
                                                  itemAO.description),
                                          //enabled: false,
                                          readOnly: true, //TODO: a revoir
                                          maxLines: maxLines,
                                          //controller: controllerLibelle,
                                        )),

                                    // ajout des  domaines de l'AO
                                    (itemAO != null &&
                                            Utilities.isNotEmpty(itemAO
                                                .datasSecteurDactiviteAppelDoffres))
                                        ? Utilities
                                            .afficherMultiSelectFormFieldInRead(
                                                list: itemAO
                                                    .datasSecteurDactiviteAppelDoffres,
                                                textField:
                                                    secteurDactiviteLibelle,
                                                titleText: LDomaine)
                                        : Container(),

                                    // ajout des fichiers
                                    (itemAO != null &&
                                            Utilities.isNotEmpty(
                                                itemAO.datasFichier))
                                        ? Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text(listeFichiers),
                                              ],
                                            ),
                                          )
                                        : Container(),
                                    /*(itemAO != null &&
                                            Utilities.isNotEmpty(
                                                itemAO.datasFichier))
                                        ? Utilities
                                            .afficherListeFichierTechargeable(
                                                context: context,
                                                datasFichier:
                                                    itemAO.datasFichier)
                                        : Container(),*/

                                    Utilities.afficherListeFichierTechargeable(
                                        context: context,
                                        datasFichier: itemAO.datasFichier),

                                    //******************* Debut formulaire de l'offre ***************************
                                    Divider(),
                                  ],
                                )
                              : Divider(),

                          // verification si l'offre est retenu ou pas
                          (_isOffreRetenu(item: offreAO))
                              ? getContenuSiOffreRetenu()
                              : getContenuSiOffreNonRetenu(),

                          (_isOffreRetenu(item: offreAO))
                              ? offreAO.isModifiable != null &&
                                      offreAO.isModifiable
                                  ? Utilities.getButtonSubmit(
                                      child: Text("MODIFIER OFFRE"),
                                      color: Colors.red,
                                      onPressed: () =>
                                          modifierOffre())
                                  : Container()
                              : (_isAttribuerAppelDoffres(item: offreAO))
                                  ? Center(
                                      child: Text(
                                        messageImpossibleDeChoisirPlusDuneOffre,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 30,
                                            color: Colors.red),
                                      ),
                                    )
                                  : Utilities.getButtonSubmit(
                                      shape: Utilities.getDefaultShape(),
                                      child: Text(chosirOffre),
                                      onPressed: () => choisirOffre(),
                                      isDisable: false),
                          //),

/*
                          (_isNewOffre)
                              ?
                              // ajout de boutton de creation de Offres
                              Center(
                                  child: Utilities.getButtonSubmit(
                                      shape: Utilities.getDefaultShape(),
                                      child: Text(
                                          _isSoumettre ? annuler : soumettre),
                                      onPressed: soumettreOuAnnulerOffre),
                                )
                              :
                              // retirer si possible
                              Center(
                                  child: Utilities.getButtonSubmit(
                                      shape: Utilities.getDefaultShape(),
                                      child: Text(retirer),
                                      onPressed: retirerOffre,
                                      isDisable: _isDisable),
                                ),
                                
                                */
                        ],
                      )
                    ],
                  ),
                )
              : Container(),
    );
  }

  voirDetailsAO() {
    setState(() {
      _isVisibleAO = !_isVisibleAO;
    });
    print("voirDetailsAO  $_isVisibleAO");
  }

  choisirOffre() {
    Utilities.getToast(messageDev);
  }

  noterOffre() {
    Utilities.getToast(messageDev);
  }

  modifierOffre() {
    Utilities.getToast(messageDev);
  }

  modifierNote() {
    Utilities.getToast(messageDev);
  }

  initialize() async {
    RequestCustom request = RequestCustom();
    RequestCustom requestAO = RequestCustom();
    User dtoOffre = User();
    User dtoAO = User();
    dtoOffre.id = widget.offreId;
    dtoAO.id = widget.appelDoffresId;
    request.data = dtoOffre;
    requestAO.data = dtoAO;

    ResponseCustom response = await baseApi.testApi(
        apiBaseUrl: AppConfig.of(context).apiBaseUrl + offre + getByCriteria,
        request: request);

    //if (widget.appelDoffresId != null && widget.appelDoffresId > 0) {
    ResponseCustom responseAO = await baseApi.testApi(
        apiBaseUrl:
            AppConfig.of(context).apiBaseUrl + appelDoffres + getByCriteria,
        request: requestAO);
    //}

    if (response != null &&
        !response.hasError &&
        Utilities.isNotEmpty(response.items)) {
      //TODO: rendre les messages d'erreur plus precis
      // recuperation des offres de l'AO
      offres = User.fromJsons(response.items);
      offreAO = (offres.isNotEmpty) ? offres[0] : null;
    } else {
      Utilities.log(erreurConnexion);
    }

    if (responseAO != null &&
        !responseAO.hasError &&
        Utilities.isNotEmpty(responseAO.items)) {
      //TODO: rendre les messages d'erreur plus precis
      // recuperation des offres de l'AO
      appelDoffre = User.fromJsons(responseAO.items);
      itemAO = (appelDoffre.isNotEmpty) ? appelDoffre[0] : null;
    } else {
      Utilities.log(erreurConnexion);
    }
    setState(() {
      _isWaiting = false;
    });
  }

  bool _isOffreRetenu({User item}) {
    return item != null &&
        item.isSelectedByClient != null &&
        item.isSelectedByClient;
  }

  bool _isAttribuerAppelDoffres({User item}) {
    return item != null &&
        offreAO.isAttribuerAppelDoffres != null &&
        offreAO.isAttribuerAppelDoffres;
  }

  Widget getContenuSiOffreRetenu() {
    return Container(
      child: Column(
        children: <Widget>[
          Text(
            messageOffreRetenueVuParClient,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 30, color: Colors.green),
          ),
          Text(partieOffre),
          // liste des criteres en disable
          Container(
            child: Column(
                children: constructionCriteresOffresAO(
                    User.fromJsons(offreAO.datasValeurCritereOffre),
                    enable: false)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: MyTextFormField(
              decoration: InputDecoration(
                labelText: commentaire,
                border:
                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
              ),

              initialValue:
                  Utilities.initialStringValueWidget(offreAO.commentaire),
              maxLines: maxLines,
              keyboardType: TextInputType.multiline,
              //enabled: false,
              readOnly: true, //TODO: a revoir

              //controller: controllerLibelle,
            ),
          ),

          /* (offreAO != null && Utilities.isNotEmpty(offreAO.datasFichier))
              ? Utilities.afficherListeFichierTechargeable(
                  context: context, datasFichier: offreAO.datasFichier)
              : Container(),*/

          Utilities.afficherListeFichierTechargeable(
              context: context, datasFichier: offreAO.datasFichier),

          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: MyTextFormField(
              decoration: InputDecoration(
                labelText: labelMessageClient,
                border:
                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
              ),
              initialValue: Utilities.initialStringValueWidget(
                offreAO.messageClient,
              ),
              maxLines: maxLines,
              hintText: defaultMessageClientVuParClent,
              //enabled: false,
              //readOnly: true, //TODO: a revoir

              //controller: controllerLibelle,
            ),
          ),
          (offreAO.noteOffre != null)
              ? Column(
                  children: <Widget>[
                    MyTextFormField(
                      decoration: InputDecoration(labelText: labelNoteClient),
                      initialValue: Utilities.initialNumberValueWidget(
                        offreAO.noteOffre,
                      ),
                      enabled: false,
                      hintText: defaultMessageClientVuParClent,
                      //controller: controllerLibelle,
                    ),
                    Utilities.getButtonSubmit(
                        child: Text("MODIFIER NOTE"),
                        color: Colors.red,
                        onPressed: () => modifierNote())
                  ],
                )
              : Utilities.getButtonSubmit(
                  child: Text("NOTER OFFRE"),
                  color: Colors.red,
                  onPressed: () => noterOffre()),
        ],
      ),
    );
  }

  
  // construction de la listes des criteres de l'AO/Offres
  //Widget constructionCriteresOffresAO(List<User> criteres) {
  List<Widget> constructionCriteresOffresAO(List<User> criteres,
      {bool enable = false}) {
    List<Widget> widgets = List();
    if (Utilities.isNotEmpty(criteres)) {
      //widgetsCriteres.add(Text(partieOffre, style: textStyleTitle,));
      for (User element in criteres) {
        //for (User element in datasValeurCritereAppelDoffres) {
        //User data = User.fromJson(element);

        Utilities.log(element.typeCritereAppelDoffresCode);

        switch (element.typeCritereAppelDoffresCode) {
          case typeDate:
            TextEditingController _controllerDate = TextEditingController();

            _controllerDate.text =
                (Utilities.isNotBlank(element.valeur) ? element.valeur : "");
            widgets.add(MyTextFormField(
              decoration: InputDecoration(
                  labelText: element.critereAppelDoffresLibelle +
                      (enable ? marqueObligatoire : "")),
              //initialValue: (Utilities.isNotBlank(element.valeur)? element.valeur: ""),
              validator: (value) {
                Utilities.log("value typeDate typeDate $value");
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              autovalidate: true,
              enabled: enable,

              //enabled: false,
              controller: _controllerDate,
              readOnly: true,
              onSaved: (input) {
                element.valeur =
                    input; // pour recuperer la valeur saisie lors du submit
              },
              onTap: () {
                Utilities.showDatePickerCustom(
                  context: context,
                  controller: _controllerDate,
                );
                //_showDatePicker(_controllerDate);
                //if (_controllerDate != null) {
                // Utilities.log(_controllerDate.text = _getFormatDate()) ;
                //}
              },
              keyboardType: TextInputType.datetime,
              //controller: controllerLibelle,
            ));
            break;

          case typeString:
            widgets.add(MyTextFormField(
              decoration: InputDecoration(
                  labelText: element.critereAppelDoffresLibelle +
                      (enable ? marqueObligatoire : "")),
              initialValue:
                  (Utilities.isNotBlank(element.valeur) ? element.valeur : ""),
              validator: (value) {
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              onSaved: (input) {
                element.valeur =
                    input; // pour recuperer la valeur saisie lors du submit
              },
              enabled: enable,

              //keyboardType: TextInputType.datetime,
              //controller: controllerLibelle,
            ));
            break;

          case typeNumber:
            TextEditingController _controllerNumber = TextEditingController();

            _controllerNumber.text =
                (element.valeur != null ? element.valeur : "");
            widgets.add(MyTextFormField(
              decoration: InputDecoration(
                  labelText: element.critereAppelDoffresLibelle +
                      (enable ? marqueObligatoire : "")),
              //initialValue: (Utilities.isNotBlank(element.valeur)? element.valeur : ""),
              validator: (value) {
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              controller: _controllerNumber,
              onChanged: (value) {
                /* 
                    if (Utilities.isNotBlank(value) &&
                        value.toString().length >= 4) {
                      _controllerNumber.text = ;
                    }
                    */
              },
              onSaved: (input) {
                element.valeur =
                    input; // pour recuperer la valeur saisie lors du submit
              },
              keyboardType: TextInputType.number,
              enabled: enable,

              //controller: controllerLibelle,
            ));
            break;

          case typeBoolean:
            widgets.add(Radio(
                //value: element.valeur,
                value: false,
                groupValue: null,
                onChanged: (input) {
                  element.valeur =
                      input; // pour recuperer la valeur saisie lors du submit
                }));
            break;

          default:
            break;
        }
      }
    }
    return widgets;
    //return test;
  }

  // contenu si offre est en edition
  Widget getContenuSiOffreNonRetenu() {
    return Container(
      child: Column(
        children: <Widget>[
          // ajouter le libelle de l'offre en question
          Text(partieOffre),

          // faire apparaitre les criteres
          Container(
              child: Column(
            children: constructionCriteresOffresAO(
                User.fromJsons(offreAO.datasValeurCritereOffre),
                enable: false),
          )),
          //constructionCriteresOffresAO(itemsCriteres),

          // ajout des autres widgets
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: MyTextFormField(
              decoration: InputDecoration(
                labelText: commentaire,
                border:
                    OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
              ),
              validator: (value) {
                Utilities.validator(value, typeContentToValidate: v_text);
              },
              onSaved: (input) {
                //widget.model.libelle = input;
                offreAO.commentaire = input;
              },
              initialValue: (Utilities.isNotBlank(offreAO.commentaire)
                  ? offreAO.commentaire
                  : ""),
              maxLines: maxLines,
              keyboardType: TextInputType.multiline,
              readOnly: true,
              //controller: controllerLibelle,
            ),
          ),

          Utilities.afficherListeFichierTechargeable(
              context: context, datasFichier: offreAO.datasFichier),

          // ajouter un divider pour separer entre la liste des fichiers et la suite du formulaire
          Divider(),
          (_isAttribuerAppelDoffres(item: offreAO))
              ? Container()
              : Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                  child: MyTextFormField(
                    decoration: InputDecoration(
                      labelText: labelMessageClient,
                      border:
                          OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                    ),
                    initialValue: Utilities.initialStringValueWidget(
                      offreAO.messageClient,
                    ),
                    maxLines: maxLines,
                    hintText: defaultMessageClientVuParClent,
                    //enabled: false,
                    //controller: controllerLibelle,
                  ),
                ),
          (_isOffreRetenu(item: offreAO))
              ? (offreAO.noteOffre != null)
                  ? Column(
                      children: <Widget>[
                        MyTextFormField(
                          decoration:
                              InputDecoration(labelText: labelNoteClient),
                          initialValue: Utilities.initialNumberValueWidget(
                            offreAO.noteOffre,
                          ),
                          enabled: false,
                          hintText: defaultMessageClientVuParClent,
                          //controller: controllerLibelle,
                        ),
                        Utilities.getButtonSubmit(
                            child: Text("MODIFIER OFFRE"),
                            color: Colors.red,
                            onPressed: () => modifierNote()),
                      ],
                    )
                  : Utilities.getButtonSubmit(
                      child: Text("NOTER OFFRE"),
                      color: Colors.red,
                      onPressed: () => noterOffre())
              : Container(),
        ],
      ),
    );
  }
}
