import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/liste_title_offre.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/details_offre_ao.dart';

class VoirListeOffresAO extends StatefulWidget {
  int appelDoffresId;

  VoirListeOffresAO({@required this.appelDoffresId});
  //VoirListeOffresAO({this.appelDoffresId});

  @override
  _VoirListeOffresAOState createState() => _VoirListeOffresAOState();
}

class _VoirListeOffresAOState extends State<VoirListeOffresAO> {
  List<User> offres;

  User currentPays;
  User currentVille;

  bool _isWaiting = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //initialize();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    initialize();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: MainAppBar(
        appBar: AppBar(),
        title: Text(listeOffreDeLAO),
      ),
      body: (_isWaiting)
          ? Center(child: CircularProgressIndicator())
          : (offres != null && offres.length > 0)
              ? Padding(
                  padding: EdgeInsets.all(8.0),
                  child: ListView(
                    children: <Widget>[
                      //offres.map((e) => Text(e.code)). ,
                      Column(
                        children: <Widget>[
                          DropdownSearch<User>(
                            mode: Mode.BOTTOM_SHEET,
                            showSelectedItem: true,
                            compareFn: (User i, User s) => i.isEqual(s),
                            label: filtreParPays,
                            onFind: (String filter) => getData(filter: filter),
                            onChanged: (User data) {
                              print(data);
                              setState(() {
                                currentPays = data;
                                if (data == null) {
                                  currentVille = null;
                                }
                              });
                            },
                            validator: (value) =>
                                Utilities.validatorObject(value),
                            selectedItem: currentPays,
                            showClearButton: true,
                            showSearchBox: true,
                            searchBoxDecoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                              labelText: searchHintPays,
                            ),
                            popupTitle: Container(
                              height: 50,
                              decoration: Utilities.getBoxDecoration(),
                              child: Center(
                                child: Text(
                                  LPays,
                                  style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            popupShape: Utilities.getRoundedRectangleBorder(),
                            emptyBuilder: (context) => Utilities.dataNotFound(),
                            //dropdownBuilder: _customDropDownExample,
                            //popupItemBuilder: _customPopupItemBuilderExample2,
                          ),
                          Divider(),
                          DropdownSearch<User>(
                            mode: Mode.BOTTOM_SHEET,
                            showSelectedItem: true,
                            compareFn: (User i, User s) => i.isEqual(s),
                            label: filtreParCritere,
                            onFind: (String filter) => getData(
                                filter: (currentPays != null)
                                    ? currentPays.id.toString()
                                    : filter,
                                isVille: true),
                            onChanged: (User data) {
                              //print(data);
                              currentVille = data;
                            },
                            onSaved: (value) {
                              if (value != null) {
                                // widget.model.villeId = value.id;
                              }
                            },
                            validator: (value) =>
                                Utilities.validatorObject(value),
                            selectedItem: currentVille,
                            showClearButton: true,
                            showSearchBox: true,
                            searchBoxDecoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                              labelText: searchHintVille,
                            ),
                            popupTitle: Container(
                              height: 50,
                              decoration: Utilities.getBoxDecoration(),
                              child: Center(
                                child: Text(
                                  LVille,
                                  style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            popupShape: Utilities.getRoundedRectangleBorder(),
                            emptyBuilder: (context) => Utilities.dataNotFound(),

                            //dropdownBuilder: _customDropDownExample,
                            //popupItemBuilder: _customPopupItemBuilderExample2,
                          ),
                          Divider(),
                          ListView.builder(
                            shrinkWrap: true,
                            itemCount: offres.length,
                            //itemCount: 2,
                            itemBuilder: (BuildContext context, int index) {
                              User data = offres[index];

                              return ListTileOffre(
                                data: data,
                                onTap: () {
                                  Utilities.navigate(
                                      context,
                                      DetailsOffresAO(
                                        appelDoffresId: widget.appelDoffresId,
                                        offreId: data.id,
                                      ));
                                },
                                withAllDetails: true,
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              : Center(
                  child: Text("Aucune offre pour cet appel d'offres"),
                ),
    );
  }

  initialize() async {
    RequestCustom request = RequestCustom();
    User dtoOffre = User();
    dtoOffre.appelDoffresId = widget.appelDoffresId;
    request.data = dtoOffre;

    ResponseCustom response = await baseApi.testApi(
        apiBaseUrl: AppConfig.of(context).apiBaseUrl + offre + getByCriteria,
        request: request);
    if (response != null &&
        !response.hasError &&
        Utilities.isNotEmpty(response.items)) {
      //TODO: rendre les messages d'erreur plus precis
      // recuperation des offres de l'AO
      offres = User.fromJsons(response.items);
    } else {
      Utilities.log(erreurConnexion);
    }
    setState(() {
      _isWaiting = false;
    });
  }

  Future<List<User>> getData({String filter, bool isVille = false}) async {
    print("getData filter $filter");
    print("getData isVille $isVille");
    RequestCustom request = RequestCustom();
    User data = User();
    List<User> models;
    String endpoint = endpointGetPays;

    if (Utilities.isNotBlank(filter)) {
      data.paysId = int.parse(filter);
      endpoint = ville + getByCriteria;
    } else {
      if (isVille) {
        return models;
      }
    }

    request.data = data;
    var res = await baseApi.testApi(
        apiBaseUrl: AppConfig.of(context).apiBaseUrl + endpoint,
        request: request);

    if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
      models = User.fromJsons(res.items);
    }
    return models;
  }
}
