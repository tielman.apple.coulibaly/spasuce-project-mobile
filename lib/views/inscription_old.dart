/*
import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/inscription_reussi.dart';

class Inscription extends StatefulWidget {
  User entreprise;
  @override
  _InscriptionState createState() => _InscriptionState();

  Inscription({this.entreprise});
}

class _InscriptionState extends State<Inscription> {
  final _formKeyEntreprise = GlobalKey<FormState>();
  final _formKeyUtilisateur = GlobalKey<FormState>();
  var _entreprise = User();
  var _utilisateur = User();

  User _itemRoleSelected;
  User _itemStatutJuridiqueSelected;
  User _itemTypeEntrepriseSelected;
  User _itemPaysSelected;
  User _itemVilleSelected;

  User dataUser = User();
  User dataEntrepise = User();

  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsRole = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsPays = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsStatutJuridique = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsTypeEntreprise = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsVille = List();

  TextEditingController _controllerLoginUser = TextEditingController();
  TextEditingController _controllerPhoneUser = TextEditingController();
  TextEditingController _controllerEmailUser = TextEditingController();
  TextEditingController _controllerNomUser = TextEditingController();
  TextEditingController _controllerPasswordUser = TextEditingController();
  TextEditingController _controllerConfirmPasswordUser =
      TextEditingController();

  TextEditingController _controllerPhoneEntrep = TextEditingController();
  TextEditingController _controllerEmailEntrep = TextEditingController();
  TextEditingController _controllerPasswordEntrep = TextEditingController();
  TextEditingController _controllerConfirmPasswordEntrep =
      TextEditingController();
  TextEditingController _controllerNomEntrep = TextEditingController();

  int _currentStep = 0;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    Utilities.begin("Inscription/didChangeDependencies");
    initialize();
  }

  @override
  Widget build(BuildContext context) {
    Utilities.begin("Inscription/build");
    if (widget.entreprise != null) {
      _controllerNomEntrep.text = widget.entreprise.nom;
    }

    // TODO: implement build
    return Scaffold(
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(titleWidegtInscription),
          appBar: AppBar()),
      body: Stepper(
        steps: _mySteps(),
        currentStep: _currentStep,
        onStepTapped: (step) {
          print(step);
          setState(() {
            // TODO: à revoir
            if (_formKeyEntreprise.currentState.validate()) {
              _currentStep = step;
            }

            //_currentStep = step;
          });
        },
        onStepCancel: () {
          Utilities.getToast("onStepCancel");
          setState(() {
            if (_currentStep > 0) {
              _currentStep--;
            }
          });
        },
        onStepContinue: () {
          Utilities.getToast("onStepContinue");

          setState(() {
            if (_currentStep < 2 - 1) {
              // verifions le formulaire de l'entreprise

              if (_formKeyEntreprise.currentState.validate()) {
                _formKeyEntreprise.currentState.save();
                // passons à l'etape suivante
                _currentStep++;
              }
            } else {
              // verifions le formulaire de l'utilisateur
              if (_formKeyUtilisateur.currentState.validate()) {
                // permet d'executer toutes les validator des TextFormField
                _formKeyUtilisateur.currentState.save();

                submit();
              }

              // passons à la soumission

            }
          });
        },
      ),
    );
  }

  List<Step> _mySteps() {
    List<Step> mySteps = [
      // le step de l'entreeprise
      Step(
        title: Text("Entreprise"),
        subtitle: Text(sousTitreEntreprise),
        isActive: _currentStep >= 0,
        content: Form(
            // mettre la condition si les datas sont disponible
            autovalidate: true,
            key: _formKeyEntreprise,
            child: Column(
              children: <Widget>[
                /*
                    Utilities.textFormFieldNom(_entreprise,
                        labelText: nom, controller: _controller),
                    Utilities.emailFormField(_entreprise, labelText: email),
                    Utilities.phoneFormField(_entreprise, labelText: phone, controller: _controller),
                    Utilities.passwordFormField(_entreprise, labelText: password, controller: _controller),
                    Utilities.passwordFormField(_entreprise, labelText: confirmer, controller: _controller),
                    */
                MyTextFormField(
                  controller: _controllerNomEntrep,
                  decoration: InputDecoration(labelText: nom),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.nom = input;
                  },
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerEmailEntrep,
                  decoration: InputDecoration(labelText: email),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.email = input;
                  },
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerPhoneEntrep,
                  decoration: InputDecoration(labelText: phone),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.telephone = input;
                  },
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerPasswordEntrep,
                  decoration: InputDecoration(labelText: password),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.password = input;
                  },
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerConfirmPasswordEntrep,
                  decoration: InputDecoration(labelText: confirmer),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.libelle = input;
                  },
                  //controller: controllerLibelle,
                ),

                DropdownButtonFormField(
                  //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                  items: _dropdownMenuItemsPays,
                  onChanged: onChangeDropdownItemPays,
                  value: _itemPaysSelected,
                  validator: (value) => Utilities.validatorObject(value,
                      typeContentToValidate: v_text),
                  decoration:
                      InputDecoration(labelText: LPays, helperText: 'test'),
                ),

                DropdownButtonFormField(
                  //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                  items: _dropdownMenuItemsVille,
                  onChanged: onChangeDropdownItemVille,
                  value: _itemVilleSelected,
                  validator: (value) => Utilities.validatorObject(value,
                      typeContentToValidate: v_text),
                  decoration:
                      InputDecoration(labelText: LVille, helperText: 'test'),
                  onSaved: (input) {
                    dataEntrepise.villeId = input.id;
                  },
                ),

                DropdownButtonFormField(
                  //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                  items: _dropdownMenuItemsTypeEntreprise,
                  onChanged: onChangeDropdownItemTypeEntreprise,
                  value: _itemTypeEntrepriseSelected,
                  validator: (value) => Utilities.validatorObject(value,
                      typeContentToValidate: v_text),
                  decoration: InputDecoration(
                      labelText: LTypeEntreprise, helperText: 'test'),
                  onSaved: (input) {
                    dataEntrepise.typeEntrepriseId = input.id;
                  },
                ),

                DropdownButtonFormField(
                  //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                  items: _dropdownMenuItemsStatutJuridique,
                  onChanged: onChangeDropdownItemStatutJuridique,
                  value: _itemStatutJuridiqueSelected,
                  validator: (value) => Utilities.validatorObject(value,
                      typeContentToValidate: v_text),
                  decoration: InputDecoration(
                      labelText: LStatutJuridique, helperText: 'test'),
                  onSaved: (input) {
                    dataEntrepise.statutJuridiqueId = input.id;
                  },
                ),
              ],
            )),
      ),

      // le step de l'utilisateur

      Step(
        title: Text("Utilisateur"),
        subtitle: Text(sousTitreUtilisateur),
        isActive: _currentStep >= 1,
        content: Form(
            key: _formKeyUtilisateur,
            child: Column(
              children: <Widget>[
                /*
                    Utilities.textFormFieldNom(_entreprise,
                        labelText: "$nom & $prenoms", controller: _controller),
                    Utilities.emailFormField(_entreprise, labelText: email),
                    Utilities.phoneFormField(_entreprise, labelText: phone, controller: _controller),
                    Utilities.textFormFieldNom(_entreprise,
                        labelText: "$nom & $prenoms", controller: _controller),
                    Utilities.emailFormField(_entreprise, labelText: email, controller: _controller),
                    Utilities.phoneFormField(_entreprise, labelText: phone),
                    */
                MyTextFormField(
                  controller: _controllerNomUser,
                  decoration: InputDecoration(labelText: nom),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.nom = input;
                  },
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerEmailUser,
                  decoration: InputDecoration(labelText: email),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.email = input;
                  },
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerLoginUser,
                  decoration: InputDecoration(labelText: login),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.login = input;
                  },
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerPasswordUser,
                  decoration: InputDecoration(labelText: password),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.password = input;
                  },

                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerConfirmPasswordUser,
                  decoration: InputDecoration(labelText: confirmer),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.libelle = input;
                  },
                  //onChanged: , comparer les deux valeurs
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerPhoneUser,
                  decoration: InputDecoration(labelText: phone),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.telephone = input;
                  },
                  //controller: controllerLibelle,
                ),
                DropdownButtonFormField(
                  //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                  items: _dropdownMenuItemsRole,
                  onChanged: onChangeDropdownItemRole,
                  value: _itemRoleSelected,
                  validator: (value) => Utilities.validatorObject(value,
                      typeContentToValidate: v_text),
                  decoration:
                      InputDecoration(labelText: fonction, helperText: 'test'),
                  onSaved: (input) {
                    dataUser.roleId = input.id;
                  },
                ),
              ],
            )),
      )
    ];

    return mySteps;
  }

  onChangeDropdownItem({dynamic itemSelected, String typeItem}) {
    setState(() {
      print("$typeItem azertyuiopazertyuio $itemSelected");
      switch (typeItem) {
        case LPays:
          _itemPaysSelected = itemSelected;
          break;
        case LTypeEntreprise:
          _itemTypeEntrepriseSelected = itemSelected;
          break;
        case fonction:
          _itemRoleSelected = itemSelected;
          break;
        case LStatutJuridique:
          _itemStatutJuridiqueSelected = itemSelected;
          break;
        case LVille:
          _itemVilleSelected = itemSelected;
          break;
        default:
          break;
      }
    });
  }

  onChangeDropdownItemPays(dynamic itemSelected) {
    print("****************************Pays*************** $itemSelected");
    setState(() {
      _itemPaysSelected = itemSelected;

      //_dropdownMenuItemsVille =  List() ;
      _dropdownMenuItemsVille.clear(); // vider la liste des villes

      // load data ville in _dropdownMenuItemsVille
      if (Utilities.isNotEmpty(_itemPaysSelected.datasVille)) {
        for (var item in _itemPaysSelected.datasVille) {
          print(item);
          //print(item['libelle']) ;
          User user = User.fromJson(item);
          _dropdownMenuItemsVille.add(DropdownMenuItem(
            child: Text(user.libelle),
            value: user,
          ));
        }

        Utilities.log(_dropdownMenuItemsVille.length);
      }
    });
  }

  onChangeDropdownItemTypeEntreprise(dynamic itemSelected) {
    print(
        "****************************TypeEntreprise*************** $itemSelected");
    setState(() {
      _itemTypeEntrepriseSelected = itemSelected;

      // load data role in _dropdownMenuItemsRole
      User basicData = User();
      User data = User();
      List<User> datas = List();
      basicData.code = ADMINISTRATEUR;
      //basicData.codeParam = {"operator": "<>"} ;
      basicData.codeParam = operatorDifferent;

      if (_itemTypeEntrepriseSelected.libelle == FOURNISSEUR) {
        data.code = FOURNISSEUR;
        datas.add(data);
      } else {
        if (_itemTypeEntrepriseSelected.libelle == CLIENTE) {
          data.code = FOURNISSEUR;
          data.codeParam = operatorDifferent;
          //data.codeParam = {"operator": "<>"} ;
          datas.add(data);
        }
      }
      RequestCustom req = RequestCustom();
      req.data = basicData;
      if (Utilities.isNotEmpty(datas)) {
        req.datas = datas;
      }
      String baseUrl = AppConfig.of(context).apiBaseUrl;

      //Utilities.log(req.data.toJson()) ;
      //Utilities.log(req.datas[0].toJson()) ;

      //_dropdownMenuItemsRole.clear() ;
      //_dropdownMenuItemsRole =  List() ; // vider la liste des roles
      req.isAnd = true; // tres important afin de bien filtrer le get
      loadDataForDropdownMenu(
          request: req,
          uri: baseUrl + endpointGetRole,
          dropdownMenuItems: _dropdownMenuItemsRole);
    });
  }

  onChangeDropdownItemStatutJuridique(dynamic itemSelected) {
    print(
        "****************************StatutJuridique************* $itemSelected");

    setState(() {
      _itemStatutJuridiqueSelected = itemSelected;
    });
  }

  onChangeDropdownItemVille(dynamic itemSelected) {
    print("****************************Ville************* $itemSelected");

    setState(() {
      _itemVilleSelected = itemSelected;
    });
  }

  onChangeDropdownItemRole(dynamic itemSelected) {
    print("****************************Role************* $itemSelected");
    setState(() {
      _itemRoleSelected = itemSelected;
    });
  }

  // initialisation des datas
  initialize({String url}) {
    Utilities.begin("initialize");

    RequestCustom request = RequestCustom();
    //String uri = apiBaseUrl + endpointObjetNousContacter;

    //String uri = base_url_prod + endpointObjetNousContacter;
    String baseUrl = AppConfig.of(context).apiBaseUrl;
    request.data = User();

    loadDataForDropdownMenu(
        request: request,
        uri: baseUrl + endpointGetStatutJuridique,
        dropdownMenuItems: _dropdownMenuItemsStatutJuridique);
    loadDataForDropdownMenu(
        request: request,
        uri: baseUrl + endpointGetTypeEntreprise,
        dropdownMenuItems: _dropdownMenuItemsTypeEntreprise);
    loadDataForDropdownMenu(
        request: request,
        uri: baseUrl + endpointGetPays,
        dropdownMenuItems: _dropdownMenuItemsPays);

    /*
    loadDataForDropdownMenu(request: request, uri : baseUrl + endpointGetRole , dropdownMenuItems: _dropdownMenuItemsPays);

    
    baseApi.testApi(apiBaseUrl: baseUrl + endpointGetTypeEntreprise, request: request).then((res) {
      print(res);
      setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItemsTypeEntreprise.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });

    baseApi.testApi(apiBaseUrl:  baseUrl + endpointGetStatutJuridique, request: request).then((res) {
      print(res);
      setState(() {

        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            print("endpointGetStatutJuridique: $element");
            _dropdownMenuItemsStatutJuridique.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });

    baseApi.testApi(apiBaseUrl:  baseUrl + endpointGetRole, request: request).then((res) {
      print(res);
      setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItemsRole.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });
    baseApi.testApi(apiBaseUrl:  baseUrl + endpointGetPays, request: request).then((res) {
      print(res);
      setState(() {
        _dropdownMenuItemsPays = List() ;

        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItemsPays.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });
    */
  }

  loadDataForDropdownMenu(
      {RequestCustom request, String uri, List dropdownMenuItems}) {
    baseApi.testApi(apiBaseUrl: uri, request: request).then((res) {
      print(res);
      setState(() {
        //dropdownMenuItems = List() ; fait un changement de reference
        dropdownMenuItems.clear(); // vider la liste

        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            dropdownMenuItems.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });
  }

  // soumission du formulaire
  void submit() async {
    if (_formKeyUtilisateur.currentState.validate() &&
        _formKeyEntreprise.currentState.validate()) {
      // permet d'executer toutes les validator des TextFormField
      _formKeyUtilisateur.currentState
          .save(); // permet d'executer toutes les onSaved des TextFormField
      _formKeyEntreprise.currentState
          .save(); // permet d'executer toutes les onSaved des TextFormField

      // vider les datas du formulaire

      _itemPaysSelected = null;
      _itemRoleSelected = null;
      _itemStatutJuridiqueSelected = null;
      _itemTypeEntrepriseSelected = null;
      _itemVilleSelected = null;

      dataUser.dataEntreprise = dataEntrepise;
      List<User> datas = List();
      print("_itemSelected du form");
      print("Content du form");

      datas.add(dataUser);

      print("_itemSelected du form");
      print("Content du form");
      print(dataUser.toJson());

      // appel de service
      var request = RequestCustom();
      request.datas = datas;
      String baseUrl = AppConfig.of(context).apiBaseUrl;

      var uri = baseUrl + user + create;
      ResponseCustom response =
          await baseApi.testApi(apiBaseUrl: uri, request: request);
      //ResponseCustom response =  baseApi.testApiCustom(apiBaseUrl: uri, request: request);
      print("Future<ResponseCustom> ${response.hasError}");
      if (response != null) {
        if (!response.hasError) {
          Utilities.messageApi(response: response);
          // vider le formmualaire
          //_controllerEmail.clear();
          Utilities.clearForm([
            _controllerConfirmPasswordEntrep,
            _controllerPhoneEntrep,
            _controllerPasswordEntrep,
            _controllerNomEntrep,
            _controllerEmailEntrep
          ]);
          Utilities.clearForm([
            _controllerConfirmPasswordUser,
            _controllerPhoneUser,
            _controllerPasswordUser,
            _controllerNomUser,
            _controllerEmailUser,
            _controllerLoginUser
          ]);

          // redirection sur la page d'inscription reussi
          Utilities.navigate(context, InscriptionReussi());
        } else {
          //Utilities.navigate(context, InscriptionReussi()) ;

          Utilities.messageApi(response: response);
        }
      } else {
        Utilities.messageApi();
      }
    }
  }
}
*/
