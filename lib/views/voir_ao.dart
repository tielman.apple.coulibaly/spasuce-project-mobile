import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/liste_title_offre.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/views/details_offre_ao.dart';
import 'package:spasuce/views/voir_liste_offres_ao.dart';

class VoirAO extends StatefulWidget {
  VoirAO({Key key, this.appelDoffres, this.baseUrl}) : super(key: key);

  User appelDoffres;
  String baseUrl;

  @override
  _VoirAOState createState() => _VoirAOState();
}

class _VoirAOState extends State<VoirAO> {
  List<User> offresAO = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    initialise();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(ConstApp.show_offres),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                partieAppelDoffres,
                textAlign: TextAlign.center,
                style: Utilities.styleTextTitreH3(),
              ),
            ),
            MyTextFormField(
              decoration: InputDecoration(labelText: libelle),
              initialValue: Utilities.initialStringValueWidget(
                  widget.appelDoffres.libelle),
              enabled: false,
              //controller: controllerLibelle,
            ),
            MyTextFormField(
              decoration: InputDecoration(labelText: LPays),
              initialValue: Utilities.initialStringValueWidget(
                  widget.appelDoffres.paysLibelle),
              enabled: false,
              //controller: controllerLibelle,
            ),
            MyTextFormField(
              decoration: InputDecoration(labelText: LVille),
              initialValue: Utilities.initialStringValueWidget(
                  widget.appelDoffres.villeLibelle),
              enabled: false,
              //controller: controllerLibelle,
            ),
            MyTextFormField(
              decoration: InputDecoration(labelText: duree),
              initialValue: Utilities.initialiserDureeAOEnGet(
                  nombreTypeDuree: widget.appelDoffres.nombreTypeDuree,
                  typeDureeLibelle: widget.appelDoffres.typeDureeLibelle),

              /*
              (widget.appelDoffres.nombreTypeDuree != null
                      ? widget.appelDoffres.nombreTypeDuree.toString() + espace
                      : "") +
                  (Utilities.isNotBlank(widget.appelDoffres.typeDureeLibelle)
                      ? widget.appelDoffres.typeDureeLibelle
                      : ""),
              (Utilities.isNotBlank(
                      widget.appelDoffres.nombreTypeDuree.toString())
                  ? widget.appelDoffres.nombreTypeDuree.toString() +
                      espace +
                      widget.appelDoffres.typeDureeLibelle
                  : ""),
                  */
              enabled: false,
              //controller: controllerLibelle,
            ),
            MyTextFormField(
              decoration: InputDecoration(labelText: dateLimite),
              initialValue: Utilities.initialStringValueWidget(
                  widget.appelDoffres.dateLimiteDepot),
              enabled: false,
              //controller: controllerLibelle,
            ),

            MyTextFormField(
              decoration: InputDecoration(labelText: nombreDoffres),
              initialValue: Utilities.initialStringValueWidget(
                  widget.appelDoffres.nombreOffre),
              enabled: false,
              //controller: controllerLibelle,
            ),

            Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 16),
                child: MyTextFormField(
                  decoration: InputDecoration(
                    labelText: description,
                    border:
                        OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                  ),
                  initialValue: Utilities.initialStringValueWidget(
                      widget.appelDoffres.description),
                  enabled: false,
                  maxLines: maxLines,
                  //controller: controllerLibelle,
                )),

            // ajout des  domaines de l'AO
            (widget.appelDoffres != null &&
                    Utilities.isNotEmpty(
                        widget.appelDoffres.datasSecteurDactiviteAppelDoffres))
                ? Utilities.afficherMultiSelectFormFieldInRead(
                    list: widget.appelDoffres.datasSecteurDactiviteAppelDoffres,
                    textField: secteurDactiviteLibelle,
                    titleText: LDomaine)
                : Container(),

            // ajout des  domaines de l'AO
            (widget.appelDoffres != null &&
                    Utilities.isNotEmpty(
                        widget.appelDoffres.datasValeurCritereAppelDoffres))
                ? Utilities.afficherMultiSelectFormFieldInRead(
                    list: widget.appelDoffres.datasValeurCritereAppelDoffres,
                    textField: critereAppelDoffresLibelle,
                    titleText: LCritere)

                /*AbsorbPointer(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 16.0),
                      child: MultiSelectFormField(
                        titleText: LCritere,
                        hintText: hintMultiSelectForm,
                        initialValue: widget
                            .appelDoffres.datasValeurCritereAppelDoffres
                            .map((e) => e[critereAppelDoffresLibelle])
                            .toList(),
                        dataSource:
                            widget.appelDoffres.datasValeurCritereAppelDoffres,
                        textField: critereAppelDoffresLibelle,
                        valueField: critereAppelDoffresLibelle,
                      ),
                    ),
                  )*/
                : Container(),

            // ajout des fichiers
            (widget.appelDoffres != null &&
                    Utilities.isNotEmpty(widget.appelDoffres.datasFichier))
                ? Container(
                    padding: const EdgeInsets.only(bottom: 30.0),
                    height: MediaQuery.of(context).size.height * 0.25,
                    child: Scrollbar(
                        child: ListView.separated(
                      itemCount: widget.appelDoffres.datasFichier.length,
                      itemBuilder: (BuildContext context, int index) {
                        final String nameFile =
                            widget.appelDoffres.datasFichier[index][name];
                        final String urlFile =
                            widget.appelDoffres.datasFichier[index][urlFichier];

                        return new ListTile(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                // return object of type Dialog
                                return AlertDialog(
                                  title: new Text(telechargerFichier),
                                  content: new Text(questionTelechargerFichier),
                                  actions: <Widget>[
                                    // usually buttons at the bottom of the dialog
                                    new FlatButton(
                                      child: new Text(reponseNon),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                        //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                                      },
                                    ),
                                    new FlatButton(
                                      child: new Text(reponseOui),
                                      onPressed: () {
                                        // telechager le file

                                        // Utilities.base64InFile( base64: widget.appelDoffres.datasFichier[index][fichierBase64],nameFile: nameFile);

                                        Utilities.saveFileFromUri(
                                            uriFile: urlFile,
                                            nameNewFile: nameFile);

                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          },

                          title: Text(
                            nameFile,
                          ),
                          //subtitle: new Text(path),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          new Divider(),
                    )),
                  )
                : Container(),

            Divider(),
            (offresAO != null && offresAO.length > 0)
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          listeOffreDeLAO,
                          style: Utilities.styleTextTitreH3(),
                        ),
                      ),
                      Utilities.getButtonSubmit(
                          child: Text(details),
                          onPressed: () {
                            Utilities.navigate(
                                context,
                                VoirListeOffresAO(
                                  appelDoffresId: widget.appelDoffres.id,
                                ));
                          })
                    ],
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          aucuneOffrePourLAO,
                          //textAlign: TextAlign.center,
                          style: Utilities.styleTextTitreH3(),
                        ),
                      ),
                    ],
                  ),

            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: offresAO.length,
                    //itemCount: 2,
                    itemBuilder: (BuildContext context, int index) {
                      User data = offresAO[index];

                      return ListTileOffre(
                        data: data,
                        onTap: () {
                          Utilities.navigate(
                              context,
                              DetailsOffresAO(
                                appelDoffresId: widget.appelDoffres.id,
                                offreId: data.id,
                              ));
                        },
                      );
                    },
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  initialise() {
    User dataAO = User();
    RequestCustom requestAO = RequestCustom();
    requestAO.data = dataAO;
    //dataAO.id = 43;
    dataAO.id = widget.appelDoffres.id;

    User dataOffre = User();
    RequestCustom requestOffre = RequestCustom();
    //dataOffre.appelDoffresId = 43;
    dataOffre.appelDoffresId = widget.appelDoffres.id;
    requestOffre.data = dataOffre;

    baseApi
        .testApi(
            apiBaseUrl: widget.baseUrl + appelDoffres + getByCriteria,
            request: requestAO)
        .then((res) {
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        // recuperation de l'AO
        setState(() {
          widget.appelDoffres = User.fromJson(res.items[0]);
        });
      } else {
        Utilities.log(erreurConnexion);
      }
    }).catchError((err) {});

    baseApi
        .testApi(
            apiBaseUrl: widget.baseUrl + offre + getByCriteria,
            request: requestOffre)
        .then((res) {
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        // recuperation des offres de l'AO
        setState(() {
          offresAO = User.fromJsons(res.items);
        });
      } else {
        Utilities.log(erreurConnexion);
      }
    }).catchError((err) {});
  }
}
