import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:spasuce/const_app.dart';

class SpasuceLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: HeartbeatProgressIndicator(
        child: SizedBox(
          height: 50.0,
          child: Image.asset(logoSpasuce),
        ),
      ),
    );
  }
}
