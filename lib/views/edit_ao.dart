import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/form_ao.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';


class EditAO extends StatefulWidget{

  User data ;
  int userClientId ;
  String mode ;
  String title ;
  EditAO({this.data, this.userClientId, this.mode = CREATE, this.title :ConstApp.edit_oa});



  @override
  _EditAOState createState() => _EditAOState();
}

class _EditAOState extends State<EditAO> {
  @override
  Widget build(BuildContext context) {

    String baseUrl = AppConfig.of(context).apiBaseUrl ;

    User model = User();
    if (widget.data != null ) {
      model = widget.data ;
    }
    model.userClientId = widget.userClientId ; // on suppose que l'AO n'est editable que pas son initiateur
    // TODO: implement build
    return Scaffold(
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(widget.title),
          appBar: AppBar()),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FormAO(model: model, baseUrl:baseUrl, mode: widget.mode,),
      ) ,

      /*
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FormAO(baseUrl :baseUrl),
          )
        ],
      ),
      */

    );
  }
}