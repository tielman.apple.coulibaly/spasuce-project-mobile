import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';

class ContenuStatique extends StatefulWidget {
  String typeContenuStatique;

  List<dynamic> listContenuStatique;

  ContenuStatique({this.typeContenuStatique = ConstApp.presentation});

  @override
  _ContenuStatiqueState createState() => _ContenuStatiqueState();
}

class _ContenuStatiqueState extends State<ContenuStatique> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    /*
    var uri = AppConfig.of(context).apiBaseUrl + "contenuStatique/getByCriteriaCustom";

    RequestCustom request = RequestCustom();

    User data = User(typeContenuStatiqueCode: widget.typeContenuStatique) ;
    request.data = data ;
    Utilities.initialize(request, uri) ;

     */
  }

  @override
  Widget build(BuildContext context) {
    print("buildbuildbuildbuild");

    // TODO: implement build
    //return Center(child: Text(widget.typeContenuStatique));
    return projectWidget() ;
  }


  Widget projectWidget() {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          //print('project snapshot data is: ${projectSnap.data}');
          return Center( child: CircularProgressIndicator(),);
        }
        return ListView.builder(
          //itemCount: projectSnap.data.items.length,
          itemCount: 1,
          itemBuilder: (context, index) {
            //ProjectModel project = projectSnap.data[index];
            //User user = User.fromJson(projectSnap.data.items[index]);
            return Stepper(
                currentStep: 0,
                steps: projectSnap.data
                //steps: <Step>[Step(title: Text(user.titre), content: Text(user.contenu))]
            );
          },
        );
      },
      future: initialise(),
    );
  }

  Future<List<Step>> initialise() async{
    var uri = AppConfig.of(context).apiBaseUrl +
        "contenuStatique/getByCriteriaCustom";

    List<Step> steps = [];
    RequestCustom request = RequestCustom();
    User data = User(typeContenuStatiqueCode: widget.typeContenuStatique);
    request.data = data;
    ResponseCustom response = await Utilities.initialize(request, uri);

    //return response ;

    //setState(() {widget.listContenuStatique = response.items; });
    print(widget.listContenuStatique);
    if(Utilities.isNotEmpty(response.items)){
      for (var element in response.items) {
        //if(Utilities.isNotEmpty(widget.listContenuStatique)){
         // for (var element in widget.listContenuStatique) {
        print("@@@@@@@@@@@@@@@@@ $element");
        User user = User.fromJson(element);
        print("############### ${user.toJson()}");
        steps.add(
            Step(title: Text(user.titre), content: Text(user.contenu))
        );
      }
      print("@@@@@@@@@@@@@@@@@ initStateinitStateinitState  stepssteps stepssteps stepssteps ${steps.length}");
    }
    return steps ;


  }
}
