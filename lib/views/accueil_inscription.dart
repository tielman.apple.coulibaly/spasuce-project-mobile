import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/inscription.dart';

class AccueilInscription extends StatefulWidget {
  MainDrawer mainDrawer;
  @override
  _AccueilInscriptionState createState() => _AccueilInscriptionState();
  AccueilInscription({this.mainDrawer});
}

class _AccueilInscriptionState extends State<AccueilInscription> {
  bool isAncien = false;
  bool errorInGet = false;
  bool isInvisiblePassword = true;

  bool _autovalidate = false;

  String messageErrorInGet = "";

  TextEditingController _controller = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  List<dynamic> products = [
    "Tets",
    "hjkl",
    "jlkp",
  ];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: widget.mainDrawer,
      appBar: AppBar(
          title: Text(ConstApp.app_name), backgroundColor: ConstApp.color_app),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            /*Ink(

              child: Text("data"),
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [ConstApp.color_app, ConstApp.color_app_gris_degrade_1])
              ),
            ),
            

            RaisedButton(
              //disabledColor: ConstApp.color_red,
              autofocus: true,
              //clipBehavior: ,
              focusColor: Colors.red,
              hoverColor:Colors.red ,



              //onPressed: null,
              onPressed: () => print("click"),
            ),*/
            RaisedButton(
                textColor: Colors.white,
                color: ConstApp.color_app,

                //textColor: isAncien ? Colors.white : Colors.black ,
                //color: isAncien ?  ConstApp.color_app :  null,
                // shape: Utilities.getShape(bottomLeft: 50, topRight: 50),
                shape: Utilities.getShape(topLeft: 50, bottomRight: 50),
                child: Text(
                  nouvelleEntreprise,
                  style: TextStyle(
                    fontStyle: FontStyle.italic,
                  ),
                ),
                onPressed: () {
                  setState(() {
                    isAncien = false;
                    errorInGet = false;
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Inscription(
                                  isNewEntreprise: true,
                                )));
                  });
                }),
            RaisedButton(
                textColor: isAncien ? Colors.black : Colors.white,
                color: isAncien ? null : ConstApp.color_app,
                shape: Utilities.getShape(bottomLeft: 50, topRight: 50),
                child: Text(nouvelUtilisateur,
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                    )),
                onPressed: () {
                  setState(() {
                    isAncien = true;
                  });
                }),
            SizedBox(
              height: 10.0,
            ),
            (isAncien)
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Form(
                        autovalidate: _autovalidate,
                        key: _formKey,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: SizedBox(
                                //height: 20.0,
                                child: MyTextFormField(
                                  decoration: InputDecoration(
                                    labelText: password,
                                    hintText: hintSearchEntrepriseByPassword,
                                    suffixIcon: Icon(Icons.vpn_key),
                                    border:
                                        OutlineInputBorder(), // pour la bordure dans le input. Remarque on doit ajouter un padding
                                  ),
                                  obscureText: isInvisiblePassword,
                                  controller: _controller,
                                  validator: (value) {
                                    if (value != null &&
                                        value.toString().length <
                                            NBRE_CARACTERE_MIN_PASSWORD) {
                                      return messageTailleMinPassword;
                                    }
                                    return null;
                                  },
                                  onChanged: (value) {
                                    setState(() {
                                      errorInGet = false;
                                    });
                                  },
                                  //onChanged: (value) { setState(() { (Utilities.isNotBlank(value))? autovalidate = true: autovalidate = false;}); },
                                ),
                              ),
                            ),
                            //Spacer(),
                            IconButton(
                              icon: Icon(
                                Icons.search,
                                size: 36.0,
                                color: Colors.blue,
                              ),
                              onPressed: () {
                                // recherche de password dans la BD des entreprises
                                searchEntrepriseByPassword();
                              },
                            ),
                          ],
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                        //splashColor: Colors.white,
                        child: Text(voirPassword,
                            style: TextStyle(color: ConstApp.color_app)),
                        onTap: () {
                          setState(() {
                            isInvisiblePassword = !isInvisiblePassword;
                          });
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                    ],
                  )
                : Container(),
            (errorInGet)
                ? Text(messageErrorInGet, style: TextStyle(color: Colors.red))
                : Container()

            /*
            SizedBox(
              height: 10.0,
            ),
            InkWell(
              //splashColor: Colors.white,
              child: Text(voirPassword,
                  style: TextStyle(color: ConstApp.color_app)),
              onTap: () {
                setState(() {
                  isInvisiblePassword = !isInvisiblePassword;
                });
              },
            )
            */
          ],
        ),
      ),
    );
  }

  searchEntrepriseByPassword() {
    if (_formKey.currentState.validate()) {
      RequestCustom request = RequestCustom();
      request.isAnd = false;
      User data = User();
      data.password = _controller.text.toString().trim();
      request.data = data;

      baseApi
          .testApi(
              apiBaseUrl:
                  AppConfig.of(context).apiBaseUrl + entreprise + getByCriteria,
              request: request)
          .then((res) {
        if (res != null && !res.hasError) {
          if (Utilities.isNotEmpty(res.items)) {
            // entreprise trouvée
            //
            User item = User.fromJson(res.items[0]);

            Utilities.log("rerererer ${item.toJson()}");

            Utilities.log("item.nombreUser ${item.nombreUser}");

            if (item.nombreUser != null &&
                item.nombreUser >= NBRE_MAXI_USER_PAR_ENTREPRISE) {
              // le nombre de user est atteint
              setState(() {
                errorInGet = true;
                messageErrorInGet = messageNombreDeUserAtteint;
              });
            } else {
              Utilities.log("blaaijlkm");

              if (item.typeEntrepriseCode == FOURNISSEUR) {
                setState(() {
                  errorInGet = true;
                  messageErrorInGet =
                      Utilities.buildErrorFindByPassword(item.nom);
                });
              } else {
                // on fait le vrai traitement
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Inscription(
                              entreprise: item,
                              isNewUser: true,
                            )));
              }
            }
          } else {
            // password innexistant
            setState(() {
              errorInGet = true;
              messageErrorInGet = messagePassordIncorrect;
            });
          }
        } else {
          Utilities.messageApi(response: res); // erreur avec le service
        }
      }).catchError((err) {
        // erreur de connexion
        Utilities.messageApi();
      });
    } else {
      setState(() {
        _autovalidate = true;
        //errorInGet = true;
        //messageErrorInGet = messageChampObligatoire;
      });
    }
  }
}
