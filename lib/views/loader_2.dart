import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/main.app.bar.dart';

class Loader2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        /*appBar: MainAppBar(
          //title: Text(lConnexion),
          elevation: 0.0,
        ),*/
        backgroundColor: ConstApp.color_app,
        //backgroundColor: ConstApp.color_app_rouge,
        //backgroundColor: ConstApp.color_app_gris,
        //backgroundColor: ConstApp.color_app_gris_degrade,
        //backgroundColor: ConstApp.color_app_gris_degrade_1,
        body: Center(
            child: SizedBox(
          height: 200.0,
          child: Image.asset(logoSpasuce),
        )));
  }
}
