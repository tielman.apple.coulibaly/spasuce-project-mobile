import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/models/user.dart';


class ShowOffres extends StatefulWidget{

  User data ;
  int idUser ;
  ShowOffres({this.data, this.idUser});

  @override
  _ShowOffresState createState() => _ShowOffresState();
}

class _ShowOffresState extends State<ShowOffres> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text(ConstApp.show_offres),),
      body: Center(child: Text(ConstApp.show_offres)),
    );
  }
}