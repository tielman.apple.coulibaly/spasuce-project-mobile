import 'package:flutter/material.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';

class MAJDomaine extends StatefulWidget {
  User modelAO = User();
  String baseUrl;

  @override
  _MAJDomaineState createState() => _MAJDomaineState();

  MAJDomaine({this.modelAO, this.baseUrl});
}

class _MAJDomaineState extends State<MAJDomaine> {
  List<dynamic> _itemsDomaine;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialize(baseUrl: widget.baseUrl);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        //appBar: MainAppBar(appBar: AppBar(),title: Text(ConstApp.maj_domaines),),
        body: (Utilities.isNotEmpty(_itemsDomaine))
            ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView(

                  children: <Widget>[
                    Center(
                        child: Text(
                      "Merci de mettre à jour vos domaines afin de recevoir les offres specifiques à votre profil !!!",
                      style: Utilities.styleText(),
                      textAlign: TextAlign.center,
                    )),
                    Divider(),
                    MultiSelectFormField(
                      //fillColor: Colors.grey,
                      //autovalidate: true,
                      titleText: LDomaine,
                      //autovalidate: false,
                      okButtonLabel: okButtonLabel,
                      cancelButtonLabel: cancelButtonLabel,
                      hintText: hintMultiSelectForm,
                      /*initialValue: Utilities.initialValueMultiSelectFormField(
                        initialList: (widget.entreprise != null)
                            ? widget.entreprise.datasSecteurDactivite
                            : null),*/

                      initialValue: (widget.modelAO != null)
                          ? widget.modelAO.datasSecteurDactivite
                              .map((e) => e[LIBELLE])
                              .toList()
                          : null,

                      dataSource: _itemsDomaine,
                      textField: LIBELLE,
                      valueField: LIBELLE,

                      close: () {
                        Utilities.log("close");
                      },
                      open: () {
                        Utilities.log("open");
                        Utilities.log("domainedomainedomainedomainedomaine");
                      },
                      change: () => Utilities.log("change") //;
                      // Utilities.log("domainedomainedomainedomainedomaine") ;
                      ,

                      validator: (value) {
                        //if (value == null && value.length == 0) {
                        if (value == null || value.length == 0) {
                          return messageChoixMultiSelectForm;
                        }
                        return null;
                      },

                      onSaved: (value) {
                        /*setState(() {
                        //itemAO.datasSecteurDactiviteAppelDoffres = value;

                        List<User> itemsLocal = List();
                        for (var item in value) {
                          for (var itemsSecteurDactivite
                              in _itemsSecteursDactivites) {
                            if (itemsSecteurDactivite[LIBELLE] == item) {
                              User user = User();
                              user.domaineId = itemsSecteurDactivite[id];
                              user.domaineLibelle =
                                  itemsSecteurDactivite[LIBELLE];
                              itemsLocal.add(user);
                              break;
                            }
                          }
                        }
                        dataEntrepise.datasDomaineEntreprise = itemsLocal;
                      });*/
                      },
                    )
                  ],
                ),
              )
            : Center(child: CircularProgressIndicator()));
  }

  initialize({String baseUrl}) {
    RequestCustom request = RequestCustom();
    //String uri = apiBaseUrl + endpointObjetNousContacter;

    //String uri = base_url_prod + endpointObjetNousContacter;
    //String baseUrl = AppConfig.of(context).apiBaseUrl;
    request.data = User();

    baseApi
        .testApi(
            apiBaseUrl:  baseUrl + secteurDactivite + getByCriteria,
            request: request)
        .then((res) {
      print(res);
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        setState(() {
          _itemsDomaine = res.items;
        });
        Utilities.log(
            "_itemsDomaine _itemsDomaine _itemsDomaine $_itemsDomaine");
      }
    }).catchError((err) {
      print(err);
    });
  }
}
