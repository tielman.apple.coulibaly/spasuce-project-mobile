import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:spasuce/views/contenu_indisponible.dart';

class ContenuStatique extends StatefulWidget {
  String typeContenuStatique;
  String titreContenuStatique;

  List<dynamic> listContenuStatique;
  MainDrawer mainDrawer ;


  ContenuStatique({this.typeContenuStatique = ConstApp.presentation, this.titreContenuStatique = ConstApp.app_name, this.mainDrawer});


  @override
  _ContenuStatiqueState createState() => _ContenuStatiqueState();
}

class _ContenuStatiqueState extends State<ContenuStatique> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    /*
    var uri = AppConfig.of(context).apiBaseUrl + "contenuStatique/getByCriteriaCustom";

    RequestCustom request = RequestCustom();

    User data = User(typeContenuStatiqueCode: widget.typeContenuStatique) ;
    request.data = data ;
    Utilities.initialize(request, uri) ;

     */
  }

  @override
  Widget build(BuildContext context) {
    print("buildbuildbuildbuild");

    var test = "";

    test = "10";

    print(test);

    // TODO: implement build
    //return Center(child: Text(widget.typeContenuStatique));
    return Scaffold(
      //padding: const EdgeInsets.all(8.0),
      //child: projectWidget(),      
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(widget.titreContenuStatique),
          appBar: AppBar()),
      
      drawer: widget.mainDrawer,
      body: projectWidget(),
    );
    /*
    return   ListView.builder(
      itemBuilder: (BuildContext context, int index) =>
          EntryItem(data[index]),
      itemCount: data.length,
    ) ;

     */
  }

  // construction d'un formBuilder afin d'appeler un service qui va modifier le contenu du widget
  Widget projectWidget() {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.data == null) {
          //if (projectSnap.connectionState == ConnectionState.none &&  projectSnap.hasData == null) {
          //print('project snapshot data is: ${projectSnap.data}');
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if(projectSnap.data != null &&
            Utilities.isNotEmpty(projectSnap.data.items)){
          return ListView.builder(
            //itemCount: projectSnap.data.items.length,
            itemCount: ((projectSnap.data != null &&
                Utilities.isNotEmpty(projectSnap.data.items))
                ? projectSnap.data.items.length
                : 0),
            itemBuilder: (context, index) {
              //ProjectModel project = projectSnap.data[index];
              User user ;
              if (projectSnap != null &&
                  projectSnap.data != null &&
                  Utilities.isNotEmpty(projectSnap.data.items)) {
                user = User.fromJson(projectSnap.data.items[index]);
              }

                return ExpansionTile(
                  //subtitle: Text(user.contenu, maxLines: 1,),
                  //initiallyExpanded: false,
                  key: PageStorageKey<User>(user),
                  //subtitle: Text("Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
                  title: Text(
                    (user.titre != null ? user.titre : "Titre par defaut"),
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 18.0, left: 18.0, bottom: 18.0,),
                      //child: Markdown (data: html2md.convert(user.contenu),)
                      child: Html(
                        renderNewlines: true,
                        data: user.contenu,
                        useRichText: true,
                      ),
                      //child: Text(user.contenu),
                    )
                  ],
                );

            },
          );
        }else{
          return ListView(
            children: <Widget>[
              ContenuIndisponible()
            ],
          );
        }

      },
      future: initialise(),
    );
  }

  Future<ResponseCustom> initialise() async {
    //Future<List<Step>> initialise() async{
    var uri = AppConfig.of(context).apiBaseUrl +
        "contenuStatique/getByCriteriaCustom";
    //var uri = base_url_prod + "contenuStatique/getByCriteriaCustom";

    List<Step> steps = [];
    RequestCustom request = RequestCustom();
    User data = User(typeContenuStatiqueCode: widget.typeContenuStatique);
    request.data = data;
    ResponseCustom response = await Utilities.initialize(request, uri);
    //setState(() {widget.listContenuStatique = response.items; });
    //setState(() {test = "esponse.items;" });

    return response;

    /*
    //setState(() {widget.listContenuStatique = response.items; });
    print(widget.listContenuStatique);
    if(Utilities.isNotEmpty(response.items)){
      for (var element in response.items) {
        //if(Utilities.isNotEmpty(widget.listContenuStatique)){
         // for (var element in widget.listContenuStatique) {
        print("@@@@@@@@@@@@@@@@@ $element");
        User user = User.fromJson(element);
        print("############### ${user.toJson()}");
        steps.add(
            Step(title: Text(user.titre), content: Text(user.contenu))
        );
      }
      print("@@@@@@@@@@@@@@@@@ initStateinitStateinitState  stepssteps stepssteps stepssteps ${steps.length}");
    }
    return steps ;

     */
  }
}

// One entry in the multilevel list displayed by this app.
class Entry {
  Entry(this.title,
      {this.contenu =
          "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique",
      this.children = const <Entry>[]});

  final String title;
  final String contenu;
  final List<Entry> children;
}

// Displays one Entry. If the entry has children then it's displayed
// with an ExpansionTile.
class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      //subtitle: Text("Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
      title: Text(
        root.title,
        style: TextStyle(),
      ),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
              "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
        )
      ],
    );
    /*

return ExpansionPanel(
      headerBuilder: (context, isExpanded){

      },
      body: Text(root.contenu),
      //key: PageStorageKey<Entry>(root),

      //title: Text(root.title),
      children: root.children.map(_buildTiles).toList(),
    );

     */
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}

// The entire multilevel list displayed by this app.
final List<Entry> data = <Entry>[
  Entry('Chapter A',
      children: <Entry>[
        Entry(
          'Section A0',
          children: <Entry>[
            Entry('Item A0.1', contenu: "Bonjour j'espere"),
            Entry('Item A0.2', contenu: "Bonjour j'espere"),
            Entry('Item A0.3', contenu: "Bonjour j'espere"),
          ],
        ),
        Entry('Section A1', contenu: "Bonjour j'espere"),
        Entry('Section A2', contenu: "Bonjour j'espere"),
      ],
      contenu:
          "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
  Entry('Chapter B',
      children: <Entry>[
        Entry('Section B0', contenu: "Bonjour j'espere"),
        Entry('Section B1', contenu: "Bonjour j'espere"),
      ],
      contenu:
          "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
  Entry('Chapter C',
      children: <Entry>[
        Entry('Section C0', contenu: "Bonjour j'espere"),
        Entry('Section C1', contenu: "Bonjour j'espere"),
        Entry('Section C2',
            children: <Entry>[
              Entry('Item C2.0', contenu: "Bonjour j'espere"),
              Entry('Item C2.1', contenu: "Bonjour j'espere"),
              Entry('Item C2.2', contenu: "Bonjour j'espere"),
              Entry('Item C2.3', contenu: "Bonjour j'espere"),
            ],
            contenu:
                "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
      ],
      contenu:
          "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"),
];

/*

// The entire multilevel list displayed by this app.
final List<Entry> data = <Entry>[
  Entry(
    'Chapter A', children :
    <Entry>[
      Entry(
        'Section A0', children:
        <Entry>[
          Entry('Item A0.1'),
          Entry('Item A0.2'),
          Entry('Item A0.3'),
        ],
      ),
      Entry('Section A1'),
      Entry('Section A2'),
    ], contenu : "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"
  ),
  Entry(
    'Chapter B', children :
    <Entry>[
      Entry('Section B0'),
      Entry('Section B1'),
    ],
      contenu : "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"
  ),
  Entry(
    'Chapter C', children :
    <Entry>[
      Entry('Section C0'),
      Entry('Section C1'),
      Entry(
        'Section C2', children :
        <Entry>[
          Entry('Item C2.0'),
          Entry('Item C2.1'),
          Entry('Item C2.2'),
          Entry('Item C2.3'),
        ],
          contenu : "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"
      ),
    ],
      contenu : "Bonjour j'espere que tout ce passe bien. Ceci est un contenu statique"
  ),
];
 */
