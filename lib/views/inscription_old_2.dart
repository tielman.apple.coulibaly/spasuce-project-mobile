/*
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/my_text_form_field.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/inscription_reussi.dart';

class Inscription extends StatefulWidget {
  User utilisateur;
  User entreprise;
  bool isUpdate;
  bool isNewEntreprise;
  bool isNewUser;
  String titreWidget;
  MainDrawer mainDrawer;

  @override
  _InscriptionState createState() => _InscriptionState();

  Inscription(
      {this.utilisateur,
      this.entreprise,
      this.isNewEntreprise,
      this.mainDrawer,
      this.titreWidget = titleWidegtInscription,
      this.isNewUser = false,
      this.isUpdate = false});
}

class _InscriptionState extends State<Inscription> {
//class _InscriptionState extends State<Inscription> with WidgetsBindingObserver {
  final _formKeyEntreprise = GlobalKey<FormState>();
  final _formKeyUtilisateur = GlobalKey<FormState>();
  var _entreprise = User();
  var _utilisateur = User();

/*
  User _itemRoleSelected;
  User _itemStatutJuridiqueSelected;
  User _itemTypeEntrepriseSelected;
  User _itemPaysSelected;
  User _itemVilleSelected;
*/
  int _itemRoleSelected;
  int _itemStatutJuridiqueSelected;
  int _itemTypeEntrepriseSelected;
  String _itemPaysSelectedx;
  int _itemVilleSelectedx;

  User dataUser = User();
  User dataEntrepise = User();

  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsRole = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsPays = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsStatutJuridique = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsTypeEntreprise = List();
  List<DropdownMenuItem<dynamic>> _dropdownMenuItemsVille = List();

  List<dynamic> _itemsSecteursDactivites = List();

  //List<dynamic> _initialValueItemsSecteursDactivites = ["INFORMATIQUE","SOUDURE"];
  List<dynamic> _initialValueItemsSecteursDactivites;

  TextEditingController _controllerPrenomUser = TextEditingController();
  TextEditingController _controllerLoginUser = TextEditingController();
  TextEditingController _controllerPhoneUser = TextEditingController();
  TextEditingController _controllerEmailUser = TextEditingController();
  TextEditingController _controllerNomUser = TextEditingController();
  TextEditingController _controllerPasswordUser = TextEditingController();
  TextEditingController _controllerConfirmPasswordUser =
      TextEditingController();

  TextEditingController _controllerPhoneEntrep = TextEditingController();
  TextEditingController _controllerEmailEntrep = TextEditingController();
  TextEditingController _controllerPasswordEntrep = TextEditingController();
  TextEditingController _controllerConfirmPasswordEntrep =
      TextEditingController();
  TextEditingController _controllerNomEntrep = TextEditingController();

  String _myActivity;

  int _currentStep = 0;

  bool _autovalidate = false;

   User currentPays;
  User currentVille;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();

    Utilities.begin("Inscription/didChangeDependencies");
    initializeUpdate();
    initialize();
  }

  /*
  @override
  void deactivate() {
    super.deactivate();
    //this method not called when user press android back button or quit
    print('deactivate');
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);

    //this method not called when user press android back button or quit
    print('dispose');
  }
  */
  @override
  Widget build(BuildContext context) {
    Utilities.begin("Inscription/build");
    if (widget.entreprise != null) {
      _controllerNomEntrep.text = widget.entreprise.nom;
    }

    // TODO: implement build
    return Scaffold(
      drawer: widget.mainDrawer,
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(widget.titreWidget),
          appBar: AppBar()),
      body: Stepper(
        steps: _mySteps(),
        currentStep: _currentStep,
        onStepTapped: (step) {
          print(step);
          setState(() {
            // TODO: à revoir
            if (_formKeyEntreprise.currentState.validate()) {
              _currentStep = step;
            }

            //_currentStep = step;
          });
        },
        onStepCancel: () {
          Utilities.getToast("onStepCancel");
          setState(() {
            if (_currentStep > 0) {
              _currentStep--;
            }
          });
        },
        onStepContinue: () {
          Utilities.getToast("onStepContinue");

          setState(() {
            if (_currentStep < 2 - 1) {
              // verifions le formulaire de l'entreprise

              if (_formKeyEntreprise.currentState.validate()) {
                _formKeyEntreprise.currentState.save();
                // passons à l'etape suivante
                _currentStep++;
              } else {
                _autovalidate = true;
              }
            } else {
              // verifions le formulaire de l'utilisateur
              if (_formKeyUtilisateur.currentState.validate()) {
                // permet d'executer toutes les validator des TextFormField
                _formKeyUtilisateur.currentState.save();
                submit();
              } else {
                _autovalidate = true;
              }

              // passons à la soumission
            }
          });
        },
      ),
    );
  }

  List<Step> _mySteps() {
    List<Step> mySteps = [
      // le step de l'entreeprise
      Step(
        title: Text("Entreprise"),
        subtitle: Text(sousTitreEntreprise),
        isActive: _currentStep >= 0,
        content: Form(
            // mettre la condition si les datas sont disponible
            autovalidate: _autovalidate,
            key: _formKeyEntreprise,
            child: Column(
              children: <Widget>[
                /*
                    Utilities.textFormFieldNom(_entreprise,
                        labelText: nom, controller: _controller),
                    Utilities.emailFormField(_entreprise, labelText: email),
                    Utilities.phoneFormField(_entreprise, labelText: phone, controller: _controller),
                    Utilities.passwordFormField(_entreprise, labelText: password, controller: _controller),
                    Utilities.passwordFormField(_entreprise, labelText: confirmer, controller: _controller),
                    */
                MyTextFormField(
                  controller: _controllerNomEntrep,
                  decoration: InputDecoration(labelText: nom),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.nom = input;
                  },
                  enabled: isEnableWidget(),
                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerEmailEntrep,
                  decoration: InputDecoration(labelText: email),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.email = input;
                  },
                  enabled: isEnableWidget(),

                  //controller: controllerLibelle,
                ),
                MyTextFormField(
                  controller: _controllerPhoneEntrep,
                  decoration: InputDecoration(labelText: phone),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataEntrepise.telephone = input;
                  },
                  enabled: isEnableWidget(),
                  //controller: controllerLibelle,
                ),






              (isEnableWidget()) ?
              DropdownSearch<User>(
                
                mode: Mode.BOTTOM_SHEET,
                showSelectedItem: true,
                compareFn: (User i, User s) => i.isEqual(s),
                label: LPays,
                onFind: (String filter) => getData(filter: filter,
                                        baseUrl: AppConfig.of(context).apiBaseUrl,
               ),
                onChanged: (User data) {
                  print(data);
                  setState(() {
                    currentPays = data;
                    if (data == null) {
                      currentVille = null;
                    }
                  });
                },
                validator: (value) => Utilities.validatorObject(value),
                selectedItem: currentPays,
                showClearButton: true,
                showSearchBox: true,
                searchBoxDecoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                  labelText: searchHintPays,
                ),
                popupTitle: Container(
                  height: 50,
                  decoration: Utilities.getBoxDecoration(),
                  child: Center(
                    child: Text(
                      LPays,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                popupShape: Utilities.getRoundedRectangleBorder(),
                emptyBuilder: (context) => Utilities.dataNotFound(),
                //dropdownBuilder: _customDropDownExample,
                //popupItemBuilder: _customPopupItemBuilderExample2,
              ) : MyTextFormField(
                        initialValue: widget.entreprise.paysLibelle,
                        decoration: InputDecoration(labelText: LPays),
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      ),

              Divider(),

              (isEnableWidget()) ?
              DropdownSearch<User>(
                
                mode: Mode.BOTTOM_SHEET,
                showSelectedItem: true,
                compareFn: (User i, User s) => i.isEqual(s),
                label: LVille,
                onFind: (String filter) => getData(
                    filter: (currentPays != null)
                        ? currentPays.id.toString()
                        : filter,
                        baseUrl: AppConfig.of(context).apiBaseUrl,
                    isVille: true),
                onChanged: (User data) {
                  //print(data);
                  currentVille = data;
                },
                onSaved: (value) {
                  if (value != null) {
                    //widget.model.villeId = value.id;
                    dataEntrepise.villeId = value.id;
                  }
                },
                validator: (value) => Utilities.validatorObject(value),
                selectedItem: currentVille,
                showClearButton: true,
                showSearchBox: true,
                searchBoxDecoration: InputDecoration(
                  border: OutlineInputBorder(),
                  contentPadding: EdgeInsets.fromLTRB(12, 12, 8, 0),
                  labelText: searchHintVille,
                ),
                popupTitle: Container(
                  height: 50,
                  decoration: Utilities.getBoxDecoration(),
                  child: Center(
                    child: Text(
                      LVille,
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                popupShape: Utilities.getRoundedRectangleBorder(),
                emptyBuilder: (context) => Utilities.dataNotFound(),

                //dropdownBuilder: _customDropDownExample,
                //popupItemBuilder: _customPopupItemBuilderExample2,
              )
              :MyTextFormField(
                        initialValue: widget.entreprise.villeLibelle,
                        decoration: InputDecoration(labelText: LVille),
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      ),
              Divider(),





/*
                (isEnableWidget())
                    //DropdownButton
                    ? DropdownButtonFormField(
                        isDense: true,
                        //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                        items: _dropdownMenuItemsPays,
                        onChanged: onChangeDropdownItemPays,
                        value: _itemPaysSelected,
                        validator: (value) => Utilities.validatorObject(value,
                            typeContentToValidate: v_text),
                        decoration: InputDecoration(
                          labelText: LPays,
                          //helperText: 'test'
                        ),
                      )
                    : MyTextFormField(
                        initialValue: widget.entreprise.paysLibelle,
                        decoration: InputDecoration(labelText: LPays),
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      ),

                (isEnableWidget())
                    ? DropdownButtonFormField(
                        isDense: true,

                        //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                        items: _dropdownMenuItemsVille,
                        onChanged: onChangeDropdownItemVille,
                        value: _itemVilleSelected,
                        validator: (value) => Utilities.validatorObject(value,
                            typeContentToValidate: v_text),
                        decoration: InputDecoration(
                          labelText: LVille,
                          //helperText: 'test'
                        ),
                        onSaved: (input) {
                          dataEntrepise.villeId = input;
                        },
                      )
                    : MyTextFormField(
                        initialValue: widget.entreprise.villeLibelle,
                        decoration: InputDecoration(labelText: LVille),
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      ),

*/









                /*
                      SizedBox(
                height: 20,
              ), // pour mettre un espace verticalement
              SearchableDropdown.single(
                // la fonction de recherche customiser
                searchFn: (keyword, items) =>
                    Utilities.searchFn(keyword, items),

                items: _dmiPays,
                value: _itemPaysSelected,
                hint: hintPays,
                searchHint: searchHintPays,
                onChanged: onChangeDropdownItemPays,

                dialogBox: false,
                isExpanded: true,
                menuConstraints: BoxConstraints.tight(Size.fromHeight(350)),
              ),

              SearchableDropdown.single(
                searchFn: (keyword, items) =>
                    Utilities.searchFn(keyword, items),
                items: _dmiVille,
                value: _itemVilleSelected,
                hint: hintVille,
                searchHint: searchHintVille,
                dialogBox: false,
                isExpanded: true,
                menuConstraints: BoxConstraints.tight(Size.fromHeight(350)),
                validator: (value) => Utilities.validatorObject(value,
                    typeContentToValidate: v_text),
                onChanged: onChangeDropdownItemVille,
              ),
                      */

/*
                      DropDownFormField(
                        //isDense: true,
                  
                  filled: false,
                  validator:(value){
                    if (value == null) {
                          return "message";
                        }
                  } , 
                  autovalidate: true,
                  titleText: 'My workout',
                  hintText: 'Please choose one',
                  value: _myActivity,
                  onSaved: (value) {
                    setState(() {
                      _myActivity = value;
                    });
                  },
                  onChanged: (value) {
                    setState(() {
                      _myActivity = value;
                    });
                  },
                  dataSource: [
                    {
                      "display": "Running",
                      "value": "Running",
                    },
                    {
                      "display": "Climbing",
                      "value": "Climbing",
                    },
                    
                  ],
                  textField: 'display',
                  valueField: 'value',
                ),
              
*/
                (isEnableWidget())
                    ? DropdownButtonFormField(
                        isDense: true,

                        //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                        items: _dropdownMenuItemsTypeEntreprise,
                        onChanged: onChangeDropdownItemTypeEntreprise,
                        value: _itemTypeEntrepriseSelected,
                        validator: (value) => Utilities.validatorObject(value,
                            typeContentToValidate: v_text),
                        decoration: InputDecoration(
                          labelText: LTypeEntreprise,
                          //helperText: 'test'
                        ),
                        onSaved: (input) {
                          dataEntrepise.typeEntrepriseId = input;
                        },
                      )
                    : MyTextFormField(
                        initialValue: widget.entreprise.typeEntrepriseLibelle,
                        decoration: InputDecoration(labelText: LTypeEntreprise),
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      ),

                (isEnableWidget())
                    ? DropdownButtonFormField(
                        isDense: true,

                        //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                        items: _dropdownMenuItemsStatutJuridique,
                        onChanged: onChangeDropdownItemStatutJuridique,
                        value: _itemStatutJuridiqueSelected,
                        validator: (value) => Utilities.validatorObject(value,
                            typeContentToValidate: v_text),
                        decoration: InputDecoration(
                          labelText: LStatutJuridique,
                          //helperText: 'test'
                        ),
                        onSaved: (input) {
                          dataEntrepise.statutJuridiqueId = input;
                        },
                      )
                    : MyTextFormField(
                        initialValue: widget.entreprise.statutJuridiqueLibelle,
                        decoration:
                            InputDecoration(labelText: LStatutJuridique),
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      ),

                (isNewEntreprise() ||
                        (isUpdate() &&
                            Utilities.isNotEmpty(_itemsSecteursDactivites) &&
                            !Utilities.isNotEmpty(
                                _initialValueItemsSecteursDactivites)))
                    ? MultiSelectFormField(
                        //autovalidate: true,
                        titleText: ajouterSecteurDactivites,
                        //autovalidate: false,
                        okButtonLabel: okButtonLabel,
                        cancelButtonLabel: cancelButtonLabel,
                        hintText: hintMultiSelectForm,
                        /*initialValue: Utilities.initialValueMultiSelectFormField(
                      initialList: (widget.entreprise != null)
                          ? widget.entreprise.datasSecteurDactivite
                          : null),*/
                        //initialValue: _initialValueItemsSecteursDactivites,

                        dataSource: _itemsSecteursDactivites,
                        textField: LIBELLE,
                        valueField: LIBELLE,

                        close: () {
                          Utilities.log("close");
                        },
                        open: () {
                          Utilities.log("open");
                          Utilities.log("domainedomainedomainedomainedomaine");
                        },
                        change: () => print("change change change") //;
                        // Utilities.log("domainedomainedomainedomainedomaine") ;
                        ,

                        validator: (value) {
                          //if (value == null && value.length == 0) {
                          if (value == null || value.length == 0) {
                            return messageChoixMultiSelectForm;
                          }
                          return null;
                        },
                        onSaved: (value) {
                          /*setState(() {
                      //itemAO.datasSecteurDactiviteAppelDoffres = value;

                      List<User> itemsLocal = List();
                      for (var item in value) {
                        for (var itemsSecteurDactivite
                            in _itemsSecteursDactivites) {
                          if (itemsSecteurDactivite[LIBELLE] == item) {
                            User user = User();
                            user.domaineId = itemsSecteurDactivite[id];
                            user.domaineLibelle =
                                itemsSecteurDactivite[LIBELLE];
                            itemsLocal.add(user);
                            break;
                          }
                        }
                      }
                      dataEntrepise.datasDomaineEntreprise = itemsLocal;
                    });*/
                        },
                      )
                    : (isUpdate() &&
                            Utilities.isNotEmpty(
                                _initialValueItemsSecteursDactivites) &&
                            Utilities.isNotEmpty(_itemsSecteursDactivites))
                        ? MultiSelectFormField(
                            //fillColor: Colors.grey,
                            //autovalidate: true,
                            titleText: ajouterSecteurDactivites,
                            //autovalidate: false,
                            okButtonLabel: okButtonLabel,
                            cancelButtonLabel: cancelButtonLabel,
                            hintText: hintMultiSelectForm,
                            /*initialValue: Utilities.initialValueMultiSelectFormField(
                      initialList: (widget.entreprise != null)
                          ? widget.entreprise.datasSecteurDactivite
                          : null),*/

                            initialValue: _initialValueItemsSecteursDactivites,

                            dataSource: _itemsSecteursDactivites,
                            textField: LIBELLE,
                            valueField: LIBELLE,

                            close: () {
                              Utilities.log("close");
                            },
                            open: () {
                              Utilities.log("open");
                              Utilities.log(
                                  "domainedomainedomainedomainedomaine");
                            },
                            change: () => Utilities.log("change") //;
                            // Utilities.log("domainedomainedomainedomainedomaine") ;
                            ,

                            validator: (value) {
                              //if (value == null && value.length == 0) {
                              if (value == null || value.length == 0) {
                                return messageChoixMultiSelectForm;
                              }
                              return null;
                            },

                            onSaved: (value) {
                              /*setState(() {
                      //itemAO.datasSecteurDactiviteAppelDoffres = value;

                      List<User> itemsLocal = List();
                      for (var item in value) {
                        for (var itemsSecteurDactivite
                            in _itemsSecteursDactivites) {
                          if (itemsSecteurDactivite[LIBELLE] == item) {
                            User user = User();
                            user.domaineId = itemsSecteurDactivite[id];
                            user.domaineLibelle =
                                itemsSecteurDactivite[LIBELLE];
                            itemsLocal.add(user);
                            break;
                          }
                        }
                      }
                      dataEntrepise.datasDomaineEntreprise = itemsLocal;
                    });*/
                            },
                          )
                        : Container(),
                isNewEntreprise()
                    ? MyTextFormField(
                        controller: _controllerPasswordEntrep,
                        decoration: InputDecoration(labelText: password),
                        validator: (value) {
                          Utilities.validator(value,
                              typeContentToValidate: v_text);
                        },
                        onSaved: (input) {
                          dataEntrepise.password = input;
                        },
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      )
                    : Container(),
                isNewEntreprise()
                    ? MyTextFormField(
                        controller: _controllerConfirmPasswordEntrep,
                        decoration: InputDecoration(labelText: confirmer),
                        validator: (value) {
                          Utilities.validator(value,
                              typeContentToValidate: v_text);
                        },
                        onSaved: (input) {
                          dataEntrepise.libelle = input;
                        },
                        enabled: isEnableWidget(),
                        //controller: controllerLibelle,
                      )
                    : Container(),

                //*/
              ],
            )),
      ),

      // le step de l'utilisateur

      Step(
        title: Text("Utilisateur"),
        subtitle: Text(sousTitreUtilisateur),
        isActive: _currentStep >= 1,
        content: Form(
            autovalidate: _autovalidate,
            key: _formKeyUtilisateur,
            child: Column(
              children: <Widget>[
                /*
                    Utilities.textFormFieldNom(_entreprise,
                        labelText: "$nom & $prenoms", controller: _controller),
                    Utilities.emailFormField(_entreprise, labelText: email),
                    Utilities.phoneFormField(_entreprise, labelText: phone, controller: _controller),
                    Utilities.textFormFieldNom(_entreprise,
                        labelText: "$nom & $prenoms", controller: _controller),
                    Utilities.emailFormField(_entreprise, labelText: email, controller: _controller),
                    Utilities.phoneFormField(_entreprise, labelText: phone),
                    */
                MyTextFormField(
                  controller: _controllerNomUser,
                  decoration: InputDecoration(labelText: nom),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.nom = input;
                  },
                  //controller: controllerLibelle,
                ),
                isUpdate()
                    ? MyTextFormField(
                        controller: _controllerPrenomUser,
                        decoration: InputDecoration(labelText: prenoms),
                        validator: (value) {
                          Utilities.validator(value,
                              typeContentToValidate: v_text);
                        },
                        onSaved: (input) {
                          dataUser.prenom = input;
                        },
                        //controller: controllerLibelle,
                      )
                    : Container(),
                MyTextFormField(
                  controller: _controllerEmailUser,
                  decoration: InputDecoration(labelText: email),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.email = input;
                  },
                  //controller: controllerLibelle,
                ),
                (isNewUser() || isNewEntreprise())
                    ? MyTextFormField(
                        controller: _controllerLoginUser,
                        decoration: InputDecoration(labelText: login),
                        validator: (value) {
                          Utilities.validator(value,
                              typeContentToValidate: v_text);
                        },
                        onSaved: (input) {
                          dataUser.login = input;
                        },
                        //controller: controllerLibelle,
                      )
                    : Container(),
                MyTextFormField(
                  controller: _controllerPhoneUser,
                  decoration: InputDecoration(labelText: phone),
                  validator: (value) {
                    Utilities.validator(value, typeContentToValidate: v_text);
                  },
                  onSaved: (input) {
                    dataUser.telephone = input;
                  },
                  //controller: controllerLibelle,
                ),
                DropdownButtonFormField(
                  isDense: true,

                  //items: (Utilities.isNotEmpty(_dropdownMenuItems) ? _dropdownMenuItems : _dropdownMenuItems),
                  items: _dropdownMenuItemsRole,
                  onChanged: onChangeDropdownItemRole,
                  value: _itemRoleSelected,
                  validator: (value) => Utilities.validatorObject(value,
                      typeContentToValidate: v_text),
                  decoration: InputDecoration(
                    labelText: fonction,
                    //helperText: 'test'
                  ),
                  onSaved: (input) {
                    dataUser.roleId = input;
                  },
                ),
                (isNewUser() || isNewEntreprise())
                    ? MyTextFormField(
                        controller: _controllerPasswordUser,
                        decoration: InputDecoration(labelText: password),
                        validator: (value) {
                          Utilities.validator(value,
                              typeContentToValidate: v_text);
                        },
                        onSaved: (input) {
                          dataUser.password = input;
                        },

                        //controller: controllerLibelle,
                      )
                    : Container(),
                (isNewUser() || isNewEntreprise())
                    ? MyTextFormField(
                        controller: _controllerConfirmPasswordUser,
                        decoration: InputDecoration(labelText: confirmer),
                        validator: (value) {
                          Utilities.validator(value,
                              typeContentToValidate: v_text);
                        },
                        onSaved: (input) {
                          dataUser.libelle = input;
                        },
                        //onChanged: , comparer les deux valeurs
                        //controller: controllerLibelle,
                      )
                    : Container(),
              ],
            )),
      )
    ];

    return mySteps;
  }

  Future<List<User>> getData({String filter, String baseUrl, bool isVille = false}) async {
    print("getData filter $filter");
    print("getData isVille $isVille");
    RequestCustom request = RequestCustom();
    User data = User();
    List<User> models;
    String endpoint = endpointGetPays;

    if (Utilities.isNotBlank(filter)) {
      data.paysId = int.parse(filter);
      endpoint = ville + getByCriteria;
    } else {
      if (isVille) {
        return models;
      }
    }

    request.data = data;
    var res = await baseApi.testApi(
        apiBaseUrl: widget.baseUrl + endpoint, request: request);

    if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
      models = User.fromJsons(res.items);
    }
    return models;
  }
  /*
  onChangeDropdownItem({dynamic itemSelected, String typeItem}) {
    setState(() {
      print("$typeItem azertyuiopazertyuio $itemSelected");
      switch (typeItem) {
        case LPays:
          _itemPaysSelected = itemSelected;
          break;
        case LTypeEntreprise:
          _itemTypeEntrepriseSelected = itemSelected;
          break;
        case fonction:
          _itemRoleSelected = itemSelected;
          break;
        case LStatutJuridique:
          _itemStatutJuridiqueSelected = itemSelected;
          break;
        case LVille:
          _itemVilleSelected = itemSelected;
          break;
        default:
          break;
      }
    });
  }

  onChangeDropdownItemPays(dynamic itemSelected) {
    print("****************************Pays*************** $itemSelected");
    setState(() {
      _itemPaysSelected = itemSelected;

      //_dropdownMenuItemsVille =  List() ;
      _dropdownMenuItemsVille.clear(); // vider la liste des villes

      // NEW
      RequestCustom req = RequestCustom();
      User data = User();
      data.paysLibelle = _itemPaysSelected;
      req.data = data;
      String baseUrl = AppConfig.of(context).apiBaseUrl;
      loadDataForDropdownMenu(
          request: req,
          uri: baseUrl + ville + getByCriteria,
          dropdownMenuItems: _dropdownMenuItemsVille,
          keySwitch: ville);

      /* OLD
      // load data ville in _dropdownMenuItemsVille
      if (Utilities.isNotEmpty(_itemPaysSelected.datasVille)) {
        for (var item in _itemPaysSelected.datasVille) {
          User user = User.fromJson(item);
          _dropdownMenuItemsVille.add(DropdownMenuItem(
            child: Text(user.libelle),
            value: user,
          ));
        }

        Utilities.log(_dropdownMenuItemsVille.length);
      }
      */
    });
  }
  */

  onChangeDropdownItemTypeEntreprise(dynamic itemSelected) {
    print(
        "****************************TypeEntreprise*************** $itemSelected");
    setState(() {
      _itemTypeEntrepriseSelected = itemSelected;

      // load data role in _dropdownMenuItemsRole
      User basicData = User();
      User data = User();
      User dataTypeEntreprise = User();
      List<User> datas = List();
      basicData.code = ADMINISTRATEUR;
      //basicData.codeParam = {"operator": "<>"} ;
      basicData.codeParam = operatorDifferent;

      String baseUrl = AppConfig.of(context).apiBaseUrl;
      dataTypeEntreprise.id = _itemTypeEntrepriseSelected;

      RequestCustom req = RequestCustom();
      req.data = dataTypeEntreprise;
      baseApi
          .testApi(
              apiBaseUrl: baseUrl + endpointGetTypeEntreprise, request: req)
          .then((res) {
        print(res);
        setState(() {
          if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
            dataTypeEntreprise = User.fromJson(res.items[0]);

            if (dataTypeEntreprise.code == FOURNISSEUR) {
              data.code = FOURNISSEUR;
              datas.add(data);
            } else {
              if (dataTypeEntreprise.code == CLIENTE) {
                data.code = FOURNISSEUR;
                data.codeParam = operatorDifferent;
                //data.codeParam = {"operator": "<>"} ;
                datas.add(data);
              }
            }
            req.data = basicData;
            if (Utilities.isNotEmpty(datas)) {
              req.datas = datas;
            }

            req.isAnd = true; // tres important afin de bien filtrer le get
            loadDataForDropdownMenu(
                request: req,
                uri: baseUrl + endpointGetRole,
                dropdownMenuItems: _dropdownMenuItemsRole,
                keySwitch: role);
          }
        });
      }).catchError((err) {
        print(err);
      });
    });
  }

  onChangeDropdownItemStatutJuridique(dynamic itemSelected) {
    print(
        "****************************StatutJuridique************* $itemSelected");

    setState(() {
      _itemStatutJuridiqueSelected = itemSelected;
    });
  }
/*
  onChangeDropdownItemVille(dynamic itemSelected) {
    print("****************************Ville************* $itemSelected");

    setState(() {
      _itemVilleSelected = itemSelected;
    });
  }
  */

  onChangeDropdownItemRole(dynamic itemSelected) {
    print("****************************Role************* $itemSelected");
    setState(() {
      _itemRoleSelected = itemSelected;
    });
  }

  bool isEnableWidget() {
    return (!widget.isNewUser || (isUpdate() && isNotSuperUser()));
  }

  bool isVisibleWidget() {
    return !(widget.isNewUser || widget.isNewEntreprise);
  }

  bool isNewEntreprise() {
    return (widget.isNewEntreprise != null && widget.isNewEntreprise);
  }

  bool isNewUser() {
    return (widget.isNewUser != null && widget.isNewUser);
  }

  bool isUpdate() {
    return (widget.isUpdate != null && widget.isUpdate);
  }

  bool isSuperUser() {
    return (widget.entreprise != null &&
        widget.utilisateur != null &&
        widget.entreprise.createdBy == widget.utilisateur.id);
  }

  bool isNotSuperUser() {
    return (widget.entreprise != null &&
        widget.utilisateur != null &&
        widget.entreprise.createdBy != widget.utilisateur.id);
  }

  // initialisation des valeurs par defaut
  initializeUpdate({String url}) async {
    Utilities.begin("Inscription/initializeUpdate");
    if (widget.entreprise != null) {
      User entrepriseUser = widget.entreprise;

      String baseUrl = AppConfig.of(context).apiBaseUrl;

      // setter tout les controller
      _controllerConfirmPasswordEntrep.text = entrepriseUser.password;
      _controllerPhoneEntrep.text = entrepriseUser.telephone;
      _controllerPasswordEntrep.text = entrepriseUser.password;
      _controllerNomEntrep.text = entrepriseUser.nom;
      _controllerEmailEntrep.text = entrepriseUser.email;

      if (isUpdate()) {
        // setter les champs non obligatoire
        //_itemPaysSelected = entreprise.paysLibelle ;
        _itemStatutJuridiqueSelected = entrepriseUser.statutJuridiqueId;
        _itemVilleSelected = entrepriseUser.villeId;
      }

      onChangeDropdownItemTypeEntreprise(widget.entreprise.typeEntrepriseId);
      onChangeDropdownItemPays(widget.entreprise.paysLibelle);

      RequestCustom request = RequestCustom();

      User data = User();
      data.id = entrepriseUser.id;
      request.data = data;
      //Utilities.log("_initialValueItemsSecteursDactivites $_initialValueItemsSecteursDactivites") ;

      // get entreprise
      baseApi
          .testApi(
              apiBaseUrl: baseUrl + entreprise + getByCriteria,
              request: request)
          .then((res) {
        print(res);
        //setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          User itemEntreprise = User();
          itemEntreprise = User.fromJson(res.items[0]);
          List<dynamic> initialValueItemsSecteursDactivites;
          if (Utilities.isNotEmpty(itemEntreprise.datasDomaine)) {
            List<User> datasDomaine =
                User.fromJsons(itemEntreprise.datasDomaine);
            initialValueItemsSecteursDactivites = datasDomaine.map((data) {
              return data.libelle;
            }).toList();
            setState(() {
              _initialValueItemsSecteursDactivites =
                  initialValueItemsSecteursDactivites;
            });
          }

          request.data = User();
          baseApi
              .testApi(
                  apiBaseUrl: baseUrl + domaine + getByCriteria,
                  request: request)
              .then((res) {
            print(res);
            if (res != null &&
                !res.hasError &&
                Utilities.isNotEmpty(res.items)) {
              setState(() {
                _itemsSecteursDactivites = res.items;
              });
              Utilities.log(
                  "_itemsSecteursDactivites _itemsSecteursDactivites _itemsSecteursDactivites $_itemsSecteursDactivites");
            }
          }).catchError((err) {
            print(err);
          });
          //Utilities.log("_itemsSecteursDactivites $_itemsSecteursDactivites") ;
          Utilities.log(
              "_initialValueItemsSecteursDactivites $_initialValueItemsSecteursDactivites");

          //_itemsSecteursDactivites = res.items;
        }
        //});
      }).catchError((err) {
        print(err);
      });
    }

    if (widget.utilisateur != null) {
      User utilisateur = widget.utilisateur;

      // setter tout les controller du user
      _controllerConfirmPasswordUser.text = utilisateur.password;
      _controllerPhoneUser.text = utilisateur.telephone;
      _controllerPasswordUser.text = utilisateur.password;
      _controllerNomUser.text = utilisateur.nom;
      _controllerEmailUser.text = utilisateur.email;
      _controllerLoginUser.text = utilisateur.login;

      if (isUpdate()) {
        // setter les champs non obligatoire
        _itemRoleSelected = utilisateur.roleId;
      }
    }
  }

  // initialisation des datas
  initialize({String url}) async {
    Utilities.begin("initialize");

    RequestCustom request = RequestCustom();
    //String uri = apiBaseUrl + endpointObjetNousContacter;

    //String uri = base_url_prod + endpointObjetNousContacter;
    String baseUrl = AppConfig.of(context).apiBaseUrl;
    request.data = User();

    loadDataForDropdownMenu(
        request: request,
        uri: baseUrl + endpointGetStatutJuridique,
        dropdownMenuItems: _dropdownMenuItemsStatutJuridique,
        keySwitch: statutJuridique);
    loadDataForDropdownMenu(
        request: request,
        uri: baseUrl + endpointGetTypeEntreprise,
        dropdownMenuItems: _dropdownMenuItemsTypeEntreprise,
        keySwitch: typeEntreprise);
    /*loadDataForDropdownMenu(
        request: request,
        uri: baseUrl + endpointGetPays,
        dropdownMenuItems: _dropdownMenuItemsPays,
        keySwitch: pays);*/

    baseApi
        .testApi(apiBaseUrl: baseUrl + endpointGetPays, request: request)
        .then((res) {
      print(res);
      setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItemsPays.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user.libelle,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });
    if (widget.entreprise == null) {
      baseApi
          .testApi(
              apiBaseUrl: baseUrl + domaine + getByCriteria, request: request)
          .then((res) {
        print(res);
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          setState(() {
            _itemsSecteursDactivites = res.items;
          });
          Utilities.log(
              "_itemsSecteursDactivites _itemsSecteursDactivites _itemsSecteursDactivites $_itemsSecteursDactivites");
        }
      }).catchError((err) {
        print(err);
      });
    }

    /*
     baseApi
        .testApi(
            apiBaseUrl: baseUrl + domaine + getByCriteria, request: request)
        .then((res) {
      print(res);
      setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          _itemsSecteursDactivites = res.items;
        }
      });
    }).catchError((err) {
      print(err);
    });
    */

    /*
    loadDataForDropdownMenu(request: request, uri : baseUrl + endpointGetRole , dropdownMenuItems: _dropdownMenuItemsPays);

    
    baseApi.testApi(apiBaseUrl: baseUrl + endpointGetTypeEntreprise, request: request).then((res) {
      print(res);
      setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItemsTypeEntreprise.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });

    baseApi.testApi(apiBaseUrl:  baseUrl + endpointGetStatutJuridique, request: request).then((res) {
      print(res);
      setState(() {

        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            print("endpointGetStatutJuridique: $element");
            _dropdownMenuItemsStatutJuridique.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });

    baseApi.testApi(apiBaseUrl:  baseUrl + endpointGetRole, request: request).then((res) {
      print(res);
      setState(() {
        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItemsRole.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });
    baseApi.testApi(apiBaseUrl:  baseUrl + endpointGetPays, request: request).then((res) {
      print(res);
      setState(() {
        _dropdownMenuItemsPays = List() ;

        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            _dropdownMenuItemsPays.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user,
            ));
          }
        }
      });
    }).catchError((err) {
      print(err);
    });
    */
  }

  loadDataForDropdownMenu(
      {RequestCustom request,
      String uri,
      List dropdownMenuItems,
      String keySwitch}) {
    baseApi.testApi(apiBaseUrl: uri, request: request).then((res) {
      print(res);
      setState(() {
        //dropdownMenuItems = List() ; fait un changement de reference
        dropdownMenuItems.clear(); // vider la liste

        // recuperer la cle du switch

        if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
          for (var element in res.items) {
            User user = User.fromJson(element);
            dropdownMenuItems.add(DropdownMenuItem(
              child: Text(user.libelle),
              value: user.id,
            ));
          }
          /*
          if (isUpdate()) {
            // setter les listes deroulantes par leurs valeurs initiales
            String roleLibelle = widget.utilisateur.roleLibelle;
            String typeEntrepriseLibelle =
                widget.entreprise.typeEntrepriseLibelle;
            String statutJuridiqueLibelle =
                widget.entreprise.statutJuridiqueLibelle;
            String villeLibelle = widget.entreprise.villeLibelle;
            String paysLibelle = widget.entreprise.paysLibelle;
            for (var element in res.items) {
              User user = User.fromJson(element);
              dropdownMenuItems.add(DropdownMenuItem(
                child: Text(user.libelle),
                value: user,
              ));

              switch (keySwitch) {
                case role:
                  if (user.libelle == roleLibelle) {
                    _itemRoleSelected = user;
                  }
                  break;
                case statutJuridique:
                  if (user.libelle == statutJuridiqueLibelle) {
                    //_itemStatutJuridiqueSelected = user;
                  }
                  break;
                case typeEntreprise:
                  if (user.libelle == typeEntrepriseLibelle) {
                    _itemTypeEntrepriseSelected = user;
                    onChangeDropdownItemTypeEntreprise(user);
                  }
                  break;
                case pays:
                  if (user.libelle == paysLibelle) {
                    _itemPaysSelected = user;
                    onChangeDropdownItemPays(user);
                  }
                  break;
                case ville:
                  if (user.libelle == villeLibelle) {
                    _itemVilleSelected = user;
                  }
                  break;
                default:
              }
            }
          } else {
            for (var element in res.items) {
              User user = User.fromJson(element);
              dropdownMenuItems.add(DropdownMenuItem(
                child: Text(user.libelle),
                value: user,
              ));
            }
          }
          */
          // prevoir la rubrique qui va setter les datas des lites deroulantes
        }
      });
    }).catchError((err) {
      print(err);
    });
  }

  // soumission du formulaire
  void submit() async {
    if (_formKeyUtilisateur.currentState.validate() &&
        _formKeyEntreprise.currentState.validate()) {
      // permet d'executer toutes les validator des TextFormField
      _formKeyUtilisateur.currentState
          .save(); // permet d'executer toutes les onSaved des TextFormField
      _formKeyEntreprise.currentState
          .save(); // permet d'executer toutes les onSaved des TextFormField

      // pour les cas de MAJ ou ajout de newUser
      if (widget.entreprise != null) {
        dataEntrepise.id = widget.entreprise.id;
      }
      if (widget.utilisateur != null) {
        dataUser.id = widget.utilisateur.id;
      }

      dataUser.dataEntreprise = dataEntrepise;
      List<User> datas = List();
      print("_itemSelected du form");
      print("Content du form");

      datas.add(dataUser);

      print("_itemSelected du form");
      print("Content du form");
      print(dataUser.toJson());

      // appel de service
      var request = RequestCustom();
      request.user = dataUser.id;
      request.datas = datas;
      String baseUrl = AppConfig.of(context).apiBaseUrl;

      var uri = baseUrl + user + (isUpdate() ? update : create);
      ResponseCustom response =
          await baseApi.testApi(apiBaseUrl: uri, request: request);
      //ResponseCustom response =  baseApi.testApiCustom(apiBaseUrl: uri, request: request);
      print("Future<ResponseCustom> ${response.hasError}");
      if (response != null) {
        if (!response.hasError) {
          Utilities.messageApi(response: response);
          // vider le formmualaire
          //_controllerEmail.clear();
          clearForm();
          // redirection à l'accueil
          Utilities.navigate(context, MyApp());

          // redirection sur la page d'inscription reussi
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: Text((isUpdate()) ? majReussi : inscriptionReussi),
                content: Text(
                  (isUpdate()) ? messageMAJProfil : messageApresInscription,
                  textAlign: TextAlign.justify,
                ),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  FlatButton(
                    child: new Text("OK"),
                    onPressed: () {
                      Navigator.of(context).pop();
                      //Utilities.onSelectDrawerItem(context: context, view: Offres());
                    },
                  ),
                ],
              );
            },
          );
        } else {
          //Utilities.navigate(context, InscriptionReussi()) ;

          Utilities.messageApi(response: response);
        }
      } else {
        Utilities.messageApi();
      }
    }
  }

  clearForm() {
    Utilities.clearForm([
      _controllerConfirmPasswordEntrep,
      _controllerPhoneEntrep,
      _controllerPasswordEntrep,
      _controllerNomEntrep,
      _controllerEmailEntrep
    ]);
    Utilities.clearForm([
      _controllerConfirmPasswordUser,
      _controllerPhoneUser,
      _controllerPasswordUser,
      _controllerNomUser,
      _controllerEmailUser,
      _controllerLoginUser
    ]);

    // vider les datas du formulaire
    _itemPaysSelected = null;
    _itemRoleSelected = null;
    _itemStatutJuridiqueSelected = null;
    _itemTypeEntrepriseSelected = null;
    _itemVilleSelected = null;
  }
}
*/*/