//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';


class InscriptionReussi extends StatelessWidget{

  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text(titreWidgetApresInscription),) ,
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Text(messageApresInscription, textAlign: TextAlign.center,)
        ),
      ),
      );
  }

}