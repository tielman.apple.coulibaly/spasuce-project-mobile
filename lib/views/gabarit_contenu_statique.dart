import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';



class GabaritContenuStatique extends StatelessWidget{


  Widget contenu ;
  String titreContenu ;
  bool isWidgetContentAppBar = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: (isWidgetContentAppBar != null && isWidgetContentAppBar ) ? null : AppBar(title: Text(titreContenu),backgroundColor: ConstApp.color_app),
      body: contenu,
    );
  }

  GabaritContenuStatique({this.contenu, this.isWidgetContentAppBar,this.titreContenu});

}