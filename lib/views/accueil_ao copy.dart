/*import 'package:flutter/material.dart';
import 'package:json_table/json_table.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/models/user.dart';

class AccueilAO extends StatefulWidget {
  @override
  _AccueilAOState createState() => _AccueilAOState();
}

class _AccueilAOState extends State<AccueilAO> {
  List<User> datas;
  List<User> datas2;
  List<dynamic> datasJsonTable;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    datas = [
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
    ];
    datas2 = [
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1"),
     ];

     datasJsonTable = [
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1").toJson(),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1").toJson(),
      User(
          libelle: "User 1",
          code: "Code 1",
          etatCode: "Etat 1",
          telephone: "Tel 1").toJson(),
     ];
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //return Center(child: Text(ConstApp.appels_doffres));

    // liste des informations
    return Scaffold(
        appBar: AppBar(
          title: Text(ConstApp.appels_doffres),
        ),
        body: ListView(children: <Widget>[
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: datasBodyTable(),
              ),
              //JsonTable(datasJsonTable,showColumnToggle: true,)
              ]
              )
              ])
              
              );

    // les actions des informations
  }

  DataTable datasBody() {
    return DataTable(

        columns: [
          DataColumn(
              label: Text("Libelle 1"),
              numeric: false,
              tooltip: "Valeur par def."),
          DataColumn(
              label: Text("Libelle 1"),
              numeric: false,
              tooltip: "Valeur par def."),
          DataColumn(
              label: Text("Libelle 1"),
              numeric: false,
              tooltip: "Valeur par def."),
          DataColumn(
              label: Text("Tel 1"),
              numeric: false,
              tooltip: "Valeur par def.")
        ],
        rows: datas2
            .map((user) => DataRow(cells: [
                  DataCell(Text(user.code)),
                  DataCell(Text(user.libelle)),
                  DataCell(Text(user.etatCode)),
                  DataCell(Text(user.telephone)),
                ]))
            .toList());
  }

  Table datasBodyTable() {
    return Table(
                border: TableBorder.all(),
                children: [
                  TableRow( children: [
                    Column(children:[
                      Icon(Icons.account_box, size: 12.0,),
                      Text('My Account')
                    ]),
                    Column(children:[
                      Icon(Icons.settings, size: 12.0,),
                      Text('Settings')
                    ]),
                    Column(children:[
                      Icon(Icons.lightbulb_outline, size: 12.0,),
                      Text('Ideas')
                    ]),
                    Column(children:[
                      Icon(Icons.lightbulb_outline, size: 12.0,),
                      Text('Ideas')
                    ]),
                    Column(children:[
                      Icon(Icons.lightbulb_outline, size: 12.0,),
                      Text('Ideas')
                    ]),
                  ]),
                 TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),TableRow(children: [
                  Text("user.code"),
                  Text("user.libelle"),
                  Text("user.etatCode"),
                  Text("user.telephone"),
                  Text("user.telephone"),
                ]),
                ]
                ) ;
      
      
  }
}
*/