import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';

class ContenuStatiqueOld extends StatefulWidget {
  String typeContenuStatiqueOld;

  List<dynamic> listContenuStatiqueOld;

  ContenuStatiqueOld({this.typeContenuStatiqueOld = ConstApp.presentation});

  @override
  _ContenuStatiqueOldState createState() => _ContenuStatiqueOldState();
}

class _ContenuStatiqueOldState extends State<ContenuStatiqueOld> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    /*
    var uri = AppConfig.of(context).apiBaseUrl + "contenuStatique/getByCriteriaCustom";

    RequestCustom request = RequestCustom();

    User data = User(typeContenuStatiqueOldCode: widget.typeContenuStatiqueOld) ;
    request.data = data ;
    Utilities.initialize(request, uri) ;

     */
  }

  @override
  Widget build(BuildContext context) {



    print("buildbuildbuildbuild");
    var uri = AppConfig.of(context).apiBaseUrl +
        "contenuStatique/getByCriteriaCustom";

    List<Step> steps = [];
    RequestCustom request = RequestCustom();
    User data = User(typeContenuStatiqueCode: widget.typeContenuStatiqueOld);
    request.data = data;

    Future<ResponseCustom> response = initialiser(request, uri) ;

    /*
    Future<ResponseCustom> response = Utilities.initialize(request, uri);


      response.then((res) {
        setState(() {
          widget.listContenuStatiqueOld = res.items;
        });
        //print("initStateinitStateinitState");
        print(widget.listContenuStatiqueOld);
        if(Utilities.isNotEmpty(widget.listContenuStatiqueOld)){
          for (var element in widget.listContenuStatiqueOld) {

            print("@@@@@@@@@@@@@@@@@ $element");

            User user = User.fromJson(element);
            print("############### ${user.toJson()}");

            steps.add(
                Step(title: Text(user.titre), content: Text(user.contenu))
            );
          }
          print("@@@@@@@@@@@@@@@@@ initStateinitStateinitState  stepssteps stepssteps stepssteps ${steps.length}");
        }
      });


    print("initStateinitStateinitState ${widget.listContenuStatiqueOld}");
    print("initStateinitStateinitState  stepssteps $steps");



     */

    // TODO: implement build
    //return Center(child: Text(widget.typeContenuStatiqueOld));
    return Container(
      child: ((response != null ) ? Stepper(
          steps: steps, currentStep: 0,
      ) : Center( child: CircularProgressIndicator(),)),
    );
  }

  Future<ResponseCustom> initialiser(RequestCustom request, String uri) async {

    ResponseCustom response = await  Utilities.initialize(request, uri);

    return response ;
  }
}
