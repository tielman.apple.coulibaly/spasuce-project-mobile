import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/const_app.dart' as globals;
import 'package:spasuce/dao/entity/user_entity.dart';
import 'package:spasuce/dao/repository/database_helper.dart';
import 'package:spasuce/dao/repository/database_helper_old_.dart';
import 'package:spasuce/dao/repository/user_repository.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/accueil.dart';

class Connexion extends StatefulWidget {
  MainDrawer mainDrawer;

  @override
  _ConnexionState createState() => _ConnexionState();
  Connexion({this.mainDrawer});
}

class _ConnexionState extends State<Connexion> {
  final _formKey = GlobalKey<FormState>();
  var apiBaseUrl = "";
  var endpointConnexion = "user/connexion";

  var _user = User();

  String _email;

  String _login;

  String _password;

  bool _autovalidate = false;
  bool _dynamicShape = true;

  bool _isDisable = false;

  @override
  Widget build(BuildContext context) {
    apiBaseUrl = AppConfig.of(context).apiBaseUrl;
    //apiBaseUrl = base_url_prod ;

    // TODO: implement build
    return Scaffold(
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(ConstApp.app_name),
          appBar: AppBar()),
      drawer: widget.mainDrawer,
      body: ListView(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            autovalidate: _autovalidate,
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                /*
                      TextFormField(
                        onSaved: (input) { _user.login = input ;}, // recuperation des données saisies
                        validator:(value) => Utilities.validator(value),
                        decoration: InputDecoration(
                            labelText: login,
                            helperText: "test login"
                        ),
                      ),

                      */
                CircleAvatar(
                  child: Image.asset(logoSpasuce),
                ),
                Text(
                  messageBienvenueConnexion,
                  style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                //ImageIcon('images/logo.jpeg'),
                //Utilities.loginFormField(_user, labelText: login),
                //Utilities.passwordFormField(_user, labelText: password),

                TextFormField(
                  onSaved: (input) {
                    _user.login = input.trim();
                  }, // recuperation des données saisies
                  onChanged: (value) {
                    setState(() {
                      _dynamicShape = !_dynamicShape;
                    });
                  },
                  validator: (value) =>
                      Utilities.validator(value, typeContentToValidate: v_text),
                  decoration: InputDecoration(labelText: login),
                  keyboardType: TextInputType.text, // le type du input
                ),
                TextFormField(
                  obscureText: true,
                  onSaved: (input) {
                    _user.password = input.trim();
                  },
                  onChanged: (value) {
                    setState(() {
                      _dynamicShape = !_dynamicShape;
                    });
                  }, // recuperation des données saisies
                  validator: (value) => Utilities.validator(value,
                      typeContentToValidate: v_password),
                  decoration: InputDecoration(labelText: password),
                  keyboardType:
                      TextInputType.visiblePassword, // le type du input
                ),
                /*
                      TextFormField(
                        onSaved: (input) { _user.password = input ;}, // recuperation des données saisies
                        //obscureText: true, // pour rendre un texte invisible
                        validator:(value) => Utilities.validator(value),
                        decoration: InputDecoration(
                            labelText: password,
                            helperText: "test password"
                        ),
                        keyboardType: TextInputType.visiblePassword, // le type du input
                      ),

                       */

                Utilities.getButtonSubmit(
                    shape: (_dynamicShape)
                        ? Utilities.getShape(topLeft: 50, bottomRight: 50)
                        : Utilities.getShape(bottomLeft: 50, topRight: 50),
                    child: Text(connexion),
                    onPressed: _onSubmit,
                    isDisable: _isDisable),
                /*
                RaisedButton(
                  //shape : Utilities.getShape(bottomLeft: 50, topRight: 50) ,
                  shape: (_dynamicShape)
                      ? Utilities.getShape(topLeft: 50, bottomRight: 50)
                      : Utilities.getShape(bottomLeft: 50, topRight: 50),
                  child: Text(connexion),
                  onPressed: _onSubmit,
                ),
                */

                InkWell(
                  child: Text(
                    ConstApp.s_inscrire,
                    style: TextStyle(color: ConstApp.color_app),
                  ),
                  onTap: () {
                    Utilities.onSelectDrawerItem(
                        context: context,
                        view: MyApp(
                          currentIndex: 3,
                        ));
                  },
                )
              ],
            ),
          ),
        ),
      ]),
    );
  }

  void disableButton() {
    setState(() {
      _isDisable = true;
    });
  }

  void enableButton() {
    setState(() {
      _isDisable = false;
    });
  }

  void changeEtat() {
    setState(() {
      _isDisable = !_isDisable;
    });
  }

  //void _onSubmit() {
  void _onSubmit() async {
    if (_formKey.currentState.validate()) {
      // desactivation du boutton  _isDisable
      disableButton();

      // permet d'executer toutes les validator des TextFormField
      _formKey.currentState
          .save(); // permet d'executer toutes les onSaved des TextFormField
      print(_user.toJson());

      // appel de service
      var request = RequestCustom();
      request.data = _user;
      var uri = apiBaseUrl + endpointConnexion;
      ResponseCustom response =
          await baseApi.testApi(apiBaseUrl: uri, request: request);
      //ResponseCustom response =  baseApi.testApiCustom(apiBaseUrl: uri, request: request);
      print("Future<ResponseCustom> ${response.hasError}");
      if (response != null &&
          !response.hasError &&
          Utilities.isNotEmpty(response.items)) {
        //globals.isLoggedIn = true; // montre que le user est connecte
        //globals.userConnecter = response.items[0][id]; // montre que le user est connecte

        Utilities.getToast(connexionReussi);

        print(
            " retroue connexion ${UserEntity.fromJson(response.items[0]).toJson()}");

/*
        UserRepository.addTodo(User.fromJson(response.items[0]));
        UserRepository.getAllTodos().then((res) {
          if (res != null) {
            for (var item in res) {
              Utilities.log("UserRepository nom ${item.name}");
              Utilities.log("UserRepository info ${item.info}");
            }
          } else {
            Utilities.log("UserEntity $res");
          }
        }).catchError((err) => print(err));
*/

        DatabaseHelper.instance.insert(UserEntity.fromJson(response.items[0]));
        //DatabaseHelper.instance.rowInsert(UserEntity.fromJson(response.items[0]));

        DatabaseHelper.instance.getByCriteria().then((res) {
          if (res != null) {
            for (var item in res) {
              Utilities.log("UserEntity ${item.toJson()}");
            }
          } else {
            Utilities.log("UserEntity $res");
          }
        }).catchError((err) => print(err));

        Navigator.pop(context);

        Utilities.navigate(
          context,
          MyApp(
            userConnecter:
                (response != null && Utilities.isNotEmpty(response.items))
                    ? User.fromJson(response.items[0])
                    : null,
            currentIndex: 0,
          ),
        );
      } else {
        Utilities.messageApi(response: response);
        // activation du boutton
        enableButton();
        // Utilities.getToast('connexion echoué!!! Login et/ou pasword incorrect') ;
      }
    } else {
      setState(() {
        _autovalidate = true;
      });
    }
  }
}
