import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/custom_widgets/card_ao.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/views/edit_ao.dart';

class AccueilAO extends StatefulWidget {
  String baseUrl;
  int userClientId;
  MainDrawer mainDrawer;

  @override
  _AccueilAOState createState() => _AccueilAOState();

  AccueilAO({this.baseUrl, this.userClientId, this.mainDrawer});
}

class _AccueilAOState extends State<AccueilAO> {
  //List<User> datas;
  List<User> datas;
  List<User> datas2;
  List<dynamic> datasJsonTable;

  List<Widget> _widget = List();

  bool erreurDeConnexion;
  bool erreurService;
  bool datasEmpty = false;
  bool _isWaiting = true;

  @override
  void initState() {
    // TODO: implement initState
    Utilities.begin("AccueilAO/initState");
    super.initState();
    initialiser();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Utilities.begin("AccueilAO/build");

    //initialiser();
    // liste des informations
    return Scaffold(
      drawer: widget.mainDrawer,
      appBar: MainAppBar(
          //appBar: AppBar(
          title: Text(ConstApp.app_name),
          //title: Text(ConstApp.appels_doffres),
          appBar: AppBar(),
          widgets: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: InkWell(
                child: Icon(Icons.create),
                onTap: () {
                  Utilities.navigate(
                      context, EditAO(userClientId: widget.userClientId));
                },
              ),
            )
          ]),
      body: (_isWaiting)
          ? Center(child: CircularProgressIndicator())
          : (datas != null && datas.length > 0)
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView.builder(
                    itemCount: (Utilities.isNotEmpty(datas) ? datas.length : 0),
                    itemBuilder: (context, index) {
                      return CardAO(model: datas[index]);
                    },
                  ),
                )
              : (datasEmpty)
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          messageAucunAOePourLeUser,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              color: ConstApp.color_app_rouge),
                        ),
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          problemeDeConnexion,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              color: ConstApp.color_app_rouge),
                        ),
                      ),
                    ),
    );
  }

  initialiser() async {
    User data = User();
    data.userClientId = widget.userClientId;
    RequestCustom request = RequestCustom();
    request.isAnd = false;
    request.data = data;

    await baseApi
        .testApi(
            apiBaseUrl: widget.baseUrl + appelDoffres + getByCriteria,
            request: request)
        .then((res) {
      if (res != null && !res.hasError) {
        if (Utilities.isNotEmpty(res.items)) {
          datas = User.fromJsons(res.items);
        } else {
          datasEmpty = true;
          datas = [];
        }
      } else {
        erreurService = true;
        // Utilities.messageApi(response: res); // erreur avec le service
      }
    }).catchError((err) {
      erreurDeConnexion = true;
      // erreur de connexion
    });

    setState(() {
      _isWaiting = false;
    });
  }
}
