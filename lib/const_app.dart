import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
//import 'package:http/http.dart';
import 'package:spasuce/controllers/base_api.dart';
import 'package:spasuce/models/menu.dart';
import 'package:spasuce/network/network_calls.dart';
import 'package:spasuce/views/accueil.dart';

//final client = http.Client();
//final ioc = HttpClient();
//ioc.badCertificateCallback = true;

//ioc.badCertificateCallback = ((X509Certificate cert, String host, int port) => true);
//final http = new IOClient(ioc);
final client = Client();
HttpClient httpClient = new HttpClient();
final netWorkCalls = NetworkCalls();
final baseApi = BaseApi();

//const userConnecter = 26;
int userConnecterX;
const double valueBorderRaduisPopupTitle = 10;

// ************************ BEGIN pour la configuration par environnement

final base_url_prod = "http://207.180.196.73:8080/projet-spasuce-0.1/";
final base_url_preprod = "http://207.180.196.73:8080/projet-spasuce-stg-0.1/";

//final base_url_prod = "https://spasuce.com/api/projet-spasuce-0.1/" ;
//final base_url_preprod = "https://spasuce.com/api/projet-spasuce-stg-0.1/" ;
//final base_url_dev = "http://192.168.1.103:8080/";
//final base_url_dev = "http://192.168.1.102:8080/";
final base_url_dev = "http://192.168.1.101:8080/";
//final base_url_dev = "http://192.168.1.100:8080/" ;
//final base_url_dev = "http://192.168.43.174:8080/" ;

final flavorNameDev = "Development";
final flavorNameProd = "Production";
final flavorNameStg = "Staging";

// ************************ END pour la configuration par environnement

//********************************BEGIN LES ROLES DES USER DE L'APPLICATIONS
const FOURNISSEUR = 'FOURNISSEUR';
const ADMINISTRATEUR = 'ADMINISTRATEUR';
const CLIENTE = 'CLIENTE';
const MIXTE = 'MIXTE';
//********************************END LES ROLES DES USER DE L'APPLICATIONS

// ********************************* BEGIN VARIABLE DE LA BD LOCALES *********************************
// database table and column names
final String tableUserEntitys = 'user_entity';
final String tableUserEntitysTest = 'user_entity_test';
final String columnId = '_id';
final String columnNom = 'nom';
final String columnPrenom = 'prenom';
final String columnTelephone = 'telephone';
final String columnEmail = 'email';
final String columnLogin = 'login';
final String columnIsLogged = 'isLogged';
final String columnRoleLibelle = 'roleLibelle';
final String columnEntrepriseNom = 'entrepriseNom';
final String columnTypeEntrepriseCode = 'typeEntrepriseCode';
final String columnDeleteAt = 'deletedAt';
final String columnDeletedBy = 'deletedBy';
final String columnCreatedBy = 'createdBy';
final String columnCreatedAt = 'createdAt';
final String columnUpdatedAt = 'updatedAt';
final String columnUpdatedBy = 'updatedBy';
final String columnIsDeleted = 'isDeleted';

/*
final String tableUserEntitys = 'user_entity';
final String columnId = '_id';
final String columnNom = 'nom';
final String columnPrenom = 'prenom';
final String columnTelephone = 'telephone';
final String columnEmail = 'email';
final String columnLogin = 'login';
final String columnIsLogged = 'islogged';
final String columnRoleLibelle = 'rolelibelle';
final String columnEntrepriseNom = 'entreprisenom';
final String columnTypeEntrepriseCode = 'typeentreprisecode';
final String columnDeleteAt = 'deletedat';
final String columnDeletedBy = 'deletedby';
final String columnCreatedBy = 'createdby';
final String columnCreatedAt = 'createdat';
final String columnUpdatedAt = 'updatedat';
final String columnUpdatedBy = 'updateday';
final String columnIsDeleted = 'isdeleted';
*/
final String column = 'word';
final String columnFrequency = 'frequency';
// ********************************* END VARIABLE DE LA BD LOCALES *********************************

//*********************************** internalisation ***********************************

// les messages d'erreurs à mettre dans un fichier d'internalisation
const erreurSaisieTexte = "Ce champ ne doit pas etre vide";
const erreurSaisieConfirmPassword = "Ces deux champs doivent être identique.";
const dataEmpty = "Donnée(s) vide";
const libelleDataNotFound = "Aucune donnée retrouvée";
const messageChampObligatoire = "Ce champ est obligatoire";
const formatInvalide = "Ce format est invalide";
const phone = "Telephone";
const login = "Login";
const email = "Email";
const commentaire = "Commentaire";

const defaultMessageClient = "Aucun message laisser par le Client";
const defaultMessageClientVuParClent =
    "Vous n'avez pas encore laisser de message";
const labelMessageClient = "Message laissé par le Client";
const labelNoteClient = "La note obtenue";
const defaultMessageNoteClient = "Le Client ne vous a pas encore noté";
const defaultMessageNoteClientVuParClient =
    "Vous n'avez pas encore noté le fournisseur";
const messageChoixMultiSelectForm =
    "Merci de selectionner un ou plusieurs option(s)";
const hintMultiSelectForm = "Merci de choisir une ou plusieurs options";

const cancelButtonLabel = "RETOUR";
const okButtonLabel = "OK";
const chosirOffre = "CHOISIR L'OFFRE";

const etatConclu = "CONCLU";
const etatEnCours = "EN COURS";
const aucunCode = "Aucun code pour cet AO";

const messageDev =
    "En cours de developpement. A venir dans les prochaines versions!!!";

const sousTitreUtilisateur = "Les informations concernant l'utilisateur";
const sousTitreEntreprise = "Les informations concernant l'entreprise";
const messageApresInscription =
    "Merci de consulter votre boite de reception (ou spam) d'email  pour finaliser votre inscription";
const messageMAJProfil = "Votre mise à jour été prise en compte";
const titreWidgetApresInscription = "Inscription reussi";

const libelle = "Libellé";
const typeAO = "Type appel d'offres";
const LDomaine = "Domaines";
const listeFichiers = "Liste(s) de(s) fichier(s)";
const ajouterSecteurDactivites = "Secteur(s) d'activité(s)";
const choisirFournisseur = "Choisir fournisseurs";
const dateLimiteDepot = "Date limite depôt";
const dateLimite = "Date limite";
const nombreDoffres = "Nombre d'offres";
const duree = "Durée";
const aucunDureeSaisie = "Aucune durée saisie";
const description = "Description";
const ajouterCritere = "Ajouter critères";
const LCritere = "Critères";
const ajouterFichier = "Ajouter fichiers";
const reponseNon = "Non";
const reponseOui = "Oui";
const supprimerFichier = "Suppression de fichier";
const questionSupprimerFichier = "Voulez vous supprimer ce fichier ?";

const questionInscriptionConnexion = "Voulez vous inscrire ou connecter ?";
const contenuImpossibleDeSoumettre =
    "Vous ne pouvez pas soumettre à un appel d'offres car vous etes une entreprise cliente !!!";
const questionRetirerOffre = "Voulez vous retirer votre offre ?";
const attention = "Attention";

const telechargerFichier = "Téléchargement de fichier";
const telechargementReussi = "Téléchargement reussi";
const consulterDossierSpasuce = "Veuillez consulter le dossier Spasuce";
const telechargementEchoue = "Téléchargement echoué";
const questionTelechargerFichier = "Voulez vous télécharger ce fichier ?";

//const ajouterUnFichier = "Ajouter fichiers";

//const duree = "Durée";
const uniteDuree = "Unité de la durée";
const nom = "Nom";
const prenoms = "Prenoms";
const password = "Password";
const hintSearchEntrepriseByPassword = "Saisir le password de l'entreprise";
const confirmer = "Confirmer Password";

const oldPassword = "Ancien Password";
const newPassword = "Nouveau Password";
const confirmerNewPassword = "Confirmer Nouveau Password";
const message = "Message";
const objet = "Objet";
const LPays = "Pays";
const LVille = "Ville";
const hintPays = "Choisir votre pays";
const searchHintPays = "Choisir votre pays";
const filtreParPays = "Fitrer par pays";
const filtreParCritere = "Fitrer par critère";
const hintVille = "Choisir votre ville";
const searchHintVille = "Choisir votre ville";
const fonction = "Fonction";
const LTypeEntreprise = "Type Entreprise";
const LStatutJuridique = "Statut Juridique";
const connexion = "Connexion";
const nouvelUtilisateur = "NOUVEL UTILISATEUR";
const nouvelleEntreprise = "NOUVELLE ENTREPRISE";
const voirPassword = "Voir password";
const envoyer = "Envoyer";
const creer = "CREER";
const modifier = "MODIFIER";
const enregistrer = "ENREGISTRER";
const annuler = "ANNULER";
const cacherDetailsAO = "CACHER DETAILS APPEL D'OFFRES";
const afficherDetailsAO = "AFFICHER DETAILS APPEL D'OFFRES";
const retirer = "RETIRER";
const messageOffreRetenue = "Votre offre a été retenue";
const messageOffreRetenueVuParClient = "L'offre que vous avez retenu";
const messageImpossibleDeChoisirPlusDuneOffre =
    "Impossible de choisir cette offre pour cet appel d'offres car vous avez a déjà été retenu une offre.";
const soumettre = "SOUMETTRE UNE OFFRE";
final contenuIndisponible =
    "Désolé, ce contenu n’est pas encore disponible. Les travaux de mise à disposition de ce service ou informations sont en cours. Merci de nous accorder toute votre confiance !!!!";
const succes = "Operation effectuée avec succès !!!";
const echec = "Votre opération a echoué. Veuillez réessayer plutard !!!";
const problemeDeConnexion =
    "Erreur de connexion. Veuillez réessayer plutard !!!";
const erreurConnexion = "Connexion echoué!!! login et/ou password incorrect";
const accesInterdit = "Accès interdit";
const inscriptionReussi = "Inscription reussi";
const majReussi = "Mise à jour reussi";
const partieOffre = "Informations de votre offre";
const listeOffreDeLAO = "Les offres de l'appel d'offres";
const aucuneOffrePourLAO =
    "Aucune offre pour l'instant pour cet appel d'offres";

const messageAucuneOffrePourLeUser =
    "Vous n'avez aucun appel d'offres qui correspond à vos domaines. Merci de mettre à jour vos domaines !!!";
const messageAucunAOePourLeUser =
    "Vous n'avez aucun appel d'offres pour l'instant. Veuillez en créer !!!";
const details = "Details";
const detailsOffresAO = "Details offre";
const partieNewOffre = "Informations à fournir";
const partieAppelDoffres = "Informations de l'appel d'offres";
const aucunAppelDoffres =
    "Vous n'avez pas d'appel d'offres. Merci d'en créer !!!";
const aucunAppelDoffresPourFournisseur =
    "Vous n'avez pas d'appel d'offres correspondant à votre profil. Merci de mettre à jour vos domaines si cela n'est pas encore fait !!!";

const spasucePourVousServir = "SPASUCE pour vous servir";

const offreNonModifiable =
    "Cette offre n'est plus modifiable car vu par le Client !!!";

const messageNombreDeUserAtteint =
    "Impossible d'associer un utilisateur à cette entreprise !!!";
const messagePassordIncorrect =
    "Le password saisi est incorrect veuillez reessayer !!!";
const messageTailleMinPassword =
    "Le password doit contenir au moins 4 caractères";
const messageDebut = "Votre entreprise ";
const messageFin = " n'a droit qu'a un seul utilisateur !!!";

const messageBienvenueConnexion = "Bienvenue chez SPASUCE";
const connexionReussi = "Connexion reussi !!!";

const titleWidegtInscription = "INSCRIPTION";

const messageDateLimitePasse =
    "Impossible de soumettre une offre car la date limite est passée";

//*********************************** internalisation ***********************************

// *********************** BEGIN API ****************************

// methode
const create = "/create";
const update = "/update";
const delete = "/delete";
const forceDelete = "/forceDelete";
const getByCriteria = "/getByCriteria";
const getByCriteriaCustom = "/getByCriteriaCustom";
const changerPassword = "/changerPassword";
const updateContenuNotificationDispo = "/updateContenuNotificationDispo";
const getBase64Files = "/getBase64Files";

// nom des tables
const pays = 'pays';
const ville = 'ville';
const role = 'role';
const offre = 'offre';
const appelDoffres = 'appelDoffres';
const appelDoffresRestreint = 'appelDoffresRestreint';

const entreprise = 'entreprise';
const user = 'user';
const domaine = 'domaine';

const statutJuridique = 'statutJuridique';
const typeAppelDoffres = 'typeAppelDoffres';
const critereAppelDoffres = 'critereAppelDoffres';
const secteurDactivite = 'secteurDactivite';
const fichier = 'fichier';
const typeEntreprise = 'typeEntreprise';
const typeDuree = 'typeDuree';
const secteurDactiviteEntreprise = "secteurDactiviteEntreprise";

// endpoint
const endpointGetPays = pays + getByCriteria;
const endpointGetRole = role + getByCriteria;
const endpointGetStatutJuridique = statutJuridique + getByCriteria;
const endpointGetTypeEntreprise = typeEntreprise + getByCriteria;
const endpointGetTypeAppelDoffres = typeAppelDoffres + getByCriteria;
const endpointGetCritereAppelDoffres = critereAppelDoffres + getByCriteria;

// constantes BD

const typeString = "STRING";
const typeNumber = "NUMBER";
const typeDate = "DATE";
const typeBoolean = "BOOLEAN";

const BROUILLON = "BROUILLON";
const ENTRANT = "ENTRANT";
const TERMINER = "TERMINER";
const RESTREINT = "RESTREINT";
const LIBELLE = "libelle";
const secteurDactiviteLibelle = "secteurDactiviteLibelle";
const critereAppelDoffresLibelle = "critereAppelDoffresLibelle";
const name = "name";
const libelleExtension = "extension";

const urlFichier = "urlFichier";
const fichierBase64 = "fichierBase64";
const baseUrlWithSSL = "https://spasuce.com/api";
const baseUrlWithoutSSL = "http://207.180.196.73:8080";

const id = "id";

// mode form & option submit
const UPDATE = "UPDATE";
const CREATE = "CREATE";
const READ = "READ";

// CONSTANT FORMAT DATE

const dd_MM_yyyy_slash = 'dd/MM/yyyy';
const dd_MM_yyyy_tiret = 'dd-MM-yyyy';

const HH_colon_mm = 'HH:mm';
const HH_dot_mm = 'HH.mm';
const H_colon_mm = 'H:mm'; // 2:56 et non 02:56
const h_colon_mm_space_a = 'h:mm'; // 2:00 pm pour 14:00

const operatorDifferent = {"operator": "<>"};

// *********************** END API ****************************

// type de input
const v_phone = "phone";
const v_login = "login";
const v_email = "email";
const v_password = "password";
const v_number = "number";
const v_datetime = "datetime";
const v_text = "text";
const v_file = "file";

// les types de contenu statiques
const PARAMETRES = "PARAMETRES";
const FONCTIONNEMENT = "FONCTIONNEMENT";
const PRESENTATION = "PRESENTATION";
const QUI_SOMMES_NOUS = "QUI_SOMMES_NOUS";
const NOS_CONDITIONS = "NOS_CONDITIONS";
const COMMENT_CA_MARCHE = "COMMENT_CA_MARCHE";
const CONTENU_STATIQUE = "Contenu statique";
const SERVICES_FONCTIONNEMENT = "SERVICES_FONCTIONNEMENT";
const accueil = "accueil";

// varible pour la connexion
bool isLoggedInX = false;

// les endpoints
const endpointSecteurDactiviteAppelDoffresGetByCriteriaCustom =
    "secteurDactiviteAppelDoffres/getByCriteriaCustom";
const endpointAppelDoffresGetByCriteria = "appelDoffres/getByCriteria";

const NBRE_MAXI_USER_PAR_ENTREPRISE = 5;
const NBRE_CARACTERE_MIN_PASSWORD = 4;
const maxLines = 10;
const minLines = 1;
const dure_carousel = 1000;
const fontSizeCustom = 18.0;

const imagesCarousel = [
  AssetImage("images/mecanique.jpg"),
  AssetImage("images/automatisme.jpg"),
  AssetImage("images/electricite.jpg"),
  AssetImage("images/etude_conseil.jpg")
];

const logoSpasuce = "images/logo.jpeg";
const folderSpasuce = "/Spasuce/files/";

const marqueObligatoire = '*';
const espace = ' ';
const pipe = "|";
const pipeAvecEspace = " | ";

// il y a des donnees à prendre en compte dans l'internationnalisation dans cette classe

class ConstApp {
  static const default_user = "Adminnistrateur";
  static const default_email = "spasuce@spasuce.com";

  // menu user
  static const profil = "Profil";
  static const maj_domaines = "MAJ domaines";
  static const mes_activites = "Mes activités";
  static const changer_password = "Changer le mot de passe";
  static const password_oublie = "Mot de passe oubié";
  static const deconnexion = "Deconnexion";

  // contenu statique
  static const test_widget = "Test widget";
  static const test_widget_2 = "Test widget 2";
  static const test_widget_3 = "Test widget 3";
  static const nous_contacter = "Nous contacter";
  static const comment_ca_marche = "Comment ça marche";
  static const qui_sommes_nous = "Qui sommes nous";
  static const parametres = "Parametres app.";
  static const nos_conditions = "Nos Conditions";

  // filtre
  static const libelle_fitre_par_date = "Date";
  static const libelle_fitre_par_domaine = "Domaines";
  static const app_name = "Spasuce";
  static const prenom = "Prénom";
  static const titre = "Ingénieur";

  // color application
  static const color_app = Color(0xff0DADEF);
  //static const color_button_disable = Colors.grey[300];
  static const color_button_disable = Color(0xffE0E0E0);

  //static const color_app = Color(0xff139BEA);
  //static const color_app = Color(0xff4AABDF);
  static const color_app_rouge = Color(0xffF05A28);
  //static const color_app_rouge = Color(0xffE0633B);
  static const color_app_gris = Color(0xff6E6D70);
  //static const color_app_gris = Color(0xff6E6E72);
  static const color_app_gris_degrade = Color(0xffE0E1E2);
  static const color_app_gris_degrade_1 = Color(0xffD2D0D1);
  static const color_red = Color(0xffff0000);

  // libelle des views
  static const edit_oa = "Editer appel d'offres";
  static const edit_offres = "Editer offre";
  static const show_oa = "Voir appel d'offres";
  static const show_offres = "Voir offre";
  static const noter_offres = "Noter offre";

  //libelle menu footer
  static const accueil = "Accueil";
  static const presentation = "Présentation";
  static const fonctionnement = "Fonctionnement";
  static const s_inscrire = "S'inscrire";
  static const connexion = "Connexion";
  static const offres = "Offres";
  static const autres = "Autres";
  static const appels_doffres = "Appels D'offres";

  // type entreprise
  static const client = "CLIENTE";
  static const fournisseur = "FOURNISSEUR";
  static const mixte = "MIXTE";

  static const List<Menu> menuFooterBasic = <Menu>[
    Menu(title: accueil, icon: Icons.home, color: color_app),
    Menu(
        title: presentation,
        icon: Icons.import_contacts,
        color: color_app,
        code: PRESENTATION),
    Menu(
        title: fonctionnement,
        icon: Icons.business_center,
        color: color_app,
        code: FONCTIONNEMENT),
    Menu(title: s_inscrire, icon: Icons.save_alt, color: color_app),
    Menu(title: connexion, icon: Icons.swap_horiz, color: color_app),
    // MenuFooter(title: 'Autres', icon:Icons.tune, color:Colors.teal),
    //*/
  ];

  static List<Test> test = <Test>[
    Test(title: accueil, icon: Icons.home, color: color_app, isSelect: false),
    Test(
        title: presentation,
        icon: Icons.import_contacts,
        color: color_app,
        code: PRESENTATION),
    Test(
        title: fonctionnement,
        icon: Icons.business_center,
        color: color_app,
        code: FONCTIONNEMENT),
    Test(title: s_inscrire, icon: Icons.save_alt, color: color_app),
    Test(title: connexion, icon: Icons.swap_horiz, color: color_app),
    // MenuFooter(title: 'Autres', icon:Icons.tune, color:Colors.teal),
    //*/
  ];

  static const List<Menu> menuFooterClient = <Menu>[
    Menu(title: accueil, icon: Icons.home, color: color_app),
    Menu(
        title: presentation,
        icon: Icons.import_contacts,
        color: color_app,
        code: PRESENTATION),
    Menu(
        title: fonctionnement,
        icon: Icons.business_center,
        color: color_app,
        code: FONCTIONNEMENT),
    Menu(title: appels_doffres, icon: Icons.phone_in_talk, color: color_app),
  ];

  static const List<Menu> menuFooterFournisseur = <Menu>[
    Menu(title: accueil, icon: Icons.home, color: color_app),
    Menu(
        title: presentation,
        icon: Icons.import_contacts,
        color: color_app,
        code: PRESENTATION),
    Menu(
        title: fonctionnement,
        icon: Icons.business_center,
        color: color_app,
        code: FONCTIONNEMENT),
    Menu(title: offres, icon: Icons.card_giftcard, color: color_app),
  ];

  static const List<Menu> menuFooterMixte = <Menu>[
    Menu(title: accueil, icon: Icons.home, color: color_app),
    Menu(
        title: presentation,
        icon: Icons.import_contacts,
        color: color_app,
        code: PRESENTATION),
    Menu(
        title: fonctionnement,
        icon: Icons.business_center,
        color: color_app,
        code: FONCTIONNEMENT),
    Menu(title: appels_doffres, icon: Icons.phone_in_talk, color: color_app),
    Menu(title: offres, icon: Icons.card_giftcard, color: color_app),
  ];

  // menu du drawer
  static const List<Menu> menuDrawerUser = <Menu>[
    Menu(title: profil, icon: Icons.settings, view: color_app),
    Menu(title: changer_password, icon: Icons.work, view: color_app),
    Menu(title: password_oublie, icon: Icons.work, view: color_app),
    Menu(title: mes_activites, icon: Icons.work, view: color_app),
    Menu(title: maj_domaines, icon: Icons.domain, view: color_app),
    Menu(title: deconnexion, icon: Icons.exit_to_app, view: color_app),
  ];

  static const List<Menu> menuDrawerSpasuce = <Menu>[
    Menu(
        title: comment_ca_marche,
        icon: Icons.build,
        view: color_app,
        code: COMMENT_CA_MARCHE),
    Menu(
        title: qui_sommes_nous,
        icon: Icons.thumbs_up_down,
        view: color_app,
        code: QUI_SOMMES_NOUS),
    Menu(
        title: nos_conditions,
        icon: Icons.closed_caption,
        view: color_app,
        code: NOS_CONDITIONS),
    Menu(
        title: nous_contacter,
        icon: Icons.message,
        view: color_app,
        code: nous_contacter),
    Menu(
        title: parametres,
        icon: Icons.settings_applications,
        view: color_app,
        code: PARAMETRES),
    /*Menu(
        title: test_widget,
        icon: Icons.data_usage,
        view: color_app,
        code: test_widget),
    Menu(
        title: test_widget_2,
        icon: Icons.data_usage,
        view: color_app,
        code: test_widget_2),
    Menu(
        title: test_widget_3,
        icon: Icons.data_usage,
        view: color_app,
        code: test_widget_3),*/
  ];
}
