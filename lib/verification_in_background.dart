import 'package:flutter/material.dart';
import 'package:spasuce/dao/repository/database_helper.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/views/loader_2.dart';

class VerificationInBackground extends StatefulWidget {
  VerificationInBackground({Key key}) : super(key: key);

  @override
  _VerificationInBackgroundState createState() =>
      _VerificationInBackgroundState();
}

class _VerificationInBackgroundState extends State<VerificationInBackground> {
  bool verificationLoading = true;
  bool _isNewUser = false;
  User userConnected;
  @override
  void initState() {
    Utilities.begin("VerificationInBackground/initState");
    // TODO: implement initState
    super.initState();
    isNewUser();
  }

  @override
  Widget build(BuildContext context) {
    Utilities.begin("VerificationInBackground/build");
    return Scaffold(
      /*appBar: AppBar(
        title: Text(appName),
        backgroundColor: colorBlueLogo,
      ),*/
      body: Container(
          child: (verificationLoading)
              //? Loader()
              ? Loader2()
              : (_isNewUser
                  ? MyApp()
                  //Navigator.push(context, MaterialPageRoute(builder: (context) => Welcome()))
                  : MyApp(userConnecter: userConnected))),
    );
  }

  // verification method if new user or old
  isNewUser() async {
    // connexion in dataBase or networkCall
    bool isConnected = false;
    await Future.delayed(Duration(seconds: 5), () {});

    await DatabaseHelper.instance.getUserConnected.then((resp) {
      if (resp != null) {
        userConnected = User.fromJson(resp.toJson());
        isConnected = true;
      }
    }).catchError((err) {
      print(err);
    });

    Utilities.begin("isConnected  : $isConnected");
    Utilities.begin(
        "userConnecter  : ${userConnected != null ? userConnected.toJson() : userConnected}");

    setState(() {
      verificationLoading = false;
      _isNewUser = isConnected;
    });
    //return true;
  }
}
