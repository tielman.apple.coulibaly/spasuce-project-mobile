//import 'dart:js';
/*
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/internationalization/app_localizations.dart';
import 'package:spasuce/main.drawer.dart';
import 'package:spasuce/main.app.bar.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/views/accueil.dart';
import 'package:spasuce/views/accueil_ao.dart';
import 'package:spasuce/views/changer_password.dart';
import 'package:spasuce/views/connexion.dart';
import 'package:spasuce/views/contenu_statique.dart';
import 'package:spasuce/views/edit_ao.dart';
import 'package:spasuce/views/edit_offres.dart';
import 'package:spasuce/views/gabarit_contenu_statique.dart';
import 'package:spasuce/views/inscription.dart';
import 'package:spasuce/views/maj_domaine.dart';
import 'package:spasuce/views/mes_activites.dart';
import 'package:spasuce/views/nous_contacter.dart';
import 'package:spasuce/views/accueil_offres.dart';
import 'package:spasuce/views/parametres_application.dart';
import 'package:spasuce/views/password_oublier.dart';
import 'package:spasuce/const_app.dart' as globals;


import 'models/menu.dart';

//void main() => runApp(MyApp());
//void main() => runApp(MaterialApp( home: MyApp(), debugShowCheckedModeBanner: false,));

class MyApp extends StatefulWidget{
  // declarations des variables
  List<Widget> _widgets;
  List<Menu> menuFooter = ConstApp.menuFooterBasic;
  List<Menu> menuDrawerSpasuce = ConstApp.menuDrawerSpasuce;
  User userConnecter;
  String apiBaseUrl ;
  int currentIndex;



  //List<Menu> menuFooter ;
  List<Menu> menuDrawerUser;
  List<Widget> drawerOptions;

  MyApp(
      {this.menuDrawerSpasuce,
      this.menuDrawerUser,
      this.menuFooter,
      this.drawerOptions,
      this.userConnecter, this.currentIndex=0});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Choice _selectedChoice = choices[0]; // The app's "state".



  // methode d'initialisation de certaines donnes statiques

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // etat de l'utilisateur actuelle connecte/visiteur ?
    //userConnecter = User(typeEntrepriseCode: ConstApp.fournisseur);
    Utilities.begin("initState main") ;
    initialize(widget.userConnecter);
    //getData("London");
  }

  void _select(Choice choice) {
    // Causes the app to rebuild with the new _selectedChoice.
    setState(() {
      _selectedChoice = choice;
    });
  }

  @override
  Widget build(BuildContext context) {
    // appeler un service du backend

    widget.apiBaseUrl = AppConfig.of(context).apiBaseUrl;
    //widget.apiBaseUrl = base_url_prod ;
    // TODO: implement build
    return Scaffold(
        drawer: MainDrawer(
            drawerOptions: widget.drawerOptions,
            menuDrawerSpasuce: widget.menuDrawerSpasuce,
            menuDrawerUser: widget.menuDrawerUser),

        //drawer: MainDrawer(),
        appBar: MainAppBar(
            //appBar: AppBar(
            title: Text(ConstApp.app_name),
            //title: Text(AppLocalizations.of(context).translate('title')),
            //backgroundColor: Color(0xff139BEA),
            appBar: AppBar(),

            //actions : <Widget>[
            widgets: (widget.userConnecter != null
                ? <Widget>[
                    // action button

                    IconButton(
                      icon: Icon(choices[0].icon),
                      onPressed: () {
                        _select(choices[0]);
                      },
                    ),
                    // user connect
                    Center(
                        child: (widget.userConnecter != null
                            ? Text(widget.userConnecter.nom)
                            : Text(""))),
                    // overflow menu
                    IconButton(
                      icon: Icon(Icons.filter_list),
                      onPressed: () {
                        _filtreEditModal(context);
                      },
                    ),

                    /*
          PopupMenuButton<Choice>(

            onSelected: _select,
           //icon: Icon(Icons.notifications_active),
            icon: Icon(Icons.filter_list),
            itemBuilder: (BuildContext context) {
              return choices.map((Choice choice) {
              //return choices.skip(2).map((Choice choice) {
                return PopupMenuItem<Choice>(


                  value: choice,
                  child : Text(choice.title), // sans icon
                  //child:ListTile(title: Text(choice.title), leading: Icon(choice.icon),), avec icon
                );
              }).toList();
            },
          ),
         */
                  ]
                : // sinon
                <Widget>[
                    // overflow menu
                    IconButton(
                      icon: Icon(Icons.filter_list),
                      onPressed: () {
                        _filtreEditModal(context);
                      },
                    ),

                    /*
          PopupMenuButton<Choice>(

            onSelected: _select,
           //icon: Icon(Icons.notifications_active),
            icon: Icon(Icons.filter_list),
            itemBuilder: (BuildContext context) {
              return choices.map((Choice choice) {
              //return choices.skip(2).map((Choice choice) {
                return PopupMenuItem<Choice>(


                  value: choice,
                  child : Text(choice.title), // sans icon
                  //child:ListTile(title: Text(choice.title), leading: Icon(choice.icon),), avec icon
                );
              }).toList();
            },
          ),
         */
                  ])),
        //appBar: MainAppBar(),

        bottomNavigationBar: BottomNavigationBar(
          //type: BottomNavigationBarType.fixed,
          //type: BottomNavigationBarType.shifting,
          //backgroundColor: ,

          currentIndex: widget.currentIndex,
          onTap: (int index) {
            setState(() {
              print(index);
              widget.currentIndex = index;
            });
          },
          items: widget.menuFooter.map((Menu destination) {
            //items: ConstApp.menuFooterBasic.map((Menu destination) {
            return BottomNavigationBarItem(
                icon: Icon(
                  destination.icon,
                ),
                backgroundColor: destination.color,
                title: Text(destination.title));
          }).toList(),
        ),
        body: ((widget._widgets == null)
            ? Center(
                child: CircularProgressIndicator(),
              )
            : widget._widgets.elementAt(widget.currentIndex)));
  }

  // initialisation des variables de base
  void initialize(User userConnecter) {
    setState(() {
      // pour changer une donner toujours dans le setState

      widget.menuDrawerSpasuce = ConstApp.menuDrawerSpasuce;
      widget.drawerOptions = <Widget>[];
      widget.menuFooter = ConstApp.menuFooterBasic;
      widget._widgets = <Widget>[
        Accueil(),
        ContenuStatique(typeContenuStatique: widget.menuFooter[1].code,),
        ContenuStatique(typeContenuStatique: widget.menuFooter[2].code,),
        Inscription(),
        Connexion(),
      ];

      if (userConnecter != null) {
        // si user connecte
        widget.menuDrawerUser = ConstApp.menuDrawerUser;
        switch (userConnecter.typeEntrepriseCode) {
          case ConstApp.client:
            widget.menuFooter = ConstApp.menuFooterClient;
            widget._widgets = <Widget>[
              Accueil(),
              //ContenuStatique(),
              //ContenuStatique(),
              ContenuStatique(typeContenuStatique: widget.menuFooter[1].code,),
              ContenuStatique(typeContenuStatique: widget.menuFooter[2].code,),
              AccueilAO(),
            ];
            break;
          case ConstApp.mixte:
            widget.menuFooter = ConstApp.menuFooterMixte;
            widget._widgets = <Widget>[
              Accueil(),
              //ContenuStatique(),
              //ContenuStatique(),
              ContenuStatique(typeContenuStatique: widget.menuFooter[1].code,),
              ContenuStatique(typeContenuStatique: widget.menuFooter[2].code,),
              AccueilAO(),
              AccueilOffres(),
            ];
            break;

          case ConstApp.fournisseur:
            widget.menuFooter = ConstApp.menuFooterFournisseur;
            widget._widgets = <Widget>[
              Accueil(),
              //ContenuStatique(),
              //ContenuStatique(),
              ContenuStatique(typeContenuStatique: widget.menuFooter[1].code,),
              ContenuStatique(typeContenuStatique: widget.menuFooter[2].code,),
              AccueilOffres(),
            ];
            break;

          default:
            widget.menuFooter = ConstApp.menuFooterBasic;
            widget._widgets = <Widget>[
              Accueil(),
              //ContenuStatique(),
              //ContenuStatique(),
              ContenuStatique(typeContenuStatique: widget.menuFooter[1].code,),
              ContenuStatique(typeContenuStatique: widget.menuFooter[2].code,),
              Inscription(),
              Connexion(),
            ];
            break;
        }
      }

      // formation du drawerOptions
      //var drawerOptions = <Widget>[];

      if (widget.menuDrawerUser != null && widget.menuDrawerUser.length > 0) {
        for (var element in widget.menuDrawerUser) {
          widget.drawerOptions.add( InkWell(
            splashColor: ConstApp.color_app,
            onTap: (){
                switch (element.title) {
                  case ConstApp.profil:
                    Utilities.onSelectDrawerItem(context: context, view: Inscription());
                    Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:Inscription(), titreContenu: ConstApp.s_inscrire,));
                    break;

                  case ConstApp.password_oublie:
                    Utilities.onSelectDrawerItem(context: context, view: PasswordOublier());
                    Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:PasswordOublier(), titreContenu: ConstApp.password_oublie,));
                    break;

                  case ConstApp.changer_password:
                    Utilities.onSelectDrawerItem(context: context, view: ChangerPassword());
                    Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:ChangerPassword(), titreContenu: ConstApp.changer_password,));

                    break;

                  case ConstApp.maj_domaines:
                    Utilities.onSelectDrawerItem(context: context, view: MAJDomaine());
                    Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:MAJDomaine(), titreContenu: ConstApp.maj_domaines,));

                    break;

                  case ConstApp.mes_activites:
                    //Utilities.onSelectDrawerItem(context: context, view: MesActivites());
                    Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:MesActivites(), titreContenu: ConstApp.mes_activites,));

                    break;

                  case ConstApp.deconnexion:

                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Déconnexion"),
                          content: new Text("Voulez vraiment vous déconnecter"),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("Non"),
                              onPressed: () {
                                Navigator.of(context).pop();
                                //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                              },
                            ),new FlatButton(
                              child: new Text("Oui"),
                              onPressed: () {
                                globals.isLoggedIn = false ;
                                Navigator.of(context).pop();
                                Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                              },
                            ),
                          ],
                        );
                      },
                    );
                    //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                    break;

                  default:
                    break;
                }
              },
            child: ListTile(
              leading: Icon(element.icon),
              title: Text(element.title),
              /*
              onTap: (){
                switch (element.title) {
                  case ConstApp.profil:
                    Utilities.onSelectDrawerItem(context: context, view: Inscription());
                    break;

                  case ConstApp.password_oublie:
                    Utilities.onSelectDrawerItem(context: context, view: PasswordOublier());
                    break;

                  case ConstApp.changer_password:
                    Utilities.onSelectDrawerItem(context: context, view: ChangerPassword());
                    break;

                  case ConstApp.maj_domaines:
                    Utilities.onSelectDrawerItem(context: context, view: MAJDomaine());
                    break;

                  case ConstApp.mes_activites:
                    Utilities.onSelectDrawerItem(context: context, view: MesActivites());
                    break;

                  case ConstApp.deconnexion:

                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text("Déconnexion"),
                          content: new Text("Voulez vraiment vous déconnecter"),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text("Non"),
                              onPressed: () {
                                Navigator.of(context).pop();
                                //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                              },
                            ),new FlatButton(
                              child: new Text("Oui"),
                              onPressed: () {
                                globals.isLoggedIn = false ;
                                Navigator.of(context).pop();
                                Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                              },
                            ),
                          ],
                        );
                      },
                    );
                   //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                    break;

                  default:
                    break;
                }


              },*/

              //selected: i = _selectedDrawerIndex,
              //onTap: () =&gt; _onSelectItem(i),
            ),
          ));
        }
        widget.drawerOptions.add(Divider());
      }

      for (var element in widget.menuDrawerSpasuce) {
        widget.drawerOptions.add(new ListTile(
          leading: new Icon(element.icon),
          title: new Text(element.title),
          onTap: (){
            switch (element.title) {
              case ConstApp.comment_ca_marche:
                //Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:ContenuStatique(typeContenuStatique: element.code,), titreContenu: ConstApp.comment_ca_marche,));
                Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:ContenuStatique(typeContenuStatique: element.code,), titreContenu: ConstApp.comment_ca_marche,));
                break;

              case ConstApp.qui_sommes_nous:
                //Utilities.onSelectDrawerItem(context: context, view: ContenuStatique(typeContenuStatique: element.code,));
                Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:ContenuStatique(typeContenuStatique: element.code,), titreContenu: ConstApp.qui_sommes_nous,));
                break;

              case ConstApp.nous_contacter:
                //Utilities.onSelectDrawerItem(context: context, view: ContenuStatique(typeContenuStatique: element.code,));
                Utilities.onSelectDrawerItem(context: context, view: NousContacter(titre: element.code,));
                break;

              case ConstApp.test_widget:
                //Utilities.onSelectDrawerItem(context: context, view: ContenuStatique(typeContenuStatique: element.code,));
                Utilities.onSelectDrawerItem(context: context, view: EditAO());
                break;
              case ConstApp.test_widget_2:
                //Utilities.onSelectDrawerItem(context: context, view: ContenuStatique(typeContenuStatique: element.code,));
                Utilities.onSelectDrawerItem(context: context, view: EditOffres());
                break;
              case ConstApp.test_widget_3:
                //Utilities.onSelectDrawerItem(context: context, view: ContenuStatique(typeContenuStatique: element.code,));
                Utilities.onSelectDrawerItem(context: context, view: AccueilAO());
                break;

              case ConstApp.nos_conditions:
                //Utilities.onSelectDrawerItem(context: context, view: ContenuStatique(typeContenuStatique: element.code,));
                Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:ContenuStatique(typeContenuStatique: element.code,), titreContenu: ConstApp.nos_conditions,));
                break;   

              case ConstApp.parametres:
                //Utilities.onSelectDrawerItem(context: context, view: ParametresApplication());
                Utilities.onSelectDrawerItem(context: context, view: GabaritContenuStatique(contenu:ParametresApplication(), titreContenu: ConstApp.parametres,));
                break;

              default:
                break;
            }
          },
          //selected: i = _selectedDrawerIndex,
          //onTap: () =&gt; _onSelectItem(i),
        ));
      }
    });
  }

  void testApi(apiBaseUrl) async {
    print("Begin backend");

    var data = '{"data":{}}';
    var request = RequestCustom();
    request.data = {};
    print("resutatl ${request.toJson()}");

    var response = await netWorkCalls.post(
        bodyRequest: request, uri: apiBaseUrl + "entreprise/getByCriteria");
    var res1 = ResponseCustom.fromJson(jsonDecode(response));

    var res = jsonDecode(response);
    print("End backend ******** ${res}");
    print("End backend res1 ******** ${res1.toJson()}");
    print("End backend ${response}");

    //var res = ResponseCustom.fromJson(json.encode(response));
    //res =

    print("End backend");
  }
}

// user defined function
void _showDialog(context) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text("Alert Dialog title"),
        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("Close"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void _filtreEditModal(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext builderContext) {
        return Container(
            child: (ConstApp.menuFooterBasic == null)
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    itemCount: (ConstApp.menuFooterBasic == null
                        ? 0
                        : ConstApp.menuFooterBasic.length),
                    // nombre d'element a afficher dans la liste
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(ConstApp.app_name),
                        leading: Icon(Icons.check_box_outline_blank),
                        onTap: () {
                          //context.widget.createElement();

                          //setState(() {//_selectedChoice = choice;});
                        },
                        //leading: Icon(Icons.indeterminate_check_box),
                        //leading: Icon(Icons.check_box),
                      );
                    },
                  ));
      });
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Bicycle', icon: Icons.notifications),
  const Choice(title: 'Admin', icon: Icons.directions_car),
  //const Choice(title: 'Bicycle', icon: Icons.notifications_active),
  const Choice(title: 'Boat', icon: Icons.directions_boat),
  const Choice(title: 'Bus', icon: Icons.directions_bus),
  const Choice(title: 'Train', icon: Icons.directions_railway),
  const Choice(title: 'Walk', icon: Icons.directions_walk),
];

getData(city) {
  //String url = "http://api.openweathermap.org/data/2.5/forecast?q=London,uk&APPID=8d256cf60c465dc34d26917fc2b9228f" ;
  String url =
      "http://api.openweathermap.org/data/2.5/forecast?q=${city},uk&APPID=8d256cf60c465dc34d26917fc2b9228f";
  client.get(url).then((resp) {
    print(resp.body);
    print(json.decode(resp.body));
  }).catchError((err) {
    print(err);
  });
}

/*
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

*/
*/