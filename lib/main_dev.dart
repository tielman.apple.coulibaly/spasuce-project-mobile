import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/internationalization/app_localizations.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/verification_in_background.dart';

void main() {
  var configuredApp = AppConfig(
    flavorName: flavorNameDev,
    child: MaterialApp(
      //home: MyApp(),
      home: VerificationInBackground(),
      debugShowCheckedModeBanner: false,
    ),

    //child: MyApp(),
    apiBaseUrl: base_url_dev,
  );

  /*var configuredApp = DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) =>
          ThemeData(brightness: brightness, accentColor: Colors.green),
      themedWidgetBuilder: (context, theme) {
        return AppConfig(
          flavorName: flavorNameDev,
          child: MaterialApp(
            home: MyApp(),
            debugShowCheckedModeBanner: false,
          ),

          //child: MyApp(),
          apiBaseUrl: base_url_dev,
        );
      });
      */

  return runApp(configuredApp);
}
