import 'package:flutter/material.dart';
import 'package:spasuce/models/menu.dart';
import 'package:spasuce/utilities.dart';
import 'package:spasuce/models/user.dart';

import 'const_app.dart';

class MainDrawer extends StatefulWidget {

  List<Menu> menuDrawerUser;
  List<Widget> drawerOptions;
  List<Menu> menuDrawerSpasuce = ConstApp.menuDrawerSpasuce;
  User userConnecter ; 

  MainDrawer({this.menuDrawerSpasuce, this.userConnecter, this.menuDrawerUser, this.drawerOptions}) ;




  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {


  int _selectedDrawerIndex = 0;

  void _onSelectDrawerItem(dynamic view, int index){

    /*
    setState(() {
      _selectedDrawerIndex = index ;
    });
    */
    Navigator.of(context).pop(); // pour fermer le menu apres clic
    Navigator.push(context, MaterialPageRoute(builder: (context)=> view));
  }


  // create drawerItem
  Widget _createDrawerItem(
      {IconData iconData, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(iconData),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(text),
          )
        ],
      ),
      onTap: onTap,
    );
  }










  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(

      child: ListView(
        children: <Widget>[
          /*DrawerHeader(
            child: Image.asset('images/logo.jpeg'),
            decoration: BoxDecoration(
                gradient:
                LinearGradient(colors: [ConstApp.color_app, Colors.white])
                //LinearGradient(colors: [Colors.deepOrange, Colors.white])
              //gradient: LinearGradient(colors: [MainConst.color_app_rouge, Colors.white])
            ),
            //child: AssetImage('images/logo.jpeg'),
          ),*/


          UserAccountsDrawerHeader(
            currentAccountPicture: Image.asset('images/logo.jpeg'),
            accountEmail: Text((widget.userConnecter != null) ? widget.userConnecter.email : "", style: TextStyle(fontWeight: FontWeight.bold),),
            accountName: Text((widget.userConnecter != null) ? widget.userConnecter.nom : "", style: TextStyle(fontWeight: FontWeight.bold),),
            decoration: BoxDecoration(
                gradient:
                LinearGradient(colors: [ConstApp.color_app, Colors.white])
                //LinearGradient(colors: [Colors.deepOrange, Colors.white])
              //gradient: LinearGradient(colors: [MainConst.color_app_rouge, Colors.white])
            ),          ),
          Column(children: widget.drawerOptions,)

          //widget.drawerOptions,


          /*
          ListTile(
            title: Text(ConstApp.profil),
            leading: Icon(Icons.settings),

            //trailing: Icon(Icons.arrow_drop_down),
          ),
          //((true) ? Divider() : ""), possible
          ListTile(
            title: Text(ConstApp.changer_password),
            leading: Icon(Icons.work),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),ListTile(
            title: Text(ConstApp.password_oublie),
            leading: Icon(Icons.work),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),ListTile(
            title: Text(ConstApp.mes_activites),
            leading: Icon(Icons.work),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),ListTile(
            title: Text(ConstApp.maj_domaines),
            leading: Icon(Icons.domain),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),
          ListTile(
            title: Text(ConstApp.deconnexion),
            leading: Icon(Icons.exit_to_app),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),

          Container(
            child: ListView.builder(
                shrinkWrap: true,

                itemCount: (widget.menuDrawerUser == null ? 0: widget.menuDrawerUser.length) , // nombre d'element a afficher dans la liste
                itemBuilder: (context, index){
                  var menu = widget.menuDrawerUser[index] ;
                  return ListTile(
                    title: Text(menu.title),
                    leading: Icon(menu.icon),
                    //onTap: () { MainUtilities.onTap(context: context, classeName: menu.view) ;},


                    //trailing: Icon(Icons.arrow_drop_down),
                  ) ;
                }
            ),
          ),

         //((widget.menuDrawerUser == null ) ? Divider(color: Colors.grey,)  : widget.menuDrawerUser) ,
          ((widget.menuDrawerUser != null) ? Divider(color: Colors.grey,) : Divider(color: Colors.red,)),

          /*
          ListTile(
              title: Text(MainConst.presentation),
            leading: Icon(Icons.import_contacts),

            //trailing: Icon(Icons.arrow_drop_down),
              //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),ListTile(
            title: Text(MainConst.fonctionnement),
            leading: Icon(Icons.build),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),
          */

          Container(
            child: ListView.builder(
                itemCount: (widget.menuDrawerSpasuce == null ? 0: widget.menuDrawerSpasuce.length) , // nombre d'element a afficher dans la liste
                itemBuilder: (context, index){
                  var menu = widget.menuDrawerSpasuce[index] ;
                  return ListTile(
                    title: Text(menu.title),
                    leading: Icon(menu.icon),
                    //onTap: () { MainUtilities.onTap(context: context, classeName: menu.view) ;},


                    //trailing: Icon(Icons.arrow_drop_down),
                  ) ;
                }
            ),
          ),

          ListTile(
            title: Text(ConstApp.nos_conditions),
            leading: Icon(Icons.closed_caption),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),ListTile(

            title: Text(ConstApp.nous_contacter),
            leading: Icon(Icons.message),
            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),ListTile(
            title: Text(ConstApp.qui_sommes_nous),
            //leading: Icon(Icons.wb_incandescent),
            leading: Icon(Icons.thumbs_up_down),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          ),ListTile(
            title: Text(ConstApp.parametres),
            //leading: Icon(Icons.wb_incandescent),
            leading: Icon(Icons.settings_applications),

            //trailing: Icon(Icons.arrow_drop_down),
            //onTap: () { MainUtilities.onTap(context: context, classeName: 'MyApp') ;},
          )
          */
        ],
      ),
    );
  }
}

Widget _createDrawerItem(
    {IconData icon, String text, GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text),
        )
      ],
    ),
    onTap: onTap,
  );
}

/*


UserAccountsDrawerHeader(
            accountName: Text(MainConst.default_user),
            accountEmail: Text(MainConst.default_email),
            //child: Image.asset('images/logo.jpeg'),
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/logo.jpeg')
                ),
                gradient:
                LinearGradient(colors: [Colors.deepOrange, Colors.white])
              //gradient: LinearGradient(colors: [MainConst.color_app_rouge, Colors.white])
            ),
            //child: AssetImage('images/logo.jpeg'),
          )
 */

class ItemFiltre {
  String libelle;

  bool isSelect;

  IconData iconItem;

  ItemFiltre({this.iconItem, this.libelle, this.isSelect});
}

List<ItemFiltre> allItemFiltre = <ItemFiltre>[
  ItemFiltre(libelle: 'Accueil', iconItem: Icons.home, isSelect: false),
  ItemFiltre(
      libelle: 'Serv & Fonc', iconItem: Icons.business_center, isSelect: false),
  ItemFiltre(libelle: 'Realisations', iconItem: Icons.home, isSelect: false),
  //*
  ItemFiltre(libelle: "S'inscrire", iconItem: Icons.add_box, isSelect: false),
  ItemFiltre(libelle: 'Connexion', iconItem: Icons.apps, isSelect: false),
  ItemFiltre(libelle: 'Autres', iconItem: Icons.build, isSelect: false),
  // MenuFooter(title: 'Autres', icon:Icons.tune, color:Colors.teal),
  //*/
];
