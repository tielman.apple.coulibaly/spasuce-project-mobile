import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:spasuce/const_app.dart';

class AppConfig extends InheritedWidget {
  AppConfig({
    //@required this.appName,
    @required this.flavorName,
    @required this.apiBaseUrl,
    @required Widget child,
  }) : super(child: child);

  //final String appName;
  final String flavorName;
  final String apiBaseUrl;

  static AppConfig of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType(aspect: AppConfig);
    //return context.inheritFromWidgetOfExactType(AppConfig);

  }
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false; // pour dire ue la config ne changera pas pendand l'excution
}