import 'dart:convert';
import 'dart:io' as Io;
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
//import 'package:simple_permissions/simple_permissions.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/dao/entity/user_entity.dart';
import 'package:spasuce/dao/repository/database_helper_old_.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/models/user.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';
import 'package:spasuce/views/spasuce_loader.dart';
import 'package:sqflite/sqlite_api.dart';

enum PermissionOld {
  RecordAudio,
  CallPhone,
  Camera,
  PhotoLibrary,
  WriteExternalStorage,
  ReadExternalStorage,
  ReadPhoneState,
  AccessCoarseLocation,
  AccessFineLocation,
  WhenInUseLocation,
  AlwaysLocation,
  ReadContacts,
  ReadSms,
  SendSMS,
  Vibrate,
  WriteContacts,
  AccessMotionSensor
}

class Utilities {
  static begin(element) {
    print("*********************BEGIN $element*********************");
  }

  static log(element) {
    print("*********************LOG $element*********************");
  }

  static end(element) {
    print("*********************END $element*********************");
  }

  static validatorObject(dynamic value,
      {String typeContentToValidate,
      int minLengthValue,
      int maxLengthValue,
      dynamic message = erreurSaisieTexte}) {
    if (value == null) {
      return message;
    }
  }

  static validator(dynamic value,
      {String typeContentToValidate,
      int minLengthValue,
      int maxLengthValue,
      dynamic message = erreurSaisieTexte}) {
    if (value != null && !value.isEmpty) {
      //if (value != null) {
      if (minLengthValue != null &&
          minLengthValue > 0 &&
          value.toString().length < minLengthValue) {
        return message;
      }
      if (maxLengthValue != null &&
          maxLengthValue > 0 &&
          value.toString().length > maxLengthValue) {
        return message;
      }

      switch (typeContentToValidate) {
        case v_email:
          Pattern pattern =
              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
          RegExp regex = new RegExp(pattern);
          if (!regex.hasMatch(value)) return formatInvalide;

          break;

        case v_password:
          break;
        case v_login:
          break;
        case v_phone:
          break;
        case v_number:
          break;

        default:
          break;
      }
    } else {
      return message;
    }
  }

  static isNotEmpty(List<dynamic> items) {
    return (items != null && items.length > 0);
  }

  static isNotBlank(String value) {
    return (value != null && value.isNotEmpty);
  }

  static String initialStringValueWidget(String initialValue,
          {String defaultValue}) =>
      isNotBlank(initialValue)
          ? initialValue
          : (isNotBlank(defaultValue) ? defaultValue : "");
  static int initialIntValueWidgetOld(int initialValue) {
    int value;
    if (initialValue != null) {
      value = initialValue;
    }
    return value;
  }

  static String initialiserDureeAOEnGet(
      {int nombreTypeDuree, String typeDureeLibelle}) {
    String value = "";
    if (nombreTypeDuree == null &&
        (typeDureeLibelle == null || typeDureeLibelle.isEmpty)) {
      value = aucunDureeSaisie;
    } else {
      if (nombreTypeDuree != null) {
        value += nombreTypeDuree.toString() + espace;
      }
      if (typeDureeLibelle.isNotEmpty) {
        value += typeDureeLibelle;
      }
    }
    return value;
  }

  static String initialNumberValueWidget(dynamic initialValue,
          {String defaultValue}) =>
      (initialValue != null)
          ? initialValue.toString()
          : (isNotBlank(defaultValue) ? defaultValue : "");

  static String initialIntValueWidget(int initialValue,
          {String defaultValue}) =>
      (initialValue != null)
          ? initialValue.toString()
          : (isNotBlank(defaultValue) ? defaultValue : "");

  static onSelectDrawerItem({dynamic context, dynamic view, int index}) {
    /*
    setState(() {
      _selectedDrawerIndex = index ;
    });
    */
    Navigator.of(context).pop(); // pour fermer le menu apres clic
    Navigator.push(context, MaterialPageRoute(builder: (context) => view));
  }

  static getToast(String message,
      {dynamic toastLength = Toast.LENGTH_SHORT,
      dynamic gravity = ToastGravity.BOTTOM,
      Color backgroundColor = Colors.black,
      Color textColor = Colors.white,
      fontSize}) {
    return Fluttertoast.showToast(
        msg: message,
        toastLength: toastLength,
        gravity: gravity,
        timeInSecForIosWeb: 1,
        backgroundColor: backgroundColor,
        textColor: textColor,
        fontSize: 16.0);
  }

  static pop(BuildContext context) => Navigator.pop(context);

  static navigate(BuildContext context, Widget view) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => view,
      ),
    );
  }

  static passwordFormField(User model,
      {String labelText,
      String helperText,
      TextEditingController controller,
      InputDecoration decoration}) {
    return TextFormField(
      controller: controller,
      obscureText: true,
      onSaved: (input) {
        model.password = input.trim();
      },
      // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_password),
      decoration: (decoration != null)
          ? decoration
          : InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.visiblePassword, // le type du input
    );
  }

  static textFormField(User model, {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        model.login = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_text),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.text, // le type du input
    );
  }

  static textFormFieldNom(User model,
      {String labelText = "",
      String helperText,
      InputBorder border,
      bool withValidator = false,
      TextEditingController controller}) {
    if (withValidator) {
      return TextFormField(
        controller: controller,
        onSaved: (input) {
          model.nom = input.trim();
        }, // recuperation des données saisies
        validator: (value) =>
            Utilities.validator(value, typeContentToValidate: v_text),
        decoration:
            InputDecoration(labelText: labelText, helperText: helperText),
        keyboardType: TextInputType.text, // le type du input
      );
    }
    return TextFormField(
      onSaved: (input) {
        model.nom = input;
      },
      // recuperation des données saisies
      //validator:(value) => Utilities.validator(value, typeContentToValidate: v_text),
      decoration: InputDecoration(
          labelText: labelText, helperText: helperText, border: border),
      keyboardType: TextInputType.text, // le type du input
    );
  }

  static textFormFieldPrenom(User model,
      {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        model.prenoms = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_text),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.text, // le type du input
    );
  }

  static textFormFieldObjet(User model,
      {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        model.code = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_text),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.text, // le type du input
    );
  }

  static loginFormField(User model,
      {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        model.login = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_text),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.text, // le type du input
    );
  }

  static urlFormField(champToSet, {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        champToSet = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_text),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.url, // le type du input
    );
  }

  static emailFormField(User model,
      {String labelText = "",
      String helperText,
      InputBorder border,
      TextEditingController controller}) {
    return TextFormField(
      controller: controller,

      onSaved: (input) {
        model.email = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_email),
      decoration: InputDecoration(
          labelText: labelText, helperText: helperText, border: border),
      keyboardType: TextInputType.emailAddress, // le type du input
    );
  }

  static phoneFormField(User model,
      {String labelText = "",
      String helperText,
      bool withValidator = false,
      InputBorder border,
      TextEditingController controller}) {
    if (withValidator) {
      return TextFormField(
        controller: controller,
        onSaved: (input) {
          model.telephone = input.trim();
        }, // recuperation des données saisies
        validator: (value) =>
            Utilities.validator(value, typeContentToValidate: v_phone),
        decoration: InputDecoration(
            labelText: labelText, helperText: helperText, border: border),
        keyboardType: TextInputType.phone, // le type du input
      );
    }

    return TextFormField(
      controller: controller,
      onSaved: (input) {
        model.telephone = input.trim();
      },
      // recuperation des données saisies
      //validator:(value) => Utilities.validator(value, typeContentToValidate: v_phone),
      decoration: InputDecoration(
          labelText: labelText, helperText: helperText, border: border),
      keyboardType: TextInputType.phone, // le type du input
    );
  }

  static dateTimeFormField(champToSet,
      {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        champToSet = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_datetime),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.datetime, // le type du input
    );
  }

  static numberFormField(champToSet,
      {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        champToSet = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_number),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.number, // le type du input
    );
  }

  static multilineFormField(User model,
      {String labelText = "",
      String helperText,
      TextEditingController controller}) {
    return TextFormField(
      controller: controller,
      maxLines: 6,
      onSaved: (input) {
        model.contenu = input.trim();
      },
      // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_text),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.multiline, // le type du input
    );
  }

  static numberWithOptionsFormField(champToSet,
      {String labelText = "", String helperText}) {
    return TextFormField(
      onSaved: (input) {
        champToSet = input.trim();
      }, // recuperation des données saisies
      validator: (value) =>
          Utilities.validator(value, typeContentToValidate: v_number),
      decoration: InputDecoration(labelText: labelText, helperText: helperText),
      keyboardType: TextInputType.numberWithOptions(), // le type du input
    );
  }

  static Future<ResponseCustom> initialize(
      RequestCustom request, String uri) async {
    //var endpoint = "secteurDactiviteAppelDoffres/getByCriteriaCustom";
    //var uri = uri;

    print("Future<Request> sent ${request}");
    return await baseApi.testApi(apiBaseUrl: uri, request: request);

    //ResponseCustom response =

    //return response ;
    /*
    print("Future<ResponseCustom> ${response.hasError}");
    if (response != null &&
        !response.hasError &&
        Utilities.isNotEmpty(response.items)) {
      setState(() {
        widget.listAO = response.items;
      });
    }

     */
  }

  static Widget projectWidget(
      Widget Template(User), Future<dynamic> futureMethode()) {
    return FutureBuilder(
      builder: (context, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.none &&
            projectSnap.hasData == null) {
          //print('project snapshot data is: ${projectSnap.data}');
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
          //itemCount: projectSnap.data.items.length,
          itemCount: ((projectSnap.data != null &&
                  Utilities.isNotEmpty(projectSnap.data.items))
              ? projectSnap.data.items.length
              : 0),
          itemBuilder: (context, index) {
            //ProjectModel project = projectSnap.data[index];
            User user = User.fromJson(projectSnap.data.items[index]);
            return Template(user);
          },
        );
      },
      future: futureMethode(),
    );
  }

  static messageApi({ResponseCustom response, String typeContenu}) {
    var message = problemeDeConnexion;
    if (response != null) {
      if (!response.hasError) {
        if (response.status != null) {
          // a voir
          Utilities.getToast(response.status['message']);
        } else {
          Utilities.getToast(succes);
        }
      } else {
        if (response.status != null) {
          Utilities.getToast(response.status['message']);
        }
      }
    } else {
      switch (typeContenu) {
        case connexion:
          message = erreurConnexion;
          break;
        default:
          break;
      }
      Utilities.getToast(echec);
    }
  }

  static clearForm(List<TextEditingController> controllers) {
    if (isNotEmpty(controllers)) {
      for (var controller in controllers) {
        if (controller != null) {
          controller.clear();
        }
      }
    }
  }

  static dynamic loadDataForDropdownMenu(
      {RequestCustom request, String uri, dynamic dropdownMenuItems}) {
    baseApi.testApi(apiBaseUrl: uri, request: request).then((res) {
      begin("Utilities/loadDataForDropdownMenu");
      print(res);

      dropdownMenuItems.clear(); // vider la liste
      if (res != null && !res.hasError && Utilities.isNotEmpty(res.items)) {
        for (var element in res.items) {
          User user = User.fromJson(element);
          dropdownMenuItems.add(DropdownMenuItem(
            child: Text(user.libelle),
            value: user,
          ));
        }
      }
      return dropdownMenuItems;
    }).catchError((err) {
      print(err);
    });
  }

  static Directory _downloadsDirectory;
  /*
  static Future<Directory> get initDownloadsDirectory async {
    if (_downloadsDirectory != null) return _downloadsDirectory;
    _downloadsDirectory = await DownloadsPathProvider.downloadsDirectory;
    return _downloadsDirectory;
  }

  static initDownloadsDirectoryOld() {
    if (_downloadsDirectory != null) {
      return _downloadsDirectory;
    } else {
      try {
        // recuperation du _downloadsDirectory
        DownloadsPathProvider.downloadsDirectory
            .then((value) => _downloadsDirectory = value)
            .catchError((err) => print(err));
      } on PlatformException {
        print('Could not get the downloads directory');
      }
    }
  }
  */

// convert file in base64
  static base64InFile({String base64, String nameFile}) async {
    /*
    try{

    }catch(Exception e){

    }

     */

    var status = await Permission.storage.status;
    PermissionStatus permission;
    if (!status.isGranted) {
      permission = await Permission.storage.request();
    }

    if ((permission != null && permission.isGranted) || status.isGranted) {
      var pathDocumentsDirectory =
          (await getApplicationDocumentsDirectory()).path;
      var pathExternal = (await getExternalStorageDirectory()).path;

      // creer le rep s'il n'existait pas

      Directory directory = Directory(pathExternal + folderSpasuce);

      bool existDirectory = await directory.exists();
      log("existDirectory $existDirectory");

      if (!existDirectory) {
        directory = await directory.create(recursive: true);
      }
      log("directory $directory");

      // if (directory) {}

      //await initDownloadsDirectory;

      final decodedBytes = base64Decode(base64);

      log("pathDocumentsDirectory $pathDocumentsDirectory");
      log("pathExternal $pathExternal");
      //log(_downloadsDirectory.path);
      //PermissionStatus permissionResult =await SimplePermissions.requestPermission(Permission.WriteExternalStorage);
      //if (permissionResult == PermissionStatus.authorized) {
      // code of read or write file in external storage (SD card)

      var test =
          "/storage/emulated/0/Android/data/com.google.android.apps.docs/cache/projector";
      var test2 = "/storage/emulated/0/Android/data/";
      var test3 = "/storage/emulated/0/Spasuce/files/";
      var test5 = "/storage/emulated/0/Download/";
      var test4 = "/storage/emulated/0/Android/";
      log(test2);

      //var file = File(_downloadsDirectory.path + "/" + nameFile);
      //var file = File(test2 + "/" + nameFile);

      //var fileO = File.fromUri(Uri.https(authority, unencodedPath));
      var file = File(test2 + nameFile);
      var file3 = File(test3 + nameFile);
      var file4 = File(test4 + nameFile);
      var file5 = File(test5 + nameFile);
      file.writeAsBytesSync(decodedBytes);
      file3.writeAsBytesSync(decodedBytes);
      file4.writeAsBytesSync(decodedBytes);
      file5.writeAsBytesSync(decodedBytes);
      var existFile = await file3.exists();
      log("existFile $existFile");
      log("file.path ${file3.path}");
    }

    //}
    //file.
  }

// convert file in base64
  static saveFileFromUri({String uriFile, String nameNewFile}) async {
    /*
    try{

    }catch(Exception e){

    }

     */

    var status = await Permission.storage.status;
    PermissionStatus permission;
    if (!status.isGranted) {
      permission = await Permission.storage.request();
    }

    if ((permission != null && permission.isGranted) || status.isGranted) {
      //var pathDocumentsDirectory = (await getApplicationDocumentsDirectory()).path;
      var pathExternal = (await getExternalStorageDirectory()).path;
      log("pathExternal $pathExternal");

      // creer le rep s'il n'existait pas
      Directory directory = Directory(pathExternal + folderSpasuce);

      bool existDirectory = await directory.exists();
      log("existDirectory $existDirectory");

      if (!existDirectory) {
        directory = await directory.create(recursive: true);
      }
      log("directory $directory");

      //final decodedBytes = base64Decode(base64);

      //log("pathDocumentsDirectory $pathDocumentsDirectory");
      downloadFileFromUri(
          uriFile: uriFile, locationNewFile: directory.path + nameNewFile);

      //copyFileFromUrl( pathNewFile: directory.path + nameNewFile, uriFile: uriFile);
      //downloadFileFromUri(uriFile: uriFile, locationNewFile:directory.path + nameNewFile ) ;
      //downloadFileFromUri(uriFile: uriFile, locationNewFile: "/storage/emulated/0/Download/" + nameNewFile) ;
      //downloadFileFromUri(uriFile: uriFile, locationNewFile: "/storage/emulated/0/" + nameNewFile) ;
    }

    //}
    //file.
  }

  static downloadFileFromUri({String uriFile, String locationNewFile}) {
    String uri = uriFile.replaceFirst(baseUrlWithSSL, baseUrlWithoutSSL);
    print('uri  uriuri  $uri');
    client.get(uri).then((response) {
      File(locationNewFile).writeAsBytes(response.bodyBytes).then((file) {
        log("existFile $locationNewFile: ${file.exists()}");
        log("file.path ${file.path}");
        getToast(telechargementReussi);
      });
    }).catchError((err) {
      getToast(telechargementEchoue);
      print("error downloadFileFromUri $err");
    });
  }

  // demande de permission
  static demandeDePermission() async {
    //PermissionStatus permissionResult =await SimplePermissions.requestPermission(Permission.WriteExternalStorage);
  }

// convert base64 in file
  static String fileInBase64(String pathFile) {
    log("pathFile: $pathFile");
    final bytes = File(pathFile).readAsBytesSync();

    String img64 = base64Encode(bytes);
    //print(img64.substring(0, 100));
    log("img64: $img64");
    return img64;
  }

// convert base64 in file
  static copyFileFromUrl({String uriFile, String pathNewFile}) {
    log("uriFile: $uriFile");
    log("pathNewFile: $pathNewFile");
    String uri = uriFile.replaceFirst(baseUrlWithSSL, baseUrlWithoutSSL);
    log("uri: $uri");

    var fileFichier = File(uri);
    log("fileFichier.path ${fileFichier.path}");

    fileFichier.copy(pathNewFile).then((file) {
      log("existFile $pathNewFile: ${file.exists()}");
      log("file.path ${file.path}");

      getToast(telechargementReussi);
    }).catchError((err) {
      print("error copyFileFromUrl $err");

      getToast(telechargementEchoue);
    });
  }

  static String formatDate({DateTime date, String format = dd_MM_yyyy_slash}) {
    return DateFormat(format).format(date);
  }

  static DateTime currentDate({String defaultFormat = dd_MM_yyyy_slash}) {
    return DateFormat(defaultFormat).parse(formatDate(date: DateTime.now()));

    //return DateTime.now();
  }

  static DateTime parseDate({String string, String format = dd_MM_yyyy_slash}) {
    return DateFormat(format).parse(string);
  }

  static String formatTime({TimeOfDay timeOfDay}) {
    return "${timeOfDay.hour}:${timeOfDay.minute}";
  }

  static TimeOfDay parseTime({String stringHeure}) {
    dynamic tHeure = stringHeure.split(":");
    return TimeOfDay(hour: int.parse(tHeure[0]), minute: int.parse(tHeure[1]));
  }

// TODO: a finialiser
  static bool isFirstTimeBeforeSecondTime(
      {TimeOfDay firstTimeOfDay, TimeOfDay secondTimeOfDay}) {
    // comparaison des heures
    if (firstTimeOfDay.hour < secondTimeOfDay.hour) {}

    return true;
  }

  static DateTime firstDateCalendar() {
    return DateTime.now();
    //return DateTime(2020, 1);
  }

  static DateTime lastDateCalendar() {
    return DateTime(2100, 12);
  }

  // calendar
  static Future<DateTime> showDatePickerCustomOld(
      {TextEditingController controller, BuildContext context}) {
    //Locale locale = Locale('fr', '') ;
    DateTime currentDate = (Utilities.isNotBlank(controller.text)
        ? Utilities.parseDate(string: controller.text)
        : DateTime.now());
    return showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: Utilities.firstDateCalendar(),
        lastDate: Utilities.lastDateCalendar());
  }

  // calendar
  static showDatePickerCustom(
      {TextEditingController controller, BuildContext context}) {
    //Locale locale = Locale('fr', '') ;
    DateTime currentDate = (Utilities.isNotBlank(controller.text)
        ? Utilities.parseDate(string: controller.text)
        : DateTime.now());
    showDatePicker(
            context: context,
            initialDate: currentDate,
            //firstDate: Utilities.firstDateCalendar(),
            firstDate: (currentDate.isBefore(DateTime.now()))
                ? currentDate
                : DateTime.now(),
            lastDate: Utilities.lastDateCalendar())
        .then((picked) {
      if (picked != null) {
        controller.text = Utilities.formatDate(date: picked);
      }
    });
  }

  // calendar
  static showTimePickerCustom(
      {TextEditingController controller, BuildContext context}) {
    //Locale locale = Locale('fr', '') ;
    TimeOfDay currentTime = (Utilities.isNotBlank(controller.text)
        ? Utilities.parseTime(stringHeure: controller.text)
        : TimeOfDay.now());
    showTimePicker(
      context: context,
      initialTime: (isFirstTimeBeforeSecondTime(
              firstTimeOfDay: currentTime, secondTimeOfDay: TimeOfDay.now()))
          ? currentTime
          : TimeOfDay.now(),
    ).then((picked) {
      if (picked != null) {
        controller.text = Utilities.formatTime(timeOfDay: picked);
      }
    });
  }

/*
  // methode de date
  _showDatePicker(TextEditingController controller) {
    //Locale locale = Locale('fr', '') ;
    DateTime currentDate = (Utilities.isNotBlank(controller.text)
        ? Utilities.parseDate(string: controller.text)
        : DateTime.now());
    showDatePicker(
            context: context,
            initialDate: currentDate,
            firstDate: Utilities.firstDateCalendar(),
            lastDate: Utilities.lastDateCalendar())
        .then((picked) {
      //if (picked != null && picked != currentDate) {
      if (picked != null) {
        setState(() {
          controller.text = Utilities.formatDate(date: picked);
        });
      }
    });
  }
  */

  static matchFn(DropdownMenuItem<dynamic> item, keyword) {
    //Utilities.log(item.value);
    //User data = User.fromJson(item.value) ;
    return (item.value.libelle
        .toString()
        .toLowerCase()
        .contains(keyword.toLowerCase()));
  }

  static searchFn(keyword, items) {
    // la fonction de recherche customiser
    //begin("searchFn keyword $keyword") ;
    List<int> shownIndexes = [];
    int i = 0;
    if (Utilities.isNotEmpty(items)) {
      items.forEach((item) {
        if (matchFn(item, keyword) || (keyword?.isEmpty ?? true)) {
          shownIndexes.add(i);
        }
        i++;
      });
    }
    return (shownIndexes);
  }

  static List<dynamic> formationDatasFiles(
      {Map<String, dynamic> listFileWithContent, bool isFichierOffre}) {
    List<User> datasFiles = List();
    begin("formationDatasFiles");
    if (listFileWithContent != null && listFileWithContent.length > 0) {
      listFileWithContent.forEach((k, v) {
        User user = User();
        //user.urlLogo = k;
        // convertion du fichier
        if (isNotBlank(v) && v.contains(".")) {
          user.fichierBase64 = Utilities.fileInBase64(v);
        } else {
          user.fichierBase64 = v;
        }

        if (isNotBlank(k) && k.contains(".")) {
          user.extensionLogo = k.split(".").last;
          user.extension = user.extensionLogo;
          user.name = k.split(".").first;
          user.urlLogo = user.name;
        } else {
          user.name = k;
          user.urlLogo = user.name;
        }
        user.isFichierOffre = isFichierOffre;
        log("--------------------------");
        log(user.name);
        log(user.extension);
        log(user.extensionLogo);
        log("dans a recuperation ${user.fichierBase64}");
        log("--------------------------");

        datasFiles.add(user);
      });
    }
    end("formationDatasFiles");
    return datasFiles;
  }

  static List<dynamic> getElementInList(
      {List<dynamic> list, int firstIndex, int lastIndex}) {
    return (isNotEmpty(list))
        ? (lastIndex > 0 && lastIndex >= firstIndex)
            ? list.sublist(firstIndex, lastIndex)
            : list.sublist(firstIndex)
        : List();
  }

  static String buildErrorFindByPassword(String nomFournisseur) {
    return messageDebut + nomFournisseur + messageFin;
  }

  static List initialValueMultiSelectFormField({List<dynamic> initialList}) {
    List finalList = List();
    if (isNotEmpty(initialList)) {
      return initialList.map((data) {
        return data['libelle'];
      }).toList();
    } else {}
    return finalList;
  }

  static Widget dataNotFound() => Center(child: Text(libelleDataNotFound));

  static getBoxDecoration() => BoxDecoration(
        color: ConstApp.color_app,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(valueBorderRaduisPopupTitle),
          topRight: Radius.circular(valueBorderRaduisPopupTitle),
        ),
      );

  static getRoundedRectangleBorder() => RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      );

  static TextStyle styleText() => TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 30,
      color: ConstApp.color_app_rouge);

  static TextStyle styleTextTitreH3() => TextStyle(
        fontWeight: FontWeight.bold,
      );

  static getShape(
          {double topRight = 0.0,
          double topLeft = 0.0,
          double bottomLeft = 0.0,
          double bottomRight = 0.0}) =>
      RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          topRight: Radius.circular(topRight),
          bottomLeft: Radius.circular(bottomLeft),
          bottomRight: Radius.circular(bottomRight),
        ),
      );
  static getDefaultShape(
          {double topRight = 10.0,
          double topLeft = 10.0,
          double bottomLeft = 10.0,
          double bottomRight = 10.0}) =>
      RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft),
          topRight: Radius.circular(topRight),
          bottomLeft: Radius.circular(bottomLeft),
          bottomRight: Radius.circular(bottomRight),
        ),
      );

  // initialisation du user connecter

  /*
  static Future<User> initialiseUserConnecter(User userConnecter) async {
    //User user;
    DatabaseHelper db = DatabaseHelper.instance;
    List<UserEntity> allUser = await db.getUserConnecter();

    if (userConnecter != null) {
      if (allUser == null) {
        await db.insert(UserEntity.fromJson(userConnecter.toJson()));
      }
    } else {
      // verifier s'il etait connecter avant
      if (allUser != null) {
        // userConnecter
        userConnecter = User();
        UserEntity userEntity = allUser[0];
        userConnecter.id = userEntity.id;
        userConnecter.nom = userEntity.nom;
        userConnecter.entrepriseNom = userEntity.entrepriseNom;
        userConnecter.login = userEntity.login;
      }
    }

    return userConnecter;
  }
  */

  static resetAndOpenPage({dynamic context, dynamic view}) {
    begin("resetAndOpenPage");
    //pushNamedAndRemoveUntil
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (BuildContext context) => view != null ? view : MyApp()),
        (Route<dynamic> route) => false

        //ModalRoute.withName('/'),
        );
    end("resetAndOpenPage");
  }

  static RaisedButton getButtonSubmit({
    Function onPressed,
    Widget child,
    RoundedRectangleBorder shape,
    Color textColor = Colors.white,
    Color color = ConstApp.color_app,
    bool isDisable = false,
  }) {
    return RaisedButton(
      color: color,
      textColor: textColor,
      //color: isDisable ? null : color,
      //textColor: isDisable ? Colors.black : textColor,
      //disabledColor: ConstApp.color_button_disable,
      disabledColor: Colors.grey[300],
      disabledTextColor: Colors.black,
      shape: shape != null ? shape : Utilities.getDefaultShape(),
      child: child,
      onPressed: isDisable ? null : onPressed,
    );
  }

  static disableButton({State<dynamic> context, bool value}) {
    context.setState(() {
      value = true;
    });
  }

  static Widget afficherListeFichierTechargeable(
      {BuildContext context, List<dynamic> datasFichier}) {
    return (datasFichier != null && datasFichier.length > 0)
        ? Container(
            padding: const EdgeInsets.only(bottom: 30.0),
            height: MediaQuery.of(context).size.height * 0.25,
            child: Scrollbar(
                child: ListView.separated(
              itemCount: datasFichier.length,
              itemBuilder: (BuildContext context, int index) {
                final String nameFile = datasFichier[index][name];
                final String urlFile = datasFichier[index][urlFichier];

                return new ListTile(
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        // return object of type Dialog
                        return AlertDialog(
                          title: new Text(telechargerFichier),
                          content: new Text(questionTelechargerFichier),
                          actions: <Widget>[
                            // usually buttons at the bottom of the dialog
                            new FlatButton(
                              child: new Text(reponseNon),
                              onPressed: () {
                                Navigator.of(context).pop();
                                //Utilities.onSelectDrawerItem(context: context, view: MyApp(userConnecter: null));
                              },
                            ),
                            new FlatButton(
                              child: new Text(reponseOui),
                              onPressed: () {
                                // telechager le file

                                //Utilities.base64InFile(base64: datasFichier[index][fichierBase64],nameFile: nameFile);

                                Utilities.saveFileFromUri(
                                    uriFile: urlFile, nameNewFile: nameFile);

                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  },

                  title: Text(
                    nameFile,
                  ),
                  //subtitle: new Text(path),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  new Divider(),
            )),
          )
        : Container();
  }

  static Widget afficherMultiSelectFormFieldInRead(
      {List<dynamic> list, String titleText, String textField}) {
    return AbsorbPointer(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16.0),
        child: MultiSelectFormField(
          titleText: titleText,
          hintText: hintMultiSelectForm,
          initialValue: list.map((e) => e[textField]).toList(),
          dataSource: list,
          textField: textField,
          valueField: textField,
        ),
      ),
    );
  }

  static progressingAfterSubmit({BuildContext context}) {
    showDialog(
      context: context,
      //barrierDismissible: false,
      builder: (BuildContext context) {
        return SpasuceLoader();
      },
    );
  }

  static progressingAfterSubmitOld({BuildContext context}) {
    showDialog(
      context: context,
      //barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              //mainAxisSize: MainAxisSize.min,
              children: [
                CircularProgressIndicator(),
                SizedBox(
                  // mettre espace entre 2 elements width/height
                  width: 12.0,
                ),
                Text(etatEnCours),
              ],
            ),
          ),
        );
      },
    );
    Future.delayed(Duration(seconds: 3), () {
      Navigator.pop(context); //pop dialog
      //_login();
    });
  }
/*
  static enableButton() {
    setState(() {
      _isDisable = false;
    });
  }

  static changeEtat({Function setState}) {
    setState(() {
      _isDisable = !_isDisable;
    });
  }
  */

  static void databaseLog(String functionName, String sql,
      [List<Map<String, dynamic>> selectQueryResult,
      int insertAndUpdateQueryResult,
      List<dynamic> params]) {
    print(functionName);
    print(sql);
    if (params != null) {
      print(params);
    }
    if (selectQueryResult != null) {
      print(selectQueryResult);
    } else if (insertAndUpdateQueryResult != null) {
      print(insertAndUpdateQueryResult);
    }
  }
}
