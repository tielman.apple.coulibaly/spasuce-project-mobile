import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:spasuce/models/response_custom.dart';
import 'package:spasuce/models/request_custom.dart';

class BaseApi{

  Future<ResponseCustom> testApi ({String apiBaseUrl, RequestCustom request}) async {
    Utilities.begin("$apiBaseUrl") ;

    //var response = await netWorkCalls.post(bodyRequest: request, uri: apiBaseUrl + "entreprise/getByCriteria") ;
    var response = await netWorkCalls.post(bodyRequest: request, uri: apiBaseUrl) ;
    var res1 = ResponseCustom.fromJson(jsonDecode(response)) ;

    Utilities.log("contenu ResponseCustom ${res1.toJson()}") ;
    return res1 ;
  }

 ResponseCustom testApiCustom({String apiBaseUrl, RequestCustom request}){
    Utilities.begin("$apiBaseUrl") ;

    //var response = await netWorkCalls.post(bodyRequest: request, uri: apiBaseUrl + "entreprise/getByCriteria") ;
    var response = netWorkCalls.postCustom(bodyRequest: request, uri: apiBaseUrl) ;
    var res1 = ResponseCustom.fromJson(jsonDecode(response)) ;

    Utilities.log("contenu ResponseCustom ${res1.toJson()}") ;
    return res1 ;
  }
 
  /*
  ResponseCustom<T> testApiCustom <T>({String apiBaseUrl, RequestCustom request}){
    print("Begin $apiBaseUrl") ;

    //var response = await netWorkCalls.post(bodyRequest: request, uri: apiBaseUrl + "entreprise/getByCriteria") ;
    var response = netWorkCalls.postCustom(bodyRequest: request, uri: apiBaseUrl) ;
    var res1 = ResponseCustom.fromJson(jsonDecode(response)) ;

    print("contenu ResponseCustom ${res1.toJson()}") ;
    print("End $apiBaseUrl") ;
    return res1 ;
  }
 
  */
}