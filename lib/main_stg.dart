import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/internationalization/app_localizations.dart';
import 'package:spasuce/main.dart';
import 'package:spasuce/ressources/config_app.dart';
import 'package:spasuce/verification_in_background.dart';

void main() {
  var configuredApp = AppConfig(
    flavorName: flavorNameStg,
    child: MaterialApp(
      //home: MyApp(),
      home: VerificationInBackground(),
      debugShowCheckedModeBanner: false,
    ),

    //child: MyApp(),
    apiBaseUrl: base_url_preprod,
  );
  return runApp(configuredApp);
}
