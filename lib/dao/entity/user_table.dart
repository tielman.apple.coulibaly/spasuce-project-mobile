import 'package:spasuce/dao/repository/database_creator.dart';
import 'package:spasuce/models/user.dart';

class UserTable {
  int id;
  String name;
  String info;
  bool isDeleted;
  //int isDeleted;

/*
  String email;
  String entrepiseNom;
  String nom;
  String typeEntrepriseCode;
  */

  UserTable({this.id, this.name, this.info, this.isDeleted});

  UserTable.fromJson(Map<String, dynamic> json) {
    this.id = json[DatabaseCreator.id];
    this.name = json[DatabaseCreator.name];
    this.info = json[DatabaseCreator.info];
    //this.isDeleted = json[DatabaseCreator.isDeleted] == 1;
    this.isDeleted = json[DatabaseCreator.isDeleted];
  }

  Map<String, dynamic> _$UserToJson(UserTable instance) => <String, dynamic>{
        'id': instance.id,
        'name': instance.name,
        'info': instance.info,
        'isDeleted': instance.isDeleted,
      };

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
