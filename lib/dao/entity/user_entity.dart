import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:spasuce/const_app.dart';

part 'user_entity.g.dart';

@JsonSerializable()
class UserEntity {
  int id;
  String nom;
  String entrepriseNom;
  String typeEntrepriseCode;
  String prenom;
  String email;
  String telephone;
  String login;
  String roleLibelle;

  int isLogged;
  int isDeleted;
  int createdBy;
  int updatedBy;
  int deletedBy;
  String createdAt;
  String updatedAt;
  String deletedAt;

  UserEntity({
    this.id,
    this.nom,
    this.prenom,
    this.entrepriseNom,
    this.typeEntrepriseCode,
    this.email,
    this.telephone,
    this.login,
    this.isLogged,
    this.roleLibelle,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.createdBy,
    this.updatedBy,
    this.deletedBy,
    this.isDeleted,
  });

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserEntityFromJson()` constructor.
  /// The constructor is named after the source class, in this case, User.
  factory UserEntity.fromJson(Map<String, dynamic> json) =>
      _$UserEntityFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserEntityToJson`.
  Map<String, dynamic> toJson() => _$UserEntityToJson(this);
}
