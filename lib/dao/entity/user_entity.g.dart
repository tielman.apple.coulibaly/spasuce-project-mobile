// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

/*
UserEntity _$UserEntityFromJson(Map<String, dynamic> json) {
  return UserEntity(
    id: (json['_id'] != null) ? json['_id'] : json['id'] as int,
    nom: json['nom'] as String,
    prenom: json['prenom'] as String,
    entrepriseNom: json['entrepriseNom'] as String,
    typeEntrepriseCode: json['typeEntrepriseCode'] as String,
    email: json['email'] as String,
    telephone: json['telephone'] as String,
    login: json['login'] as String,
    isLogged: ((json['isLogged'] != null)
        ? ((json['isLogged'] == false) ? 0 : 1)
        : 1) as int,
    roleLibelle: json['roleLibelle'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
    deletedAt: json['deletedAt'] as String,
    createdBy: json['createdBy'] as int,
    updatedBy: json['updatedBy'] as int,
    deletedBy: json['deletedBy'] as int,
    isDeleted: ((json['isDeleted'] != null)
        ? (json['isDeleted'] == true) ? 1 : 0
        : 0) as int,
  );
}

Map<String, dynamic> _$UserEntityToJson(UserEntity instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'nom': instance.nom,
      'entrepriseNom': instance.entrepriseNom,
      'typeEntrepriseCode': instance.typeEntrepriseCode,
      'prenom': instance.prenom,
      'email': instance.email,
      'telephone': instance.telephone,
      'login': instance.login,
      'roleLibelle': instance.roleLibelle,
      'isLogged': instance.isLogged,
      'isDeleted': instance.isDeleted,
      'createdBy': instance.createdBy,
      'updatedBy': instance.updatedBy,
      'deletedBy': instance.deletedBy,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
      'deletedAt': instance.deletedAt,
    };
Map<String, dynamic> _$UserEntityToJson(UserEntity instance) =>
    <String, dynamic>{
      columnId: instance.id,
      columnNom: instance.nom,
      "entreprisenom": instance.entrepriseNom,
      "typeentreprisecode": instance.typeEntrepriseCode,
      columnPrenom: instance.prenom,
      columnEmail: instance.email,
      columnTelephone: instance.telephone,
      columnLogin: instance.login,
      "rolelibelle": instance.roleLibelle,
    };

    UserEntity _$UserEntityFromJson(Map<String, dynamic> json) {
  return UserEntity(
    id: (json[id] != null) ? json[id] : json[id] as int,
    nom: json[columnNom] as String,
    prenom: json[columnPrenom] as String,
    entrepriseNom: json[columnEntrepriseNom] as String,
    typeEntrepriseCode: json[columnTypeEntrepriseCode] as String,
    email: json[columnEmail] as String,
    telephone: json[columnTelephone] as String,
    login: json[columnLogin] as String,
    isLogged: ((json[columnIsLogged] != null)
        ? ((json[columnIsLogged] == false) ? 0 : 1)
        : 1) as int,
    roleLibelle: json[columnRoleLibelle] as String,
    createdAt: json[columnCreatedAt] as String,
    updatedAt: json[columnUpdatedAt] as String,
    deletedAt: json[columnDeleteAt] as String,
    createdBy: json[columnCreatedBy] as int,
    updatedBy: json[columnUpdatedBy] as int,
    deletedBy: json[columnDeletedBy] as int,
    isDeleted: ((json[columnIsDeleted] != null)
        ? (json[columnIsDeleted] == true) ? 1 : 0
        : 0) as int,
  );

    

*/

Map<String, dynamic> _$UserEntityToJson(UserEntity instance) =>
    <String, dynamic>{
      columnId: instance.id,
      columnNom: instance.nom,
      columnEntrepriseNom: instance.entrepriseNom,
      columnTypeEntrepriseCode: instance.typeEntrepriseCode,
      columnPrenom: instance.prenom,
      columnEmail: instance.email,
      columnTelephone: instance.telephone,
      columnLogin: instance.login,
      columnRoleLibelle: instance.roleLibelle,
      columnIsLogged: instance.isLogged,
      columnIsDeleted: instance.isDeleted,
      columnCreatedBy: instance.createdBy,
      columnUpdatedBy: instance.updatedBy,
      columnDeletedBy: instance.deletedBy,
      columnCreatedAt: instance.createdAt,
      columnUpdatedAt: instance.updatedAt,
      columnDeleteAt: instance.deletedAt,
    };

UserEntity _$UserEntityFromJson(Map<String, dynamic> json) {
  return UserEntity(
    id: (json['_id'] != null) ? json['_id'] : json['id'] as int,
    nom: json['nom'] as String,
    prenom: json['prenom'] as String,
    entrepriseNom: json['entrepriseNom'] as String,
    typeEntrepriseCode: json['typeEntrepriseCode'] as String,
    email: json['email'] as String,
    telephone: json['telephone'] as String,
    login: json['login'] as String,
    isLogged: ((json['isLogged'] != null)
        ? ((json['isLogged'] == false) ? 0 : 1)
        : 1) as int,
    roleLibelle: json['roleLibelle'] as String,
    createdAt: json['createdAt'] as String,
    updatedAt: json['updatedAt'] as String,
    deletedAt: json['deletedAt'] as String,
    createdBy: json['createdBy'] as int,
    updatedBy: json['updatedBy'] as int,
    deletedBy: json['deletedBy'] as int,
    isDeleted: ((json['isDeleted'] != null)
        ? (json['isDeleted'] == true) ? 1 : 0
        : 0) as int,
  );
}
