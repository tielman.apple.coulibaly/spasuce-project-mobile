import 'package:path/path.dart';
import 'package:spasuce/const_app.dart';
import 'package:spasuce/dao/entity/user_entity.dart';
import 'package:spasuce/helpers/utilities.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite_migration/sqflite_migration.dart';

// singleton class to manage the database
class DatabaseHelper {
  static DatabaseHelper _databaseHelper; // singleton databaseHelper
  static Database _database; // singleton database
  static int userConnectedId;
  static UserEntity userConnected;

  static String _path;
  Future<String> get getPath async {
    if (_path == null) {
      _path = join(await getDatabasesPath(), _databaseName);
      //_database = await initializeDatabase();
    }
    return _path;
  }

  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "spasuce.db";
  //static final _databaseName = "ttttt.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 8;

  DatabaseHelper._createInstance(); // Named constructor to create instance of DatabaseHelper
  factory DatabaseHelper() {
    if (_databaseHelper == null) {
      _databaseHelper = DatabaseHelper
          ._createInstance(); // This is executed only once, singleton object
    }
    return _databaseHelper;
  }

  static DatabaseHelper instance = DatabaseHelper(); // singleton databaseHelper

  Future<Database> get database async {
    Utilities.begin(_database);
    if (_database == null) {
      //_database = await initializeDatabaseWithMigration();
      _database = await initializeDatabase();
    }
    Utilities.end(_database);
    return _database;
  }

  Future<UserEntity> get getUserConnected async {
    if (userConnected == null) {
      List<UserEntity> users = await getByCriteria();
      if (users != null && users.length > 0) {
        userConnected = users[0];
      }
    }
    return userConnected;
  }

  static final initialScript = [
    '''
              CREATE TABLE $tableUserEntitys (
                $columnId INTEGER PRIMARY KEY,
                $columnNom TEXT NULL,
                $columnEntrepriseNom TEXT NULL,
                $columnTypeEntrepriseCode TEXT NULL,
                $columnPrenom TEXT NULL,
                $columnEmail TEXT NULL,
                $columnTelephone TEXT NULL,
                $columnLogin TEXT NULL,
                $columnRoleLibelle TEXT NULL,
                $columnIsDeleted INTEGER NULL, 
                $columnIsLogged INTEGER NULL,
                $columnCreatedAt TEXT NULL,
                $columnCreatedBy INTEGER NULL,
                $columnUpdatedAt TEXT NULL,
                $columnUpdatedBy INTEGER NULL,
                $columnDeleteAt TEXT NULL,
                $columnDeletedBy INTEGER NULL
              )
              '''
  ];
  static final migrations = [
    "DROP TABLE IF EXISTS $tableUserEntitys",
    '''
              CREATE TABLE $tableUserEntitys (
                $columnId INTEGER PRIMARY KEY,
                $columnNom TEXT NULL,
                $columnEntrepriseNom TEXT NULL,
                $columnTypeEntrepriseCode TEXT NULL,
                $columnPrenom TEXT NULL,
                $columnEmail TEXT NULL,
                $columnTelephone TEXT NULL,
                $columnLogin TEXT NULL,
                $columnRoleLibelle TEXT NULL,
                $columnIsDeleted INTEGER NULL, 
                $columnIsLogged INTEGER NULL,
                $columnCreatedAt TEXT NULL,
                $columnCreatedBy INTEGER NULL,
                $columnUpdatedAt TEXT NULL,
                $columnUpdatedBy INTEGER NULL,
                $columnDeleteAt TEXT NULL,
                $columnDeletedBy INTEGER NULL
              )
              '''
  ];

  final config = MigrationConfig(
      initializationScript: initialScript, migrationScripts: migrations);

  initializeDatabaseWithMigration() async {
    // Open/create database
    return await openDatabaseWithMigration(await getPath, config);
    //join(await getDatabasesPath(), _databaseName), config);
  }

  initializeDatabase() async {
    // Open/create database
    //return await openDatabase(join(await getDatabasesPath(), _databaseName),
    return await openDatabase(await getPath,
        version: _databaseVersion, onCreate: _createDB, onUpgrade: _updateDB
        // bool is not a supported SQLite type. Use INTEGER and 0 and 1 values.
        //await db.execute("CREATE TABLE $tableUserEntitys ($columnId INTEGER PRIMARY KEY,$columnNom TEXT NULL,$columnEntrepriseNom TEXT NULL,$columnTypeEntrepriseCode TEXT NULL,$columnPrenom TEXT NULL,$columnEmail TEXT NULL,$columnTelephone TEXT NULL,$columnLogin TEXT NULL,$columnRoleLibelle TEXT NULL,$columnIsDeleted INTEGER NULL,  $columnIsLogged INTEGER NULL,$columnCreatedAt TEXT NULL,$columnCreatedBy INTEGER NULL,$columnUpdatedAt TEXT NULL,$columnUpdatedBy INTEGER NULL,$columnDeleteAt TEXT NULL,$columnDeletedBy INTEGER NULL)");
        );
  }

  void _createDB(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableUserEntitys (
                $columnId INTEGER PRIMARY KEY,
                $columnNom TEXT NULL,
                $columnEntrepriseNom TEXT NULL,
                $columnTypeEntrepriseCode TEXT NULL,
                $columnPrenom TEXT NULL,
                $columnEmail TEXT NULL,
                $columnTelephone TEXT NULL,
                $columnLogin TEXT NULL,
                $columnRoleLibelle TEXT NULL,
                $columnIsDeleted INTEGER NULL, 
                $columnIsLogged INTEGER NULL,
                $columnCreatedAt TEXT NULL,
                $columnCreatedBy INTEGER NULL,
                $columnUpdatedAt TEXT NULL,
                $columnUpdatedBy INTEGER NULL,
                $columnDeleteAt TEXT NULL,
                $columnDeletedBy INTEGER NULL
              )
              ''');
  }

  void _updateDB(Database db, int oldVersion, int newVersion) async {
    await db.execute("DROP TABLE IF EXISTS $tableUserEntitys");
    await db.execute('''
              CREATE TABLE $tableUserEntitys (
                $columnId INTEGER PRIMARY KEY,
                $columnNom TEXT NULL,
                $columnEntrepriseNom TEXT NULL,
                $columnTypeEntrepriseCode TEXT NULL,
                $columnPrenom TEXT NULL,
                $columnEmail TEXT NULL,
                $columnTelephone TEXT NULL,
                $columnLogin TEXT NULL,
                $columnRoleLibelle TEXT NULL,
                $columnIsDeleted INTEGER NULL, 
                $columnIsLogged INTEGER NULL,
                $columnCreatedAt TEXT NULL,
                $columnCreatedBy INTEGER NULL,
                $columnUpdatedAt TEXT NULL,
                $columnUpdatedBy INTEGER NULL,
                $columnDeleteAt TEXT NULL,
                $columnDeletedBy INTEGER NULL
              )
              ''');
    await db.execute("DROP TABLE IF EXISTS test");

    await db.execute('''
              CREATE TABLE test (
                $columnId INTEGER PRIMARY KEY,
                $columnNom TEXT NULL,
                $columnEntrepriseNom TEXT NULL,
                $columnTypeEntrepriseCode TEXT NULL,
                $columnPrenom TEXT NULL,
                $columnEmail TEXT NULL,
                $columnTelephone TEXT NULL,
                $columnLogin TEXT NULL,
                $columnRoleLibelle TEXT NULL,
                $columnIsDeleted INTEGER NULL, 
                $columnIsLogged INTEGER NULL,
                $columnCreatedAt TEXT NULL,
                $columnCreatedBy INTEGER NULL,
                $columnUpdatedAt TEXT NULL,
                $columnUpdatedBy INTEGER NULL,
                $columnDeleteAt TEXT NULL,
                $columnDeletedBy INTEGER NULL
              )
              ''');
  }

  // getAll/getByCriteria
  Future<List<UserEntity>> getByCriteria({int id}) async {
    Utilities.begin("DatabaseHelper/getByCriteria or getAll");
    Database db = await database;
    List<Map> maps = await db.query(tableUserEntitys,
        //columns: [columnId, columnNom, columnEmail],
        where: (id != null) ? '$columnId = ?' : null,
        whereArgs: (id != null) ? [id] : null);
    Utilities.end("DatabaseHelper/getByCriteria or getAll");

    if (maps.length > 0) {
      Utilities.end("DatabaseHelper/getByCriteria maps $maps");

      return maps.map((map) => UserEntity.fromJson(map)).toList();
    }
    return null;
  }

  // insert
  Future<UserEntity> insert(UserEntity userEntity) async {
    Utilities.begin("DatabaseHelper/insert");
    Database db = await database;
    //int id = await db.insert(tableUserEntitys, userEntity.toJson());
    print(" data to insert :  ${userEntity.toJson()}");
    userEntity.id = await db.insert(tableUserEntitys, userEntity.toJson());
    Utilities.end("DatabaseHelper/insert");
    return userEntity;
  }

  //deleteDatabase
  Future<void> deleteDB({String path}) async {
    Utilities.begin("DatabaseHelper/deleteDB");
    // Delete the database
    String defaultPath = await getPath;
    print("defaultPath ::: $defaultPath");

    await closeDB();

    //await deleteDatabase(path != null ? path : await getPath);
    await deleteDatabase(defaultPath);
    Utilities.end("DatabaseHelper/deleteDB");

    bool exist = await databaseExists(path != null ? path : await getPath);

    if (exist) {
      Utilities.begin("DatabaseHelper/databaseExists  $exist");
    } else {
      Utilities.end("DatabaseHelper/databaseExists  $exist");
    }
  }

  //rowInsert
  Future<void> rowInsert(UserEntity userEntity) async {
    /*final sql = '''INSERT INTO ${DatabaseCreator.todoTable}
    (
      ${DatabaseCreator.id},
      ${DatabaseCreator.name},
      ${DatabaseCreator.info},
      ${DatabaseCreator.isDeleted}
    )
    VALUES 
    (
      ${todo.id},
      "${todo.name}",
      "${todo.info}",
      ${todo.isDeleted ? 1 : 0}
    )''';*/

    final sql = '''INSERT INTO $tableUserEntitys
    (
      $columnEntrepriseNom,
      $columnTypeEntrepriseCode,
      $columnLogin,
      $columnRoleLibelle
    )
    VALUES (?,?,?,?)''';
    List<dynamic> params = [
      /*todo.id,
      todo.name,
      todo.info,
      todo.isDeleted ? 1 : 0,
      */

      userEntity.entrepriseNom,
      userEntity.typeEntrepriseCode,
      userEntity.login,
      userEntity.roleLibelle,
      //todo.isVisibleByClient ? 1 : 0
    ];
    Database db = await database;

    final result = await db.rawInsert(sql, params);
    Utilities.databaseLog('rowInsert', sql, null, result, params);
  }

  // update
  Future<UserEntity> update(UserEntity userEntity) async {
    Utilities.begin("DatabaseHelper/update");
    Database db = await database;
    //int id = await db.insert(tableUserEntitys, userEntity.toJson());
    print(" data to update :  ${userEntity.toJson()}");
    userEntity.id = await db.update(tableUserEntitys, userEntity.toJson(),
        where: (userEntity != null) ? '$columnId = ?' : null,
        whereArgs: (userEntity != null) ? [userEntity.id] : null);
    Utilities.end("DatabaseHelper/update");
    return userEntity;
  }

  // deleteAll
  deleteAll() async {
    Utilities.begin("DatabaseHelper/deleteAll");

    Database db = await database;
    return await db.delete(tableUserEntitys, where: '1');
  }

  // delete
  Future<int> delete(int id) async {
    Utilities.begin("DatabaseHelper/delete");

    Database db = await database;

    return await db
        .delete(tableUserEntitys, where: '$columnId = ?', whereArgs: [id]);
  }

  // close
  Future closeDB() async {
    Utilities.begin("DatabaseHelper/closeDB");
    Database db = await database;
    db.close();
    //db = null;
    _database = null;
    Utilities.end("DatabaseHelper/closeDB");
  }

  // close
  Future open() async {
    Database db = await database;
    db.isOpen;
  }

/*
  // Make this a singleton class.
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _databasez;
  Future<Database> get databasez async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  static List<UserEntity> userConnecter;
  Future<List<UserEntity>> getUserConnecter({int id}) async {
    //Future<List<UserEntity>> get userConnecter({int id}) async {
    if (userConnecter != null) return userConnecter;
    userConnecter = await getByCriteria(id: id);
    return userConnecter;
  }

  static int _idUuserConnecter;
  Future<int> idUserConnecter({int id}) async {
    //Future<List<UserEntity>> get userConnecter({int id}) async {
    if (_idUuserConnecter != null) {
      return _idUuserConnecter;
    } else {
      await getUserConnecter();
      if (userConnecter != null) {
        _idUuserConnecter = userConnecter[0].id;
      }
    }
    return _idUuserConnecter;
  }

  // open the database
  _initDatabase() async {
    Utilities.begin("DatabaseHelper/_initDatabase");

    // The path_provider plugin gets the right directory for Android or iOS.
    //Directory documentsDirectory = await getApplicationDocumentsDirectory();
    //String path = join(documentsDirectory.path, _databaseName);

    //String path = join(await getDatabasesPath(), _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(join(await getDatabasesPath(), _databaseName),
        version: _databaseVersion, onCreate: _onCreate
        // bool is not a supported SQLite type. Use INTEGER and 0 and 1 values.
        //await db.execute("CREATE TABLE $tableUserEntitys ($columnId INTEGER PRIMARY KEY,$columnNom TEXT NULL,$columnEntrepriseNom TEXT NULL,$columnTypeEntrepriseCode TEXT NULL,$columnPrenom TEXT NULL,$columnEmail TEXT NULL,$columnTelephone TEXT NULL,$columnLogin TEXT NULL,$columnRoleLibelle TEXT NULL,$columnIsDeleted INTEGER NULL,  $columnIsLogged INTEGER NULL,$columnCreatedAt TEXT NULL,$columnCreatedBy INTEGER NULL,$columnUpdatedAt TEXT NULL,$columnUpdatedBy INTEGER NULL,$columnDeleteAt TEXT NULL,$columnDeletedBy INTEGER NULL)");
        );
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    Utilities.begin("DatabaseHelper/_onCreate");

    // bool is not a supported SQLite type. Use INTEGER and 0 and 1 values.
    //await db.execute("CREATE TABLE $tableUserEntitys ($columnId INTEGER PRIMARY KEY,$columnNom TEXT NULL,$columnEntrepriseNom TEXT NULL,$columnTypeEntrepriseCode TEXT NULL,$columnPrenom TEXT NULL,$columnEmail TEXT NULL,$columnTelephone TEXT NULL,$columnLogin TEXT NULL,$columnRoleLibelle TEXT NULL,$columnIsDeleted INTEGER NULL,  $columnIsLogged INTEGER NULL,$columnCreatedAt TEXT NULL,$columnCreatedBy INTEGER NULL,$columnUpdatedAt TEXT NULL,$columnUpdatedBy INTEGER NULL,$columnDeleteAt TEXT NULL,$columnDeletedBy INTEGER NULL)");

    /*await db.execute('''
              CREATE TABLE $tableUserEntitys (
                $columnId INTEGER PRIMARY KEY,
                $columnNom TEXT NULL,
                $columnEntrepriseNom TEXT NULL,
                $columnTypeEntrepriseCode TEXT NULL,
                $columnPrenom TEXT NULL,
                $columnEmail TEXT NULL,
                $columnTelephone TEXT NULL,
                $columnLogin TEXT NULL,
                $columnRoleLibelle TEXT NULL,
                $columnIsDeleted INTEGER NULL, 
                $columnIsLogged INTEGER NULL,
                $columnCreatedAt TEXT NULL,
                $columnCreatedBy INTEGER NULL,
                $columnUpdatedAt TEXT NULL,
                $columnUpdatedBy INTEGER NULL,
                $columnDeleteAt TEXT NULL,
                $columnDeletedBy INTEGER NULL
              )
              ''');*/

    await db.execute('''
              CREATE TABLE $tableUserEntitys (
                $columnId INTEGER PRIMARY KEY,
                $columnNom TEXT NULL,
                entreprisenom TEXT NULL,
                typeentreprisecode TEXT NULL,
                prenom TEXT NULL,
                $columnEmail TEXT NULL,
                $columnTelephone TEXT NULL,
                $columnLogin TEXT NULL,
                rolelibelle TEXT NULL
              )
              ''');
  }

  // Database helper methods:

  Future<UserEntity> insert(UserEntity userEntity) async {
    Utilities.begin("DatabaseHelper/insert");
    Database db = await database;
    //int id = await db.insert(tableUserEntitys, userEntity.toJson());
    print(" data to insert :  ${userEntity.toJson()}");
    userEntity.id = await db.insert(tableUserEntitys, userEntity.toJson());
    Utilities.end("DatabaseHelper/insert");
    return userEntity;
  }

  Future<UserEntity> queryUserEntity(int id) async {
    Utilities.begin("DatabaseHelper/queryUserEntity");

    Database db = await database;
    List<Map> maps = await db.query(tableUserEntitys,
        columns: [columnId, columnNom, columnEmail],
        where: '$columnId = ?',
        whereArgs: [id]);
    Utilities.end("DatabaseHelper/queryUserEntity");

    if (maps.length > 0) {
      return UserEntity.fromJson(maps.first);
    }
    return null;
  }

  Future<List<UserEntity>> getByCriteria({int id}) async {
    Utilities.begin("DatabaseHelper/getByCriteria");
    Database db = await database;
    List<Map> maps = await db.query(tableUserEntitys,
        columns: [columnId, columnNom, columnEmail],
        where: (id != null) ? '$columnId = ?' : null,
        whereArgs: (id != null) ? [id] : null);
    Utilities.end("DatabaseHelper/getByCriteria");

    if (maps.length > 0) {
      return maps.map((map) => UserEntity.fromJson(map)).toList();
    }
    return null;
  }

  Future<UserEntity> getTodo(int id) async {
    Database db = await database;

    List<Map> maps = await db.query(tableUserEntitys,
        columns: [columnId, columnNom, columnEmail],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return UserEntity.fromJson(maps.first);
    }
    return null;
  }

  /*

  Future<UserEntity> getTodo(int id) async {
    List<Map> maps = await db.query(tableUserEntitys,
        columns: [columnId, columnNom, columnEmail],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return UserEntity.fromJson(maps.first);
    }
    return null;
  }

  Future<int> delete(int id) async {
    return await db.delete(tableUserEntitys, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int> update(Todo todo) async {
    return await db.update(tableUserEntitys, todo.toMap(),
        where: '$columnId = ?', whereArgs: [todo.id]);
  }

  

  */

  // TODO: queryAllUserEntitys()
  // TODO: delete(int id)
  // TODO: update(UserEntity userEntity)


  */
}
