import 'package:flutter/material.dart';

class MainUtilities  {

  static onTap({BuildContext context, dynamic classeName}){
      Navigator.of(context).pop(); // pour fermer le menu apres clic
      Navigator.push(context,
        MaterialPageRoute(builder: (context) => classeName()));
  }
}