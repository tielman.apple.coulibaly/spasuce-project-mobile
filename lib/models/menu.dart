import 'package:flutter/material.dart';
import 'package:spasuce/const_app.dart';

class Menu {
  final String title;
  final String code;
  final List<String> childrens;
  final IconData icon;
  final bool isSelect;
  final Color color;
  final dynamic view;

  const Menu(
      {this.title,
      this.icon,
      this.color,
      this.childrens,
      this.view,
      this.code,
      this.isSelect = false});
}
