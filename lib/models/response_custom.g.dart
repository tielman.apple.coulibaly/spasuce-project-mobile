// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_custom.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseCustom _$ResponseCustomFromJson(Map<String, dynamic> json) {
  return ResponseCustom(
    hasError: json['hasError'] as bool,
    count: json['count'] as int,
    status: json['status'],
    items: json['items'] as List,
  );
}

Map<String, dynamic> _$ResponseCustomToJson(ResponseCustom instance) =>
    <String, dynamic>{
      'hasError': instance.hasError,
      'count': instance.count,
      'status': instance.status,
      'items': instance.items,
    };
