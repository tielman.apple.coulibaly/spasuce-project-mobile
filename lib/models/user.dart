import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  int id;
  String nom;
  String prenoms;
  String login;
  String password;
  String email;
  String telephone;
  String roleLibelle;
  String roleCode;
  String typeEntrepriseLibelle;
  String typeEntrepriseCode;
  String etatCode;
  String typeAppelDoffresCode;
  String typeContenuStatiqueCode;
  String titre;
  String contenu;
  String typeCritereAppelDoffresCode;
  String valeur;
  String critereAppelDoffresLibelle;
  String critereLibelle;

  String messageClient;

  dynamic dataEntreprise;
  //User dataEntreprise ;

  String dateSoumission;
  String codeContrat;
  bool isVisualiserNoteByFournisseur;
  bool isVisualiserNotifByClient;
  bool isPayer;

  int roleId;
  int villeId;
  int typeEntrepriseId;
  int statutJuridiqueId;

  String nombreOffre;

  String statutJuridiqueLibelle;

  String numeroFax;
  String numeroDuns;
  String numeroFix;
  String numeroImmatriculation;
  String classeNoteCode;
  String extensionLogo;
  String urlLogo;
  double note;
  double noteOffre;
  //String fichierBase64;
  dynamic fichierBase64;
  String cheminFichier;
  String paysLibelle;
  String villeLibelle;

  int nombreUser;
  int paysId;

  List<dynamic> datasSecteurDactiviteEntreprise;
  List<dynamic> datasSecteurDactivite;
  List<dynamic> datasDomaineEntreprise;
  List<dynamic> datasDomaine;
  List<dynamic> datasFichierDescription;
  List<dynamic> datasPartenaire;
  List<dynamic> datasFichierPartenaire;
  List<dynamic> datasValeurCritereOffre;

  List<dynamic> datasValeurCritereAppelDoffres;
  //List<User> datasValeurCritereAppelDoffres ;

  String commentaire;

  bool isAttribuer;
  bool isSelect;

  bool isAttribuerAppelDoffres;

  bool isModifiable;

  int objetNousContacterId;

  // accueil
  String dateLimiteDepot;
  String dateCreation;
  String code;
  int nbreOF;

  String libelle;
  String description;
  String baseUrl;

  int userFournisseurId;
  int appelDoffresId;
  bool isRetirer;

  List<dynamic> datasVille;
  dynamic codeParam;
  dynamic typeEntrepriseCodeParam;

  // critere AO/OF
  int critereId;
  int offreId;

  int nombreTypeDuree;
  String typeDureeLibelle;

  String typeAppelDoffresLibelle;
  int typeDureeId;
  int userClientId;
  List<dynamic> datasEntreprise;
  List<dynamic> datasSecteurDactiviteAppelDoffres;
  List<dynamic> datasFichier;

  int secteurDactiviteId;
  String secteurDactiviteLibelle;
  int domaineId;
  String domaineLibelle;

  bool isFichierOffre;
  String name;
  String extension;

  int createdBy;

  String dateCreationAppelDoffres;
  String dateLimiteDepotAppelDoffres;
  bool isSelectedByClient;

  String appelDoffresLibelle;
  String villeAppelDoffres;
  bool isVisibleByClient;
  bool isLocked;

  bool isVisualiserContenuByFournisseur;

  bool isAppelRestreint;
  bool isContenuNotificationDispo;

  int typeAppelDoffresId;

  String newPassword;

  String entrepriseNom;
  String userPrenoms;
  String userNom;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  static List<User> fromJsons(List<dynamic> jsons) => _$UsersFromJsons(jsons);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  User({
    this.id,
    this.nom,
    this.prenoms,
    this.login,
    this.password,
    this.newPassword,
    this.email,
    this.telephone,
    this.roleLibelle,
    this.roleCode,
    this.typeEntrepriseLibelle,
    this.typeEntrepriseCode,
    this.etatCode,
    this.typeAppelDoffresCode,
    this.typeContenuStatiqueCode,
    this.titre,
    this.contenu,
    this.typeCritereAppelDoffresCode,
    this.valeur,
    this.critereAppelDoffresLibelle,
    this.critereLibelle,
    this.dataEntreprise,
    this.roleId,
    this.villeId,
    this.typeEntrepriseId,
    this.statutJuridiqueId,
    this.numeroFax,
    this.numeroDuns,
    this.numeroFix,
    this.numeroImmatriculation,
    this.classeNoteCode,
    this.extensionLogo,
    this.urlLogo,
    this.note,
    this.fichierBase64,
    this.cheminFichier,
    this.paysLibelle,
    this.nombreUser,
    this.paysId,
    this.datasSecteurDactiviteEntreprise,
    this.datasSecteurDactivite,
    this.datasDomaineEntreprise,
    this.datasDomaine,
    this.datasFichierDescription,
    this.datasPartenaire,
    this.datasFichierPartenaire,
    this.isAttribuer,
    this.isSelect,
    this.objetNousContacterId,
    this.dateLimiteDepot,
    this.dateCreation,
    this.code,
    this.nbreOF = 0,
    this.libelle,
    this.description,
    this.datasVille,
    this.codeParam,
    this.isRetirer,
    this.appelDoffresId,
    this.userFournisseurId,
    this.baseUrl,
    this.villeLibelle,
    this.critereId,
    this.offreId,
    this.datasValeurCritereOffre,
    this.nombreTypeDuree,
    this.typeDureeLibelle,
    this.commentaire,
    this.datasValeurCritereAppelDoffres,
    this.datasEntreprise,
    this.datasFichier,
    this.datasSecteurDactiviteAppelDoffres,
    this.typeAppelDoffresLibelle,
    this.typeDureeId,
    this.userClientId,
    this.secteurDactiviteId,
    this.secteurDactiviteLibelle,
    this.domaineId,
    this.domaineLibelle,
    this.extension,
    this.isFichierOffre,
    this.nombreOffre,
    this.name,
    this.createdBy,
    this.dateCreationAppelDoffres,
    this.isSelectedByClient,
    this.dateLimiteDepotAppelDoffres,
    this.villeAppelDoffres,
    this.appelDoffresLibelle,
    this.statutJuridiqueLibelle,
    this.isLocked,
    this.entrepriseNom,
    this.isVisibleByClient,
    this.isAppelRestreint,
    this.isContenuNotificationDispo,
    this.typeAppelDoffresId,
    this.userNom,
    this.userPrenoms,
    this.noteOffre,
    this.messageClient,
    this.isVisualiserContenuByFournisseur,
    this.typeEntrepriseCodeParam,
    this.dateSoumission,
    this.codeContrat,
    this.isPayer,
    this.isVisualiserNoteByFournisseur,
    this.isVisualiserNotifByClient,
    this.isAttribuerAppelDoffres,
    this.isModifiable,
  });

/*
  @override
  String toString() {
    // TODO: implement toString
    return super.toString();
  }
  */

  bool isEqual(User user) {
    return this?.id == user?.id;
  }

  @override
  String toString() => libelle;
}
