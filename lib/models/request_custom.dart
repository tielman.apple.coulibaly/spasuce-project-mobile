import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

//part 'response_custom.g.dart';

part 'request_custom.g.dart';

@JsonSerializable()
class RequestCustom {
  bool isAnd;
  dynamic data;
  int user;
  List<dynamic> datas;

  RequestCustom({this.isAnd, this.data, this.user, this.datas});

  factory RequestCustom.fromJson(Map<String, dynamic> json) =>
      _$RequestCustomFromJson(json);

  Map<String, dynamic> toJson() => _$RequestCustomToJson(this);
}
